#!/bin/sh

echo Checking IO information in this project @ `date`
a=`pwd`
a=`dirname $a`
PROJECTBASE=`dirname $a`
PLATFORM_CONFIG=$PROJECTBASE/src/platform_config.h
IOC=$PROJECTBASE/src/STM32F407.ioc

echo "IOC file" > ioc
grep '^P[A-Z][0-9A-Z-]*.GPIO_Label' $IOC | sed 's/.GPIO_Label=/	/' | sed 's/-WKUP//' | sort --key=1.1,1.2 --key=1.3n >> ioc

echo "PlatformConfig.h" > pc
sh listIo.sh | grep -v '^Checking' | sed 's/GPIO/P/' | sed 's/--GPIO_PIN_//' | sed 's/_GPIO_/_/' | sed 's/ /	/' | sort --key=1.1,1.2 --key=1.3n >> pc


sdiff ioc pc
