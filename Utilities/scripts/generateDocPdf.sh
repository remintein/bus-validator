#!/bin/sh


cd ../../doxygen

/Applications/Doxygen.app//Contents/Resources/doxygen Doxyfile
(
cd html

#scp -r . . doxygen@cns-dutchnfcconsult.nl:documentation/
tar -czf - . | ssh doxygen@cns-dutchnfcconsult.nl "cd documentation ; tar -xzvf - "
)
exit
(
cd latex

/Library/TeX/Distributions/.DefaultTeX/Contents/Programs/texbin/pdflatex refman.tex
)
