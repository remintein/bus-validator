#!/bin/sh

echo Checking IO information in this project @ `date`
a=`pwd`
a=`dirname $a`
PROJECTBASE=`dirname $a`
PLATFORM_CONFIG=$PROJECTBASE/src/platform_config.h

# Check 1: The defines only mention SPI1/SPI2 etc. once

grep "^#define" $PLATFORM_CONFIG | awk '{print $3}' | grep "^SPI" | sort | uniq -c | awk '{
if ($1 > 1) { printf("Error: %s is used more than once\n", $2); }}'


# Check 2: GPIO and PIN configuration are used only once

grep "^#define" $PLATFORM_CONFIG | egrep '_GPIO_|_PIN_' | awk '
/_PIN_/ {thePin=$2; thePinDef=$3}
/_GPIO_/ {theGpio=$2; theGpioDef=$3;printf("%s--%s\n", theGpioDef, thePinDef);

}
' | sort | uniq -c | awk '{
if ($1 > 1) { printf("Error: %s combination used more than once\n", $2); }}'

# Check 3: No GPIO pins mentioned in the code except the initialization

find $PROJECTBASE/src -name "*.c" -exec egrep 'GPIO_PIN_[0-9]' {} /dev/null \; 

find $PROJECTBASE/src -name "*.c" -exec egrep '[^A-Z_]GPIO[ABCDE][^A-Z_]' {} /dev/null \; 