#!/bin/sh

echo Checking IO information in this project @ `date`
a=`pwd`
a=`dirname $a`
PROJECTBASE=`dirname $a`
PLATFORM_CONFIG=$PROJECTBASE/src/platform_config.h

# List: GPIO and PIN configuration are used only once

grep "^#define" $PLATFORM_CONFIG | egrep '_GPIO_|_PIN_' | awk '
/_PIN_/ {thePin=$2; thePinDef=$3}
/_GPIO_/ {theGpio=$2; theGpioDef=$3;printf("%s--%s %s\n", theGpioDef, thePinDef,$2);

}
'
