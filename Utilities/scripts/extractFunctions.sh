#!/bin/sh
cd ../../src/smartcardIf/flowController
(
 cat  flowControllerDefines.txt |  
 while read DEFINE line
  do
   line=`cat flowController.c | awk '
     /case '$DEFINE':/ { nxtline=1 }
     {if (nxtline == 2) { if ($1 == "outcome") {print  $3;} nxtline++; }
      if (nxtline == 1) { nxtline++; }
     }
    '`
   
   echo $DEFINE"					"`echo $line | sed 's/(.*//'`
   
  done | sort > /tmp/list.txt

#  if [ `cat /tmp/list.txt | wc -l` -eq `cat flowControllerDefines.txt | wc -l` ]
#   then
#    cp /tmp/list.txt flowControllerDefines.txt
#   fi
)

for i in `cat /tmp/list.txt | awk '{print $2}'`
 do
  echo "uint8_t $i(char **step, DesfireHandlingStruct *controller);"
 done  > /tmp/func.txt

ls -l flowController.h
awk -v new_file=/tmp/func.txt '
    /START_DEFINE_OF_FUNCTIONS/ {print; system("cat " new_file); banner=1; next}
    /END_DEFINE_OF_FUNCTIONS/ {banner=0}
    {if (banner == 0) print}
' flowController.h > /tmp/fc_tmp.txt
mv /tmp/fc_tmp.txt flowController.h


if [ -f /tmp/fc_def.txt ]
 then
  rm /tmp/fc_def.txt
 fi
for i in `grep '#define CNS_' ../../_configuration/deviceSelector_defines.h  | awk '{print $2}'`
 do
# Find the right flow...
  file=`cat ../flows/controllerFlow.h | awk -F\" -v TARGET=$i '
    /#if CURRENT_TARGET == / {if (match($0,".*"TARGET".*") ) {nxtline=1;}else{nxtline=0;} }
    /#include/ { if (nxtline) {print $2 }}
   '`
  if [ -f "../flows/$file" ]
   then
    file=../flows/$file 
    echo "#if CURRENT_TARGET == $i" >> /tmp/fc_def.txt
    cat $file | awk '
      /^[ 	]*{/ {print $3 }
     ' | sort -u | grep -v '^$' >> /tmp/fc_def.txt
    echo "#endif" >> /tmp/fc_def.txt
   fi
 done

awk -v new_file=/tmp/fc_def.txt '
    /START_DEFINE_OF_ENUM/ {print; system("cat " new_file); banner=1; next}
    /END_DEFINE_OF_ENUM/ {banner=0}
    {if (banner == 0) print}
' flowController.h > /tmp/fc_tmp.txt
mv /tmp/fc_tmp.txt flowController.h


cat /tmp/fc_def.txt | sed 's/,//' | while read line
 do
case "$line" in
 \#* ) echo "$line" ;;
 * ) echo "case  $line : outcome = "`awk -v DEF=$line '{if ($1 == DEF) {print $2} }' flowControllerDefines.txt`"(step, handel); break;" 
     ;;
esac
 done > /tmp/fc_case.txt


awk -v new_file=/tmp/fc_case.txt '
    /START_DEFINE_OF_CASE/ {print; system("cat " new_file); banner=1; next}
    /END_DEFINE_OF_CASE/ {banner=0}
    {if (banner == 0) print}
' flowController.c > /tmp/fc_tmp.txt
mv /tmp/fc_tmp.txt flowController.c

