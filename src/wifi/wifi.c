/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"
#include "wifi.h"
#include "engine.h"
#define STACK_SIZE 900


/**
 * @file
 * @brief Contains the functions for the Wifi module
 *
 */

/**
 * @addtogroup wifi
 *
 * @{
 */
void apiDnfccWifiEngineInit();
void apiDnfccWifiEngineAdvance();

/**
 * @brief Initialization routine for module Wifi
 *
 * @param uxPriority - the FreeRTOS priority
 */
BaseType_t nfccapiDnfccWifiEngineInit(UBaseType_t uxPriority) {
	deviceStatus.wifiRxQueue = xQueueCreate(WIFI_QUEUE_LENGTH, WIFI_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.wifiRxQueue, "Wifi");

	setWaitForSystemReadyThisTask(TASK_BIT_WIFI);
	return xTaskCreate(nfccWifi, "Wifi", STACK_SIZE, NULL, uxPriority, &deviceStatus.wifiTask);
}

/**
 * @brief Wifi main loop.
 *
 */
void nfccWifi(void *pvParameters) {
	char buf[WIFI_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;
	UBaseType_t speed;
	int wifiEnabled = 1;

	apiDnfccWifiEngineInit();

	setTaskIsSystemReady(TASK_BIT_WIFI);
	waitForSystemReady();

	for (;;) {
		if (wifiEnabled) {
			apiDnfccWifiEngineAdvance();
		}
// Can be dynamic based on whether we are sending... (wait time max)

		if (apiDnfccGetPriority() == PRIORITY_IDLE || apiDnfccGetPriority() == PRIORITY_GET_DATA_FROM_BACKEND) {
			speed = 1;
		} else {
			speed = 250;
		}
		if (xQueueReceive(deviceStatus.wifiRxQueue, buf, speed) == pdPASS) {
			switch (buf[0]) {
			case COMCOM_SELFTEST:
				uxHighWaterMark = uxTaskGetStackHighWaterMark(deviceStatus.wifiTask);
				sprintf(stbuf, "!WWM%ld", uxHighWaterMark);
				stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
				stbuf[1] = COMSELFTEST_WIFI;
				queueSendToDataDistributor(stbuf, portMAX_DELAY);
				break;
			}
		}
	}
}

/** @} */
