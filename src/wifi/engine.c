/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"

#include "wifi.h"
#include "WifiFlow.h"
#include "../_definition/deviceStatus.h"
#include "../_configuration/backendCommunication.h"
#include <string.h>
#include "../datadistributor/datadistributor.h"

#include "engine.h"

/**
 * @file
 * @brief Contains the functions for the Wifi module
 *
 */

/**
 * @addtogroup wifi
 *
 * @{
 */
UART_HandleTypeDef huart_wifi;
SerialCommunicatorHandler_type wifiEngine;

#define WIFI_BUFFER_LENGTH 2000

extern TransmitMessage TransmitWifiMessage;
static uint8_t wifiReceiveBuffer[WIFI_BUFFER_LENGTH];
static uint8_t wifiTransmitBuffer[WIFI_BUFFER_LENGTH];

static int curAccessPoint = -1;
static int curAccessPointStrength = -999;
static int bytesToProcess = 0;

#include "../_configuration/wifiAccessPoints.h"

/**
 * @brief callback routing for the Wifi module
 *
 * @param data - data given from to the callback routine
 * @param intData - data given to the callback routine
 * @return if the call is finished (and the flow can continue) 1 = yes, 0 = not
 */
uint8_t WifiCallbackFunction(char * data, int32_t intData) {
	char buf[2000];
	int receiveHere;
	char ssidName[33]; // Max length SSID + 1
	int strength;
	uint16_t length;

	int i;
	switch (intData) {

	case RESET_CALLBACK:
		curAccessPoint = -1;
		curAccessPointStrength = -999;
		// Reset command; toggle the RST pin low
		HAL_GPIO_WritePin(WIFI_GPIO_RST, WIFI_PIN_RST, GPIO_PIN_RESET);
		vTaskDelay(500);
		HAL_GPIO_WritePin(WIFI_GPIO_RST, WIFI_PIN_RST, GPIO_PIN_SET);
		wifiEngine.engine->lastCommunication = xTaskGetTickCount();
		break;

	case VERIFY_ACCESS_POINT_CALLBACK:
		memset(buf, '\0', DISPLAY_BUFFER_SIZE);
		for (i = 0; i < 150; i++) {
			int pos = (wifiEngine.engine->receiveProcessedTill + i) % wifiEngine.engine->receiveBufferLength;
			if (pos == wifiEngine.engine->receivePointer || wifiEngine.engine->receiveBuffer[pos] == '\r') {
				break;
			}
			buf[i] = wifiEngine.engine->receiveBuffer[pos];
			buf[i + 1] = '\0';
		}

		// String is like:    :(4,"accesspointname"

		if (sscanf(buf + 3, ",\"%32[^\"]\",%d", ssidName, &strength) == 2) {
			if (strength > curAccessPointStrength || curAccessPoint == -1) {
				for (i = 0; i < sizeof(accessPoints) / sizeof(accessPointDefinition_type); i++) {
					if (strcmp(accessPoints[i].name, ssidName) == 0) {
						if (strength > curAccessPointStrength) {
							curAccessPoint = i;
							curAccessPointStrength = strength;
						}
					}
				}
			}
		}
		break;

	case CONNECT_ACCESS_POINT_IF_AVAILABLE:
		if (curAccessPoint >= 0) {
			sprintf(buf, "AT+CWJAP=\"%s\",\"%s\"\015\012", accessPoints[curAccessPoint].name,
					accessPoints[curAccessPoint].password);
			apiDnfccSerialEngineTransmit(buf, WIFI_UART_NR);
		}
		break;

	case GET_RECEIVE_BYTES:
		bytesToProcess = 0;
		memset(buf, '\0', DISPLAY_BUFFER_SIZE);
		for (i = 0; i < 150; i++) {
			int pos = (wifiEngine.engine->receiveProcessedTill + i) % wifiEngine.engine->receiveBufferLength;
			if (pos == wifiEngine.engine->receivePointer || wifiEngine.engine->receiveBuffer[pos] == ':') {
				break;
			}
			buf[i] = wifiEngine.engine->receiveBuffer[pos];
			buf[i + 1] = '\0';
		}
		sscanf(buf, "%d", &bytesToProcess);
		break;

	case RECEIVE_BY_LENGTH:
		receiveHere = 0;
		while (bytesToProcess > 0) {
			if (wifiEngine.engine->receiveProcessedTill != wifiEngine.engine->receivePointer) {
				buf[receiveHere++] = wifiEngine.engine->receiveBuffer[wifiEngine.engine->receiveProcessedTill];
				if (receiveHere > sizeof(buf) - 2) {
					receiveHere--;
				}
				buf[receiveHere] = '\0';
				wifiEngine.engine->receiveProcessedTill = (wifiEngine.engine->receiveProcessedTill + 1)
						% wifiEngine.engine->receiveBufferLength;
				bytesToProcess--;
			} else {
				vTaskDelay(1); // Timeout handling here....
			}
		}
		// Get the {...] into the output
		for (int i = 0; i < strlen(buf); i++) {
			if (buf[i] == '{') {
				strcpy(wifiEngine.jsonReceiveData, buf + i);
				break;
			}
		}
		TransmitWifiMessage.transmitStatus = STATUS_FINISHED;
		sprintf(buf, "R%ld", TransmitWifiMessage.messageId);
		buf[0] = COMCOM_DATADISTRIBUTOR_MESSAGE_SEND;
		queueSendToDataDistributor(buf, portMAX_DELAY);

		break;

	case WAIT_1_SECONDS:
		vTaskDelay(1000);
		vTaskDelay(1);
		break;

	case WAIT_2_SECONDS:
		vTaskDelay(2000);
		vTaskDelay(1);
		break;

	case WAIT_5_SECONDS:
		vTaskDelay(5000);
		vTaskDelay(1);
		break;

	case WAIT_10_SECONDS:
		vTaskDelay(10000);
		vTaskDelay(1);
		break;

	case BREAKPOINT_NOW:
		vTaskDelay(1000);
		vTaskDelay(1);
		break;

	case CHECK_WAIT_FOR_WORK:
		switch (TransmitWifiMessage.transmitStatus) {
		case STATUS_IDLE:
			vTaskDelay(100);
			return 0;
		case STATUS_WAITING:
			TransmitWifiMessage.transmitStatus = STATUS_WORK;
			wifiEngine.jsonEndPoint = TransmitWifiMessage.endpoint;
			wifiEngine.jsonRequestData = TransmitWifiMessage.data;
			wifiEngine.jsonReceiveData = TransmitWifiMessage.response;
			return 1;
		case STATUS_WORK:
			return 1;
		case STATUS_FINISHED:
			vTaskDelay(100);
			return 0;
		}
		return 0;

	case CHECK_COMMIT_WORK:
		switch (TransmitWifiMessage.transmitStatus) {
		case STATUS_IDLE:
			break;
		case STATUS_WAITING:
			break;
		case STATUS_WORK:
			TransmitWifiMessage.transmitStatus = STATUS_FINISHED;
			break;
		case STATUS_FINISHED:
			break;
		}
		break;

	case CIPSEND_LENGTH:
		length = 72; // 72 is the additional data sent excluding the end-point and the data (which are variable of length)
		length += strlen(wifiEngine.jsonEndPoint);
		length += strlen(wifiEngine.jsonRequestData);
		sprintf(buf, "AT+CIPSEND=%d\015\012", length);
		apiDnfccSerialEngineTransmit(buf, WIFI_UART_NR);
		logWifiString(buf);
		break;

	case SEND_REQUEST:
		length = 0;
		sprintf(buf, "POST %s HTTP/1.1\015\012", wifiEngine.jsonEndPoint);
		length += strlen(buf);
		apiDnfccSerialEngineTransmit(buf, WIFI_UART_NR);
		sprintf(buf, "Content-Type: application/json\015\012");
		length += strlen(buf);
		apiDnfccSerialEngineTransmit(buf, WIFI_UART_NR);

		sprintf(buf, "Content-Length:%05d\015\012", strlen(wifiEngine.jsonRequestData) + 100);
		length += strlen(buf);
		apiDnfccSerialEngineTransmit(buf, WIFI_UART_NR);

		sprintf(buf, "\015\012");
		length += 2;
		apiDnfccSerialEngineTransmit(buf, WIFI_UART_NR);

		sprintf(buf, wifiEngine.jsonRequestData);

		apiDnfccSerialEngineTransmit(buf, WIFI_UART_NR);
		length += strlen(buf);
		sprintf(buf, "LL: %d", length);
		logWifiString(buf);
		break;
	}

	return 1;
}

/**
 * @brief Initialization routine for the Wifi Engine
 */
void apiDnfccWifiEngineInit() { // TODO: move all to apiDnfccSerialCommunicationInitialize
	apiDnfccSerialCommunicationInitialize(&wifiEngine, Wifi, sizeof(Wifi) / sizeof(SerialControllerFlow_type),
			WIFI_UART_NR);

	wifiEngine.engine = apiDnfccSerialEngineStartListening(&huart_wifi,
	WIFI_UART_NR, wifiReceiveBuffer, WIFI_BUFFER_LENGTH, wifiTransmitBuffer,
	WIFI_BUFFER_LENGTH);
	wifiEngine.currentStep = EXECUTE_RESET;
	wifiEngine.flow = Wifi;
	wifiEngine.flowSize = sizeof(Wifi) / sizeof(SerialControllerFlow_type);
	wifiEngine.callbackFunction = WifiCallbackFunction;
	wifiEngine.uartNr = WIFI_UART_NR;
}

/**
 * @brief Advance 1 in the Wifi module. Logging to the screen is handled here (if define is set)
 */
void apiDnfccWifiEngineAdvance() {
#if SERIAL_WIFI_LOG == 1
	int curStep = wifiEngine.currentStep;
	char *step = "?";
#endif
	apiDnfccSerialCommunicationAdvance(&wifiEngine);
#if SERIAL_WIFI_LOG == 1
	if (curStep != wifiEngine.currentStep) {
		switch (wifiEngine.currentStep) {
			case EXECUTE_RESET:
			step = "EXECUTE_RESET";
			break;
			case WAIT_FINISH_RESET:
			step = "WAIT_FINISH_RESET";
			break;
			case GET_CWMODE:
			step = "GET_CWMODE";
			break;
			case GET_CWMODE_RESP:
			step = "GET_CWMODE_RESP";
			break;
			case GET_CWMODE_RESP_OK:
			step = "GET_CWMODE_RESP_OK";
			break;
			case GET_CWMODE_RESPSET_OK:
			step = "GET_CWMODE_RESPSET_OK";
			break;
			case SET_CWMODE:
			step = "SET_CWMODE";
			break;
			case SET_CWMODE_OK:
			step = "SET_CWMODE_OK";
			break;
			case DELAY_FOR_CIPSTATUS:
			step = "DELAY_FOR_CIPSTATUS";
			break;
			case GET_CWJAP:
			step = "GET_CWJAP";
			break;
			case GET_CWJAP_RESP:
			step = "GET_CWJAP_RESP";
			break;
			case GET_CWJAP_RESPOK:
			step = "GET_CWJAP_RESPOK";
			break;
			case GET_CWJAP_RESP_NOAPOK:
			step = "GET_CWJAP_RESP_NOAPOK";
			break;
			case LIST_ACCESS_POINTS:
			step = "LIST_ACCESS_POINTS";
			break;
			case LIST_ACCESS_POINTS_RESP:
			step = "LIST_ACCESS_POINTS_RESP";
			break;
			case PROCESS_LIST_ACCESS_POINTS:
			step = "PROCESS_LIST_ACCESS_POINTS";
			break;
			case CONNECT_TO_ACCESSPOINT:
			step = "CONNECT_TO_ACCESSPOINT";
			break;
			case DELAY_FOR_CONNECT_TO_ACCESSPOINT:
			step = "DELAY_FOR_CONNECT_TO_ACCESSPOINT";
			break;
			case VERIFY_IP_ADDRESS:
			step = "VERIFY_IP_ADDRESS";
			break;
			case VERIFY_IP_ADDRESS_RESP:
			step = "VERIFY_IP_ADDRESS_RESP";
			break;
			case VERIFY_IP_ADDRESS_RESPOK:
			step = "VERIFY_IP_ADDRESS_RESPOK";
			break;
			case READY_TO_SEND:
			step = "READY_TO_SEND";
			break;
			case CHECK_FOR_WORK:
			step = "CHECK_FOR_WORK";
			break;
			case SEND_CONNECT_STRING:
			step = "SEND_CONNECT_STRING";
			break;
			case SEND_CONNECT_STRING_RESP:
			step = "SEND_CONNECT_STRING_RESP";
			break;
			case SEND_CIPLENGTH:
			step = "SEND_CIPLENGTH";
			break;
			case SEND_CIPLENGTH_RESP:
			step = "SEND_CIPLENGTH_RESP";
			break;
			case SEND_DATA:
			step = "SEND_DATA";
			break;
			case SEND_DATA_RESP:
			step = "SEND_DATA_RESP";
			break;
			case WAIT_FOR_IPD:
			step = "WAIT_FOR_IPD";
			break;
			case RETRIEVE_IPD_LENGTH:
			step = "RETRIEVE_IPD_LENGTH";
			break;
			case RECEIVE_DATA:
			step = "RECEIVE_DATA";
			break;
			case CLOSE_IPCONNECTION:
			step = "CLOSE_IPCONNECTION";
			break;
			case CLOSE_IPCONNECTION_RESP:
			step = "CLOSE_IPCONNECTION_RESP";
			break;
		}

		char buf[DISPLAY_BUFFER_SIZE];
		sprintf(buf, "%cWifi: %d->%d = %s", COMCOM_DISPLAY_LOGGING, curStep,
				wifiEngine.currentStep, step);
		queueSendToDisplay(buf, 1000);
	}
#endif

	/** @} */
}

