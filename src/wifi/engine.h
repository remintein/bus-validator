/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef WIFI_ENGINE_H_
#define WIFI_ENGINE_H_
#include "../_library/serialEngine/serialEngine.h"

typedef enum {
    EXECUTE_RESET,                      // Does a hardware reset
    WAIT_FINISH_RESET,                  // Wait for "ready"
    GET_CWMODE,						    // Sends AT+CWMODE?
    GET_CWMODE_RESP,					// Wait for +CWMODE:...
    GET_CWMODE_RESP_OK,				    // CWMODE = 1, wait for OK
    GET_CWMODE_RESPSET_OK,			    // CWMODE <> 1, wait for OK
    SET_CWMODE,      			        // Need to send AT+CWMODE=1, next is check OK
    SET_CWMODE_OK,   				    // Wait for OK
    DELAY_FOR_CIPSTATUS,			 	// Wait 1 sec
    GET_CWJAP,					        // Send AT+CWJAP?
    GET_CWJAP_RESP,				        // Wait for +CWJAP::#
    GET_CWJAP_RESPOK,                   // Wait for OK after +CWJAP
    GET_CWJAP_RESP_NOAPOK,              // Wait for OK after No AP
    LIST_ACCESS_POINTS,				    // Run AT+CWLAP
    LIST_ACCESS_POINTS_RESP,			// Get response, like +CWLAP or OK
    PROCESS_LIST_ACCESS_POINTS,		    // Got an access point line, process it
    CONNECT_TO_ACCESSPOINT,	            // All access points are received, connect if one is available
    DELAY_FOR_CONNECT_TO_ACCESSPOINT,   // After try to connect wait 2 seconds for answer
    VERIFY_IP_ADDRESS,				    // Run AT+CIFSR
    VERIFY_IP_ADDRESS_RESP,			    // Run AT+CIFSR
    VERIFY_IP_ADDRESS_RESPOK,		    // Run AT+CIFSR
    READY_TO_SEND,                      // This is the status when it got an IP address.
    CHECK_FOR_WORK,                     // See if something needs to be sent
    SEND_CONNECT_STRING,                // Send the connectstring
    SEND_CONNECT_STRING_RESP,           // Validate the response - should be OK
    SEND_CIPLENGTH,                     // Send the length of the string to sent
    SEND_CIPLENGTH_RESP,                // Wait for the >
    SEND_DATA,                          // Send the actual data
    SEND_DATA_RESP,                     // Wait for the SEND OK
    WAIT_FOR_IPD,                       // Wait for the response length back
    RETRIEVE_IPD_LENGTH,                // Retrieve the number of bytes to receive
    RECEIVE_DATA,                       // Get the actual data back, size retrieved on calls above
    CLOSE_IPCONNECTION,                 // Close the connection and loop back
    CLOSE_IPCONNECTION_RESP,            // Wait for the OK
	RESTORE_ESP,						// Send the AT+RESTORE
	RESTORE_ESP_WAIT,					// For a small wait after restore
} WifiCurrentStatus;

typedef enum {
    RESET_CALLBACK,
    VERIFY_ACCESS_POINT_CALLBACK,
    CONNECT_ACCESS_POINT_IF_AVAILABLE,
    WAIT_1_SECONDS,
    WAIT_2_SECONDS,
    WAIT_5_SECONDS,
    WAIT_10_SECONDS,
    BREAKPOINT_NOW,                     // To set a breakpoint
    GET_RECEIVE_BYTES, // +IPD###: -> Called after :, to retrieve the amount of bytes.
    RECEIVE_BY_LENGTH,
    CHECK_WAIT_FOR_WORK,
    CHECK_COMMIT_WORK,
    CIPSEND_LENGTH,
    SEND_REQUEST,

} WifiCallbackKind;

typedef enum {
    IDLE, TRANSMITTING
} WifiTransmitStatus;

typedef struct {
    WifiCurrentStatus status;
    WifiCurrentStatus prevStatus;
    WifiTransmitStatus transmitStatus;

    SerialEngineStruct *serialEngine;
    uint16_t rawToReceive;
    uint16_t receiveTill;

    uint8_t contentLength[5];
} WifiEngineStruct;

uint8_t WifiTransmitCommand(char *s);
#endif /* WIFI_ENGINE_H_ */
