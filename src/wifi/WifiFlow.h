/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef WIFI_WIFIFLOW_H_
#define WIFI_WIFIFLOW_H_

#include "../_library/serialCommunication/SerialCommunicator.h"
#include "../_configuration/backendCommunication.h"
#include "Engine.h"

// Step - Action - Xmit or Xrec - Next action
static SerialControllerFlow_type Wifi[] = {

// Reset procedure; execute, and wait for "ready", timeout goes back to reset

		{ EXECUTE_RESET, EXECUTE_CALLBACK_WITH_PARAMETER, "RESET", RESET_CALLBACK, WAIT_FINISH_RESET },

		{ WAIT_FINISH_RESET, VERIFY_DATA, "ready\r\n", 0, GET_CWMODE },

		{ WAIT_FINISH_RESET, VERIFY_TIMEOUT_HAS_PASSED, "", 30000, EXECUTE_RESET },

		// CWMODE procedure; verify CWMODE = 1, if not set so

		{ GET_CWMODE, SEND_LITERAL_TEXT, "AT+CWMODE?\015\012", 0, GET_CWMODE_RESP },

		{ GET_CWMODE_RESP, VERIFY_DATA, "+CWMODE:1\r\n", 0, GET_CWMODE_RESP_OK },

		{ GET_CWMODE_RESP, VERIFY_DATA, "OK\r\n", 0, SET_CWMODE }, // OK without +CWMODE:1

		{ GET_CWMODE_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 30000, EXECUTE_RESET },

		// CWMODE == 1, wait for OK
		{ GET_CWMODE_RESP_OK, VERIFY_DATA, "OK\r\n", 0, GET_CWJAP },

		{ GET_CWMODE_RESP_OK, VERIFY_TIMEOUT_HAS_PASSED, "", 30000, EXECUTE_RESET },

		// CWMODE <> 1, wait for OK
		{ GET_CWMODE_RESPSET_OK, VERIFY_DATA, "OK\r\n", 0, SET_CWMODE },

		{ GET_CWMODE_RESPSET_OK, VERIFY_TIMEOUT_HAS_PASSED, "", 30000, EXECUTE_RESET },

		{ SET_CWMODE, SEND_LITERAL_TEXT, "AT+CWMODE=1\015\012", 0, SET_CWMODE_OK },

		{ SET_CWMODE_OK, VERIFY_DATA, "OK\r\n", 0, DELAY_FOR_CIPSTATUS },

		{ DELAY_FOR_CIPSTATUS, EXECUTE_CALLBACK_WITH_PARAMETER, "", WAIT_5_SECONDS, GET_CWJAP },

		{ GET_CWJAP, SEND_LITERAL_TEXT, "AT+CWJAP?\r\n", 0, GET_CWJAP_RESP },

		{ GET_CWJAP_RESP, VERIFY_DATA, "+CWJAP:", 0, GET_CWJAP_RESPOK },

		{ GET_CWJAP_RESP, VERIFY_DATA, "No AP\r\n", 0, GET_CWJAP_RESP_NOAPOK },

		{ GET_CWJAP_RESP, VERIFY_DATA, "busy p...\r\n", 0, DELAY_FOR_CIPSTATUS },

		{ GET_CWJAP_RESPOK, VERIFY_DATA, "OK\r\n", 0, VERIFY_IP_ADDRESS },

		{ GET_CWJAP_RESP_NOAPOK, VERIFY_DATA, "OK\r\n", 0, LIST_ACCESS_POINTS },

		{ GET_CWJAP_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 30000, EXECUTE_RESET },

		// List access points and get a valid from there
		{ LIST_ACCESS_POINTS, SEND_LITERAL_TEXT, "AT+CWLAP\015\012", 0, LIST_ACCESS_POINTS_RESP },

		// Response can be OK or +CWLAP (list access point)

		{ LIST_ACCESS_POINTS_RESP, VERIFY_DATA, "+CWLAP", 0, PROCESS_LIST_ACCESS_POINTS },

		{ LIST_ACCESS_POINTS_RESP, VERIFY_DATA, "ERROR\r\n", 0, DELAY_FOR_CIPSTATUS },

		{ LIST_ACCESS_POINTS_RESP, VERIFY_DATA, "OK\r\n", 0, CONNECT_TO_ACCESSPOINT },

		{ LIST_ACCESS_POINTS_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 30000, EXECUTE_RESET },

		// Got a +CWLAP line, wait for end of line, and process it then
		{ PROCESS_LIST_ACCESS_POINTS, VERIFY_DATA_WITH_CALLBACK, "\r\n", VERIFY_ACCESS_POINT_CALLBACK,
				LIST_ACCESS_POINTS_RESP },

// Next will AT+CWJAP when a known one is available
		{ CONNECT_TO_ACCESSPOINT, EXECUTE_CALLBACK_WITH_PARAMETER, "", CONNECT_ACCESS_POINT_IF_AVAILABLE,
				DELAY_FOR_CONNECT_TO_ACCESSPOINT },

		{ DELAY_FOR_CONNECT_TO_ACCESSPOINT, EXECUTE_CALLBACK_WITH_PARAMETER, "", WAIT_10_SECONDS, DELAY_FOR_CIPSTATUS },

		{ VERIFY_IP_ADDRESS, SEND_LITERAL_TEXT, "AT+CIFSR\015\012", 0, VERIFY_IP_ADDRESS_RESP },

		{ VERIFY_IP_ADDRESS_RESP, VERIFY_DATA, "+CIFSR:", 0, VERIFY_IP_ADDRESS_RESPOK },

		{ VERIFY_IP_ADDRESS_RESPOK, VERIFY_DATA, "OK\r\n", 0, CHECK_FOR_WORK },

		{ CHECK_FOR_WORK, VALIDATE_CALLBACK, "", CHECK_WAIT_FOR_WORK, SEND_CONNECT_STRING },

		{ CHECK_FOR_WORK, VERIFY_TIMEOUT_HAS_PASSED, "", 30000, DELAY_FOR_CIPSTATUS },

		{ SEND_CONNECT_STRING, SEND_LITERAL_TEXT, "AT+CIPSTART="  STRING_CONNECT_MIDDLEWARE_SERVER, 0, SEND_CONNECT_STRING_RESP },

		{ SEND_CONNECT_STRING_RESP, VERIFY_DATA, "OK\r\n", 0, SEND_CIPLENGTH },

		{ SEND_CONNECT_STRING_RESP, VERIFY_DATA, "ERROR\r\n", 0, DELAY_FOR_CIPSTATUS },

		{ SEND_CONNECT_STRING_RESP, VERIFY_DATA, "CLOSED\r\n", 0, DELAY_FOR_CIPSTATUS },

		{ SEND_CIPLENGTH, EXECUTE_CALLBACK_WITH_PARAMETER, "", CIPSEND_LENGTH, SEND_CIPLENGTH_RESP },

		{ SEND_CIPLENGTH_RESP, VERIFY_DATA, ">", 0, SEND_DATA },

		{ SEND_DATA, EXECUTE_CALLBACK_WITH_PARAMETER, "", SEND_REQUEST, SEND_DATA_RESP },

		{ SEND_DATA_RESP, VERIFY_DATA, "SEND OK", 0, WAIT_FOR_IPD },

		{ SEND_DATA_RESP, VERIFY_DATA, "SEND FAIL", 0, EXECUTE_RESET },

		{ SEND_DATA_RESP, VERIFY_DATA, "busy s...", 0, EXECUTE_RESET },

		{ SEND_DATA_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 4999, EXECUTE_RESET },

		{ SEND_DATA_RESP, VERIFY_DATA, "CLOSED", 0, EXECUTE_RESET },

		{ WAIT_FOR_IPD, VERIFY_DATA, "+IPD,", 0, RETRIEVE_IPD_LENGTH },

		{ RETRIEVE_IPD_LENGTH, VERIFY_DATA_WITH_CALLBACK, ":", GET_RECEIVE_BYTES, RECEIVE_DATA },

		{ RECEIVE_DATA, EXECUTE_CALLBACK_WITH_PARAMETER, "", RECEIVE_BY_LENGTH, CLOSE_IPCONNECTION },

		{ CLOSE_IPCONNECTION, SEND_LITERAL_TEXT, "AT+CIPCLOSE\015\012", 0, CLOSE_IPCONNECTION_RESP },

		{ CLOSE_IPCONNECTION_RESP, VERIFY_DATA, "OK\r\n", 0, CHECK_FOR_WORK },

		{ CLOSE_IPCONNECTION_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 5000, DELAY_FOR_CIPSTATUS },

		// Next are not in use... can be used for debugging

		{ 1111, EXECUTE_CALLBACK_WITH_PARAMETER, "", BREAKPOINT_NOW, DELAY_FOR_CIPSTATUS },

		{ READY_TO_SEND, VERIFY_TIMEOUT_HAS_PASSED, "", 30000, DELAY_FOR_CIPSTATUS },

		{ RESTORE_ESP, SEND_LITERAL_TEXT, "RESTORE_ESP\015\012",0, RESTORE_ESP_WAIT },


		{ RESTORE_ESP_WAIT, EXECUTE_CALLBACK_WITH_PARAMETER, "",WAIT_5_SECONDS, EXECUTE_RESET },


};
#endif /* WIFI_WIFIFLOW_H_ */
