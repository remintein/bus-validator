/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"
#include "../menu.h"
#include "../menu_definition.h"

#include "../../_definition/buttonTypes.h"

#if CURRENT_TARGET == CNS_TOPUP

extern char valueLabel[];
extern char topupValueLabel[];
extern char validTillLabel[];
extern char topupLabel1[];
extern char topupLabel2[];
extern char topupLabel3[];
extern char topupLabel4[];


/**
 * @file
 * @brief Contains the definition for the menus
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */
static button buttons_topup_start[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO

		{ 10, 480, 460, 300, "\n\n\n"LOCALE_TAP_YOUR_CARD, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 240, 680, 230, 100, LOCAL_BUTTON_TOPUP, COMMAND_MENU_TOPUP_SELECT, BUTTON_NORMAL }, //TONY Top Up button

		{ 10, 680, 230, 100, LOCAL_BUTTON_LOGOUT, COMMAND_MENU_LOGOUT, BUTTON_NORMAL }, //TONY Top Up button

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

static button buttons_display_balance[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO

		{ 10, 480, 460, 100, valueLabel, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 10, 580, 460, 100, validTillLabel, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 240, 680, 230, 100, LOCAL_BUTTON_TOPUP, COMMAND_MENU_TOPUP_SELECT, BUTTON_NORMAL }, //TONY Top Up button

		{ 10, 680, 230, 100, LOCAL_BUTTON_LOGOUT, COMMAND_MENU_LOGOUT, BUTTON_NORMAL }, //TONY Top Up button

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

static button buttons_valueselect[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO
//		{ 10, 380, 460, 100, LOCALE_SELECT_VALUE"\n "LOCALE_FOR_TOPUP, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 10, 480, 225, 100, topupLabel1, COMMAND_MENU_TOPUP1, BUTTON_NORMAL },

		{ 10, 580, 225, 100, topupLabel2, COMMAND_MENU_TOPUP2, BUTTON_NORMAL },

		{ 245, 480, 225, 100, topupLabel3, COMMAND_MENU_TOPUP3, BUTTON_NORMAL },

		{ 245, 580, 225, 100, topupLabel4, COMMAND_MENU_TOPUP4, BUTTON_NORMAL },

		{ 10, 680, 460, 100, LOCALE_MENU_CANCEL, COMMAND_MENU_TOPUP_CANCEL, BUTTON_NORMAL },

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};
static button buttons_topupconfirm[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO

		{ 10, 480, 460, 100, "\n"LOCALE_CONFIRM_TOPUP_VALUE, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 10, 580, 460, 100, topupValueLabel, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 10, 680, 225, 100, LOCALE_MENU_CANCEL, COMMAND_MENU_TOPUP_CANCEL, BUTTON_NORMAL },

		{ 240, 680, 230, 100, LOCALE_MENU_CONFIRM, COMMAND_MENU_TOPUP_CONFIRM, BUTTON_NORMAL },

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

button buttons_taptotop[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO

		{ 10, 480, 460, 300, "\n\n\n"LOCALE_TAP_YOUR_CARD" \n "LOCALE_FOR_TOPUP, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 10, 680, 460, 100, LOCALE_MENU_CANCEL, COMMAND_MENU_TOPUP_CANCEL, BUTTON_NORMAL },

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

menuDefinition menu_topup_start;
menuDefinition menu_topup_valueselect;
menuDefinition menu_topup_confirm;
menuDefinition menu_topup_display_balance;
menuDefinition menu_topup_tapto_top;

/**
 * @brief Initialize topup menus
 */
void apiDnfccMenuInitializeAllMenusTopup() {

	apiDnfccMenuInitializeOneMenu(&menu_topup_start, buttons_topup_start);
	apiDnfccMenuInitializeOneMenu(&menu_topup_display_balance, buttons_display_balance);
	apiDnfccMenuInitializeOneMenu(&menu_topup_valueselect, buttons_valueselect);
	apiDnfccMenuInitializeOneMenu(&menu_topup_confirm, buttons_topupconfirm);
	apiDnfccMenuInitializeOneMenu(&menu_topup_tapto_top, buttons_taptotop);
}

/** @}*/
#endif
