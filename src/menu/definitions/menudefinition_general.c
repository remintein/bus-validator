/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"
#include "../menu.h"
#include "../menu_definition.h"

#include "../../_definition/buttonTypes.h"

extern char updateinfo[];

/**
 * @file
 * @brief Contains the definition for the menus
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */
static button buttons_loginscreen[] =
		{

#if CURRENT_TARGET == CNS_HANDHELD
				{	10, 10, 460, 100, "\n\n"LOCALE_HANDHELD, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},
#elif CURRENT_TARGET == CNS_TOPUP
				{	10, 10, 460, 100, "\n\n"LOCALE_TOPUP, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},
#elif CURRENT_TARGET == CNS_VALIDATOR
				{	10, 10, 460, 100, "\n\n"LOCALE_SCHOOL, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},
				{	10, 110, 460, 50, LOCALE_ATTENDANT, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},
#elif CURRENT_TARGET == CNS_FORMATTER
				{	10, 10, 530, 100, "\n"LOCALE_FORMATTER, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

				{	80, 150, 320, 140, "", COMMAND_VOID, LOGO_CNS_NOCLEAR}, ////80, 100, 140, 0

//				{	480, 150, 200, 200, LOCALE_LOG_IN, COMMAND_VOID, BUTTON_LED},
//
//				{	10, 420, 780, 50, updateinfo, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

				{	530, 10, 260, 100, " ", COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

#elif CURRENT_TARGET == CNS_BUSCONSOLE
				{	270, 10, 260, 100, "\n"LOCALE_BUS_CONSOLE, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

				{	10, 10, 260, 50, LOCALE_ROUTE , COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

				{	10, 60, 260, 50, LOCALE_SERVICE, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

				{	530, 10, 260, 100, " ", COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

#endif
#if CURRENT_TARGET == CNS_BUSCONSOLE
				{	240, 150, 320, 140, "", COMMAND_VOID, LOGO_CNS_NOCLEAR}, ////80, 100, 140, 0

//		{	480, 150, 200, 200, LOCALE_LOG_IN, COMMAND_VOID, BUTTON_LED},

				{ 10, 420, 780, 50, updateinfo, COMMAND_VOID,
						WINDOW_WITH_TEXT_NOCLEAR },

#else

				{	80, 200, 320, 140, "", COMMAND_VOID, LOGO_CNS_NOCLEAR}, ////80, 100, 140, 0

				{	150, 450, 200, 200, LOCALE_LOG_IN, COMMAND_VOID, BUTTON_LED},

				{	10, 740, 460, 50, updateinfo, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

#endif
				{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

		};

button buttons_empty[] = {

{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

menuDefinition menu_loginscreen;
menuDefinition menu_empty;

/**
 * @brief Initialize generic menus
 */
void apiDnfccMenuInitializeAllMenusGeneral() {
	apiDnfccMenuInitializeOneMenu(&menu_loginscreen, buttons_loginscreen);
	apiDnfccMenuInitializeOneMenu(&menu_empty, buttons_empty);
}

/** @} */
