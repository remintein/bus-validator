/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"
#include "../menu.h"
#include "../menu_definition.h"

#include "../../_definition/buttonTypes.h"

#if CURRENT_TARGET == CNS_BUSCONSOLE

extern char payInfo[];
extern char payinfoSubtitle[];
extern char busline[];
extern char valueLabel[];
extern char serviceline[];
extern char validatorInfo[];
extern char validatorInfoSubtitle[];
#if DNFCC_ADD_GSM_DISABLE == 1
extern char gsmStatusTitle[];
#endif

/**
 * @file
 * @brief Contains the definition for the menus
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */

static button buttons_busconsole_main[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO

	{	10, 110, 780, 80, valueLabel, COMMAND_VOID, WINDOW_WITH_TEXT},

	{	10, 190, 260, 108, LOCALE_MENU_ROUTE, COMMAND_MENU_ROUTESELECT, BUTTON_NORMAL},

	{	10, 298, 260, 108, LOCALE_MENU_ENDSHIFT, COMMAND_MENU_LOGOUT, BUTTON_NORMAL},

	{	10, 406, 260, 72, LOCALE_MENU_STARTTRIP, COMMAND_MENU_STARTTRIP, BUTTON_NORMAL},

	{	270, 190, 520, 216, "", COMMAND_VOID, BUTTON_NORMAL},

	{	370, 210, 320, 140, "", COMMAND_VOID, LOGO_CNS},

	{	10, 10, 260, 50, busline, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

	{	10, 60, 260, 50, serviceline, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR},

	{	270, 406, 260, 72, LOCALE_MENU_LOGOUT, COMMAND_MENU_LOGOUT, WINDOW_WITH_TEXT},

	{	530, 406, 260, 72, LOCALE_MENU_CANCEL, COMMAND_MENU_LOGOUT, WINDOW_WITH_TEXT},

	{	0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL}};

// check buttons_serviceselect same button s but different enter menu button
button buttons_routeselect[] = {

	{	270, 190, 104, 72, "1", COMMAND_1, BUTTON_NORMAL},		//row 1

	{	374, 190, 104, 72, "2", COMMAND_2, BUTTON_NORMAL},

	{	478, 190, 104, 72, "3", COMMAND_3, BUTTON_NORMAL},

	{	582, 190, 104, 72, "4", COMMAND_4, BUTTON_NORMAL},

//		{ 686, 190, 104, 72, "-", COMMAND_MIN ,BUTTON_NORMAL},

	{	270, 262, 104, 72, "5", COMMAND_5, BUTTON_NORMAL},		// row 2

	{	374, 262, 104, 72, "6", COMMAND_6, BUTTON_NORMAL},

	{	478, 262, 104, 72, "7", COMMAND_7, BUTTON_NORMAL},

	{	582, 262, 104, 72, "8", COMMAND_8, BUTTON_NORMAL},

//		{ 686, 270, 104, 80, "D", COMMAND_D ,BUTTON_NORMAL},

	{	270, 334, 104, 72, "9", COMMAND_9, BUTTON_NORMAL}, // row 3

	{	374, 334, 104, 72, "0", COMMAND_0, BUTTON_NORMAL},

	{	478, 334, 104, 72, "<=", COMMAND_BACK, BUTTON_NORMAL},

	{	582, 334, 104, 72, LOCALE_MENU_CANCEL, COMMAND_MENU_MAIN, BUTTON_NORMAL},

	{	686, 190, 104, 216, LOCALE_MENU_ENTER, COMMAND_MENU_ROUTE_ENTER, BUTTON_NORMAL},
// Left menu
	{	10, 110, 780, 80, valueLabel, COMMAND_VOID, WINDOW_WITH_TEXT},

	{	10, 190, 260, 108, LOCALE_MENU_ROUTE, COMMAND_MENU_ROUTESELECT, BUTTON_NORMAL},

	{	10, 298, 260, 108, LOCALE_MENU_ENDSHIFT, COMMAND_MENU_LOGOUT, BUTTON_NORMAL},

	{	10, 406, 260, 72, LOCALE_MENU_STARTTRIP, COMMAND_MENU_STARTTRIP, BUTTON_NORMAL},

	{	270, 406, 260, 72, "", COMMAND_VOID, BUTTON_NORMAL},

	{	530, 406, 260, 72, "", COMMAND_VOID, BUTTON_NORMAL},
//////////
	{	0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL}};

// almost copy of routeselect but different enter button
button buttons_serviceselect[] = {

	{	270, 190, 104, 72, "1", COMMAND_1, BUTTON_NORMAL}, //row 1

	{	374, 190, 104, 72, "2", COMMAND_2, BUTTON_NORMAL},

	{	478, 190, 104, 72, "3", COMMAND_3, BUTTON_NORMAL},

	{	582, 190, 104, 72, "4", COMMAND_4, BUTTON_NORMAL},

//		{ 686, 190, 104, 72, "-", COMMAND_MIN ,BUTTON_NORMAL},

	{	270, 262, 104, 72, "5", COMMAND_5, BUTTON_NORMAL}, // row 2

	{	374, 262, 104, 72, "6", COMMAND_6, BUTTON_NORMAL},

	{	478, 262, 104, 72, "7", COMMAND_7, BUTTON_NORMAL},

	{	582, 262, 104, 72, "8", COMMAND_8, BUTTON_NORMAL},

//		{ 686, 270, 104, 72, "D", COMMAND_D ,BUTTON_NORMAL},

	{	270, 334, 104, 72, "9", COMMAND_9, BUTTON_NORMAL}, // row 3

	{	374, 334, 104, 72, "0", COMMAND_0, BUTTON_NORMAL},

	{	478, 334, 104, 72, "<=", COMMAND_BACK, BUTTON_NORMAL},

	{	582, 334, 104, 72, LOCALE_MENU_CANCEL, COMMAND_MENU_MAIN, BUTTON_NORMAL},

	{	686, 190, 104, 216, LOCALE_MENU_ENTER, COMMAND_MENU_SERVICE_ENTER, BUTTON_NORMAL},
// Left menu
	{	10, 110, 780, 80, valueLabel, COMMAND_VOID, WINDOW_WITH_TEXT},

	{	10, 190, 260, 108, LOCALE_MENU_ROUTE, COMMAND_MENU_ROUTESELECT, BUTTON_NORMAL},

	{	10, 298, 260, 108, LOCALE_MENU_ENDSHIFT, COMMAND_MENU_LOGOUT, BUTTON_NORMAL},

	{	10, 406, 260, 72, LOCALE_MENU_STARTTRIP, COMMAND_MENU_STARTTRIP, BUTTON_NORMAL},

	{	270, 406, 260, 72, "", COMMAND_VOID, BUTTON_NORMAL},

	{	530, 406, 260, 72, "", COMMAND_VOID, BUTTON_NORMAL},
//////////
	{	0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL}};

button buttons_driveoperations[] = {
// 1th must be the led for the busconsole, 2th for the validator

	{	90, 144, 200, 200, payInfo, COMMAND_VOID, BUTTON_LED},

	{	490, 144, 200, 200, validatorInfo, COMMAND_VOID, BUTTON_LED},

	{	10, 349, 390, 50, payinfoSubtitle, COMMAND_VOID, WINDOW_WITH_TEXT_NOBACKGROUND},

	{	400, 349, 390, 50, validatorInfoSubtitle, COMMAND_VOID, WINDOW_WITH_TEXT_NOBACKGROUND},

	{	10, 406, 260, 72, LOCALE_MENU_ENDTRIP, COMMAND_MENU_ENDTRIP, BUTTON_NORMAL},

#if DNFCC_ADD_GSM_DISABLE == 1
//	{	270, 406, 260, 72, LOCALE_MENU_TICKET1, COMMAND_TICKET_1, BUTTON_NORMAL},

//	{	530, 406, 130, 72, LOCALE_MENU_TICKET2, COMMAND_TICKET_2, BUTTON_NORMAL},

	{	270, 406, 260, 72,  "", COMMAND_VOID, BUTTON_NORMAL},

	{	530, 406, 260, 72, gsmStatusTitle, COMMAND_GSM_TOGGLE, BUTTON_NORMAL},

#else
	{	270, 406, 260, 72, LOCALE_MENU_TICKET1, COMMAND_TICKET_1, BUTTON_NORMAL},

	{	530, 406, 260, 72, LOCALE_MENU_TICKET2, COMMAND_TICKET_2, BUTTON_NORMAL},

#endif
	{	0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL}};

menuDefinition menu_busconsole_main;
menuDefinition menu_driveoperations;
menuDefinition menu_routeselect;
menuDefinition menu_serviceselect;

/**
 * @brief Initialize bus console menus
 */
void apiDnfccMenuInitializeAllMenusBusConsole() {
	apiDnfccMenuInitializeOneMenu(&menu_busconsole_main, buttons_busconsole_main);
	apiDnfccMenuInitializeOneMenu(&menu_driveoperations, buttons_driveoperations);
	apiDnfccMenuInitializeOneMenu(&menu_routeselect, buttons_routeselect);
	apiDnfccMenuInitializeOneMenu(&menu_serviceselect, buttons_serviceselect);
}

/** @} */
#endif
