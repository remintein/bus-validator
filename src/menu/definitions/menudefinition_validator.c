/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"
#include "../menu.h"
#include "../menu_definition.h"

#include "../../_definition/buttonTypes.h"
#if CURRENT_TARGET == CNS_VALIDATOR

extern char busline[];
extern char validatorButtonLabel[];
extern char ticketvalue[];
extern char serviceline[];

/**
 * @file
 * @brief Contains the definition for the menus
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */

static button buttons_validatorscreen[] = {

{ 150, 450, 200, 200, ticketvalue, COMMAND_VOID, BUTTON_LED },

//{ 10, 110, 230, 50, busline, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR },
//
//{ 240, 110, 230, 50, serviceline, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR },

//{ 10, 110, 230, 50, busline, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR },
//{ 240, 110, 230, 50, serviceline, COMMAND_VOID, WINDOW_WITH_TEXT_NOCLEAR },

{ 10, 680, 460, 50, validatorButtonLabel, COMMAND_VOID, WINDOW_WITH_TEXT_NOBACKGROUND },

{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

menuDefinition menu_validator_main;

/**
 * @brief Initialize handheld menus
 */

apiDnfccMenuInitializeAllMenusValidator() {

	apiDnfccMenuInitializeOneMenu(&menu_validator_main, buttons_validatorscreen);
}
/** @} */
#endif


