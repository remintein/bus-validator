/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"
#include "../menu.h"
#include "../menu_definition.h"

#include "../../_definition/buttonTypes.h"

#if CURRENT_TARGET == CNS_HANDHELD

extern char handheldInfo[];
extern char handheldPenaltyCash[];
extern char handheldPenaltyCard[];

/**
 * @file
 * @brief Contains the definition for the menus
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */
button buttons_handheld[] = {
// x, y, width, height
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO
		{ 10, 380, 460, 200, "\n\n\n"LOCALE_TAP_FOR_CONTROL, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 10, 580, 460, 100, LOCALE_MENU_PENALTY, COMMAND_MENU_HANDHELD_PENALTY, BUTTON_NORMAL },

		{ 10, 680, 460, 100, LOCALE_MENU_LOGOUT, COMMAND_MENU_LOGOUT, BUTTON_NORMAL },

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

//**** HANDHELD ******** HANDHELD ******** HANDHELD ******** HANDHELD ******** HANDHELD ****
button buttons_handheldPass[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO
		{ 10, 380, 460, 200, handheldInfo, COMMAND_VOID, WINDOW_WITH_TEXT_GREEN },

		{ 10, 580, 460, 100, LOCALE_MENU_CORRECT, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 10, 680, 460, 100, LOCALE_MENU_CANCEL, COMMAND_MENU_HANDHELD_CANCEL, BUTTON_NORMAL },

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};
//**** HANDHELD ******** HANDHELD ******** HANDHELD ******** HANDHELD ******** HANDHELD ****
button buttons_handheldFail[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO
		{ 10, 380, 460, 200, handheldInfo, COMMAND_MENU_HANDHELD_PENALTY, WINDOW_WITH_TEXT_RED },

		{ 10, 580, 460, 100, LOCALE_MENU_PENALTY, COMMAND_MENU_HANDHELD_PENALTY, BUTTON_NORMAL },

		{ 10, 680, 460, 100, LOCALE_MENU_CANCEL, COMMAND_MENU_HANDHELD_CANCEL, BUTTON_NORMAL },

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};
button buttons_penaltypay[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO

		{ 10, 380, 460, 100, LOCALE_PENALTY"\n"LOCALE_SELECT_PAYMENT_METHOD, COMMAND_MENU_HANDHELD_HELP, WINDOW_WITH_TEXT },

		{ 10, 480, 460, 100, handheldPenaltyCard, COMMAND_MENU_HANDHELD_CARD, BUTTON_NORMAL },

		{ 10, 580, 460, 100, handheldPenaltyCash, COMMAND_MENU_HANDHELD_CASH, BUTTON_NORMAL },

		{ 10, 680, 460, 100, LOCALE_MENU_CANCEL, COMMAND_MENU_HANDHELD_CANCEL, BUTTON_NORMAL },

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

button buttons_cardPayment[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO

		{ 10, 380, 460, 300, "\n\n\n\n"LOCALE_TAP_YOUR_CARD"\n"LOCALE_TO_PAY, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 10, 680, 460, 100, LOCALE_MENU_CANCEL, COMMAND_MENU_HANDHELD_CANCEL, BUTTON_NORMAL },

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

button buttons_cashPayment[] = {
// x, y, width, heigth
// For icons: x, y, scale, scale; text = =ICONNAME
// ICONNAME:  =OK, =ERR, =TRY, =OKLO

		{ 10, 380, 460, 150, "\n\n"LOCALE_TAP_YOUR_CARD"\n"LOCALE_TO_CONFIRM_PAYMENT, COMMAND_VOID, WINDOW_WITH_TEXT },

		{ 10, 530, 460, 150, "\n\n"LOCALE_PRINT_BILL, COMMAND_MENU_HANDHELD_PRINT_BILL, WINDOW_WITH_TEXT },

		{ 10, 680, 460, 100, LOCALE_MENU_CANCEL, COMMAND_MENU_HANDHELD_CANCEL, BUTTON_NORMAL },

		{ 0, 0, 0, 0, NULL, COMMAND_ENDOFLIST, BUTTON_NORMAL }

};

menuDefinition menu_handheld;
menuDefinition menu_handheldPass;
menuDefinition menu_handheldFail;
menuDefinition menu_handheld_selectpenaltypay;
menuDefinition menu_handheld_card_payment;
menuDefinition menu_handheld_cash_payment;

/**
 * @brief Initialize handheld menus
 */
void apiDnfccMenuInitializeAllMenusHandheld() {

	apiDnfccMenuInitializeOneMenu(&menu_handheld, buttons_handheld);
	apiDnfccMenuInitializeOneMenu(&menu_handheldPass, buttons_handheldPass);
	apiDnfccMenuInitializeOneMenu(&menu_handheldFail, buttons_handheldFail);
	apiDnfccMenuInitializeOneMenu(&menu_handheld_selectpenaltypay, buttons_penaltypay);
	apiDnfccMenuInitializeOneMenu(&menu_handheld_card_payment, buttons_cardPayment);
	apiDnfccMenuInitializeOneMenu(&menu_handheld_cash_payment, buttons_cashPayment);

}
/** @} */
#endif
