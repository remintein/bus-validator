/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef MENU_H_
#define MENU_H_

#include "../project_includes.h"

BaseType_t nfccMenuInit(UBaseType_t uxPriority);
void nfccMenu(void *pvParameters);
#include "../_definition/buttonTypes.h"

typedef enum {
	COMMAND_0,
	COMMAND_1,
	COMMAND_2,
	COMMAND_3,
	COMMAND_4,
	COMMAND_5,
	COMMAND_6,
	COMMAND_7,
	COMMAND_8,
	COMMAND_9,
	COMMAND_BACK,
	COMMAND_MIN,
	COMMAND_D,
	COMMAND_CANCEL,
	COMMAND_MENU_MAIN,
	COMMAND_MENU_ROUTESELECT,
	COMMAND_MENU_ROUTE_ENTER,
	COMMAND_MENU_SERVICE_ENTER,
	COMMAND_MENU_STARTTRIP,
	COMMAND_MENU_ENDTRIP,
	COMMAND_VOID,
	COMMAND_TICKET_1,
	COMMAND_TICKET_2,
	COMMAND_MENU_TOPUP_SELECT,
	COMMAND_MENU_FORMATTER_START,
	COMMAND_MENU_FORMATTER_STOP,
	COMMAND_MENU_LOGOUT,
	COMMAND_MENU_TOPUP1,
	COMMAND_MENU_TOPUP2,
	COMMAND_MENU_TOPUP3,
	COMMAND_MENU_TOPUP4,
	COMMAND_MENU_TOPUP_CANCEL,
	COMMAND_MENU_TOPUP_HELP,
	COMMAND_MENU_TOPUP_CONFIRM,
	COMMAND_MENU_HANDHELD_CANCEL,
	COMMAND_MENU_HANDHELD_PRINT_BILL,
	COMMAND_MENU_HANDHELD_CORRECT,
	COMMAND_MENU_HANDHELD_PENALTY,
	COMMAND_MENU_HANDHELD_CARD,
	COMMAND_MENU_HANDHELD_CASH,
	COMMAND_MENU_HANDHELD_HELP,
#if DNFCC_ADD_GSM_DISABLE
	COMMAND_GSM_TOGGLE,
#endif
	COMMAND_ENDOFLIST
} commandType;

typedef struct {
	uint16_t x;
	uint16_t y;
	uint16_t xsize;
	uint16_t ysize;
	char *label;
	commandType command;
	buttonType type;
} button;

typedef enum {
	STANDBY = 'S',
	ACTIVE = 'A',
	DISABLED = 'D',
	HOVER = 'H',
	BUTTON_RED = 'R',
	BUTTON_GREEN = 'G',
	BUTTON_YELLOW = 'Y'
} buttonState;

typedef struct {
	button *buttons;
	buttonState curButtonState[20]; // For redraw
	buttonState nextButtonState[20]; // For redraw
	int xStart, xEnd, yStart, yEnd;
} menuDefinition;

#endif /* MENU_H_ */
