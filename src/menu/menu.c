/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "menu.h"
#include "menu_definition.h"
#include "draw.h"

/**
 * @file
 * @brief Contains the functions for the menu
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */

#define STACK_SIZE 500

#define MAXROUTE  10
#define MAXSERVICE 5

void apiDnfccMenuInitializeLabels();
void apiDnfccMenuVerifyTimeout();
void apiDnfccMenuProcessMessage(char *buf);

/**
 * @brief initialization routine for menu module
 *
 * @param uxPriority - The FreeRTOS priority
 */
BaseType_t nfccMenuInit(UBaseType_t prio) {
	deviceStatus.menuRxQueue = xQueueCreate(MENU_QUEUE_LENGTH,
			MENU_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.menuRxQueue, "Menu");

	apiDnfccMenuInitializeAllMenus();
	setWaitForSystemReadyThisTask(TASK_BIT_MENU);
	return xTaskCreate(nfccMenu, "menu", STACK_SIZE, NULL, prio,
			&deviceStatus.menuTask);
}

/**
 * @brief the main group for menu
 *
 * @param pvParameters - the parameters from FreeRTOS
 */
void nfccMenu(void *pvParameters) {
	char buf[MENU_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;

	setTaskIsSystemReady(TASK_BIT_MENU);
	waitForSystemReady();

	initMenu(deviceStatus.curMenu);
	apiDnfccMenuInitializeLabels();
	for (;;) {
		if (xQueueReceive(deviceStatus.menuRxQueue, buf, 300) != pdPASS) {
			apiDnfccMenuVerifyTimeout(); // See folder menu for the implementation for each device
			continue;
		}

		switch (buf[0]) {

		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(
					deviceStatus.menuTask);
			sprintf(stbuf, "!TWM%ld", uxHighWaterMark);
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_MENU;
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;

		default:
			apiDnfccMenuProcessMessage(buf); // See folder menu for the implementation for each device
			break;

		} // Switch[receive buffer]
	} // Endless for loop
}
/** @} */
