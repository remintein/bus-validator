/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"
#include "menu.h"
#include "menu_definition.h"

#include "../_definition/buttonTypes.h"

/**
 * @file
 * @brief Contains the functions for the menu
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */

#if CURRENT_TARGET == CNS_BUSCONSOLE
void apiDnfccMenuInitializeAllMenusBusConsole();
#endif
#if CURRENT_TARGET == CNS_VALIDATOR
void apiDnfccMenuInitializeAllMenusValidator();
#endif
#if CURRENT_TARGET == CNS_HANDHELD
void apiDnfccMenuInitializeAllMenusHandheld();
#endif
#if CURRENT_TARGET == CNS_TOPUP
void apiDnfccMenuInitializeAllMenusTopup();
#endif

/**
 * @brief Initialize one menu from the definition
 * @param menu  - the menu definition
 * @param buttons - the buttons definition
 */
void apiDnfccMenuInitializeOneMenu(menuDefinition *menu, button *buttons) {
	uint16_t i;
	menu->buttons = buttons;

	menu->xStart = 999;
	menu->xEnd = 0;
	menu->yStart = 999;
	menu->yEnd = 0;
	for (i = 0; i < 20; i++) {
		menu->curButtonState[i] = menu->nextButtonState[i] = ACTIVE;
	}

	for (i = 0; menu->buttons[i].command != COMMAND_ENDOFLIST; i++) {
		if (menu->buttons[i].type == WINDOW_WITH_TEXT_NOCLEAR
				|| menu->buttons[i].type == LOGO_CNS_NOCLEAR) {
			continue;
		}
		if (menu->xStart > menu->buttons[i].x) {
			menu->xStart = menu->buttons[i].x;
		}
		if (menu->yStart > menu->buttons[i].y) {
			menu->yStart = menu->buttons[i].y;
		}
		if (menu->xEnd < menu->buttons[i].x + menu->buttons[i].xsize) {
			menu->xEnd = menu->buttons[i].x + menu->buttons[i].xsize;
		}
		if (menu->yEnd < menu->buttons[i].y + menu->buttons[i].ysize) {
			menu->yEnd = menu->buttons[i].y + menu->buttons[i].ysize;
		}
	}
}

/**
 * @brief Initalizes all necessary menus
 */
void apiDnfccMenuInitializeAllMenus() {
	apiDnfccMenuInitializeAllMenusGeneral();

#if CURRENT_TARGET == CNS_BUSCONSOLE
	apiDnfccMenuInitializeAllMenusBusConsole();
#endif
#if CURRENT_TARGET == CNS_VALIDATOR
	apiDnfccMenuInitializeAllMenusValidator();
#endif
#if CURRENT_TARGET == CNS_HANDHELD
	apiDnfccMenuInitializeAllMenusHandheld();
#endif
#if CURRENT_TARGET == CNS_TOPUP
	apiDnfccMenuInitializeAllMenusTopup();
#endif
}
/** @} */
