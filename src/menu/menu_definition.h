/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef DISPLAY_MENU_DEFINITION_H_
#define DISPLAY_MENU_DEFINITION_H_

#include "menu.h"

extern menuDefinition menu_loginscreen;

/**** TOPUP MENU's *****/
extern menuDefinition menu_topup_start;
extern menuDefinition menu_topup_valueselect;
extern menuDefinition menu_topup_confirm;
extern menuDefinition menu_topup_display_balance;
extern menuDefinition menu_topup_tapto_top;
/**** FORMATTER MENU's *****/
extern menuDefinition menu_formatter_start;
extern menuDefinition menu_formatter_stop;
/**** HANDHELD MENU's *****/
extern menuDefinition menu_handheld;
extern menuDefinition menu_handheldPass;
extern menuDefinition menu_handheldFail;
extern menuDefinition menu_handheld_selectpenaltypay;
extern menuDefinition menu_handheld_card_payment;
extern menuDefinition menu_handheld_cash_payment;

extern menuDefinition menu_busconsole_main;
extern menuDefinition menu_validator_main;
extern menuDefinition menu_routeselect;
extern menuDefinition menu_serviceselect;
extern menuDefinition menu_driveoperations;
extern menuDefinition menu_empty;

void apiDnfccMenuInitializeAllMenusGeneral();
void apiDnfccMenuInitializeAllMenusTopup();

void apiDnfccMenuInitializeLabels() ;

void apiDnfccMenuInitializeOneMenu(menuDefinition *menu, button *buttons);

#endif /* DISPLAY_MENU_DEFINITION_H_ */
