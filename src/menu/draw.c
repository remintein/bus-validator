/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"
#include "menu.h"
#include "menu_definition.h"
#include <string.h>

/**
 * @file
 * @brief Contains the functions for the menu
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */
/** @brief Needed for determining the window to clear */
static int lastXStart = 999;
/** @brief Needed for determining the window to clear */
static int lastXEnd = 0;
/** @brief Needed for determining the window to clear */
static int lastYStart = 999;
/** @brief Needed for determining the window to clear */
static int lastYEnd = 0;

void apiDnfccMenuDrawButton(button *button, buttonState col);

/**
 * @brief Initialize menu
 *
 * @param menu to initialize
 */
void initMenu(menuDefinition *curMenu) {

// draw menu buttons
// with activeButton -1 all buttons in standby mode,
// else only active button in active state rest in buttonstate bs (e.g. disabled or standby)
// Clear indicates if to clear area

	uint16_t i;
	char buf[DISPLAY_BUFFER_SIZE + 1];

	if (curMenu->xStart < lastXStart)
		lastXStart = curMenu->xStart;
	if (curMenu->yStart < lastYStart)
		lastYStart = curMenu->yStart;
	if (curMenu->xEnd > lastXEnd)
		lastXEnd = curMenu->xEnd;
	if (curMenu->yEnd > lastYEnd)
		lastYEnd = curMenu->yEnd;

	sprintf(buf, "%cX%dY%dX%dY%d", COMCOM_DISPLAY_CLEAR_AREA_BY_COORD,
			lastXStart, lastYStart, lastXEnd, lastYEnd);
	queueSendToDisplay(buf, portMAX_DELAY);

	lastXStart = curMenu->xStart;
	lastYStart = curMenu->yStart;
	lastXEnd = curMenu->xEnd;
	lastYEnd = curMenu->yEnd;
	for (i = 0; curMenu->buttons[i].command != COMMAND_ENDOFLIST; i++) {
		curMenu->nextButtonState[i] = STANDBY;
		apiDnfccMenuDrawButton(&curMenu->buttons[i], curMenu->nextButtonState[i]);
		curMenu->curButtonState[i] = curMenu->nextButtonState[i];
	}
}

/**
 * @brief redraw changed buttons
 *
 * @param curMenu - menu to redraw
 */
void apiDnfccMenuRedrawChangedButtons(menuDefinition *curMenu) {
	for (int i = 0; curMenu->buttons[i].command != COMMAND_ENDOFLIST; i++) {
		if (curMenu->curButtonState[i] != curMenu->nextButtonState[i]) {
			apiDnfccMenuDrawButton(&curMenu->buttons[i], curMenu->nextButtonState[i]);
			curMenu->curButtonState[i] = curMenu->nextButtonState[i];
		}
	}
}

/**
 * @brief Redraw window buttons
 *
 * @param curMenu - menu to redraw
 */
void apiDnfccMenuRedrawWindowButtons(menuDefinition *curMenu) {
	for (int i = 0; curMenu->buttons[i].command != COMMAND_ENDOFLIST; i++) {
		if (curMenu->buttons[i].type == WINDOW_WITH_TEXT
				|| curMenu->buttons[i].type == WINDOW_WITH_TEXT_RED
				|| curMenu->buttons[i].type == WINDOW_WITH_TEXT_GREEN
				|| curMenu->buttons[i].type == WINDOW_WITH_TEXT_NOCLEAR
				|| curMenu->buttons[i].type == WINDOW_WITH_TEXT_NOBACKGROUND) {
			apiDnfccMenuDrawButton(&curMenu->buttons[i], curMenu->nextButtonState[i]);
			curMenu->curButtonState[i] = curMenu->nextButtonState[i];
		}
	}
}

/**
 * @brief Redraw window buttons and LED's
 *
 * @param curMenu - menu to redraw
 */
void apiDnfccMenuRedrawWindowButtonsAndLeds(menuDefinition *curMenu) {
	for (int i = 0; curMenu->buttons[i].command != COMMAND_ENDOFLIST; i++) {
		if (curMenu->buttons[i].type == WINDOW_WITH_TEXT
				|| curMenu->buttons[i].type == WINDOW_WITH_TEXT_RED
				|| curMenu->buttons[i].type == WINDOW_WITH_TEXT_GREEN
				|| curMenu->buttons[i].type == WINDOW_WITH_TEXT_NOCLEAR
				|| curMenu->buttons[i].type == BUTTON_LED
				|| curMenu->buttons[i].type == BUTTON_LEDCIRCLE
				|| curMenu->buttons[i].type == WINDOW_WITH_TEXT_NOBACKGROUND) {
			apiDnfccMenuDrawButton(&curMenu->buttons[i], curMenu->nextButtonState[i]);
			curMenu->curButtonState[i] = curMenu->nextButtonState[i];
		}
	}
}

/**
 * @brief Redraw button
 *
 * @param button - Button to redraw
 * @param col - New button state
 */
void apiDnfccMenuDrawButton(button *button, buttonState col) {
	char buf[DISPLAY_BUFFER_SIZE + 1];
	int i, line;
	buttonType myButtonType = button->type;
	if (button->type == WINDOW_WITH_TEXT_GREEN) {
		myButtonType = WINDOW_WITH_TEXT;
		col = 'G';
	}
	if (button->type == WINDOW_WITH_TEXT_RED) {
		myButtonType = WINDOW_WITH_TEXT;
		col = 'R';
	}
	switch (myButtonType) {
	case BUTTON_NORMAL:
	case BUTTON_SMALLTEXT:
	case LOGO_CNS:
	case LOGO_CNS_NOCLEAR:
	case BUTTON_LED:
	case BUTTON_LEDCIRCLE:
		sprintf(buf, "%cT%dX%dY%dx%dy%dC%cL%s", COMCOM_DISPLAY_CREATE_BUTTON,
				myButtonType, button->x, button->y, button->xsize,
				button->ysize, col, button->label);
		queueSendToDisplay(buf, portMAX_DELAY);
		break;
	case WINDOW_WITH_TEXT:
	case WINDOW_WITH_TEXT_NOCLEAR:
	case WINDOW_WITH_TEXT_NOBACKGROUND:
		line = 2;
		sprintf(buf, "%cT%dX%dY%dx%dy%dC%cL", COMCOM_DISPLAY_CREATE_BUTTON,
				myButtonType, button->x, button->y, button->xsize,
				button->ysize, col);

		for (i = 0; button->label[i] != 0; i++) {
			if (button->label[i] == '\n') {
				queueSendToDisplay(buf, portMAX_DELAY);
				sprintf(buf, "%cT%dX%dY%dx%dy%dC%cl%dL",
				COMCOM_DISPLAY_ADD_TO_WINDOW, myButtonType, button->x,
						button->y, button->xsize, button->ysize, col, line++);
				continue;
			}
			int cl = strlen(buf);
			buf[cl + 1] = '\0';
			buf[cl] = button->label[i];
		}
		queueSendToDisplay(buf, portMAX_DELAY);
		break;
	case WINDOW_WITH_TEXT_GREEN:
	case WINDOW_WITH_TEXT_RED:
		break;
	}
}

/** @} */
