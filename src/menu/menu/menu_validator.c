/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../../project_includes.h"
#include "../menu.h"
#include "../menu_definition.h"
#include "../draw.h"

#include <string.h>

#if CURRENT_TARGET == CNS_VALIDATOR

/**
 * @file
 * @brief Contains the functions for the menu
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */

/** @brief Labels for display */
char payInfo[70], payinfoSubtitle[40], updateinfo[70];
char busline[50], serviceline[50], validatorButtonLabel[30], ticketvalue[50];

TickType_t timeoutForClean = 0;
TickType_t timeoutForClean2 = 0;

/**
 * @brief Initialize labels
 */
void apiDnfccMenuInitializeLabels() {
// Initialize the labels
	strcpy(payinfoSubtitle, LOCALE_MENU_SELL_TICKET);
	strcpy(validatorButtonLabel, LOCALE_TAP_TO_GO);
	sprintf(updateinfo, LOCALE_FIRMWARE_VERSION_PRINT, deviceStatus.firmwareVersion);
}

/**
 * @brief Set the entered route
 *
 * @param route the route to set
 */
static void setRoute(char* route) {
	// bus route entered
	sscanf(route, "%ld", &deviceStatus.deviceRouteId);// store in device
	sprintf(busline, LOCALE_BUS_LINE_PRINT, deviceStatus.deviceRouteId);
	apiDnfccMenuRedrawWindowButtons(deviceStatus.curMenu);
}

/**
 * @brief set the entered service
 *
 * @param service the service to set
 */
static void setService(char* service) {
	// bus service code entered
	sscanf(service, "%ld", &deviceStatus.deviceServiceId);// store in device
	sprintf(serviceline, LOCALE_SERVICE_LINE_PRINT, deviceStatus.deviceServiceId);
	apiDnfccMenuRedrawWindowButtons(deviceStatus.curMenu);
}

/**
 * @brief Verify timeout and perform the correct actions
 */
void apiDnfccMenuVerifyTimeout() {
	if (timeoutForClean != 0) {
		if (xTaskGetTickCount() > timeoutForClean) {
			if (deviceStatus.curMenu == &menu_validator_main) {
				menu_validator_main.nextButtonState[0] = ACTIVE;
				strcpy(validatorButtonLabel, LOCALE_TAP_TO_GO);
				strcpy(ticketvalue, LOCALE_TAP);
				initMenu(deviceStatus.curMenu);
			}
			timeoutForClean = 0;
		}
	}
	if (timeoutForClean2 != 0) {
		if (xTaskGetTickCount() > timeoutForClean2) {
			timeoutForClean2 = 0;
		}
	}
}

/**
 * @brief Process one message from the queue
 */
void apiDnfccMenuProcessMessage(char *buf) {

	int lastHoverButton = -1; // at each switch of menu remind to reset this to -1 !!
	int i, r = 0, s = 0;
	int x, y;

	char route[] = "\0\0\0\0\0\0\0\0\0\0\0";
	char service[] = "\0\0\0\0\0";
	char dBuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];

	switch (buf[0]) {

		case COMCOM_MENU_ENABLE_VALIDATOR:
		deviceStatus.curMenu = &menu_validator_main;
		deviceStatus.isLoggedIn = 1;
		lastHoverButton = -1;
		initMenu(deviceStatus.curMenu);
		setRoute(buf + 1);
		break;

		case COMCOM_MENU_ENABLE_VALIDATOR2:
		deviceStatus.curMenu = &menu_validator_main;
		deviceStatus.isLoggedIn = 1;
		lastHoverButton = -1;
		initMenu(deviceStatus.curMenu);
		setService(buf + 1);
		apiDnfccSetRouteAndFare(deviceStatus.deviceRouteId, deviceStatus.deviceServiceId);
		break;

		case COMCOM_MENU_DISABLE_VALIDATOR:
		deviceStatus.curMenu = &menu_loginscreen;
		deviceStatus.isLoggedIn = 0;
		lastHoverButton = -1;
		initMenu(deviceStatus.curMenu);
		break;

		case COMCOM_MENU_CARD_ENTER_SUCCESSFULL:
		case COMCOM_MENU_QRCODE_SUCCESSFULL:
		menu_validator_main.nextButtonState[0] = BUTTON_GREEN;
		payInfo[0] = '\0';
		strcpy(payinfoSubtitle, buf + 1);
		strcpy(validatorButtonLabel, LOCALE_WELCOME);
		strcpy(ticketvalue, 0); //TONY Display nothing
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 1000;
		break;

		case COMCOM_MENU_CARD_ENTER_SUCCESSFULL_WARNING:
		menu_validator_main.nextButtonState[0] = BUTTON_YELLOW;
		payInfo[0] = '\0';
		strcpy(payinfoSubtitle, buf + 1);
		strcpy(validatorButtonLabel, LOCALE_WELCOME);
		strcpy(ticketvalue, buf + 1);
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 1000;
		break;

		case COMCOM_MENU_CARD_ENTER_REJECT:
		case COMCOM_MENU_QRCODE_REJECT:
		menu_validator_main.nextButtonState[0] = BUTTON_RED;
		payInfo[0] = '\0';
		strcpy(validatorButtonLabel, LOCALE_NOTWELCOME);
		strcpy(payinfoSubtitle, buf + 1);

		sscanf(buf + 1, "%[^\n]", validatorButtonLabel);
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 1000;
		break;

		case COMCOM_MENU_CARD_ENTER_RETRY:
		menu_validator_main.nextButtonState[0] = BUTTON_RED;
		payInfo[0] = '\0';
		strcpy(payinfoSubtitle, LOCALE_RETRY);

		strcpy(validatorButtonLabel, LOCALE_TRY_AGAIN);
				apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
				timeoutForClean = xTaskGetTickCount() + 1000;
				break;

				case COMCOM_SELFTEST:
				// Handeled in menu.c
				break;
			}
		}
/** @} */
#endif
