/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../../project_includes.h"
#include "../menu.h"
#include "../menu_definition.h"
#include "../draw.h"
#include "../../sdio/DeviceDataFiles.h"
#include <string.h>

#if CURRENT_TARGET == CNS_TOPUP

#define DELAY_SHOW_BALANCE 7000

/**
 * @file
 * @brief Contains the functions for the menu
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */

/** @brief Labels for display */
char valueLabel[50], updateinfo[70];
char payInfo[70], payinfoSubtitle[40], topupValueLabel[50];
char topupLabel1[20];
char topupLabel2[20];
char topupLabel3[20];
char topupLabel4[20];

char validTillLabel[50];
char busline[50], serviceline[50], validatorButtonLabel[30], ticketvalue[50];
int32_t curBalance = 0;
TickType_t timeoutForClean = 0;
TickType_t timeoutForClean2 = 0;

/**
 * @brief Initialize labels
 */
void apiDnfccMenuInitializeLabels() {

// Initialize the labels
	valueLabel[0] = '\0';
	topupValueLabel[0] = '\0';
	validTillLabel[0] = '\0';

	deviceStatus.deviceRouteId = 0;
	deviceStatus.deviceServiceId = 2;
	apiDnfccSetRouteAndFare(deviceStatus.deviceRouteId,
			deviceStatus.deviceServiceId);

	sprintf(topupLabel1, "%lu VND", apiDnfccGetTicketFare(0));
	sprintf(topupLabel2, "%lu VND", apiDnfccGetTicketFare(1));
	sprintf(topupLabel3, "%lu VND", apiDnfccGetTicketFare(2));
	sprintf(topupLabel4, "%lu VND", apiDnfccGetTicketFare(3));
	strcpy(payinfoSubtitle, LOCALE_MENU_SELL_TICKET);
	strcpy(validatorButtonLabel, LOCALE_TAP_TO_GO);
	sprintf(updateinfo, LOCALE_FIRMWARE_VERSION_PRINT,
			deviceStatus.firmwareVersion);
}

/**
 * @brief Verify timeout and perform the correct actions
 */
void apiDnfccMenuVerifyTimeout() {
	if (timeoutForClean != 0) {
		if (xTaskGetTickCount() > timeoutForClean) {
			if (deviceStatus.curMenu == &menu_topup_display_balance) {
				deviceStatus.curMenu = &menu_topup_start;
				initMenu(deviceStatus.curMenu);
			}

			timeoutForClean = 0;
		}
	}

	if (timeoutForClean2 != 0) {
		if (xTaskGetTickCount() > timeoutForClean2) {

			timeoutForClean2 = 0;
		}
	}

}

/**
 * @brief Process one message from the queue
 */
void apiDnfccMenuProcessMessage(char *buf) {
	char soundBuf[SOUND_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];

	int lastHoverButton = -1; // at each switch of menu remind to reset this to -1 !!
	int i;
	int x, y;

	char dBuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];

	switch (buf[0]) {
	case COMCOM_MENU_TOUCH_CURRENT_POSITION: // Find position hovered button
		if (sscanf(buf + 1, "X%dY%d", &x, &y) != 2) {
			break; // command misformed
		}
		for (i = 0;
				deviceStatus.curMenu->buttons[i].command != COMMAND_ENDOFLIST;
				i++) {
			if (deviceStatus.curMenu->buttons[i].x > x
					|| deviceStatus.curMenu->buttons[i].y > y) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].x
					+ deviceStatus.curMenu->buttons[i].xsize < x
					|| deviceStatus.curMenu->buttons[i].y
							+ deviceStatus.curMenu->buttons[i].ysize < y) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].command != COMMAND_ENDOFLIST) {
				// got the button hovered. If it differs from lastHovered redraw
				if (lastHoverButton != i) {
					if (lastHoverButton != -1) {
						apiDnfccMenuDrawButton(
								&(deviceStatus.curMenu->buttons[lastHoverButton]),
								STANDBY);
					}
					apiDnfccMenuDrawButton(&(deviceStatus.curMenu->buttons[i]),
							HOVER);
					lastHoverButton = i;
				}
			}
		}
		break;

	case COMCOM_MENU_STARTSCREEN:
		// reseller logs in, switch to topup menu
		deviceStatus.isLoggedIn = 1;
		lastHoverButton = -1;
		deviceStatus.curMenu = &menu_topup_start;
		initMenu(deviceStatus.curMenu);
		break;

	case COMCOM_MENU_TOPUP_NEWBALACE:
		validTillLabel[0] = '\n';
		sscanf(buf + 1, "%ld,%ss", &curBalance, stbuf);
		//sprintf(validTillLabel, "\n%2.2s-%2.2s-%4.4s", stbuf + 6, stbuf + 4, stbuf);
		sprintf(validTillLabel, "%s: %2.2s-%2.2s-%4.4s", LOCALE_EXPIRED_DATE,
				stbuf + 6, stbuf + 4, stbuf);
		sprintf(valueLabel, "%s: %ld", LOCALE_TOPUP_BALANCE, curBalance);
		deviceStatus.curMenu = &menu_topup_display_balance;
		initMenu(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + DELAY_SHOW_BALANCE;
		break;

	case COMCOM_MENU_CARD_ENTER_REJECT:
		sprintf(valueLabel, buf + 1);
		deviceStatus.curMenu = &menu_topup_display_balance;
		initMenu(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + DELAY_SHOW_BALANCE;
		break;

	case COMCOM_MENU_TOUCH_POSITION:
		// // Find position touched button, make other buttons disabled
		if (sscanf(buf + 1, "X%dY%d", &x, &y) != 2) {
			break;
		}

		for (i = 0;
				deviceStatus.curMenu->buttons[i].command != COMMAND_ENDOFLIST;
				i++) {
			if (deviceStatus.curMenu->buttons[i].command == COMMAND_VOID) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].x > x
					|| deviceStatus.curMenu->buttons[i].y > y) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].x
					+ deviceStatus.curMenu->buttons[i].xsize < x
					|| deviceStatus.curMenu->buttons[i].y
							+ deviceStatus.curMenu->buttons[i].ysize < y) {
				continue;
			}

			apiDnfccMenuRedrawChangedButtons(deviceStatus.curMenu);
//TONY
			soundBuf[0] = COMCOM_SOUND_KEYCLICK;
			queueSendToSound(soundBuf, 100);

			switch (deviceStatus.curMenu->buttons[i].command) {

			case COMMAND_MENU_LOGOUT:
				// log end shift at datadistributor
				sprintf(dBuf, "%c%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				COMCOM_DATADISTRIBUTOR_END_SHIFT, deviceStatus.loginId[0],
						deviceStatus.loginId[1], deviceStatus.loginId[2],
						deviceStatus.loginId[3], deviceStatus.loginId[4],
						deviceStatus.loginId[5], deviceStatus.loginId[6],
						deviceStatus.loginId[7], deviceStatus.loginId[8],
						deviceStatus.loginId[9]);
				apiDnfccClockFetchDateTime(dBuf + 21); // add time of this event
				queueSendToDataDistributor(dBuf, portMAX_DELAY);

				// logout and return to start screen
				deviceStatus.isLoggedIn = 0;

				deviceStatus.curMenu = &menu_loginscreen;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_TOPUP1:
				deviceStatus.topupValue = apiDnfccGetTicketFare(0);
				sprintf(topupValueLabel, "%s: %ld", LOCALE_TOPUP_WITH,
						deviceStatus.topupValue);
				deviceStatus.curMenu = &menu_topup_confirm;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_TOPUP2:
				deviceStatus.topupValue =  apiDnfccGetTicketFare(1);
				sprintf(topupValueLabel, "%s: %ld", LOCALE_TOPUP_WITH,
						deviceStatus.topupValue);
				deviceStatus.curMenu = &menu_topup_confirm;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_TOPUP3:
				deviceStatus.topupValue =  apiDnfccGetTicketFare(2);
				sprintf(topupValueLabel, "%s: %ld", LOCALE_TOPUP_WITH,
						deviceStatus.topupValue);
				deviceStatus.curMenu = &menu_topup_confirm;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_TOPUP4:
				deviceStatus.topupValue =  apiDnfccGetTicketFare(3);
				sprintf(topupValueLabel, "%s: %ld", LOCALE_TOPUP_WITH,
						deviceStatus.topupValue);
				deviceStatus.curMenu = &menu_topup_confirm;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_TOPUP_CANCEL:
				deviceStatus.topupValue = 0;
				lastHoverButton = -1;
				deviceStatus.curMenu = &menu_topup_start;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_TOPUP_SELECT:
				deviceStatus.curMenu = &menu_topup_valueselect;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_TOPUP_CONFIRM:
				deviceStatus.curMenu = &menu_topup_tapto_top;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;
			default:
				break;
			}
			break;
		}
		if (deviceStatus.curMenu->buttons[i].command == COMMAND_ENDOFLIST) {
			// user touch released outside menu
			if (lastHoverButton != -1) {
				apiDnfccMenuDrawButton(
						&deviceStatus.curMenu->buttons[lastHoverButton],
						STANDBY);
			}
		}

		break;
	case COMCOM_SELFTEST:
		// Handeled in menu.c
		break;
	} // Switch[receive buffer]
}
/** @} */
#endif
