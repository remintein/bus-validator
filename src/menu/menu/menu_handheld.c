/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../../project_includes.h"
#include "../menu.h"
#include "../menu_definition.h"
#include "../draw.h"
#include "../../sdio/DeviceDataFiles.h"
#include <string.h>

#if CURRENT_TARGET == CNS_HANDHELD

/**
 * @file
 * @brief Contains the functions for the menu
 *
 */
/**
 * @addtogroup menu
 *
 * @{
 */

/** @brief Labels for display */
char valueLabel[50];
char payInfo[70], payinfoSubtitle[40], updateinfo[70];
char handheldInfo[50];
char handheldPenaltyCash[50];
char handheldPenaltyCard[50];
char validatorButtonLabel[30], ticketvalue[50];

TickType_t timeoutForClean = 0;
TickType_t timeoutForClean2 = 0;

/**
 * @brief Initialize labels
 */
void apiDnfccMenuInitializeLabels() {
	valueLabel[0] = '\0';

	deviceStatus.deviceRouteId = 0;
	deviceStatus.deviceServiceId = 1;
	apiDnfccSetRouteAndFare(deviceStatus.deviceRouteId,
			deviceStatus.deviceServiceId);

	sprintf(handheldPenaltyCash, "%s %lu VND", LOCALE_PAY_BY_CASH,
			apiDnfccGetTicketFare(0));
	sprintf(handheldPenaltyCard, "%s %lu VND", LOCALE_PAY_BY_CARD,
			apiDnfccGetTicketFare(1));
	strcpy(payinfoSubtitle, LOCALE_MENU_SELL_TICKET);
	strcpy(handheldInfo, LOCALE_HANDHELD_INFO);
	strcpy(validatorButtonLabel, LOCALE_TAP_TO_GO);
	sprintf(updateinfo, LOCALE_FIRMWARE_VERSION_PRINT,
			deviceStatus.firmwareVersion);
}

//TONY
static void displayHandheldInfo(char *val) {
	char date[25];
	char ticketval[10];
	sscanf(val, "%[^\n]\n%s", date, ticketval);
	sprintf(handheldInfo, "%s:\n%s\n%s:%s", LOCALE_HANDHELD_READ, date,
	LOCALE_BALANCE, ticketval);
	if (deviceStatus.curMenu == &menu_handheldPass) {
		apiDnfccMenuRedrawWindowButtons(&menu_handheldPass);
	} else if (deviceStatus.curMenu == &menu_handheldFail) {
		apiDnfccMenuRedrawWindowButtons(&menu_handheldFail);
	}
}

/**
 * @brief Verify timeout and perform the correct actions
 */
void apiDnfccMenuVerifyTimeout() {
	if (timeoutForClean != 0) {
		if (xTaskGetTickCount() > timeoutForClean) {
			if (deviceStatus.curMenu == &menu_handheldPass) {
				deviceStatus.curMenu = &menu_handheld;
				initMenu(deviceStatus.curMenu);
			}
			timeoutForClean = 0;
		}
	}

	if (timeoutForClean2 != 0) {
		if (xTaskGetTickCount() > timeoutForClean2) {

			timeoutForClean2 = 0;
		}
	}
}

/**
 * @brief Process one message from the queue
 */
void apiDnfccMenuProcessMessage(char *buf) {
	char soundBuf[SOUND_BUFFER_SIZE + 1];

	int lastHoverButton = -1; // at each switch of menu remind to reset this to -1 !!
	int i;
	int x, y;

	char dBuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];

	switch (buf[0]) {
	case COMCOM_MENU_TOUCH_CURRENT_POSITION: // Find position hovered button
		if (sscanf(buf + 1, "X%dY%d", &x, &y) != 2) {
			break; // command misformed
		}
		for (i = 0;
				deviceStatus.curMenu->buttons[i].command != COMMAND_ENDOFLIST;
				i++) {
			if (deviceStatus.curMenu->buttons[i].x > x
					|| deviceStatus.curMenu->buttons[i].y > y) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].x
					+ deviceStatus.curMenu->buttons[i].xsize < x
					|| deviceStatus.curMenu->buttons[i].y
							+ deviceStatus.curMenu->buttons[i].ysize < y) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].command != COMMAND_ENDOFLIST) {
				// got the button hovered. If it differs from lastHovered redraw
				if (lastHoverButton != i) {
					if (lastHoverButton != -1) {
						apiDnfccMenuDrawButton(
								&(deviceStatus.curMenu->buttons[lastHoverButton]),
								STANDBY);
					}
					apiDnfccMenuDrawButton(&(deviceStatus.curMenu->buttons[i]),
							HOVER);
					lastHoverButton = i;
				}
			}
		}
		break;

	case COMCOM_MENU_STARTSCREEN:
		deviceStatus.isLoggedIn = 1;
		// handheld logs in, switch to menu
		deviceStatus.curMenu = &menu_handheld;
		lastHoverButton = -1;
		initMenu(deviceStatus.curMenu);
		break;

	case COMCOM_MENU_HANDHELD_LOGDATA:
		displayHandheldInfo(buf + 1);
		break;

	case COMCOM_MENU_HANDHELD_CARD_FAIL:
		deviceStatus.curMenu = &menu_handheldFail;
		initMenu(deviceStatus.curMenu);
		break;

	case COMCOM_MENU_QRCODE_REJECT:
		sprintf(handheldInfo, "\n\n%s", LOCALE_QRCODE_FAIL);
		deviceStatus.curMenu = &menu_handheldFail;
		initMenu(deviceStatus.curMenu);
		break;

	case COMCOM_MENU_QRCODE_SUCCESSFULL:
		sprintf(handheldInfo, "\n\n%s", LOCALE_QRCODE_PASS);
		deviceStatus.curMenu = &menu_handheldPass;
		initMenu(deviceStatus.curMenu);
		break;

	case COMCOM_MENU_HANDHELD_CARD_PASS:
		deviceStatus.curMenu = &menu_handheldPass;
		initMenu(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 7000;
		break;

	case COMCOM_MENU_CARD_ENTER_REJECT:
		sprintf(handheldInfo, buf + 1);
		deviceStatus.curMenu = &menu_handheldFail;
		initMenu(deviceStatus.curMenu);
		break;

	case COMCOM_MENU_TOUCH_POSITION:
		// // Find position touched button, make other buttons disabled
		if (sscanf(buf + 1, "X%dY%d", &x, &y) != 2) {
			break;
		}
		for (i = 0;
				deviceStatus.curMenu->buttons[i].command != COMMAND_ENDOFLIST;
				i++) {
			if (deviceStatus.curMenu->buttons[i].command == COMMAND_VOID) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].x > x
					|| deviceStatus.curMenu->buttons[i].y > y) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].x
					+ deviceStatus.curMenu->buttons[i].xsize < x
					|| deviceStatus.curMenu->buttons[i].y
							+ deviceStatus.curMenu->buttons[i].ysize < y) {
				continue;
			}

			apiDnfccMenuRedrawChangedButtons(deviceStatus.curMenu);
			soundBuf[0] = COMCOM_SOUND_KEYCLICK;
			queueSendToSound(soundBuf, 100);
			switch (deviceStatus.curMenu->buttons[i].command) {

			case COMMAND_MENU_LOGOUT:
				// log end shift at datadistributor
				sprintf(dBuf, "%c%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				COMCOM_DATADISTRIBUTOR_END_SHIFT, deviceStatus.loginId[0],
						deviceStatus.loginId[1], deviceStatus.loginId[2],
						deviceStatus.loginId[3], deviceStatus.loginId[4],
						deviceStatus.loginId[5], deviceStatus.loginId[6],
						deviceStatus.loginId[7], deviceStatus.loginId[8],
						deviceStatus.loginId[9]);
				apiDnfccClockFetchDateTime(dBuf + 21); // add time of this event
				queueSendToDataDistributor(dBuf, portMAX_DELAY);

				// logout and return to start screen
				deviceStatus.isLoggedIn = 0;
				deviceStatus.deviceRouteId = 0;
				deviceStatus.deviceServiceId = 0;
				deviceStatus.curMenu = &menu_loginscreen;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_HANDHELD_PENALTY:
				deviceStatus.curMenu = &menu_handheld_selectpenaltypay;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_HANDHELD_CANCEL:
				deviceStatus.penaltyCard = 0;
				deviceStatus.penaltyCash = 0;
				deviceStatus.curMenu = &menu_handheld;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_HANDHELD_CARD:
				deviceStatus.penaltyCard = 50000;
				deviceStatus.curMenu = &menu_handheld_card_payment;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_HANDHELD_CASH:
				deviceStatus.penaltyCash = 50000;
				deviceStatus.curMenu = &menu_handheld_cash_payment;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_HANDHELD_PRINT_BILL:
				buf[0] = COMCOM_PRINT_TICKET;
				queueSendToPrinter(buf, 5000);
				initMenu(deviceStatus.curMenu);
				break;

			default:
				break;
			}
			break;
		}
		if (deviceStatus.curMenu->buttons[i].command == COMMAND_ENDOFLIST) {
			// user touch released outside menu
			if (lastHoverButton != -1) {
				apiDnfccMenuDrawButton(
						&deviceStatus.curMenu->buttons[lastHoverButton],
						STANDBY);
			}
		}

		break;
	case COMCOM_SELFTEST:
		// Handeled in menu.c
		break;
	} // Endless for loop
}

/** @} */
#endif
