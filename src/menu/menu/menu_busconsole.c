/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../../project_includes.h"
#include "../menu.h"
#include "../menu_definition.h"
#include "../draw.h"
#include "../../sdio/DeviceDataFiles.h"
#include <string.h>

#if CURRENT_TARGET == CNS_BUSCONSOLE

#define MAXROUTE  10
#define MAXSERVICE 5


/**
 * @file
 * @brief Contains the functions for the menu
 *
 */

/**
 * @addtogroup menu
 *
 * @{
 */

/** @brief Labels for display */
char valueLabel[50];
char payInfo[70], payinfoSubtitle[40];
char validatorInfo[70], validatorInfoSubtitle[40], updateinfo[70];
char busline[50], serviceline[50], validatorButtonLabel[30], ticketvalue[50];

#if DNFCC_ADD_GSM_DISABLE == 1
char gsmStatusTitle[20];
#endif
TickType_t timeoutForClean = 0;
TickType_t timeoutForClean2 = 0;

/**
 * @brief Initialize labels
 */
void apiDnfccMenuInitializeLabels() {
	valueLabel[0] = '\0';
	strcpy(payinfoSubtitle, LOCALE_MENU_SELL_TICKET);
	strcpy(validatorInfoSubtitle, LOCALE_ATTENDANT);
	strcpy(validatorButtonLabel, LOCALE_TAP_TO_GO);
	sprintf(updateinfo, LOCALE_FIRMWARE_VERSION_PRINT,
			deviceStatus.firmwareVersion);
#if DNFCC_ADD_GSM_DISABLE == 1
	sprintf(gsmStatusTitle, "%s: %d",LOCALE_GSM, deviceStatus.gsmEnabled);
#endif
}

/**
 * @brief Set the entered route
 *
 * @param route the route to set
 */
static void setRoute(char* route) {
	sscanf(route, "%ld", &deviceStatus.deviceRouteId); // store in device
	sprintf(busline, LOCALE_BUS_LINE_PRINT, deviceStatus.deviceRouteId);
	apiDnfccMenuRedrawWindowButtons(deviceStatus.curMenu);
}

/**
 * @brief set the entered service
 *
 * @param service the service to set
 */
static void setService(char* service) {
	sscanf(service, "%ld", &deviceStatus.deviceServiceId); // store in device
	sprintf(serviceline, LOCALE_SERVICE_LINE_PRINT,
			deviceStatus.deviceServiceId);
	apiDnfccMenuRedrawWindowButtons(deviceStatus.curMenu);
}

/**
 * @brief Verify timeout and perform the correct actions
 */
void apiDnfccMenuVerifyTimeout() {
	// All timeout / clear values
	if (timeoutForClean != 0) {
		if (xTaskGetTickCount() > timeoutForClean) {
			if (deviceStatus.curMenu == &menu_driveoperations) {
				menu_driveoperations.nextButtonState[0] = ACTIVE;
				strcpy(payinfoSubtitle, LOCALE_MENU_SELL_TICKET);
//						menu_driveoperations.buttons[0].type = BUTTON_LED;
				apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
			}
			timeoutForClean = 0;
		}
	}

	if (timeoutForClean2 != 0) {
		if (xTaskGetTickCount() > timeoutForClean2) {
			if (deviceStatus.curMenu == &menu_driveoperations) {
				validatorInfo[0] = '\0';
				strcpy(validatorInfoSubtitle, LOCALE_ATTENDANT);
				menu_driveoperations.nextButtonState[1] = STANDBY;
				apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
			}
			timeoutForClean2 = 0;
		}
	}
}

/**
 * @brief holder for the current route
 */
static char route[] = "\0\0\0\0\0\0\0\0\0\0\0";
/**
 * @brief holder for the current service
 */
static char service[] = "\0\0\0\0\0";
/** @brief Pointer for which character in route and service is currently being entered */
static int r = 0, s = 0;

/**
 * @brief Process one message from the queue
 */
void apiDnfccMenuProcessMessage(char *buf) {
	char soundBuf[SOUND_BUFFER_SIZE + 1];
	char cbuf[CANBUS_BUFFER_SIZE + 1];

	int lastHoverButton = -1; // at each switch of menu remind to reset this to -1 !!
	int i;
	int x, y;

	char dBuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];

	switch (buf[0]) {

	case COMCOM_MENU_STARTSCREEN:
		// driver logs in, switch to main
		deviceStatus.isLoggedIn = 1;
		deviceStatus.curMenu = &menu_busconsole_main;
		initMenu(deviceStatus.curMenu);
		break;

	case COMCOM_MENU_CARD_ENTER_SUCCESSFULL:
	case COMCOM_MENU_QRCODE_SUCCESSFULL:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		menu_driveoperations.nextButtonState[0] = BUTTON_GREEN;
		menu_driveoperations.buttons[0].type = BUTTON_LED;
		payInfo[0] = '\0';
		strcpy(payinfoSubtitle, buf + 1);
		strcpy(validatorButtonLabel, LOCALE_WELCOME);
		strcpy(ticketvalue, buf + 1);
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 1000;
		break;

	case COMCOM_MENU_CARD_ENTER_SUCCESSFULL_WARNING:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		menu_driveoperations.nextButtonState[0] = BUTTON_YELLOW;
		menu_driveoperations.buttons[0].type = BUTTON_LED;
		payInfo[0] = '\0';
		strcpy(payinfoSubtitle, buf + 1);
		strcpy(validatorButtonLabel, LOCALE_WELCOME);
		strcpy(ticketvalue, buf + 1);
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 1000;
		break;

	case COMCOM_MENU_CARD_ENTER_REJECT:
	case COMCOM_MENU_QRCODE_REJECT:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		menu_driveoperations.nextButtonState[0] = BUTTON_RED;
		menu_driveoperations.buttons[0].type = BUTTON_LED;
		payInfo[0] = '\0';
		strcpy(payinfoSubtitle, buf + 1);

		uint32_t tmp = 0;
		sscanf(buf + 1, "%ld,%[^\n]", &tmp, validatorButtonLabel);
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 1000;
		break;

	case COMCOM_MENU_CARD_ENTER_RETRY:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		menu_driveoperations.nextButtonState[0] = BUTTON_RED;
		menu_driveoperations.buttons[0].type = BUTTON_LED;
		payInfo[0] = '\0';
		strcpy(payinfoSubtitle, LOCALE_RETRY);

		strcpy(validatorButtonLabel, LOCALE_TRY_AGAIN);
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 1000;
		break;

	case COMCOM_MENU_DRIVERCARD_ENTER_SUCCESSFULL:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		menu_driveoperations.nextButtonState[0] = BUTTON_GREEN;
		menu_driveoperations.buttons[0].type = BUTTON_LEDCIRCLE;
		// Saldo oid.
		sprintf(payInfo, "%ld", deviceStatus.purseTransactionCount);
		strcpy(payinfoSubtitle, buf + 1);
		strcpy(validatorButtonLabel, LOCALE_WELCOME);
		strcpy(ticketvalue, buf + 1);
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 1000;
		break;

	case COMCOM_MENU_DRIVERCARD_ENTER_SUCCESSFULL_WARNING:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		menu_driveoperations.nextButtonState[0] = BUTTON_YELLOW;
		menu_driveoperations.buttons[0].type = BUTTON_LEDCIRCLE;
		// Saldo oid.
		strcpy(validatorButtonLabel, LOCALE_WELCOME);
		strcpy(ticketvalue, buf + 1);
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean = xTaskGetTickCount() + 1000;
		break;

// From busconsole, validator did send this (can)
	case COMCOM_MENU_VALIDATOR_ENTER_SUCCESSFULL:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		strcpy(validatorInfoSubtitle, buf + 1);
		menu_driveoperations.nextButtonState[1] = BUTTON_GREEN;
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean2 = xTaskGetTickCount() + 1000;
		break;

	case COMCOM_MENU_VALIDATOR_ENTER_SUCCESSFULL_WARNING:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		strcpy(validatorInfoSubtitle, buf + 1);
		menu_driveoperations.nextButtonState[1] = BUTTON_YELLOW;
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean2 = xTaskGetTickCount() + 1000;
		break;

	case COMCOM_MENU_VALIDATOR_ENTER_RETRY:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		strcpy(validatorInfoSubtitle, LOCALE_RETRY);
		menu_driveoperations.nextButtonState[1] = BUTTON_RED;
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean2 = xTaskGetTickCount() + 1000;
		break;

	case COMCOM_MENU_VALIDATOR_ENTER_REJECT:
		if (deviceStatus.curMenu != &menu_driveoperations) {
			break;
		}
		strcpy(validatorInfoSubtitle, LOCALE_REJECT);
		menu_driveoperations.nextButtonState[1] = BUTTON_RED;
		apiDnfccMenuRedrawWindowButtonsAndLeds(deviceStatus.curMenu);
		timeoutForClean2 = xTaskGetTickCount() + 1000;
		break;

	case COMCOM_MENU_TOUCH_POSITION:
		// // Find position touched button, make other buttons disabled
		if (sscanf(buf + 1, "X%dY%d", &x, &y) != 2) {
			break;
		}
		for (i = 0;
				deviceStatus.curMenu->buttons[i].command != COMMAND_ENDOFLIST;
				i++) {
			if (deviceStatus.curMenu->buttons[i].command == COMMAND_VOID) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].x > x
					|| deviceStatus.curMenu->buttons[i].y > y) {
				continue;
			}
			if (deviceStatus.curMenu->buttons[i].x
					+ deviceStatus.curMenu->buttons[i].xsize < x
					|| deviceStatus.curMenu->buttons[i].y
							+ deviceStatus.curMenu->buttons[i].ysize < y) {
				continue;
			}

			apiDnfccMenuRedrawChangedButtons(deviceStatus.curMenu);

			soundBuf[0] = COMCOM_SOUND_KEYCLICK;
			queueSendToSound(soundBuf, 100);
			switch (deviceStatus.curMenu->buttons[i].command) {

			case COMMAND_MENU_ROUTESELECT:
				deviceStatus.curMenu = &menu_routeselect;
				lastHoverButton = -1;
				// init values or should we keep those and set at enter?
				deviceStatus.deviceRouteId = 0;
				deviceStatus.deviceServiceId = 0;
				deviceStatus.deviceRouteId = 0;
				strncpy(route, "\0\0\0\0\0\0\0\0\0\0\0", MAXROUTE);
				strncpy(service, "\0\0\0\0\0", MAXSERVICE);
				r = 0, s = 0;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_MAIN:
				deviceStatus.curMenu = &menu_busconsole_main;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_STARTTRIP:
				if (deviceStatus.deviceRouteId != 0
						&& deviceStatus.deviceServiceId != 0) {
					deviceStatus.curMenu = &menu_driveoperations;
					lastHoverButton = -1;
					initMenu(deviceStatus.curMenu);
					// signal Validator via CAN bus to wakeup
					char cbuf[CANBUS_BUFFER_SIZE + 1];
					cbuf[0] = COMCOM_CANBUS_ENABLE_VALIDATORS;
					queueSendToCanbus(cbuf, portMAX_DELAY);
					// log start trip at datadistributor
					sprintf(dBuf,
							"%c%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%010ld%05ld",
							COMCOM_DATADISTRIBUTOR_START_TRIP,
							deviceStatus.loginId[0], deviceStatus.loginId[1],
							deviceStatus.loginId[2], deviceStatus.loginId[3],
							deviceStatus.loginId[4], deviceStatus.loginId[5],
							deviceStatus.loginId[6], deviceStatus.loginId[7],
							deviceStatus.loginId[8], deviceStatus.loginId[9],
							deviceStatus.deviceRouteId,
							deviceStatus.deviceServiceId);
					apiDnfccClockFetchDateTime(dBuf + 36); // add time of this event
					queueSendToDataDistributor(dBuf, portMAX_DELAY);
					apiDnfccSetRouteAndFare(deviceStatus.deviceRouteId, deviceStatus.deviceServiceId);
				} else {
					// Human flow mistake: bus driver cannot start trip if route/service not set
					// make routeselect curMenu and display error
					deviceStatus.curMenu = &menu_routeselect;
					lastHoverButton = -1;
					// make route button active in main, rest disabled
					initMenu(deviceStatus.curMenu);
					deviceStatus.deviceRouteId = 0;
					strncpy(route, "\0\0\0\0\0\0\0\0\0\0\0", MAXROUTE);
					strncpy(service, "\0\0\0\0\0", MAXSERVICE);
					r = 0, s = 0;
				}
				break;

			case COMMAND_MENU_ENDTRIP:
				// signal validator to idle state
				sprintf(cbuf, "%c", COMCOM_CANBUS_DISABLE_VALIDATORS);
				queueSendToCanbus(cbuf, portMAX_DELAY);
				// log at datadistributor
				sprintf(dBuf,
						"%c%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%010ld%05ld",
						COMCOM_DATADISTRIBUTOR_END_TRIP,
						deviceStatus.loginId[0], deviceStatus.loginId[1],
						deviceStatus.loginId[2], deviceStatus.loginId[3],
						deviceStatus.loginId[4], deviceStatus.loginId[5],
						deviceStatus.loginId[6], deviceStatus.loginId[7],
						deviceStatus.loginId[8], deviceStatus.loginId[9],
						deviceStatus.deviceRouteId,
						deviceStatus.deviceServiceId);
				apiDnfccClockFetchDateTime(dBuf + 36);	// add time of this event check length of above
				queueSendToDataDistributor(dBuf, portMAX_DELAY);

				// and return to main menu
				deviceStatus.curMenu = &menu_busconsole_main;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;
#if DNFCC_ADD_GSM_DISABLE == 1
				case COMMAND_GSM_TOGGLE:
				deviceStatus.gsmEnabled = 1-deviceStatus.gsmEnabled;
				sprintf(gsmStatusTitle, "GSM: %d", deviceStatus.gsmEnabled);
				initMenu(deviceStatus.curMenu);
				break;
#endif
			case COMMAND_MENU_LOGOUT:
				// log end shift at datadistributor
				sprintf(cbuf, "%c", COMCOM_CANBUS_DISABLE_VALIDATORS);
				queueSendToCanbus(cbuf, portMAX_DELAY);
				sprintf(dBuf, "%c%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
				COMCOM_DATADISTRIBUTOR_END_SHIFT, deviceStatus.loginId[0],
						deviceStatus.loginId[1], deviceStatus.loginId[2],
						deviceStatus.loginId[3], deviceStatus.loginId[4],
						deviceStatus.loginId[5], deviceStatus.loginId[6],
						deviceStatus.loginId[7], deviceStatus.loginId[8],
						deviceStatus.loginId[9]);
				apiDnfccClockFetchDateTime(dBuf + 21);		// add time of this event
				queueSendToDataDistributor(dBuf, portMAX_DELAY);

				// logout and return to start screen
				deviceStatus.isLoggedIn = 0;
				deviceStatus.deviceRouteId = 0;
				deviceStatus.deviceServiceId = 0;
				deviceStatus.curMenu = &menu_loginscreen;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_0:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '0';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '0';
					}
				break;

			case COMMAND_1:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '1';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '1';
					}
				break;

			case COMMAND_2:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '2';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '2';
					}
				break;

			case COMMAND_3:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '3';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '3';
					}
				break;

			case COMMAND_4:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '4';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '4';
					}
				break;

			case COMMAND_5:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '5';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '5';
					}
				break;

			case COMMAND_6:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '6';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '6';
					}
				break;

			case COMMAND_7:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '7';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '7';
					}
				break;

			case COMMAND_8:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '8';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '8';
					}
				break;

			case COMMAND_9:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r < MAXROUTE) {
						route[r++] = '9';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s < MAXSERVICE) {
						service[s++] = '9';
					}
				break;

			case COMMAND_BACK:
				if (deviceStatus.curMenu == &menu_routeselect)
					if (r >= 0) {
						route[--r] = '\0';
					}
				if (deviceStatus.curMenu == &menu_serviceselect)
					if (s >= 0) {
						service[--s] = '\0';
					}
				break;

			case COMMAND_MENU_ROUTE_ENTER:
				sprintf(valueLabel, "%s", route);
				setRoute(valueLabel);
				// route entered continue to service select for service code
				deviceStatus.curMenu = &menu_serviceselect;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_MENU_SERVICE_ENTER:
				sprintf(valueLabel, "%s", service);
				setService(valueLabel);
				// service entered, ready return to main
				deviceStatus.curMenu = &menu_busconsole_main;
				lastHoverButton = -1;
				initMenu(deviceStatus.curMenu);
				break;

			case COMMAND_TICKET_1:
				apiDnfccSetRouteType('L');
				break;

			case COMMAND_TICKET_2:
				apiDnfccSetRouteType('S');
				break;

			default: break;
			}
			break;
		}
		if (deviceStatus.curMenu->buttons[i].command == COMMAND_ENDOFLIST) {
			// user touch released outside menu
			if (lastHoverButton != -1) {
				apiDnfccMenuDrawButton(&deviceStatus.curMenu->buttons[lastHoverButton],
						STANDBY);
			}
		}
		if (deviceStatus.curMenu == &menu_routeselect) {
			sprintf(valueLabel, "\nRoute: %s", route);
			apiDnfccMenuRedrawWindowButtons(deviceStatus.curMenu);
		}
		if (deviceStatus.curMenu == &menu_serviceselect) {
			sprintf(valueLabel, "\nService: %s", service);
			apiDnfccMenuRedrawWindowButtons(deviceStatus.curMenu);
		}
		break;
	case COMCOM_SELFTEST:
		// Handeled in menu.c
		break;
	}
}
/** @} */
#endif
