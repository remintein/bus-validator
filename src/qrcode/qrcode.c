/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#define QRCODE_COMMUNICATION
#include "qrcode.h"
#include "../_library/serialEngine/serialEngine.h"
#include "../_definition/deviceStatus.h"
#include <string.h>

/**
 * @file
 * @brief Contains the functions for the qrcode module
 */

/**
 * @addtogroup qrcode
 *
 * @{
 */

extern UART_HandleTypeDef huart_qrcodeprinter;

#define STACK_SIZE 200
#define QRCODE_MAXLENGTH 30

/**
 * @brief Initialize qrcode
 *
 * @param uxPriority - the prio to run on
 */
BaseType_t nfccQrcodeInit(UBaseType_t uxPriority) {
	deviceStatus.qrcodeRxQueue = xQueueCreate(QRCODE_QUEUE_LENGTH, QRCODE_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.qrcodeRxQueue, "QrCode");
	setWaitForSystemReadyThisTask(TASK_BIT_QRCODE);
	return xTaskCreate(nfccQrcode, "Qrcode", STACK_SIZE, NULL, uxPriority, &deviceStatus.qrcodeTask);
}
uint8_t qrcodeReceiveBuffer[350];

/**
 * @brief QRCode main loop
 *
 * The main loop for the Qrcode
 */
void nfccQrcode(void *pvParameters) {
	char buf[QRCODE_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;

	SerialEngineStruct *serialEngine = apiDnfccSerialEngineStartListening(&huart_qrcodeprinter, QRCODEPRINTER_UART_NR,
			qrcodeReceiveBuffer, 350, 0, 0);

	char stringToFind[MENU_BUFFER_SIZE];
	char qrbuffer[QRCODE_MAXLENGTH + 1];
	int qrbufferPointer;

	setTaskIsSystemReady(TASK_BIT_QRCODE);
	waitForSystemReady() ;

	for (;;) {
		buf[0] = '\0';
		if (xQueueReceive(deviceStatus.qrcodeRxQueue, buf,
				250) != pdPASS) {

			qrbufferPointer = 0;
			// The following commands are accepted:
			// First character of the command
			while (serialEngine->receivePointer != serialEngine->receiveProcessedTill) {
				// Probably check if end of block is received, then interpret the outcome
				if (qrbufferPointer >= QRCODE_MAXLENGTH) {
					qrbufferPointer = 0;
					qrbuffer[qrbufferPointer] = '\0';
				}

				if (serialEngine->receiveBuffer[(serialEngine->receiveProcessedTill + qrbufferPointer)
						% serialEngine->receiveBufferLength] != '\r') {
					qrbuffer[qrbufferPointer] = serialEngine->receiveBuffer[(serialEngine->receiveProcessedTill
							+ qrbufferPointer) % serialEngine->receiveBufferLength];
					qrbufferPointer++;
					qrbuffer[qrbufferPointer] = '\0';
					continue;
				}
				serialEngine->receiveProcessedTill = (serialEngine->receiveProcessedTill + qrbufferPointer + 1)
						% serialEngine->receiveBufferLength;

				// Here a string is received

				sprintf(stringToFind, "CNS:");
				apiDnfccClockFetchDate(&(stringToFind[strlen(stringToFind)]));

				if (strstr(qrbuffer, stringToFind) != NULL) {
					sprintf(stringToFind, "%c%s", COMCOM_MENU_QRCODE_SUCCESSFULL,LOCALE_QRCODE_PASS);
					queueSendToMenu(stringToFind, 1000);
#if CURRENT_TARGET == CNS_VALIDATOR
					buf[0] = COMCOM_CANBUS_VALID_IN;
					queueSendToCanbus(buf, portMAX_DELAY);
#endif
					apiDnfccDisplayLedControlGreen(1);
				} else {
					sprintf(stringToFind, "%c%s", COMCOM_MENU_QRCODE_REJECT,LOCALE_QRCODE_FAIL);
					queueSendToMenu(stringToFind, 1000);
					apiDnfccDisplayLedControlGreen(0);
#if CURRENT_TARGET == CNS_VALIDATOR
					buf[0] = COMCOM_CANBUS_REJECT_IN;
					queueSendToCanbus(buf, portMAX_DELAY);
#endif
				}
				serialEngine->receiveProcessedTill = serialEngine->receivePointer;
			}
			continue;
		}

		switch (buf[0]) {

		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(deviceStatus.qrcodeTask);
			sprintf(stbuf, "!EWM%ld", uxHighWaterMark);
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_QRCODE;
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;
		}
	}
}

/** @} */
