/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"
#include "../gsmgps/gsm.h"

#include "../gsmgps/engine.h"

#include <string.h>

#define STACK_SIZE 1000

/**
 * @file
 * @brief Contains the functions for the GSM/GPS module
 *
 */

/**
 * @addtogroup gpsgsm
 *
 * @{
 */

void apiDnfccGsmEngineInit();
void apiDnfccGsmEngineAdvance();

/**
 * @brief initialization routine for GSM/GPS module
 *
 * @param uxPriority - The FreeRTOS priority
 */
BaseType_t nfccGsmGpsInit(UBaseType_t uxPriority) {
	deviceStatus.gsmRxQueue = xQueueCreate(GSM_QUEUE_LENGTH, GSM_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.gsmRxQueue, "Gsm");
	setWaitForSystemReadyThisTask(TASK_BIT_GSM);

	return xTaskCreate(nfccGsmGps, "GsmGps", STACK_SIZE, NULL, uxPriority,
			&deviceStatus.gsmTask);
}

/**
 * @brief the main group for GPS/GSM
 *
 * @param pvParameters - the parameters from FreeRTOS
 */
void nfccGsmGps(void *pvParameters) {
	char buf[GSM_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;
	UBaseType_t speed;

	apiDnfccGsmEngineInit();

	setTaskIsSystemReady(TASK_BIT_GSM);
	waitForSystemReady();

// Device is not reset
	for (;;) {
#if DNFCC_ADD_GSM_DISABLE == 1
		if (deviceStatus.gsmEnabled) {
			apiDnfccGsmEngineAdvance();
		}
#else
		apiDnfccGsmEngineAdvance();
#endif
		// Can be dynamic based on whether we are sending... (wait time max)

		if (apiDnfccGetPriority() == PRIORITY_IDLE
				|| apiDnfccGetPriority() == PRIORITY_GET_DATA_FROM_BACKEND) {
			speed = 1;
		} else {
			speed = 10;
		}

		if (xQueueReceive(deviceStatus.gsmRxQueue, buf, speed) == pdPASS) {
			switch (buf[0]) {
			case COMCOM_SELFTEST:
				uxHighWaterMark = uxTaskGetStackHighWaterMark(
						deviceStatus.gsmTask);
				sprintf(stbuf, "!WWM%ld", uxHighWaterMark);
				stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
				stbuf[1] = COMSELFTEST_GSM;
				queueSendToDataDistributor(stbuf, portMAX_DELAY);
				break;
			}
		}
	}
}
/** @} */
