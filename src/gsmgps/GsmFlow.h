/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef GSMGPS_GSMFLOW_H_
#define GSMGPS_GSMFLOW_H_

#include "../_library/serialCommunication/SerialCommunicator.h"
#include "../_configuration/backendCommunication.h"
#include "../gsmgps/engine.h"

// Step - Action - Xmit or Xrec - Next action
static SerialControllerFlow_type Gsm[] = {

		/*
		 *  Current step, What to do, Text parameter if needed, integer parameter if needed, What to do when "What to do" is successfull
		 */
// Reset procedure; execute, and wait for "ready", timeout goes back to reset
		{ GSM_EXECUTE_RESET, EXECUTE_CALLBACK_WITH_PARAMETER, "", GSM_RESET_CALLBACK, GSM_WAIT_FOR_5_SECONDS },

		{ GSM_WAIT_FOR_5_SECONDS, EXECUTE_CALLBACK_WITH_PARAMETER, "", GSM_WAIT_10_SECONDS, GSM_PERFORM_AT },

		{ GSM_PERFORM_AT, SEND_LITERAL_TEXT, "AT\015\012", 0, GSM_PERFORM_AT_RESP },

		{ GSM_PERFORM_AT_RESP, VERIFY_DATA, "OK\r\n", 0, GSM_GNSS_GET_POWERSTATUS },

		{ GSM_PERFORM_AT_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 10000, GSM_EXECUTE_RESET },

		{ GSM_GNSS_GET_POWERSTATUS, SEND_LITERAL_TEXT, "AT+QGNSSC?\015\012", 0, GSM_GNSS_GET_POWERSTATUS_RESP },

		{ GSM_GNSS_GET_POWERSTATUS_RESP, VERIFY_DATA, "+QGNSSC: 0", 0, GSM_GNSS_OFF_RESP },

		{ GSM_GNSS_GET_POWERSTATUS_RESP, VERIFY_DATA, "+QGNSSC: 1", 0, GSM_GNSS_ON_RESP },

		{ GSM_GNSS_GET_POWERSTATUS_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 10000, GSM_EXECUTE_RESET },

		{ GSM_GNSS_OFF_RESP, VERIFY_DATA, "OK\r\n", 0, GSM_GNSS_SWITCH_ON },

		{ GSM_GNSS_OFF_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 10000, GSM_WAIT_FOR_5_SECONDS },

		{ GSM_GNSS_SWITCH_ON, SEND_LITERAL_TEXT, "AT+QGNSSC=1\015\012", 0, 700 },

		{ GSM_GNSS_SWITCH_ON_RESP, VERIFY_DATA, "OK\r\n", 0, 700 },

		{ GSM_GNSS_ON_RESP, VERIFY_DATA, "OK\r\n", 0, 700 },

		{ 700, SEND_LITERAL_TEXT, "AT+QINDI=1\015\012", 0, 701 },

		{ 701, VERIFY_DATA, "OK\r\n", 0, GSM_GNSS_WAIT_FOR_WORK },

		{ GSM_GNSS_INQUIRE_POSITION, SEND_LITERAL_TEXT, "AT+QGNSSRD?\015\012", 0, GSM_GNSS_INQUIRE_POSITION_RESP },

		{ GSM_GNSS_INQUIRE_POSITION_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 5000, GSM_WAIT_FOR_5_SECONDS },

		{ GSM_GNSS_INQUIRE_POSITION_RESP, VERIFY_DATA, "OK\r\n", 0, GSM_GNSS_WAIT_FOR_WORK },

		{ GSM_GNSS_INQUIRE_POSITION_RESP, EXECUTE_CALLBACK_WITH_PARAMETER, "\r\n", GSM_GNSS_LINE_CALLBACK,
				GSM_GNSS_INQUIRE_POSITION_RESP },

		{ GSM_GNSS_WAIT_FOR_WORK, VALIDATE_CALLBACK, "", GSM_GNSS_CHECK_FOR_WORK, GSM_ATCREG },

		{ GSM_GNSS_WAIT_FOR_WORK, VERIFY_TIMEOUT_HAS_PASSED, "", 5000, GSM_GNSS_INQUIRE_POSITION },

		{ GSM_ATCREG, SEND_LITERAL_TEXT, "AT+CREG?\015\012", 0, GSM_ATCREG_RESP },

		{ GSM_ATCREG_RESP, VERIFY_TIMEOUT_HAS_PASSED, "", 5000, GSM_ATCREG },

		{ GSM_ATCREG_RESP, VERIFY_DATA, "+CREG: 0", 0, 7777 }, // 0 try to search for a new network

		{ GSM_ATCREG_RESP, VERIFY_DATA, "+CREG: 2", 0, 10000 }, // 2 = registred

		{ 7777, SEND_LITERAL_TEXT, "AT+CREG=2\015\012", 0, 7778 },

		{ 7778, VERIFY_DATA, "OK\r\n", 0, 8889 },

		{ 8889, VERIFY_TIMEOUT_HAS_PASSED, "", 5000, GSM_ATCREG },

		{ 8889, VERIFY_DATA, "+CREG: 0", 0, 9999 },

		{ 10000, SEND_LITERAL_TEXT, "AT+QIOPEN=" STRING_CONNECT_MIDDLEWARE_SERVER, 0, 20000 },

		{ 20000, VERIFY_TIMEOUT_HAS_PASSED, "", 20000, GSM_WAIT_FOR_5_SECONDS },

		{ 20000, VERIFY_DATA, "CONNECT OK\r\n", 0, 20001 },

		{ 20000, VERIFY_DATA, "CONNECT FAIL\r\n", 0, GSM_GNSS_INQUIRE_POSITION},

		{ 20001, SEND_LITERAL_TEXT, "AT+QISEND\015\012", 0, 20002 },

		{ 20002, VERIFY_DATA, ">", 0, 20003 },

		{ 20003, EXECUTE_CALLBACK_WITH_PARAMETER, "", GSM_SEND_REQUEST, 20004 },

		{ 20004, SEND_LITERAL_TEXT, "\x1a", 0, 20005 },

		{ 20005, VERIFY_DATA, "+QIRDI: 0,1,0\r\n", 0, 20006 }, // TODO: Timeout here

		{ 20006, SEND_LITERAL_TEXT, "AT+QIRD=0,1,0,1400\r\n", 0, 20007 },

		{ 20007, VERIFY_DATA, "+QIRD:", 0, 20008 },

		{ 20008, VERIFY_DATA_WITH_CALLBACK, "\r\n", GSM_GET_RECEIVE_BYTES, 20009 },

		{ 20009, EXECUTE_CALLBACK_WITH_PARAMETER, "", GSM_RECEIVE_BY_LENGTH, 20010 },

		{ 20010, SEND_LITERAL_TEXT, "AT+QICLOSE\r\n", 0, 20011 },

		{ 20011, VERIFY_DATA, "CLOSE OK\r\n", 0, GSM_GNSS_WAIT_FOR_WORK },

		{ 30000, VERIFY_TIMEOUT_HAS_PASSED, "", 5000, 30000 }, };
#endif /* GSMGPS_GSMFLOW_H_ */
