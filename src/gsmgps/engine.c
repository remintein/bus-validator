/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"
#include "../gsmgps/engine.h"

#include "../_definition/deviceStatus.h"
#include "../_configuration/backendCommunication.h"
#include <string.h>
#include "../datadistributor/datadistributor.h"

#include "../gsmgps/gsm.h"
#include "../gsmgps/GsmFlow.h"


/**
 * @file
 * @brief Contains the functions for the GSM/GPS module
 *
 */

/**
 * @addtogroup gpsgsm
 *
 * @{
 */


UART_HandleTypeDef huart_gsm;
SerialCommunicatorHandler_type gsmEngine;

extern TransmitMessage TransmitGsmMessage;
static uint8_t gsmReceiveBuffer[1500];
static uint8_t gsmTransmitBuffer[1500];

static int curAccessPoint = -1;
static int curAccessPointStrength = -999;
static int bytesToProcess = 0;

/**
 * @brief callback routing for the GSM/GPS module
 *
 * @param data - data given from to the callback routine
 * @param intData - data given to the callback routine
 * @return if the call is finished (and the flow can continue) 1 = yes, 0 = not
 */
static uint8_t intDnfccGsmCallbackFunction(char * data, int32_t intData) {
	char buf[1000];
	int receiveHere;
	int myStartPointer, myEndPointer;

	int i;
	switch ((GsmCallbackKind) intData) {

	case GSM_EXECUTE_RESET:
		curAccessPoint = -1;
		curAccessPointStrength = -999;
		// Reset command; toggle the RST pin low
		HAL_GPIO_WritePin(GSM_GPIO_PWRKEY, GSM_PIN_PWRKEY, GPIO_PIN_SET);
		vTaskDelay(250);
		HAL_GPIO_WritePin(GSM_GPIO_PWRKEY, GSM_PIN_PWRKEY, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GSM_GPIO_GPSCTRL, GSM_PIN_GPSCTRL, GPIO_PIN_SET);

		vTaskDelay(1000);
		HAL_GPIO_WritePin(GSM_GPIO_PWRKEY, GSM_PIN_PWRKEY, GPIO_PIN_SET);
		vTaskDelay(1000);
		gsmEngine.engine->lastCommunication = xTaskGetTickCount();
		break;

	case GSM_GNSS_LINE_CALLBACK:
		myStartPointer = gsmEngine.engine->receiveProcessedTill;
// Skip \r and \n if they are at the beginning
		while (myStartPointer != gsmEngine.engine->receivePointer
				&& ((gsmEngine.engine->receiveBuffer[myStartPointer] == '\r')
						|| (gsmEngine.engine->receiveBuffer[myStartPointer] == '\n'))) {
			myStartPointer++;
		}
		myEndPointer = myStartPointer;

		while (myEndPointer != gsmEngine.engine->receivePointer) {
			myEndPointer = (myEndPointer + 1) % gsmEngine.engine->receiveBufferLength;
			if (gsmEngine.engine->receiveBuffer[myEndPointer] == '\r') {
				if (gsmEngine.engine->receiveBuffer[myStartPointer % gsmEngine.engine->receiveBufferLength] == 'O'
						&& gsmEngine.engine->receiveBuffer[(myStartPointer + 1) % gsmEngine.engine->receiveBufferLength]
								== 'K') {
					return 0;
				}

				// Process now from start till end
				gsmEngine.engine->receiveProcessedTill = myEndPointer;
				myStartPointer = myEndPointer;
				// Skip \r and \n if they are at the beginning
				while (myStartPointer != gsmEngine.engine->receivePointer
						&& ((gsmEngine.engine->receiveBuffer[myStartPointer] == '\r')
								|| (gsmEngine.engine->receiveBuffer[myStartPointer] == '\n'))) {
					myStartPointer++;
				}
				myEndPointer = myStartPointer;
			}

			// Now process up to next 'r or \n
			receiveHere = 0;
			buf[0] = '\0';
			int loop = 0;

			while (gsmEngine.engine->receiveBuffer[myEndPointer] != '\r'
					&& gsmEngine.engine->receiveBuffer[myEndPointer] != '\n') {
				if (gsmEngine.engine->receivePointer != myEndPointer) {
					buf[receiveHere++] = gsmEngine.engine->receiveBuffer[myEndPointer];
					buf[receiveHere] = '\0';
					myEndPointer++;
					if (receiveHere > 100) {
						break;
					}
				} else {
					if (loop++ > 2000) {
						return 1;
					}
					vTaskDelay(1); // Allow FreeRTOS to do something else
				}
			}
			if (strncmp(buf, "$GNGGA", 6) == 0) {
				strncpy(deviceStatus.location, buf + 6, 39);
				char  *lon, *lat, comma;
				// Get the LONG/LAT
				comma = 0;
				int len = strlen(buf);
				for (i = 0; i < len; i++) {
					if (buf[i] == ',') {
						comma++;
						if (comma == 2) {
							lat = buf + i + 1;
						} else if (comma == 4) {
							lon = buf + i + 1;
						}
						buf[i] = '\0';
					}
				}
				if (*lon != '\0') {
					strncpy(deviceStatus.longitude, lon, 20);
				} else {
					strcpy(deviceStatus.longitude, "No fix");
				}
				if (*lat != '\0') {
					strncpy(deviceStatus.lattitude, lat, 20);
				} else {
					strcpy(deviceStatus.lattitude, "No fix");
				}
			}
		}

		break;

	case GSM_GNSS_CHECK_FOR_WORK:
		switch (TransmitGsmMessage.transmitStatus) {

		case STATUS_IDLE:
			vTaskDelay(100);
			return 0;

		case STATUS_WAITING:
			TransmitGsmMessage.transmitStatus = STATUS_WORK;
			gsmEngine.jsonEndPoint = TransmitGsmMessage.endpoint;
			gsmEngine.jsonRequestData = TransmitGsmMessage.data;
			gsmEngine.jsonReceiveData = TransmitGsmMessage.response;
			return 1;

		case STATUS_WORK:
			return 1;

		case STATUS_FINISHED:
			vTaskDelay(100);
			return 0;
		}
		return 0;

	case GSM_GET_RECEIVE_BYTES:
		bytesToProcess = 0;
		memset(buf, '\0', DISPLAY_BUFFER_SIZE);
		// Need to verify +QRID: <ip>:<port>,TCP,<cnt>\r\n
		for (i = 0; i < 150; i++) {
			int pos = (gsmEngine.engine->receiveProcessedTill + i) % gsmEngine.engine->receiveBufferLength;
			if (pos == gsmEngine.engine->receivePointer || gsmEngine.engine->receiveBuffer[pos] == '\r') {
				break;
			}
			buf[i] = gsmEngine.engine->receiveBuffer[pos];
			buf[i + 1] = '\0';
		}
		char dummy[40];
		sscanf(buf, "%[^,],%[^,],%d", dummy, dummy, &bytesToProcess);
		break;

	case GSM_RECEIVE_BY_LENGTH:
		receiveHere = 0;
		buf[0] = '\0';
		while (bytesToProcess > 0) {
			if (gsmEngine.engine->receiveProcessedTill != gsmEngine.engine->receivePointer) {
				buf[receiveHere++] = gsmEngine.engine->receiveBuffer[gsmEngine.engine->receiveProcessedTill];
				if (receiveHere > sizeof(buf) - 2) {
					receiveHere--;
				}
				buf[receiveHere] = '\0';
				gsmEngine.engine->receiveProcessedTill = (gsmEngine.engine->receiveProcessedTill + 1)
						% gsmEngine.engine->receiveBufferLength;
				bytesToProcess--;
			} else {
				vTaskDelay(1); // Timeout handling here....
			}
		}
		// Get the {...] into the output
		for (int i = 0; i < strlen(buf); i++) {
			if (buf[i] == '{') {
				strcpy(gsmEngine.jsonReceiveData, buf + i);
				break;
			}
		}
		TransmitGsmMessage.transmitStatus = STATUS_FINISHED;
		sprintf(buf, "R%ld", TransmitGsmMessage.messageId);
		buf[0] = COMCOM_DATADISTRIBUTOR_MESSAGE_SEND;
		queueSendToDataDistributor(buf, portMAX_DELAY);

		break;

	case GSM_WAIT_1_SECONDS:
		vTaskDelay(1000);
		vTaskDelay(1);
		break;

	case GSM_WAIT_2_SECONDS:
		vTaskDelay(2000);
		vTaskDelay(1);
		break;

	case GSM_WAIT_5_SECONDS:
		vTaskDelay(5000);
		vTaskDelay(1);
		break;

	case GSM_WAIT_10_SECONDS:
		vTaskDelay(10000);
		vTaskDelay(1);
		break;

	case GSM_BREAKPOINT_NOW:
		vTaskDelay(1000);
		vTaskDelay(1);
		break;

	case GSM_SEND_REQUEST:
		sprintf(buf, "POST %s HTTP/1.1\015\012", gsmEngine.jsonEndPoint);
		apiDnfccSerialEngineTransmit(buf, GSM_UART_NR);
		sprintf(buf, "Content-Type: application/json\015\012");
		apiDnfccSerialEngineTransmit(buf, GSM_UART_NR);

		sprintf(buf, "Content-Length:%05d\015\012", strlen(gsmEngine.jsonRequestData) + 100);
		apiDnfccSerialEngineTransmit(buf, GSM_UART_NR);

		sprintf(buf, "\015\012");
		apiDnfccSerialEngineTransmit(buf, GSM_UART_NR);

		sprintf(buf, gsmEngine.jsonRequestData);

		apiDnfccSerialEngineTransmit(buf, GSM_UART_NR);
		break;
	}

	return 1;
}

/**
 * @brief Initialization routine for the GMS Engine
 */
void apiDnfccGsmEngineInit() {
	gsmEngine.engine = apiDnfccSerialEngineStartListening(&huart_gsm,
	GSM_UART_NR, gsmReceiveBuffer, 1500, gsmTransmitBuffer, 1500);
	gsmEngine.currentStep = GSM_EXECUTE_RESET;
	gsmEngine.flow = Gsm;
	gsmEngine.flowSize = sizeof(Gsm) / sizeof(SerialControllerFlow_type);
	gsmEngine.callbackFunction = intDnfccGsmCallbackFunction;
	gsmEngine.uartNr = GSM_UART_NR;
}

/**
 * @brief Advance 1 in the GSM module. Logging to the screen is handled here (if define is set)
 */
void apiDnfccGsmEngineAdvance() {
#if SERIAL_GSM_LOG == 1
	int curStep = gsmEngine.currentStep;
#endif
	apiDnfccSerialCommunicationAdvance(&gsmEngine);
#if SERIAL_GSM_LOG == 1
	if (curStep != gsmEngine.currentStep) {
		char buf[DISPLAY_BUFFER_SIZE];
		sprintf(buf, "lGSM: %d->%d", curStep, gsmEngine.currentStep);
		queueSendToDisplay(buf, 1000);
	}
#endif
}

/** @} */
