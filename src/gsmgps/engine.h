/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef GSMGPS_ENGINE_H_
#define GSMGPS_ENGINE_H_
#include "../_library/serialEngine/serialEngine.h"

typedef enum {
    GSM_EXECUTE_RESET,                      	// Does a hardware reset
    GSM_PERFORM_AT,                  		// Send AT
    GSM_PERFORM_AT_RESP,                  	// Wait for OK
	GSM_WAIT_FOR_5_SECONDS,					// Step wait for 5 seconds
	GSM_GNSS_GET_POWERSTATUS,				// Get the power status with AT+QGNSSC
	GSM_GNSS_GET_POWERSTATUS_RESP,			// Wait for the response
	GSM_GNSS_OFF_RESP,						// Wait for OK after response GNSS is off
	GSM_GNSS_ON_RESP,						// Wait for OK after response GNSS is on
	GSM_GNSS_SWITCH_ON,						// Switch the power on
	GSM_GNSS_SWITCH_ON_RESP,					// Switch the power on
	GSM_GNSS_INQUIRE_POSITION,				// Get the position
	GSM_GNSS_INQUIRE_POSITION_RESP,			// Get the response, till OK
	GSM_GNSS_WAIT_FOR_WORK,					// Check if there is some work to do
	GSM_ATCREG,								// Check network status
	GSM_ATCREG_RESP,							// Wait for the response

    GSM_GET_CWMODE,						    // Sends AT+CWMODE?
    GSM_GET_CWMODE_RESP,					// Wait for +CWMODE:...
    GSM_GET_CWMODE_RESP_OK,				    // CWMODE = 1, wait for OK
    GSM_GET_CWMODE_RESPSET_OK,			    // CWMODE <> 1, wait for OK
    GSM_SET_CWMODE,      			        // Need to send AT+CWMODE=1, next is check OK
    GSM_SET_CWMODE_OK,   				    // Wait for OK
    GSM_DELAY_FOR_CIPSTATUS,			 	// Wait 1 sec
    GSM_GET_CWJAP,					        // Send AT+CWJAP?
    GSM_GET_CWJAP_RESP,				        // Wait for +CWJAP::#
    GSM_GET_CWJAP_RESPOK,                   // Wait for OK after +CWJAP
    GSM_GET_CWJAP_RESP_NOAPOK,              // Wait for OK after No AP
    GSM_LIST_ACCESS_POINTS,				    // Run AT+CWLAP
    GSM_LIST_ACCESS_POINTS_RESP,			// Get response, like +CWLAP or OK
    GSM_PROCESS_LIST_ACCESS_POINTS,		    // Got an access point line, process it
    GSM_CONNECT_TO_ACCESSPOINT,	            // All access points are received, connect if one is available
    GSM_DELAY_FOR_CONNECT_TO_ACCESSPOINT,   // After try to connect wait 2 seconds for answer
    GSM_VERIFY_IP_ADDRESS,				    // Run AT+CIFSR
    GSM_VERIFY_IP_ADDRESS_RESP,			    // Run AT+CIFSR
    GSM_VERIFY_IP_ADDRESS_RESPOK,		    // Run AT+CIFSR
    GSM_READY_TO_SEND,                      // This is the status when it got an IP address.
    GSM_CHECK_FOR_WORK,                     // See if something needs to be sent
    GSM_SEND_CONNECT_STRING,                // Send the connectstring
    GSM_SEND_CONNECT_STRING_RESP,           // Validate the response - should be OK
    GSM_SEND_CIPLENGTH,                     // Send the length of the string to sent
    GSM_SEND_CIPLENGTH_RESP,                // Wait for the >
    GSM_SEND_DATA,                          // Send the actual data
    GSM_SEND_DATA_RESP,                     // Wait for the SEND OK
    GSM_WAIT_FOR_IPD,                       // Wait for the response length back
    GSM_RETRIEVE_IPD_LENGTH,                // Retrieve the number of bytes to receive
    GSM_RECEIVE_DATA,                       // Get the actual data back, size retrieved on calls above
    GSM_CLOSE_IPCONNECTION,                 // Close the connection and loop back
    GSM_CLOSE_IPCONNECTION_RESP,            // Wait for the OK
} GsmCurrentStatus;

typedef enum {
    GSM_RESET_CALLBACK,
	GSM_GNSS_LINE_CALLBACK,
	GSM_GNSS_CHECK_FOR_WORK,

    GSM_WAIT_1_SECONDS,
    GSM_WAIT_2_SECONDS,
    GSM_WAIT_5_SECONDS,
    GSM_WAIT_10_SECONDS,
    GSM_BREAKPOINT_NOW,                     // To set a breakpoint
    GSM_GET_RECEIVE_BYTES, // +IPD###: -> Called after :, to retrieve the amount of bytes.
    GSM_RECEIVE_BY_LENGTH,
    GSM_SEND_REQUEST,

} GsmCallbackKind;

typedef enum {
    IDLE, TRANSMITTING
} GsmTransmitStatus;

typedef struct {
    GsmCurrentStatus status;
    GsmCurrentStatus prevStatus;
    GsmTransmitStatus transmitStatus;

    SerialEngineStruct *serialEngine;
    uint16_t rawToReceive;
    uint16_t receiveTill;

    uint8_t contentLength[5];
} GsmEngineStruct;

uint8_t GsmTransmitCommand(char *s);
#endif /* GSMGPS_ENGINE_H_ */
