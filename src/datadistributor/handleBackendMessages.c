/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "../_configuration/backendCommunication.h"
#include <string.h>
#include "../sdio/FileFunctions.h"
#include "handleBackendMessages.h"

/**
 * @file
 * @brief To parse strings from the backend and take appropriate actions
 */
/**
 * @addtogroup datadistributor
 * @{
 */

/**
 * @brief First data block pointer
 */
extern SdioBlockInfo sdioFirstBlockDataPointer;
/**
 * @brief Check if a field is in the data, and return it
 *
 * Search for the field in the input, and return the data associated in outData
 * field should contain the total string like:   "data":  (so including the " and the :
 * @param in - The input string
 * @param field - The field to search for in JSON
 * @param outData - Pointer to the position to store it
 */
static int getField(char *in, char *field, char *outData) {
	char *p = strstr(in, field);
	outData[0] = '\0';
	if (p == NULL) {
		return 0;
	}
	p += strlen(field);

	if (*p == '"') {
		return sscanf(p, "\"%[^\"]", outData);
	}
	return sscanf(p, "%[^,]", outData);
}

/**
 * @brief Process a message from the backend
 *
 * @param message - the message to search for/in
 */
void apiDnfccProcessMessageFromBackend(char *message) {
	// Process the date field
	char clockBuf[CLOCK_BUFFER_SIZE + 1];
	if (getField(message, BACKEND_MESSAGE_FIELD_DATE, clockBuf + 1) == 1) {
		char curDate[10];
		apiDnfccClockFetchDate(curDate);
		if (strncmp(clockBuf + 1, curDate, 8) != 0) {
			clockBuf[0] = COMCOM_CLOCK_SETDATE;
			queueSendToClock(clockBuf, portMAX_DELAY);
		}
	}
	if (getField(message, BACKEND_MESSAGE_FIELD_TIME, clockBuf + 1) == 1) {
		char curTime[10];
		apiDnfccClockFetchTime(curTime);
		if (strncmp(clockBuf + 1, curTime, 6) != 0) {
			clockBuf[0] = COMCOM_CLOCK_SETTIME;
			queueSendToClock(clockBuf, portMAX_DELAY);
		}
	}

// Verify if there are field "perform" and "performData" available
	char *perform = strstr(message, BACKEND_MESSAGE_FIELD_PERFORM);
	if (perform != NULL) {
		perform += strlen(BACKEND_MESSAGE_FIELD_PERFORM);
	}

	char *performData = strstr(message, BACKEND_MESSAGE_FIELD_PERFORMDATA);
	if (performData != NULL) {
		performData += strlen(BACKEND_MESSAGE_FIELD_PERFORMDATA);
	}

	if (perform != NULL) {
		if (strncmp(perform + 1, BACKEND_PERFORM_UPDATE_FARE,
				strlen(BACKEND_PERFORM_UPDATE_FARE)) == 0) {
			char stbuf[SDIO_BUFFER_SIZE];
			stbuf[0] = COMCOM_SDIO_INITIALIZE_FARE;
			strcpy(stbuf + 1, NAME_FILE_FARE);
			memcpy(stbuf + 5, performData + 1, 5);
			queueSendToSdio(stbuf, portMAX_DELAY);
		}
		if (strncmp(perform + 1, BACKEND_PERFORM_UPDATE_PRINTER,
				strlen(BACKEND_PERFORM_UPDATE_PRINTER)) == 0) {
			char stbuf[SDIO_BUFFER_SIZE];
			stbuf[0] = COMCOM_SDIO_INITIALIZE_PRINTER;
			strcpy(stbuf + 1, NAME_FILE_PRINTER);
			memcpy(stbuf + 5, performData + 1, 5);
			queueSendToSdio(stbuf, portMAX_DELAY);
		}
		if (strncmp(perform + 1, BACKEND_PERFORM_UPDATE_BLACKLIST,
				strlen(BACKEND_PERFORM_UPDATE_BLACKLIST)) == 0) {
			char stbuf[SDIO_BUFFER_SIZE];
			stbuf[0] = COMCOM_SDIO_INITIALIZE_BLACKLIST;
			strcpy(stbuf + 1, NAME_FILE_BLACKLIST);
			memcpy(stbuf + 5, performData + 1, 5);
			queueSendToSdio(stbuf, portMAX_DELAY);
		}
		if (strncmp(perform + 1, BACKEND_PERFORM_UPDATE_FIRMWARE,
				strlen(BACKEND_PERFORM_UPDATE_FIRMWARE)) == 0) {
			char stbuf[FLASH_PROGRAMMER_BUFFER_SIZE];
			stbuf[0] = COMCOM_FLASH_UPDATE_FIRMWARE;
			strcpy(stbuf + 1, NAME_FILE_FIRMWARE);
			memcpy(stbuf + 5, performData + 1, 5);
			queueSendToFlashProgrammer(stbuf, portMAX_DELAY);
		}
		if (strncmp(perform + 1, BACKEND_PERFORM_UPDATE_WIFI,
				strlen(BACKEND_PERFORM_UPDATE_WIFI)) == 0) {
			char stbuf[SDIO_BUFFER_SIZE];
			stbuf[0] = COMCOM_SDIO_INITIALIZE_WIFI;
			strcpy(stbuf + 1, NAME_FILE_WIFI);
			memcpy(stbuf + 5, performData + 1, 5);
			queueSendToSdio(stbuf, portMAX_DELAY);
		}
		if (strncmp(perform + 1, BACKEND_PERFORM_INIT_FILE,
				strlen(BACKEND_PERFORM_INIT_FILE)) == 0) {
			char stbuf[SDIO_BUFFER_SIZE];
			memcpy(stbuf, performData, 5);
			// IfareV001<length:9><sha1:40>
			// 01   5   9         18
			getField(message, "\"fileVersion\":", stbuf + 5);
			strcpy(stbuf + 9, "         ");
			getField(message, "\"totalLength\":", stbuf + 9);
			getField(message, "\"sha1\":", stbuf + 18);
			if (strncmp(stbuf + 1, NAME_FILE_FIRMWARE, 4) == 0) {
				stbuf[0] = COMCOM_FLASH_INIT_FILE;
				queueSendToFlashProgrammer(stbuf, portMAX_DELAY);
			} else {
				stbuf[0] = COMCOM_SDIO_INIT_FILE;
				queueSendToSdio(stbuf, portMAX_DELAY);
			}
		}
		if (strncmp(perform + 1, BACKEND_PERFORM_WRITE_FILE,
				strlen(BACKEND_PERFORM_WRITE_FILE)) == 0) {
			char stbuf[SDIO_BUFFER_SIZE];
			stbuf[0] = COMCOM_SDIO_DATA_FOR_FILE;
			memcpy(stbuf + 1, performData + 1, 4);
			memset(stbuf + 5, '\0', 16);
			getField(message, "\"offset\":", stbuf + 5);
			getField(message, "\"length\":", stbuf + 13);
			getField(message, "\"md5sum\":", stbuf + 18);
			getField(message, "\"data\":", stbuf + 50);
			if (strncmp(stbuf + 1, NAME_FILE_FIRMWARE, 4) == 0) {
				stbuf[0] = COMCOM_FLASH_DATA_FOR_FIRMWARE;
				queueSendToFlashProgrammer(stbuf, portMAX_DELAY);
			} else {
				stbuf[0] = COMCOM_SDIO_DATA_FOR_FILE;
				queueSendToSdio(stbuf, portMAX_DELAY);
			}
		}
		if (strncmp(perform + 1, BACKEND_PERFORM_SETID,
				strlen(BACKEND_PERFORM_SETID)) == 0) {
			memcpy(deviceStatus.deviceId, performData + 1, 4);
			apiDnfccSetWriteData(&sdioFirstBlockDataPointer, 10,
					(uint8_t *) deviceStatus.deviceId, 4);
		}
		if (strncmp(perform + 1, BACKEND_PERFORM_SELFTEST,
				strlen(BACKEND_PERFORM_SELFTEST)) == 0) {
			char buf[MAX_BUFFER_SIZE];
			buf[0] = COMCOM_SELFTEST;
			queueSendToDataDistributor(buf, portMAX_DELAY); // at front, as this can be blocking otherwise
			queueSendToSound(buf, portMAX_DELAY);
			queueSendToCanbus(buf, portMAX_DELAY);
			queueSendToClock(buf, portMAX_DELAY);
			queueSendToDisplay(buf, portMAX_DELAY);
			queueSendToQrcode(buf, portMAX_DELAY);
			queueSendToFlashProgrammer(buf, portMAX_DELAY);
			queueSendToSam(buf, portMAX_DELAY);
			queueSendToGps(buf, portMAX_DELAY);
			queueSendToGsm(buf, portMAX_DELAY);
			queueSendToPrinter(buf, portMAX_DELAY);
			queueSendToSdio(buf, portMAX_DELAY);
			queueSendToSmartcard(buf, portMAX_DELAY);
			queueSendToTouch(buf, portMAX_DELAY);
			queueSendToWifi(buf, portMAX_DELAY);
		}
	}
}
/** @} */
