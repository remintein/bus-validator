/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"

#include "../platform_config.h"
#include "datadistributor.h"
#include "JsonGenerator.h"
#include "../_definition/deviceStatus.h"
#include "../_configuration/backendCommunication.h"
#include "handleBackendMessages.h"
#include "JsonGenerator.h"
#include "../sdio/FileFunctions.h"
#include <strings.h>
#include <string.h>

/**
 * @file
 * @brief Data Distributor
 */

/**
 * @addtogroup datadistributor
 * @{
 */

#define STACK_SIZE 1000

/**
 * @brief Initialization routine
 *
 * @param uxPriority - PRiority for the task
 */
BaseType_t nfccDatadistributorInit(UBaseType_t uxPriority) {
	deviceStatus.datadistributorRxQueue = xQueueCreate(DATADISTRIBUTOR_QUEUE_LENGTH, DATADISTRIBUTOR_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.datadistributorRxQueue, "DataDistri");

	setWaitForSystemReadyThisTask(TASK_BIT_DATADISTRIBUTOR);

	return xTaskCreate(nfccDatadistributor, "Datadistributor", STACK_SIZE, NULL, uxPriority,
			&deviceStatus.datadistributorTask);
}

/**
 * @brief The main dataDistributors
 *
 * @param - Main loop for the dataDistributor
 */
void nfccDatadistributor(void *pvParameters) {
	char buf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;
	int selfTest = 1;

	TransmitWifiMessage.transmitStatus = STATUS_IDLE;
	TransmitWifiMessage.messageId = 0;
	TransmitWifiMessage.response = wifiResponse;

	TransmitGsmMessage.transmitStatus = STATUS_IDLE;
	TransmitGsmMessage.messageId = 0;
	TransmitGsmMessage.response = gsmResponse;

	lastMessageId = 0; // Last message sent.
	nextMessageId = 1;

	setTaskIsSystemReady(TASK_BIT_DATADISTRIBUTOR);
	waitForSystemReady();

	beginMessageQueue = endMessageQueue = 0;
	if (strncmp(deviceStatus.deviceId, "????", 4) == 0) {
		messages[endMessageQueue].messageId = nextMessageId;
		messages[endMessageQueue].transmitStatus = STATUS_WAITING;
		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
		sprintf(messages[endMessageQueue].data + strlen(messages[endMessageQueue].data), ",\"uuid\":\"%s\"}\r\n",
				deviceStatus.uuid);
		messages[endMessageQueue].endpoint = ENDPOINT_REQUESTID;

		endMessageQueue++;
	}
	messages[endMessageQueue].messageId = nextMessageId;
	messages[endMessageQueue].transmitStatus = STATUS_WAITING;
	messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
	apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
	sprintf(messages[endMessageQueue].data + strlen(messages[endMessageQueue].data), ",\"uuid\":\"%s\"",
			deviceStatus.uuid);
	strcat(messages[endMessageQueue].data, ",\"start\":\"ok\"}\r\n");
	messages[endMessageQueue].endpoint = ENDPOINT_START;

	endMessageQueue++;

	for (;;) {

		if (beginMessageQueue == endMessageQueue) {
			uint8_t ior = apiDnfccGetNextMessageFromLogging(buf);
			/* Get data from logfile if present, and so, process it */
			if (ior == 0) {
				apiDnfccGenerateJsonMessageInQueue((uint8_t *)buf);
			}
		}
		if (beginMessageQueue != endMessageQueue) {
			if (TransmitWifiMessage.transmitStatus == STATUS_FINISHED
					&& TransmitWifiMessage.messageId < messages[beginMessageQueue].messageId) {
				TransmitWifiMessage.transmitStatus = STATUS_IDLE;
			}
			if (TransmitGsmMessage.transmitStatus == STATUS_FINISHED
					&& TransmitGsmMessage.messageId < messages[beginMessageQueue].messageId) {
				TransmitGsmMessage.transmitStatus = STATUS_IDLE;
			}
			// For WIFI
			if (TransmitWifiMessage.transmitStatus == STATUS_FINISHED
					&& TransmitWifiMessage.messageId == messages[beginMessageQueue].messageId) {
				apiDnfccProcessMessageFromBackend(TransmitWifiMessage.response);
				TransmitWifiMessage.transmitStatus = STATUS_IDLE;
				messages[beginMessageQueue].transmitStatus = STATUS_IDLE;
				beginMessageQueue = (beginMessageQueue + 1) % MESSAGE_BUFFER;
				continue;
			}
			// For GSM
			if (TransmitGsmMessage.transmitStatus == STATUS_FINISHED
					&& TransmitGsmMessage.messageId == messages[beginMessageQueue].messageId) {
				apiDnfccProcessMessageFromBackend(TransmitGsmMessage.response);
				TransmitGsmMessage.transmitStatus = STATUS_IDLE;
				messages[beginMessageQueue].transmitStatus = STATUS_IDLE;
				beginMessageQueue = (beginMessageQueue + 1) % MESSAGE_BUFFER;
				continue;
			}

// beginMessageQueue points to a fresh message
// See if it is in all messages buffers, if they are idle...
			if (TransmitWifiMessage.transmitStatus == STATUS_IDLE) {
				TransmitWifiMessage.messageId = messages[beginMessageQueue].messageId;
				TransmitWifiMessage.data = messages[beginMessageQueue].data;
				TransmitWifiMessage.endpoint = messages[beginMessageQueue].endpoint;
				TransmitWifiMessage.transmitStatus =
				STATUS_WAITING;
				messages[beginMessageQueue].transmitStatus =
				STATUS_WORK;
			}
			if (TransmitGsmMessage.transmitStatus == STATUS_IDLE) {
				TransmitGsmMessage.messageId = messages[beginMessageQueue].messageId;
				TransmitGsmMessage.data = messages[beginMessageQueue].data;
				TransmitGsmMessage.endpoint = messages[beginMessageQueue].endpoint;
				TransmitGsmMessage.transmitStatus =
				STATUS_WAITING;
				messages[beginMessageQueue].transmitStatus =
				STATUS_WORK;
			}
		}

		if (selfTest == 0) {
			if (xTaskGetTickCount() > 10000) {
				apiDnfccProcessMessageFromBackend(",\"perform\":\"selftest\"");
				selfTest = 1;
			}
		}

		buf[0] = '\0';
		if (xQueueReceive(deviceStatus.datadistributorRxQueue, buf,
				1000) != pdPASS) {
			continue;
		}

		switch (buf[0]) {
		case COMCOM_DATADISTRIBUTOR_ERROR_CARD:
		case COMCOM_DATADISTRIBUTOR_OK_CARD:
		case COMCOM_DATADISTRIBUTOR_START_SHIFT:
		case COMCOM_DATADISTRIBUTOR_LOGIN:
		case COMCOM_DATADISTRIBUTOR_END_SHIFT:
		case COMCOM_DATADISTRIBUTOR_START_TRIP:
		case COMCOM_DATADISTRIBUTOR_END_TRIP:
			apiDnfccWriteLogfile128Bytes(buf); // Put it in the logfile; from there it will be transmitted later
			break;
		case COMCOM_DATADISTRIBUTOR_INIT_FILE:
		case COMCOM_DATADISTRIBUTOR_GET_DATA_FROM_FILE:
		case COMCOM_DATADISTRIBUTOR_KEEPALIVE:
			apiDnfccGenerateJsonMessageInQueue((uint8_t*)buf); // Keep alive, put in the wifi queue
			break;
		case COMCOM_DATADISTRIBUTOR_MESSAGE_SEND:
			break;
		case COMCOM_DATADISTRIBUTOR_TOPUP_PERFORMED: // Topup performed
			apiDnfccWriteLogfile128Bytes(buf); // Put it in the logfile; from there it will be transmitted later
			break;
		case COMCOM_DATADISTRIBUTOR_SELFTESTRESULT:
			apiDnfccWriteLogfile128Bytes(buf); // Put it in the logfile; from there it will be transmitted later
			break;
		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(deviceStatus.datadistributorTask);
			sprintf(stbuf, "!DAWM%ld", uxHighWaterMark);
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_DATATDISTRIBUTOR;
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;
		}
	}
}

/** @} */
