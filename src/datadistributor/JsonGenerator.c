/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

/**
 * @file
 * @brief Json generator files
 *
 * json generator routines
 */
/**
 * @addtogroup datadistributor
 * @{
 */

#include <strings.h>
#include <string.h>
#include "../project_includes.h"
#include "../platform_config.h"
#include "../_definition/deviceStatus.h"
#include "../_configuration/backendCommunication.h"

#define extern /**/
#include "datadistributor.h"
#undef extern

#include "JsonGenerator.h"

/**
 * @brief The default file version
 */
extern uint8_t *defaultFileVersion;

/**
 * @brief Buffer to store the sanitized data
 */
static char buf[5];
/**
 * @brief Sanitize the character input of 4 bytes (to fix incorrect characters in the versions
 *
 * @param in - string to sanitize
 */
static char *intDnfccSanitize4(char *in) {
	for (int i = 0; i < 4; i++) {
		buf[i] = in[i];
		if (buf[i] < ' ' || buf[i] >= 0x7f || buf[i] == '"' || buf[i] == '\\') {
			buf[i] = '?';
		}
	}
	buf[4] = '\0';
	return buf;
}

/**
 * @brief Create a json header for a message
 *
 * @param p - Pointer to which to store the string
 * @param msgId - The ID of the message
 * @param dateTime - The date/time string to send
 */
static void intDnfccStartJsonHeaderDateTime(char *p, uint32_t msgId, char *dateTime) {
	sprintf(p,
			"{\"id\":%lu,\"device\":\"%4.4s\",\"version\":\"%4.4s\",\"date\":\"%s\",\"role\":\"%2s\"",
			msgId, deviceStatus.deviceId,
			intDnfccSanitize4(deviceStatus.firmwareVersion), dateTime,
			CURRENT_TARGET_BACKEND);
	if (strncmp((char *) deviceStatus.blckVersion, (char *) defaultFileVersion,
			4) != 0)
		sprintf(p + strlen(p), ",\"blckVers\":\"%4.4s\"",
				intDnfccSanitize4((char *) deviceStatus.blckVersion));
	if (strncmp((char *) deviceStatus.fareVersion, (char *) defaultFileVersion,
			4) != 0)
		sprintf(p + strlen(p), ",\"fareVers\":\"%4.4s\"",
				intDnfccSanitize4((char *) deviceStatus.fareVersion));
	if (strncmp((char *) deviceStatus.printerVersion,
			(char *) defaultFileVersion, 4) != 0)
		sprintf(p + strlen(p), ",\"prntVers\":\"%4.4s\"",
				intDnfccSanitize4((char *) deviceStatus.printerVersion));
	if (strncmp((char *) deviceStatus.wifiVersion, (char *) defaultFileVersion,
			4) != 0)
		sprintf(p + strlen(p), ",\"wifiVers\":\"%4.4s\"",
				intDnfccSanitize4((char *) deviceStatus.wifiVersion));
	if (strlen(deviceStatus.longitude) > 0)
		sprintf(p + strlen(p), ",\"longitude\":\"%s\"", deviceStatus.longitude);
	if (strlen(deviceStatus.lattitude) > 0)
		sprintf(p + strlen(p), ",\"lattitude\":\"%s\"", deviceStatus.lattitude);
	if (strlen(deviceStatus.location) > 0)
		sprintf(p + strlen(p), ",\"location\":\"%s\"", deviceStatus.location);

}

/**
 * @brief Create a json header for a message This will create a message, after getting the date/time
 *
 * @param p - Pointer to which to store the string
 * @param msgId - The ID of the message
 */
void apiDnfccStartJsonHeader(char *p, uint32_t msgId) {
	char dt[20];
	apiDnfccClockFetchDateTime(dt);

	intDnfccStartJsonHeaderDateTime(p, msgId, dt);
}

/**
 * @brief Generate a json message in the buffer. The message is received from the queue
 *
 * @param buf - Data sent in queue
 */
void apiDnfccGenerateJsonMessageInQueue(uint8_t *buf) {
	int i;
	switch (buf[0]) {
	case COMCOM_DATADISTRIBUTOR_OK_CARD:
		nextMessageId++;

		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		intDnfccStartJsonHeaderDateTime(messages[endMessageQueue].data, nextMessageId,
				(char *) (buf + 65));
		messages[endMessageQueue].messageId = nextMessageId;

		// 0 = message
		// 1..14 = uid
		// 15..24 = Ticket Balance
		// 25..34 = Purse Balance
		// 35..44 = Card Transaction
		// 45..64 = CardId
		// 65..79 = date/time

		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				",\"cardUid\":\"%14.14s\",\"ticketVal\":\"%10.10s\",\"purseVal\":\"%10.10s\",\"txCnt\":\"%10.10s\",\"cardId\":\"%20.20s\"}\r\n",
				buf + 1, buf + 15, buf + 25, buf + 35, buf + 45);
		messages[endMessageQueue].endpoint = ENDPOINT_CARDENTEROK;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_ERROR_CARD:
		if (((endMessageQueue + 1) % MESSAGE_BUFFER) == beginMessageQueue) {
			break;
		}
		nextMessageId++;
		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		messages[endMessageQueue].messageId = nextMessageId;
		intDnfccStartJsonHeaderDateTime(messages[endMessageQueue].data, nextMessageId,
				messages[endMessageQueue].data + 65);

		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				",\"card\":\"%20.20s\",\"info\":\"%10.10s\"}\r\n", buf + 1,
				buf + 80);
		messages[endMessageQueue].endpoint = ENDPOINT_CARDENTERERROR;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_TOPUP_PERFORMED: // Topup performed
		if (((endMessageQueue + 1) % MESSAGE_BUFFER) == beginMessageQueue) {
			break;
		}
		nextMessageId++;
		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
		messages[endMessageQueue].messageId = nextMessageId;
		int val1, val2;
		val1 = val2 = 0;
		sscanf((char *) (buf + 15), "V%uT%u", &val1, &val2);

		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				",\"card\":\"%14.14s\",\"info\":\"OK\"", buf + 1);
		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				"\"newVal\":\"%d\",\"topupWith\":\"%d\"}\r\n", val1, val2);
		messages[endMessageQueue].endpoint = ENDPOINT_CARDTOPUP;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_SELFTESTRESULT:
		if (((endMessageQueue + 1) % MESSAGE_BUFFER) == beginMessageQueue) {
			break;
		}
		nextMessageId++;
		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		messages[endMessageQueue].messageId = nextMessageId;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);

		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				",\"selftest\":\"%s\"}\r\n", buf + 1);
		messages[endMessageQueue].endpoint = ENDPOINT_SELFTEST;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_GET_DATA_FROM_FILE:
		nextMessageId++;

		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
		messages[endMessageQueue].messageId = nextMessageId;
		uint32_t startPos = 0, length = 0;
		sscanf((char *) (buf + 9), "%lu,%lu", &startPos, &length);
		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				",\"retrieve\":\"%4.4s\",\"versionToGet\":\"%4.4s\",\"startPos\":\"%lu\",\"length\":\"%lu\"}\r\n",
				buf + 1, buf + 5, startPos, length);
		messages[endMessageQueue].endpoint = ENDPOINT_GET_FILE_DATA;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_LOGIN:
		nextMessageId++;

		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
		messages[endMessageQueue].messageId = nextMessageId;
		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				",\"time\":\"%s\"}\r\n", buf + 16);

		messages[endMessageQueue].endpoint = ENDPOINT_START_SHIFT;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_START_SHIFT:
		nextMessageId++;

		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
		messages[endMessageQueue].messageId = nextMessageId;
		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				",\"time\":\"%s\"}\r\n", buf + 16);

		messages[endMessageQueue].endpoint = ENDPOINT_START_SHIFT;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_END_SHIFT:
		nextMessageId++;

		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
		messages[endMessageQueue].messageId = nextMessageId;
		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data), "}\r\n");
		messages[endMessageQueue].endpoint = ENDPOINT_END_SHIFT;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_START_TRIP:
		nextMessageId++;

		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
		messages[endMessageQueue].messageId = nextMessageId;
		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data), "}\r\n");
		messages[endMessageQueue].endpoint = ENDPOINT_START_TRIP;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_END_TRIP:
		nextMessageId++;

		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
		messages[endMessageQueue].messageId = nextMessageId;
		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data), "}\r\n");
		messages[endMessageQueue].endpoint = ENDPOINT_END_TRIP;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_INIT_FILE:
		nextMessageId++;

		messages[endMessageQueue].messageKind = MESSAGE_KIND_NORMAL;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);
		messages[endMessageQueue].messageId = nextMessageId;
		sprintf(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				",\"file\":\"%4.4s\",\"versionToGet\":\"%4.4s\"}\r\n", buf + 1,
				buf + 5);
		messages[endMessageQueue].endpoint = ENDPOINT_INITFILE;
		messages[beginMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;

	case COMCOM_DATADISTRIBUTOR_KEEPALIVE: // Keep alive
		// Check if keepalive is already in the queue, then do not register!
		for (i = beginMessageQueue; i != endMessageQueue;
				i = (i + 1) % MESSAGE_BUFFER) {
			if (messages[i].messageKind == MESSAGE_KIND_KEEPALIVE) {
				break;
			}
		}
		if (i != endMessageQueue) {
			break;
		}
		if (beginMessageQueue != endMessageQueue) {
			break;
		}
		nextMessageId++;
		messages[endMessageQueue].messageKind = MESSAGE_KIND_KEEPALIVE;
		messages[endMessageQueue].messageId = nextMessageId;
		apiDnfccStartJsonHeader(messages[endMessageQueue].data, nextMessageId);

		strcpy(
				messages[endMessageQueue].data
						+ strlen(messages[endMessageQueue].data),
				",\"info\":\"alive\"}\r\n");
		messages[endMessageQueue].endpoint = ENDPOINT_KEEPALIVE;
		messages[endMessageQueue].transmitStatus = STATUS_WAITING;
		endMessageQueue = (endMessageQueue + 1) % MESSAGE_BUFFER;
		break;
	}
}
/** @} */
