/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include "../_configuration/queueDefinitions.h"

/**
 * @file
 * @brief Defines the functions for the DataDistributor module
 */
/**
 * @addtogroup datadistributor
 * @{
 */
/**
 * Initialize routine, called from main.c before the FreeRTOS scheduler is started.
 */
BaseType_t nfccDatadistributorInit(UBaseType_t uxPriority);
/**
 * Main loop for the clock routine
 */
void nfccDatadistributor(void *pvParameters);

/**
 * Status of the backend messages
 */
typedef enum _transmitStatus {
	STATUS_IDLE,    /**< Transmittor idle */
	STATUS_WAITING, /**< Transmission waiting for work */
	STATUS_WORK,    /**< Transmission in progredd */
	STATUS_FINISHED /**< Transmission finished */
} TransmitStatus; /**< Status of the backend messages */

#define TRANxSMIT_STATUS_IDLE 		0
/**
 * Status of the backend messages: Waiting for work
 */
#define TRANSxMIT_STATUS_WAITING 	1
/**
 * Status of the backend messages: Transmission in progress
 */
#define TRANSMxIT_STATUS_WORK 		2
/**
 * Status of the backend messages: Transmission finished
 */
#define TRANSMIxT_STATUS_FINISHED 	3

/**
 * Defines the max length of the json messages
 */
#define MESSAGE_MAX_RESPONSE_LENGTH 900

/**
 * Structure to hold the status of the transmitting message
 */
typedef struct {
	TransmitStatus transmitStatus; /** Status (can be one of the above defines */
	uint32_t messageId; /** Message ID, to see which one is sent */
	char *data; /** Pointer to the data */
	char *endpoint; /** Endpoint for the current message */
	char *response; /** Response will be stored here */
} TransmitMessage;

#define MESSAGE_KIND_KEEPALIVE 'K'
#define MESSAGE_KIND_NORMAL 'N'

typedef struct {
	uint32_t messageId;
	uint8_t transmitStatus;
	uint8_t messageKind;
	char data[500];
	char *endpoint;
} MessageToSend;

#define MESSAGE_BUFFER 20
/**
 *  In JsonGenerator.c extern is defined as a comment string, to make sure the variables are allocated
 */

extern MessageToSend messages[MESSAGE_BUFFER];
extern uint32_t lastMessageId;
extern uint32_t nextMessageId;

extern int beginMessageQueue; // Points to next message to send
extern int endMessageQueue; // Points to position next message can be placed in the queue.

extern TransmitMessage TransmitWifiMessage;
extern char wifiResponse[MESSAGE_MAX_RESPONSE_LENGTH + 1];

extern TransmitMessage TransmitGsmMessage;
extern char gsmResponse[MESSAGE_MAX_RESPONSE_LENGTH + 1];

/** @} */
