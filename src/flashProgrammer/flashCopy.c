/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include <string.h>

/**
 * @file
 * @brief Flash copy routine
 *
 * Here the routines needed to flash from memory are specified
 */

/**
 * @addtogroup FlashProgrammer
 * @{
 */

#define FLASH_TIMEOUT_VALUE       50000U /* 50 s */

extern FLASH_ProcessTypeDef pFlash;

static inline HAL_StatusTypeDef inline_HAL_FLASH_Unlock(void);
static inline HAL_StatusTypeDef inline_HAL_FLASH_Lock(void);
static inline void inline_FLASH_Erase_Sector(uint32_t Sector,
		uint8_t VoltageRange);
static inline void inline_FLASH_Program_Byte(uint32_t Address, uint8_t Data);
static inline HAL_StatusTypeDef inline_FLASH_WaitForLastOperation(
		uint32_t Timeout);
static inline HAL_StatusTypeDef inline_HAL_FLASH_Program(uint32_t TypeProgram,
		uint32_t Address, uint64_t Data);

static inline void inline_NVIC_SystemReset(void);
void intDnfccFlasherCopy();

/**
 * @brief This is the function to copy data from bank 0x8080000 to 0x8000000
 * It should be sent to RAM first. That is done by function intDnfccFlasherCopy().
 */
void intDnfccCopyFlash() {
	uint32_t pMemory;
	uint32_t tMemory;
	inline_HAL_FLASH_Unlock();
	__HAL_FLASH_CLEAR_FLAG(
			FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

	inline_FLASH_Erase_Sector(0, VOLTAGE_RANGE_3);
	inline_FLASH_Erase_Sector(1, VOLTAGE_RANGE_3);
	inline_FLASH_Erase_Sector(2, VOLTAGE_RANGE_3);
	inline_FLASH_Erase_Sector(3, VOLTAGE_RANGE_3);
	inline_FLASH_Erase_Sector(4, VOLTAGE_RANGE_3);
	inline_FLASH_Erase_Sector(5, VOLTAGE_RANGE_3);
	inline_FLASH_Erase_Sector(6, VOLTAGE_RANGE_3);
	inline_FLASH_Erase_Sector(7, VOLTAGE_RANGE_3);

	pMemory = 0x8080000;
	char *ppMemory = (char *) pMemory;
	tMemory = 0x8000000;

	// Now positioned at the beginning of the byte string...

	for (long i = 0; i < 0x80000; i++) {
		if (*((char *) pMemory) != 0xff) {
			inline_HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, tMemory,
					*ppMemory);
		}
		tMemory++;
		ppMemory++;
	}

	inline_HAL_FLASH_Lock();

	inline_NVIC_SystemReset();
}

/**
 * @brief  System Reset
 *
 * The function initiates a system reset request to reset the MCU.
 */
static inline void inline_NVIC_SystemReset(void) { // Verified
	__DSB(); /* Ensure all outstanding memory accesses included
	 buffered write are completed before reset */

	SCB->AIRCR = ((0x5FA << SCB_AIRCR_VECTKEY_Pos)
			| (SCB->AIRCR & SCB_AIRCR_PRIGROUP_Msk) |
			SCB_AIRCR_SYSRESETREQ_Msk); /* Keep priority group unchanged */
	__DSB(); /* Ensure completion of memory access */

	while (1)
		; /* wait until reset */
}

/**
 * @brief  Unlock the FLASH control register access
 * @retval HAL Status
 */
static inline HAL_StatusTypeDef inline_HAL_FLASH_Unlock(void) {
	if ((FLASH->CR & FLASH_CR_LOCK) != RESET) {
		/* Authorize the FLASH Registers access */
		FLASH->KEYR = FLASH_KEY1;
		FLASH->KEYR = FLASH_KEY2;
	} else {
		return HAL_ERROR;
	}

	return HAL_OK;
}

/**
 * @brief  Locks the FLASH control register access
 * @retval HAL Status
 */
static inline HAL_StatusTypeDef inline_HAL_FLASH_Lock(void) {
	/* Set the LOCK Bit to lock the FLASH Registers access */
	FLASH->CR |= FLASH_CR_LOCK;

	return HAL_OK;
}

/**
 * @brief helper function, for flashigh a sector
 *
 * Helper functions, copied from stm32f4xx_hal_flash_ex.c
 * */
static inline
void inline_FLASH_Erase_Sector(uint32_t Sector, uint8_t VoltageRange) { // verified
	uint32_t tmp_psize = 0U;

	/* Check the parameters */
	assert_param(IS_FLASH_SECTOR(Sector));
	assert_param(IS_VOLTAGERANGE(VoltageRange));

	if (VoltageRange == FLASH_VOLTAGE_RANGE_1) {
		tmp_psize = FLASH_PSIZE_BYTE;
	} else if (VoltageRange == FLASH_VOLTAGE_RANGE_2) {
		tmp_psize = FLASH_PSIZE_HALF_WORD;
	} else if (VoltageRange == FLASH_VOLTAGE_RANGE_3) {
		tmp_psize = FLASH_PSIZE_WORD;
	} else {
		tmp_psize = FLASH_PSIZE_DOUBLE_WORD;
	}

	/* If the previous operation is completed, proceed to erase the sector */
	CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
	FLASH->CR |= tmp_psize;
	CLEAR_BIT(FLASH->CR, FLASH_CR_SNB);
	FLASH->CR |= FLASH_CR_SER | (Sector << POSITION_VAL(FLASH_CR_SNB));
	FLASH->CR |= FLASH_CR_STRT;
}

/**
 * @brief  Program byte (8-bit) at a specified address.
 * @note   This function must be used when the device voltage range is from
 *         1.8V to 3.6V.
 *
 * @note   If an erase and a program operations are requested simultaneously,
 *         the erase operation is performed before the program one.
 *
 * @param  Address specifies the address to be programmed.
 * @param  Data specifies the data to be programmed.
 * @retval None
 */
static inline void inline_FLASH_Program_Byte(uint32_t Address, uint8_t Data) { // check
	/* Check the parameters */
	assert_param(IS_FLASH_ADDRESS(Address));

	/* If the previous operation is completed, proceed to program the new data */
	CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);
	FLASH->CR |= FLASH_PSIZE_BYTE;
	FLASH->CR |= FLASH_CR_PG;

	*(__IO uint8_t*) Address = Data;
}

/**
 * @brief  Set the specific FLASH error flag.
 * @retval None
 */
static inline void inline_FLASH_SetErrorCode(void) {
	if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_WRPERR) != RESET) {
		pFlash.ErrorCode |= HAL_FLASH_ERROR_WRP;

		/* Clear FLASH write protection error pending bit */
		__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_WRPERR);
	}

	if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_PGAERR) != RESET) {
		pFlash.ErrorCode |= HAL_FLASH_ERROR_PGA;

		/* Clear FLASH Programming alignment error pending bit */
		__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_PGAERR);
	}

	if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_PGPERR) != RESET) {
		pFlash.ErrorCode |= HAL_FLASH_ERROR_PGP;

		/* Clear FLASH Programming parallelism error pending bit */
		__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_PGPERR);
	}

	if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_PGSERR) != RESET) {
		pFlash.ErrorCode |= HAL_FLASH_ERROR_PGS;

		/* Clear FLASH Programming sequence error pending bit */
		__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_PGSERR);
	}

	if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_RDERR) != RESET) {
		pFlash.ErrorCode |= HAL_FLASH_ERROR_RD;

		/* Clear FLASH Proprietary readout protection error pending bit */
		__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_RDERR);
	}

	if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_OPERR) != RESET) {
		pFlash.ErrorCode |= HAL_FLASH_ERROR_OPERATION;

		/* Clear FLASH Operation error pending bit */
		__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_OPERR);
	}
}

extern __IO uint32_t uwTick;
static inline uint32_t inline_HAL_GetTick(void) {
	return uwTick;
}
/**
 * @brief  Wait for a FLASH operation to complete.
 * @param  Timeout maximum flash operationtimeout
 * @retval HAL Status  ***
 */
static inline HAL_StatusTypeDef inline_FLASH_WaitForLastOperation(
		uint32_t Timeout) { // Done
	uint32_t tickstart = 0U;

	/* Clear Error Code */
	pFlash.ErrorCode = HAL_FLASH_ERROR_NONE;

	/* Wait for the FLASH operation to complete by polling on BUSY flag to be reset.
	 Even if the FLASH operation fails, the BUSY flag will be reset and an error
	 flag will be set */
	/* Get tick */
	tickstart = inline_HAL_GetTick();

	while (__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET) {
		if (Timeout != HAL_MAX_DELAY) {
			if ((Timeout == 0U)
					|| ((inline_HAL_GetTick() - tickstart) > Timeout)) {
				return HAL_TIMEOUT;
			}
		}
	}

	/* Check FLASH End of Operation flag  */
	if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_EOP)) {
		/* Clear FLASH End of Operation pending bit */
		__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP);
	}

	if (__HAL_FLASH_GET_FLAG(
			(FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR | FLASH_FLAG_RDERR))
			!= RESET) {
		/*Save the error code*/
		inline_FLASH_SetErrorCode();
		return HAL_ERROR;
	}

	/* If there is no error flag set */
	return HAL_OK;

}

/**
 * @brief  Program byte, halfword, word or double word at a specified address
 * @param  TypeProgram  Indicate the way to program at a specified address.
 *                           This parameter can be a value of @ref FLASH_Type_Program
 * @param  Address  specifies the address to be programmed.
 * @param  Data specifies the data to be programmed
 *
 * @retval HAL_StatusTypeDef HAL Status
 */
static inline HAL_StatusTypeDef inline_HAL_FLASH_Program(uint32_t TypeProgram,
		uint32_t Address, uint64_t Data) {
	HAL_StatusTypeDef status = HAL_ERROR;

	/* Process Locked */
	__HAL_LOCK(&pFlash);

	/* Check the parameters */
	assert_param(IS_FLASH_TYPEPROGRAM(TypeProgram));

	/* Wait for last operation to be completed */
	status = inline_FLASH_WaitForLastOperation((uint32_t) FLASH_TIMEOUT_VALUE);

	if (status == HAL_OK) {
		if (TypeProgram == FLASH_TYPEPROGRAM_BYTE) {
			/*Program byte (8-bit) at a specified address.*/
			inline_FLASH_Program_Byte(Address, (uint8_t) Data);
		}

		/* Wait for last operation to be completed */
		status = inline_FLASH_WaitForLastOperation(
				(uint32_t) FLASH_TIMEOUT_VALUE);

		/* If the program operation is completed, disable the PG Bit */
		FLASH->CR &= (~FLASH_CR_PG);
	}

	/* Process Unlocked */
	__HAL_UNLOCK(&pFlash);

	return status;
}

/**
 * @brief Copy the flash routine
 *
 * This routine copies the copy-function to RAM, so that the Flash memory is free to be manipulated
 */
void intDnfccFlasherCopy() {

	char targetBytes[5000];
	uint32_t pMemory;
	uint32_t tMemory;
	void (*copyFunction)(void);
	copyFunction = &intDnfccCopyFlash;
	pMemory = (uint32_t) copyFunction;
	pMemory = (pMemory / 16) * 16; // Get a few bytes in front...
	tMemory = (uint32_t) targetBytes;
	tMemory = (tMemory / 16) * 16 + 16;

	memcpy((uint8_t *) tMemory, (uint8_t *) pMemory, 4000);

	uint32_t offset = (uint32_t) copyFunction;
	offset = offset % 16;
	copyFunction = (void *) (tMemory + offset);

	__disable_irq();

	HAL_FLASH_Unlock();

	copyFunction();
}

/** @} */
