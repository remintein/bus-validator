/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"

#define FLASH_PROGRAMMER_COMMUNICATION
#include "FlashProgrammer.h"
#include <string.h>
#include <ctype.h>
#include "../_definition/deviceStatus.h"

#include "mbedtls/sha256.h"
#include "mbedtls/md5.h"

/**
 * @file
 * @brief Flash copy routine
 *
 * Here the routines needed to flash from memory are specified
 */

/**
 * @addtogroup FlashProgrammer
 * @{
 */

#define STACK_SIZE 600

void intDnfccFlasherCopy();

/**
 * @brief FlashProgrammer initialization routine
 *
 * @param prio - FreeRTOS priority
 */

BaseType_t nfccFlashProgrammerInit(UBaseType_t prio) {
	deviceStatus.flashProgrammerRxQueue = xQueueCreate(
			FLASH_PROGRAMMER_QUEUE_LENGTH, FLASH_PROGRAMMER_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.flashProgrammerRxQueue, "Flash");

	setWaitForSystemReadyThisTask(TASK_BIT_FLASHPROGRAMMER);
	return xTaskCreate(nfccFlashProgrammer, "flashProgrammer", STACK_SIZE, NULL,
			prio, &deviceStatus.flashProgrammerTask);
}


/**
 * @brief FlashProgrammer main routine
 *
 * The following tasks are defined:
 * - Incrementing time / date on second basis
 * - Sending the new Time or Date when changed to the display unit
 * - When a new time or date is transmitted to here, pick it up and use it.
 *
 * This routine will wait for 200 clock ticks (xQueueReceive)
 */
void nfccFlashProgrammer(void *pvParameters) {
	char buf[FLASH_PROGRAMMER_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;

	uint8_t shaChecksum[41];
	uint32_t newLength;
	uint32_t writePosition;
	uint8_t newVersion[5];
	int32_t length;
	int i;
	int len;

	setTaskIsSystemReady(TASK_BIT_FLASHPROGRAMMER);
	waitForSystemReady();

	for (;;) {
		buf[0] = '\0';
		if (xQueueReceive(deviceStatus.flashProgrammerRxQueue, buf,
				200) == pdPASS) {

			// The following commands are accepted:
			// First character of the command

			// E - Erase the application memory 1 or 2
			// L - Store length and version of the new application
			// W - Write data to memory
			// S - SHA-256 checksum, store
			// M - MD5 checksum, store
			// V  Verify application, and if OK, switch
			// G - Get memory as stated
			// ?  Run selftest

			// Date is send directly here, as it changes in a too less frequency, time is sent when changing

			switch (buf[0]) {
			case COMCOM_FLASH_UPDATE_FIRMWARE:

				stbuf[0] = COMCOM_DATADISTRIBUTOR_INIT_FILE;
				strncpy(stbuf + 1, buf + 1, 8);
				queueSendToDataDistributor(stbuf, portMAX_DELAY);

				break;

			case COMCOM_FLASH_INIT_FILE:
				strncpy((char *) newVersion, buf + 5, 4);
				newVersion[4] = '\0';
				sscanf(buf + 9, "%ld", &newLength);
				strncpy((char *) shaChecksum, buf + 18, 40);
				writePosition = 0;

				HAL_FLASH_Unlock();

				// Step 1: Clear the flash on the device
				// Do this as least intrusive as possible
				// So only when no prio is requested, and 1 block and then wait for a short while
				for (int sector = 8; sector <= 11; sector++) {
					// We might have to swallow the messages, but I do not think so (queue full, wait)
					while (apiDnfccGetPriority() != PRIORITY_IDLE) {
						vTaskDelay(100);
					}
					FLASH_Erase_Sector(sector, VOLTAGE_RANGE_3);
					vTaskDelay(1000);
				}

				HAL_FLASH_Lock();

				// Request data...
				stbuf[0] = COMCOM_DATADISTRIBUTOR_GET_DATA_FROM_FILE;
				memcpy(stbuf + 1, NAME_FILE_FIRMWARE, 4);
				memcpy(stbuf + 5, newVersion, 4);
				len = BLOCKLENGTH_FILEDATA;
				if (writePosition + len > newLength) {
					len = newLength - writePosition;
				}
				sprintf(stbuf + 9, "%ld,%d", writePosition, len);
				queueSendToDataDistributor(stbuf, portMAX_DELAY);

				break;

			case COMCOM_FLASH_DATA_FOR_FIRMWARE:

				sscanf(buf + 5, "%lu", &writePosition);
				sscanf(buf + 13, "%ld", &length);
				char cnvBuf[BLOCKLENGTH_FILEDATA + 1];
				// TODO: Check if MD5sum is correct
				HAL_FLASH_Unlock();
				for (i = 0; i < length; i++) {
					sscanf(buf + 50 + i * 2, "%2hhx", cnvBuf + i);
					HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,
							writePosition + i + 0x8080000, cnvBuf[i]);
				}
				HAL_FLASH_Lock();

				writePosition += length;
				if (writePosition == newLength) {
					queueSendToDisplay("lFlashing in 2 seconds", 2000);
					vTaskDelay(2000);
					strncpy((char *) deviceStatus.firmwareVersion,
							(char *) newVersion, 4);
					intDnfccFlasherCopy();
					while (1) {
					};
				} else {
					// Request data...
					stbuf[0] = COMCOM_DATADISTRIBUTOR_GET_DATA_FROM_FILE;
					memcpy(stbuf + 1, NAME_FILE_FIRMWARE, 4);
					memcpy(stbuf + 5, newVersion, 4);
					len = BLOCKLENGTH_FILEDATA;
					if (writePosition + len > newLength) {
						len = newLength - writePosition;
					}
					sprintf(stbuf + 9, "%lu,%d", writePosition, len);
					queueSendToDataDistributor(stbuf, portMAX_DELAY);
				}
				break;

			case '?':
				uxHighWaterMark = uxTaskGetStackHighWaterMark(
						deviceStatus.flashProgrammerTask);
				sprintf(stbuf, "!FWM%ld", uxHighWaterMark);
				stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
				stbuf[1] = COMSELFTEST_FLASHPROGRAMMER;
				queueSendToDataDistributor(stbuf, portMAX_DELAY);
				break;

			} // Switch[receive buffer]}
		} // Endless for loop
	}
}
/** @} */
