/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "CanBus.h"
#include "../_definition/CanbusMessages.h"
#include <string.h>
#include <stdlib.h>

void nfccCanbusInitHardware();
void Error_Handler();
void intDnfccProcessCanMessage(CanRxMsgTypeDef *message);

/**
 * @file
 * @brief Contains the functions for the canbus module
 *
 * This file contains the C-routines for the canbus module.
 *
 * Canbus routine
 *
 * The canbus module will use the canbus hardware as embedded in the STM32F407 processor.
 *
 * In the main loop it will
 * - Get the message from the CANBus is available.
 *   The data retrieved will be parsed and the correct action will be taken.
 * - Read the messages from the rest of the system, and act accordingly.
 *   This can mean sending data through the canbus interface
 *
 * This routine will wait for 100 clock ticks (xQueueReceive)
 */

/**
 * Size of the stack of the canbus-main routine
 */
#define STACK_SIZE 250
/**
 * Hardware handle for the can hardware
 */
CAN_HandleTypeDef hcan1;
/**
 * @addtogroup canbus
 * @{
 */

/**
 * @brief initialization routine for CAN module
 *
 * @param uxPriority - The FreeRTOS priority
 */

BaseType_t nfccCanBusInit(UBaseType_t uxPriority) {
	BaseType_t out;
	deviceStatus.canbusRxQueue = xQueueCreate(CANBUS_QUEUE_LENGTH,
			CANBUS_BUFFER_SIZE);

	vQueueAddToRegistry(deviceStatus.canbusRxQueue, "Canbus");

	nfccCanbusInitHardware();

	setWaitForSystemReadyThisTask(TASK_BIT_CANBUS);

	out = xTaskCreate(nfccCanBus, "canbus", STACK_SIZE, NULL, uxPriority,
			&deviceStatus.canbusTask);
	if (out != pdPASS) {
		return out;
	}

	return pdPASS;
}


/**
 * Default CAN Bus ID. Can be different based on the role of the device
 */
#if CURRENT_TARGET == CNS_BUSCONSOLE
int dnfccCanbusId = 0;
#else // not sufficient if there are more than one validator on CAN bus
int dnfccCanbusId = 1;
#endif

/**
 * @brief Transmit a message through the CAN bus
 *
 * @param messId - Id of the message
 * @param - Data to transmit
 * @param length - length of the data
 */
static void nfccCanTransmitMessage(uint16_t messId, uint8_t *data,
		uint8_t length) {
	CanTxMsgTypeDef message;

	message.Data[0] = data[0];
	message.Data[1] = data[1];
	message.Data[2] = data[2];
	message.Data[3] = data[3];
	message.Data[4] = data[4];
	message.Data[5] = data[5];
	message.Data[6] = data[6];
	message.Data[7] = data[7];
	message.DLC = length;
	message.StdId = messId | (dnfccCanbusId & CANBUS_MSG_MASK_SOURCE);
	message.ExtId = 0;
	message.RTR = CAN_RTR_DATA;
	message.IDE = CAN_ID_STD;

	hcan1.pTxMsg = &message;
	HAL_CAN_Transmit_IT(&hcan1);
}
/**
 * Initialize the hardware (CAN interface)
 */
void nfccCanbusInitHardware() {
	HAL_StatusTypeDef res;
	// Setup CAN1
	hcan1.Instance = CAN1;
	hcan1.Init.Prescaler = 450;
	hcan1.Init.Mode = CAN_MODE_NORMAL;
	hcan1.Init.SJW = CAN_SJW_1TQ;
	hcan1.Init.BS1 = CAN_BS1_3TQ;
	hcan1.Init.BS2 = CAN_BS2_5TQ;
	hcan1.Init.TTCM = DISABLE;
	hcan1.Init.ABOM = DISABLE;
	hcan1.Init.AWUM = DISABLE;
	hcan1.Init.NART = DISABLE;
	hcan1.Init.RFLM = DISABLE;
	hcan1.Init.TXFP = DISABLE;
	if ((res = HAL_CAN_Init(&hcan1)) != HAL_OK) {
		Error_Handler();
	}

	CAN_FilterConfTypeDef sFilterConfig;

	sFilterConfig.FilterNumber = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x0000;
	sFilterConfig.FilterIdLow = 0x0000;
	sFilterConfig.FilterMaskIdHigh = 0x0000;
	sFilterConfig.FilterMaskIdLow = 0x0000;
	sFilterConfig.FilterFIFOAssignment = 0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.BankNumber = 14;

	if (HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig) != HAL_OK) {
		/* Filter configuration Error */
		Error_Handler();
	}
}


#if defined(VALIDATOR_SIMULATOR) &&  VALIDATOR_SIMULATOR == 1
int simCount = 0;
#endif
/**
 * @brief Main loop for the CAN module
 *
 * @param pvParameters - parameters sent by FreeRTOS
 */
void nfccCanBus(void *pvParameters) {
	char buf[CANBUS_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;

	CanRxMsgTypeDef recMessage;
	hcan1.pRxMsg = &recMessage;

	setTaskIsSystemReady(TASK_BIT_CANBUS);
	waitForSystemReady();

	for (;;) {
		buf[0] = '\0';

		if ( xQueueReceive(deviceStatus.canbusRxQueue, buf,
				100) != pdPASS) {
			uint8_t ior = HAL_CAN_Receive(&hcan1, 0, 1);
// TODO: Move this to interrupt routine
			if (ior == 0) {
				intDnfccProcessCanMessage(&recMessage);
			}
			vTaskDelay(1);
#if defined(VALIDATOR_SIMULATOR) &&  VALIDATOR_SIMULATOR == 1
			char simBuf[MENU_BUFFER_SIZE];
			simBuf[1] = '\0';
// CAN simulate each status every x seconds
			switch (simCount++ % 4) {
				case 0:
				simBuf[0] = COMCOM_MENU_VALIDATOR_ENTER_SUCCESSFULL;
				break;
				case 1:
				simBuf[0] = COMCOM_MENU_VALIDATOR_ENTER_SUCCESSFULL_WARNING;
				break;
				case 2:
				simBuf[0] = COMCOM_MENU_VALIDATOR_ENTER_REJECT;
				break;
				case 3:
				simBuf[0] = COMCOM_MENU_VALIDATOR_ENTER_RETRY;
				break;
			}
			queueSendToMenu(simBuf, 3000);
#endif

			continue;
		}
		unsigned long int v;
		char *ptr;
		uint8_t canMsg[8];
		canMsg[0] = deviceStatus.deviceId[0];
		canMsg[1] = deviceStatus.deviceId[1];
		canMsg[2] = deviceStatus.deviceId[2];
		canMsg[3] = deviceStatus.deviceId[3];

		switch (buf[0]) {
		case COMCOM_CANBUS_VALID_IN:
			v = strtoul(buf + 1, &ptr, 10); // convert text value to long int
			memcpy(canMsg + 4, &v, sizeof(long int));
			nfccCanTransmitMessage(CANBUS_MSG_VALIDATOR_ENTER_OK, canMsg, 8);
			break;

		case COMCOM_CANBUS_VALID_IN_LOWVAL:
			v = strtoul(buf + 1, &ptr, 10);
			memcpy(canMsg + 4, &v, sizeof(long int));
			nfccCanTransmitMessage(CANBUS_MSG_VALIDATOR_ENTER_OKLOWVAL, canMsg,
					8);
			break;

		case COMCOM_CANBUS_REJECT_IN:
			nfccCanTransmitMessage(CANBUS_MSG_VALIDATOR_ENTER_REJECT, canMsg,
					4);
			break;

		case COMCOM_CANBUS_CARD_ENTER_RETRY:
			nfccCanTransmitMessage(CANBUS_MSG_VALIDATOR_ENTER_RETRY, canMsg, 4);
			break;

		case COMCOM_CANBUS_DISABLE_VALIDATORS:
			nfccCanTransmitMessage(CANBUS_MSG_DISABLE_VALIDATORS, canMsg, 4);
			break;

		case COMCOM_CANBUS_ENABLE_VALIDATORS:
			v = deviceStatus.deviceRouteId;
			memcpy(canMsg + 4, &v, sizeof(long int));
			nfccCanTransmitMessage(CANBUS_MSG_ENABLE_VALIDATORS, canMsg, 8);
			v = deviceStatus.deviceServiceId;
			memcpy(canMsg + 4, &v, sizeof(long int));
			nfccCanTransmitMessage(CANBUS_MSG_ENABLE_VALIDATORS2, canMsg, 8);
			break;

		case COMCOM_CANBUS_ROUTESERVICE_CHANGE: // obsolete
			break;

		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(
					deviceStatus.canbusTask);
			sprintf(stbuf, "!CWMI%ld", uxHighWaterMark);
			sprintf(stbuf + strlen(stbuf), "O%ld", uxHighWaterMark);
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_CANBUS;
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;

		case COMCOM_SELFTEST_DISPLAY:
			sprintf(stbuf, "i%02dCanbus: Testing", 2);
			xQueueSend(deviceStatus.displayRxQueue, stbuf, portMAX_DELAY);
			// ToDo something
			sprintf(stbuf, "i%02dCanbus: OK", 2);
			xQueueSend(deviceStatus.displayRxQueue, stbuf, portMAX_DELAY);
			break;
		}
	}
}
/** @} */

/**
 * @brief Can1 interrupt handler. Will can the HAL-level and receive data if available.
 */
void CAN1_RX0_IRQHandler(void) {
	HAL_CAN_IRQHandler(&hcan1);
	HAL_CAN_Receive_IT(&hcan1, 0);
}

/**
 * @brief Process a CAN message. Based on the message it will also perform the correct action.
 *
 * @param message - the message received
 */
void intDnfccProcessCanMessage(CanRxMsgTypeDef *message) {
	uint32_t route, service, v;

	if ((message->StdId & CANBUS_MSG_MASK_REQUEST_ADDRESS)
			== CANBUS_MSG_REQUEST_ADDRESS) {
		uint8_t data[8];
		data[0] = 7;

		nfccCanTransmitMessage(
				CANBUS_MSG_SUBMIT_ADDRESS
						| (message->StdId & CANBUS_MSG_MASK_REQUEST_ADDRESS_ID),
				data, 1);
		return;
	}
	char stbuf[MENU_BUFFER_SIZE + 1];
	stbuf[1] = '\0';

	switch (message->StdId & 0b111111110000) {
	case CANBUS_MSG_VALIDATOR_ENTER_OK:
		stbuf[0] = COMCOM_MENU_VALIDATOR_ENTER_SUCCESSFULL;
		v = (uint8_t) message->Data[4] | ((uint8_t) message->Data[5] << 8)
				| ((uint8_t) message->Data[6] << 16)
				| ((uint8_t) message->Data[7] << 24);
		sprintf(stbuf + 1, "%ld", v);
		queueSendToMenu(stbuf, 10);
		break;
	case CANBUS_MSG_VALIDATOR_ENTER_OKLOWVAL:
		stbuf[0] = COMCOM_MENU_VALIDATOR_ENTER_SUCCESSFULL_WARNING;
		v = (uint8_t) message->Data[4] | ((uint8_t) message->Data[5] << 8)
				| ((uint8_t) message->Data[6] << 16)
				| ((uint8_t) message->Data[7] << 24);
		sprintf(stbuf + 1, "%ld", v);
		queueSendToMenu(stbuf, 10);
		break;
	case CANBUS_MSG_VALIDATOR_ENTER_REJECT:
		stbuf[0] = COMCOM_MENU_VALIDATOR_ENTER_REJECT;
		queueSendToMenu(stbuf, 10);
		break;
	case CANBUS_MSG_VALIDATOR_ENTER_RETRY:
		stbuf[0] = COMCOM_MENU_VALIDATOR_ENTER_RETRY;
		queueSendToMenu(stbuf, 10);
		break;
	case CANBUS_MSG_DRIVER_LOGIN:
		break;
	case CANBUS_MSG_ENABLE_VALIDATORS:
		stbuf[0] = COMCOM_MENU_ENABLE_VALIDATOR;
		v = (uint8_t) message->Data[4] | ((uint8_t) message->Data[5] << 8)
				| ((uint8_t) message->Data[6] << 16)
				| ((uint8_t) message->Data[7] << 24);
		sprintf(stbuf + 1, "%ld", v);
		queueSendToMenu(stbuf, 10);
		break;
	case CANBUS_MSG_ENABLE_VALIDATORS2:
		stbuf[0] = COMCOM_MENU_ENABLE_VALIDATOR2;
		v = (uint8_t) message->Data[4] | ((uint8_t) message->Data[5] << 8)
				| ((uint8_t) message->Data[6] << 16)
				| ((uint8_t) message->Data[7] << 24);
		sprintf(stbuf + 1, "%ld", v);
		queueSendToMenu(stbuf, 10);
		break;
	case CANBUS_MSG_DISABLE_VALIDATORS:
		deviceStatus.deviceRouteId = 0;
		deviceStatus.deviceServiceId = 0;
		stbuf[0] = COMCOM_MENU_DISABLE_VALIDATOR;
		queueSendToMenu(stbuf, 10);
		break;
	case CANBUS_MSG_ROUTESERVICE_CHANGE:
		sscanf((char *) (message->Data), "%4s%02lx%02lx", stbuf, &route,
				&service);

	}
	message->StdId = 0;
}

/**
 * @brief Variable to hold the clock enable (to enable it only once)
 */
static uint32_t HAL_RCC_CAN1_CLK_ENABLED = 0;

/**
 * @brief HAL_init routine, to set the clock and the pins.
 *
 * @param hcan - handle to hardware can
 */
void HAL_CAN_MspInit(CAN_HandleTypeDef* hcan) {

	GPIO_InitTypeDef GPIO_InitStruct;
	if (hcan->Instance == CAN1) {
		/* Peripheral clock enable */
		HAL_RCC_CAN1_CLK_ENABLED++;
		if (HAL_RCC_CAN1_CLK_ENABLED == 1) {
			__HAL_RCC_CAN1_CLK_ENABLE()
			;
		}

		GPIO_InitStruct.Pin = CANBUS_PIN_RX1;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
		HAL_GPIO_Init(CANBUS_GPIO_RX1, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = CANBUS_PIN_TX1;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
		HAL_GPIO_Init(CANBUS_GPIO_TX1, &GPIO_InitStruct);
	}

}
