/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "../platform_config.h"
#define OTG_COMMUNICATION
#include "otg.h"
#include "../_definition/deviceStatus.h"

#define STACK_SIZE 500

#include "../_definition/deviceStatus.h"
#include "../display/Display.h"


/**
 * @file
 * @brief Contains the functions for the OTG module
 *
 */

/**
 * @addtogroup otg
 *
 * @{
 */

void MX_USB_DEVICE_Init();
uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);

/**
 * @brief initialization routine for GSM/GPS module
 *
 * @param uxPriority - The FreeRTOS priority
 */
BaseType_t nfccOtgInit(UBaseType_t uxPriority) {
	deviceStatus.otgRxQueue = xQueueCreate(OTG_QUEUE_LENGTH, OTG_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.otgRxQueue, "Otg");
	setWaitForSystemReadyThisTask(TASK_BIT_OTG);
	return xTaskCreate(nfccOtg, "Otg", STACK_SIZE, NULL, uxPriority, &deviceStatus.otgTask);
}

uint8_t otgReceiveBuffer[64];

/**
 * @brief the main group for OTG
 *
 * @param pvParameters - the parameters from FreeRTOS
 */
void nfccOtg(void *pvParameters) {
	char buf[OTG_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;

#define DELAY portMAX_DELAY

	MX_USB_DEVICE_Init();


	setTaskIsSystemReady(TASK_BIT_OTG);
	waitForSystemReady() ;

	for (;;) {

		buf[0] = '\0';
		if (xQueueReceive(deviceStatus.otgRxQueue, buf, DELAY) != pdPASS) {
			vTaskDelay(1);
			CDC_Transmit_FS((uint8_t *)"Please wait\n\r", 13);
			continue;
		}
		vTaskDelay(100);

		switch (buf[0]) {

		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(deviceStatus.otgTask);
			sprintf(stbuf, "!EWM%ld", uxHighWaterMark);
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_QRCODE;
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;

		}
	}
}

/** @} */
