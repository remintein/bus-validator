/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

/**
 * @file
 * @addtogroup main
 * @{
 */
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include "project_includes.h"
#include "_definition/deviceStatus.h"
#include "canbus/CanBus.h"
#include "clock/Clock.h"
#include "display/Display.h"
#include "flashProgrammer/FlashProgrammer.h"
#include "otg/otg.h"
#include "printer/printer.h"
#include "sam/sam.h"
#include "sdio/Sdio.h"
#include "smartcardif/SmartcardIf.h"
#include "touch/touch.h"
#include "wifi/wifi.h"
#include "menu/menu.h"
#include "datadistributor/datadistributor.h"
#include "gsmgps/gsm.h"
#include "qrcode/qrcode.h"
#include "sound/sound.h"

void Error_Handler();
void initDeviceStatus();
void initializeHardware();
void xPortSysTickHandler(void);
static int checkForError(BaseType_t t);



static int freeRTOSstarted = 0;

/**
 * @brief Main routine.
 *
 * Main routine will call all init routines and then start the FreeRTOS scheduler
 *
 * @return should not return
 */
int main(void) {
	BaseType_t tmp;

	/* Initialize the HAL of the processor, and get the ticks going. */
	HAL_Init();
	HAL_ResumeTick();
	initDeviceStatus();

	tmp = pdPASS;
	initializeHardware();
	/* Initialize the application tasks, Argument is the priority. - lower = more priority. It will also do the xTaskCreate for that module */
	tmp = nfccSoundInit(4);
	checkForError(tmp);
#if CURRENT_TARGET == CNS_BUSCONSOLE || CURRENT_TARGET == CNS_VALIDATOR
	tmp = nfccCanBusInit(3);
#endif
	checkForError(tmp);
	tmp = nfccClockInit(1);
	checkForError(tmp);
	tmp = nfccDisplayInit(1);
	checkForError(tmp);
	tmp = nfccFlashProgrammerInit(5);
	checkForError(tmp);
	tmp = nfccGsmGpsInit(5);
	checkForError(tmp);
	tmp = nfccOtgInit(5);
	checkForError(tmp);
	tmp = nfccPrinterInit(5);
	checkForError(tmp);
	tmp = nfccSdioInit(2);
	checkForError(tmp);
	tmp = nfccSmartcardIfInit(2);
	checkForError(tmp);
#if DNFCC_ENABLE_WIFI == 1
	tmp = nfccapiDnfccWifiEngineInit(2);
#endif
	checkForError(tmp);
	tmp = nfccQrcodeInit(2);
	checkForError(tmp);
	tmp = nfccSamInit(2);
	checkForError(tmp);
#if  CURRENT_TARGET != CNS_VALIDATOR
	tmp = nfccTouchInit(2);
	checkForError(tmp);
#endif
	tmp = nfccDatadistributorInit(4);
	checkForError(tmp);
	tmp = nfccMenuInit(4);
	checkForError(tmp);

	/* Start the RTOS scheduler, this function should not return as it causes the
	 execution context to change from main() to one of the created tasks. */
	freeRTOSstarted = 1;
#if  DNFCC_NOLOGIN == 1
	char dBuf[MENU_BUFFER_SIZE];
#if CURRENT_TARGET == CNS_VALIDATOR
	sprintf(dBuf, "%c3", COMCOM_MENU_ENABLE_VALIDATOR);
	queueSendToMenu(dBuf, portMAX_DELAY);
	sprintf(dBuf, "%c5", COMCOM_MENU_ENABLE_VALIDATOR2);
	queueSendToMenu(dBuf, portMAX_DELAY);
#else
	dBuf[0] = COMCOM_MENU_STARTSCREEN;
	queueSendToMenu(dBuf, portMAX_DELAY);
#endif
#endif
	vTaskStartScheduler();

	/* Should never get here */
	return 0;
}

/**
 * @brief SysTick handler.
 * Should call  the FreeRTOS handler (but only after start)
 */
void SysTick_Handler(void) {
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();
	if (freeRTOSstarted) {
		xPortSysTickHandler();
	}
}

/**
 * @brief check if the argument is pdPASS, if not call Error_Handler
 * @param t - variable to check
 */
static int checkForError(BaseType_t t) {
	if (t == pdPASS) {
		return 0;
	}
	Error_Handler();
	return 1;
}
/**
 * @brief Holds the processing. But with an infinitive loop which can be broken, that is for debugging
 */
void Error_Handler() {
	int t = 1;
	while (1) {
		if (t == 0) {
			return;
		}
	}
	return;
}
/** @} */
