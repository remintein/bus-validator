/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "../platform_config.h"
#define PRINTER_COMMUNICATION
#include "printer.h"
#include "../_library/serialEngine/serialEngine.h"

#include "../_definition/deviceStatus.h"
#include "../sdio/FileFunctions.h"
#include "../clock/Clock.h"
#include <string.h>

/**
 * @file
 * @brief Contains the functions for the printer module
 */

/**
 * @addtogroup printer
 *
 * @{
 */
extern UART_HandleTypeDef huart_qrcodeprinter;

extern SdioBlockInfo sdioPrinterDataPointer;
extern SdioBlockInfo sdioFirstBlockDataPointer;
uint8_t printerTransmitBuffer[100];

#define STACK_SIZE 400

/**
 * @brief Initialize printer
 *
 * @param uxPriority - the prio to run on
 */
BaseType_t nfccPrinterInit(UBaseType_t uxPriority) {
	deviceStatus.printerRxQueue = xQueueCreate(PRINTER_QUEUE_LENGTH,
			PRINTER_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.printerRxQueue, "Printer");
	setWaitForSystemReadyThisTask(TASK_BIT_PRINTER);
	return xTaskCreate(nfccPrinter, "Printer", STACK_SIZE, NULL, uxPriority,
			&deviceStatus.printerTask);
}

/**
 * @brief Printer main loop
 *
 * The main loop for the printer
 */
void nfccPrinter(void *pvParameters) {
	char buf[PRINTER_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;
	uint8_t *p;
	uint8_t *printdata;

	apiDnfccSerialEngineStartListening(&huart_qrcodeprinter, QRCODEPRINTER_UART_NR, 0,
			0, printerTransmitBuffer, 350);

	setTaskIsSystemReady(TASK_BIT_PRINTER);
	waitForSystemReady();

	for (;;) {

		buf[0] = '\0';
		if (xQueueReceive(deviceStatus.printerRxQueue, buf,
				portMAX_DELAY) != pdPASS) {
			vTaskDelay(1);
			continue;
		}


		sprintf(stbuf, "Got command: %c", buf[0]);
		logPrinterString(stbuf);

		switch (buf[0]) {
		case COMCOM_PRINT_TICKET:

			while (sdioFirstBlockDataPointer.readStatus == EMPTY) {
				vTaskDelay(10);
			}
			apiDnfccReadSector(&sdioPrinterDataPointer, (DWORD) 0);
			printdata = sdioPrinterDataPointer.data;
			for (p = printdata; *p != '\0'; p++) {
				if (strncmp((char *) p, "%R", 2) == 0) {
					sprintf(stbuf, "%lu", deviceStatus.deviceRouteId);
					apiDnfccSerialEngineTransmit(stbuf, QRCODEPRINTER_UART_NR);
					p++;
				} else if (strncmp((char *) p, "%P", 2) == 0) {
					sprintf(stbuf, "%lu", deviceStatus.penaltyCard);
					apiDnfccSerialEngineTransmit(stbuf, QRCODEPRINTER_UART_NR);
					p++;
				} else if (strncmp((char *) p, "%p", 2) == 0) {
					sprintf(stbuf, "%lu", deviceStatus.penaltyCash);
					apiDnfccSerialEngineTransmit(stbuf, QRCODEPRINTER_UART_NR);
					p++;
				} else if (strncmp((char *) p, "%S", 2) == 0) {
					sprintf(stbuf, "%lu", deviceStatus.deviceServiceId);
					apiDnfccSerialEngineTransmit(stbuf, QRCODEPRINTER_UART_NR);
					p++;
				} else if (strncmp((char *) p, "%D", 2) == 0) {
					apiDnfccClockFetchDateTimeHuman(stbuf);
					apiDnfccSerialEngineTransmit(stbuf, QRCODEPRINTER_UART_NR);
					p++;
				} else if (strncmp((char *) p, "%A", 2) == 0) {
					apiDnfccSerialEngineTransmit(buf + 1, QRCODEPRINTER_UART_NR);
					p++;
				} else {
					stbuf[0] = *p;
					stbuf[1] = '\0';

					apiDnfccSerialEngineTransmit(stbuf, QRCODEPRINTER_UART_NR);
					if (*p == '\n') {
						vTaskDelay(200);
						if ((*p + 1) != '\r') {
							apiDnfccSerialEngineTransmit("\r", QRCODEPRINTER_UART_NR);
						}
					}
				}
			}
			break;

		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(
					deviceStatus.printerTask);
			sprintf(stbuf, "!PWM%ld", uxHighWaterMark);
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_PRINTER;
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;
		}
	}
}

/** @} */
