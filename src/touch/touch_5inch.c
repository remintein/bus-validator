/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"
#include "touch.h"

/**
 * @file
 * @brief Contains the functions for the touch; implementation 5 inch
 *
 */

/**
 * @addtogroup touch
 *
 * @{
 */
#if DISPLAY_SIZE == 5

#define STACK_SIZE 500

extern I2C_HandleTypeDef i2cTouch;

/**
 * @brief Touch routine
 *
 * The following tasks are defined:
 * - Incrementing time / date on second basis
 * - Sending the new Time or Date when changed to the display unit
 * - When a new time or date is transmitted to here, pick it up and use it.
 *
 * This routine will wait for 100 ms for polling (xQueueReceive)
 *
 * @param prio - the FreeRTOS priority
 */
BaseType_t nfccTouchInit(UBaseType_t prio) {
	deviceStatus.touchRxQueue = xQueueCreate(TOUCH_QUEUE_LENGTH, TOUCH_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.touchRxQueue, "Touch");

	setWaitForSystemReadyThisTask(TASK_BIT_TOUCH);

	return xTaskCreate(nfccTouch, "touch", STACK_SIZE, NULL, prio, &deviceStatus.touchTask);
}

#define SLAVE_ADDRESS 0x40

#define WRITE_ADD 0x80
#define READ_ADD 0x81

/**
 * @brief I2C read function
 * @param reg - pointer to the register to read
 * @param addr - Address to send the resulting data to
 * @param cnt - The number of reads to perform
 */
static void GSLX680_I2C_Read(uint8_t *reg, uint8_t *addr, ulong cnt) {
	HAL_I2C_Master_Transmit_IT(&i2cTouch, WRITE_ADD, reg, 1);
	while (HAL_I2C_GetState(&i2cTouch) != HAL_I2C_STATE_READY) {
		vTaskDelay(1);
	}
	HAL_I2C_Master_Receive_IT(&i2cTouch, READ_ADD, addr, cnt);
}

/**
 * @brief the main group for Touch
 *
 * @param pvParameters - the parameters from FreeRTOS
 */
void nfccTouch(void *pvParameters) {
	char buf[TOUCH_BUFFER_SIZE + 1];
	char mBuf[MENU_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;
	uint8_t registerToRead = 0x80;
	uint8_t registers[26];
	uint16_t x;
	uint16_t y;
	int lastTouches = 0;
	uint8_t ior;
	TOUCH_Init();
	/*
	 * Start receiving
	 */
	uint8_t slave = 0x81;
	registers[0] = 0x80;

	setTaskIsSystemReady(TASK_BIT_TOUCH);
	waitForSystemReady();

	for (;;) {
		// Loop will be executed every 100ms (100 in xQueueReceive, 100 for waiting on READY)

		/*
		 * When I2C is in the ready state, Receive has been done, do evaluate and start the
		 */
		if (HAL_I2C_GetState(&i2cTouch) == HAL_I2C_STATE_READY) {
			if (registers[0] == 1) {
				// Touch is going on, send this to the menu, for displaying which button is under touch

				x = ((((uint32_t) registers[5]) << 8) | (uint32_t) registers[4]) & 0x00000FFF; // 12 bits of X coord
				y = ((((uint32_t) registers[7]) << 8) | (uint32_t) registers[6]) & 0x00000FFF;

				// Swap X/Y if rotated
				// Update X/Y if rotated
				uint16_t tmp;
				switch (deviceStatus.rotation) {
				case 0:
					break;
				case 1:
					tmp = y;
					y = 800 - x;
					x = tmp;
					break;
				case 2:
					x = 800-x;
					y = 480-y;
					break;
				case 3:
					tmp = y;
					y = x;
					x = 480-tmp;
					break;
				}
				sprintf(mBuf, "%cX%uY%u",
				COMCOM_MENU_TOUCH_CURRENT_POSITION, x, y);
				queueSendToMenu(mBuf, portMAX_DELAY);
			}

			if (registers[0] == 0 && lastTouches == 1) { // Touch is released, send the x/y coordinate to the menu. This is the point of the action
				sprintf(mBuf, "%cX%uY%u", COMCOM_MENU_TOUCH_POSITION, x, y);
				queueSendToMenu(mBuf, portMAX_DELAY);
			}

			lastTouches = registers[0];

			vTaskDelay(50); // Wait a bit between
			/*
			 * Handling of touch status is done, now restart touch retrieve
			 */
			GSLX680_I2C_Read(&registerToRead, registers, 24);
		}

		if (xQueueReceive(deviceStatus.touchRxQueue, buf, 50) != pdPASS) {
			continue;
		}

		switch (buf[0]) {
		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(deviceStatus.touchTask);
			sprintf(stbuf, "!TWM%ld", uxHighWaterMark);
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_TOUCH;
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;
		} // Switch[receive buffer]
	} // Endless for loop
}

/**
 * @brief IRQ handler for i2c
 */
void I2C2_EV_IRQHandler(void) {
	HAL_I2C_EV_IRQHandler(&i2cTouch);

}

/**
 * @brief IRQ error handler for i2c
 */
void I2C2_ER_IRQHandler(void) {
	HAL_I2C_ER_IRQHandler(&i2cTouch);
}

/** @} */
#endif
