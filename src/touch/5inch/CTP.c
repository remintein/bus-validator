/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"

/**
 * @file
 * @brief Contains the functions for the touch; implementation 5 inch
 *
 */

/**
 * @addtogroup touch
 *
 * @{
 */

#if DISPLAY_SIZE == 5
#include "gslX680.h"

extern I2C_HandleTypeDef i2cTouch;

#define WRITE_ADD 0x80
#define READ_ADD 0x81

/**
 * @brief Write routine to register
 * @param reg - Register to write
 * @param addr - Address with data to write
 * @param cnt - Count to write
 */
static void GSLX680_I2C_Write(uint8_t *reg, uint8_t *addr, ulong cnt) {
	uint i = 0;
	uint8_t buffer[30];
	buffer[0] = *reg;
	memcpy(buffer + 1, addr, cnt);
	i = HAL_I2C_Master_Transmit(&i2cTouch, WRITE_ADD, buffer, cnt + 1, 1900);
}

/**
 * @brief Clear register
 */
static void _GSLX680_clr_reg(void) {
	uint8_t addr;
	uint8_t Wrbuf[4];

	addr = 0xe0;
	Wrbuf[0] = 0x88;
	GSLX680_I2C_Write(&addr, Wrbuf, 1);
	vTaskDelay(1); //delayus(200);
	addr = 0x80;
	Wrbuf[0] = 0x01;
	GSLX680_I2C_Write(&addr, Wrbuf, 1);
	vTaskDelay(1); //delayus(50);
	addr = 0xe4;
	Wrbuf[0] = 0x04;
	GSLX680_I2C_Write(&addr, Wrbuf, 1);
	vTaskDelay(1); //delayus(50);
	addr = 0xe0;
	Wrbuf[0] = 0x00;
	GSLX680_I2C_Write(&addr, Wrbuf, 1);
	vTaskDelay(1); //delayus(50);
}

/**
 * @brief reset chip
 */
static void _GSLX680_reset_chip(void) {
	uint8_t addr;
	uint8_t Wrbuf[4];

	addr = 0xe0;
	Wrbuf[0] = 0x88;
	GSLX680_I2C_Write(&addr, Wrbuf, 1);
	vTaskDelay(1); //	delayus(50);

	addr = 0xe4;
	Wrbuf[0] = 0x04;
	GSLX680_I2C_Write(&addr, Wrbuf, 1);
	vTaskDelay(1); //delayus(50);

	addr = 0xbc;
	Wrbuf[0] = 0x00;
	Wrbuf[1] = 0x00;
	Wrbuf[2] = 0x00;
	Wrbuf[3] = 0x00;
	GSLX680_I2C_Write(&addr, Wrbuf, 4);
	vTaskDelay(1); //	delayus(50);

}

/**
 * @brief load the firmware to the chip. This needs to be done every power-on
 */
static void _GSLX680_load_fw(void) {
	uint8_t addr;
	uint8_t Wrbuf[4];
	uint source_line = 0;
	uint source_len = sizeof(GSLX680_FW) / sizeof(struct fw_data);

	for (source_line = 0; source_line < source_len; source_line++) {
		addr = GSLX680_FW[source_line].offset;
		Wrbuf[0] = (char) (GSLX680_FW[source_line].val & 0x000000ff);
		Wrbuf[1] = (char) ((GSLX680_FW[source_line].val & 0x0000ff00) >> 8);
		Wrbuf[2] = (char) ((GSLX680_FW[source_line].val & 0x00ff0000) >> 16);
		Wrbuf[3] = (char) ((GSLX680_FW[source_line].val & 0xff000000) >> 24);

		GSLX680_I2C_Write(&addr, Wrbuf, 4);
		if (source_line % (source_len / 100) == 0) {
			vTaskDelay(1);
		}
	}

}

/**
 * @brief startup touch chip
 */
static void _GSLX680_startup_chip(void) {
	uint8_t addr;
	uint8_t Wrbuf[4];

	addr = 0xe0;
	Wrbuf[0] = 0x00;
	GSLX680_I2C_Write(&addr, Wrbuf, 1);
}

/**
 * @brief initialize touch hardware
 */
void TOUCH_Init(void) {
	HAL_GPIO_WritePin(TOUCH_GPIO_WAKE, TOUCH_PIN_WAKE, GPIO_PIN_SET);
	vTaskDelay(20);
	HAL_GPIO_WritePin(TOUCH_GPIO_WAKE, TOUCH_PIN_WAKE, GPIO_PIN_RESET);
	vTaskDelay(20);
	HAL_GPIO_WritePin(TOUCH_GPIO_WAKE, TOUCH_PIN_WAKE, GPIO_PIN_SET);
	vTaskDelay(20);
	_GSLX680_clr_reg();
	_GSLX680_reset_chip();
	_GSLX680_load_fw();
////	_GSLX680_startup_chip();
	_GSLX680_reset_chip();
	_GSLX680_startup_chip();
}
/** @} */
#endif
