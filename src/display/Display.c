/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "Display.h"

#include "RA8875-0.70/RA8875.h"
#include "font.h"
#include "icons.h"
#include "time.h"
#include "string.h"
#include "button.h"

#include "../smartcardif/DesfireFunctions.h"


/**
 * @file
 * @brief Contains the functions for the display
 *
 */

/**
 * @addtogroup display
 *
 * @{
 */


#define STACK_SIZE 500


#define LOG_START_PIXEL 20

#define TOPBAR 300  // y positions
#define BOTTOMBAR 430
#define BORDER 10


/**
 * @brief Clears screen on specified coordinates
 *
 * @param ra8875 - Pointer to the display struct
 * @param x - start x
 * @param y - start y
 * @param x2 - end x
 * @param y2 end y
 */
static void nfccDisplayClearScreenByCoordinates(RA8875_struct *ra8875, int x,
		int y, int x2, int y2) {
	RA8875_fillRect(ra8875, x, y, x2 - x, y2 - y, RA8875_BLACK);
}

/**
 * @brief Displays the start screen
 *
 * @param ra8875 - Pointer to the display struct
 */
static void nfccDisplayStartScreen(RA8875_struct *ra8875) {
	int16_t i;
	/*****
	 * Draw the blending from blue to black on top part of the screen
	 */
	for (i = BORDER; i < TOPBAR; i++) {
		RA8875_drawFastHLine(ra8875, BORDER, i, ra8875->_width - 2 * BORDER,
				RA8875_colorInterpolation(0x0090, 0x0020, i, TOPBAR));
	}
	RA8875_drawFastHLine(ra8875, BORDER, TOPBAR - 1,
			ra8875->_width - 2 * BORDER, RA8875_BLACK);
	/*****
	 * Clear the rest of the screen black
	 */
	RA8875_fillRect(ra8875, BORDER, TOPBAR, ra8875->_width - 2 * BORDER,
			ra8875->_height - 2 * BORDER - TOPBAR,
			RA8875_BLACK);
	/*****
	 * Clear the LED's to 'off'
	 */
	apiDnfccDisplayLedControlRed(0);
	apiDnfccDisplayLedControlGreen(0);
	apiDnfccDisplayLedControlBlue(0);
}

static RA8875_struct ra8875;
static int16_t log_X, log_Y;

/**
 * @brief Task initialization routine
 *
 * @param prio - the FreeRTOS priority
 */
BaseType_t nfccDisplayInit(UBaseType_t prio) {
	deviceStatus.displayRxQueue = xQueueCreate(DISPLAY_QUEUE_LENGTH,
			DISPLAY_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.displayRxQueue, "Display");

	setWaitForSystemReadyThisTask(TASK_BIT_DISPLAY);

	return xTaskCreate(nfccDisplay, "display", STACK_SIZE, NULL, prio,
			&deviceStatus.displayTask);
}

/**
 * @brief The freeRTOS task display main routine
 * @param pvParameters - the parameters given by FreeRTOS
 */
void nfccDisplay(void *pvParameters) {
	char buf[DISPLAY_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;

	int x, y, tx, ty;

	// Here it starts, specify screen size and color depth
#if CURRENT_TARGET == CNS_BUSCONSOLE
	RA8875_begin(&ra8875, RA8875_800x480, 16);
#elif CURRENT_TARGET == CNS_VALIDATOR
	RA8875_begin(&ra8875, RA8875_800x480, 16);
	RA8875_setRotation(&ra8875, 3); // rotate 90, portrait, beware: sometimes x,y need to be swapped (text)!
	deviceStatus.rotation = 3;
#elif CURRENT_TARGET == CNS_TOPUP
	RA8875_begin(&ra8875, RA8875_800x480, 16);
	RA8875_setRotation(&ra8875, 3); // rotate 90, portrait, beware: sometimes x,y need to be swapped (text)!
	deviceStatus.rotation = 3;
#elif CURRENT_TARGET == CNS_HANDHELD
	RA8875_begin(&ra8875, RA8875_800x480, 16);
	RA8875_setRotation(&ra8875, 3); // rotate 90, portrait, beware: sometimes x,y need to be swapped (text)!
	deviceStatus.rotation = 3;
#endif

	RA8875_uploadFonts(&ra8875);
#if defined(VN)
	RA8875_setExternalFontRom(&ra8875, GT30L16U2W, /* ER3303_1*/LATIN, ARIAL);
	RA8875_setFont(&ra8875, EXT);
	RA8875_setFontSize(&ra8875, X16);
//	RA8875_setFontExt(&ra8875, &VNFont); // !! textWrite crashes later in RA8875 after this
#endif
	RA8875_uploadFonts(&ra8875);
	RA8875_setTextColor(&ra8875, RA8875_WHITE, RA8875_BLACK);
	RA8875_setFontScale(&ra8875, 0);
	RA8875_getCursorFast(&ra8875, &log_X, &log_Y);

	RA8875_fillRect(&ra8875, 0, 0, ra8875._width, ra8875._height,
	RA8875_LIGHT_ORANGE);

	RA8875_setFontScale(&ra8875, 2);
	RA8875_setTextColor(&ra8875, RA8875_BLACK, RA8875_LIGHT_ORANGE);

	while (deviceStatus.systemReady) {
		sprintf(buf, LOCALE_SYSTEM_WELCOME_PRINT, deviceStatus.systemReady);
		RA8875_textPosition(&ra8875, 100, 100, true);

		RA8875_textWrite(&ra8875, buf, strlen(buf));
		vTaskDelay(150);
		setTaskIsSystemReady(TASK_BIT_DISPLAY);
	}

	RA8875_fillRect(&ra8875, 0, 0, ra8875._width, ra8875._height, RA8875_BLACK);

	nfccDisplayStartScreen(&ra8875);

	for (;;) { // start processing display queue
		vTaskDelay(1);
		buf[0] = '\0';

		if (xQueueReceive(deviceStatus.displayRxQueue, buf, 2000) != pdPASS) {
			continue;
		}

		switch (buf[0]) {

		case COMCOM_DISPLAY_CREATE_BUTTON:
			displayDrawButton(&ra8875, buf);
			break;

		case COMCOM_DISPLAY_ADD_TO_WINDOW:
			displayAddButton(&ra8875, buf);
			break;

		case COMCOM_DISPLAY_CLEAR_AREA_BY_COORD:
			sscanf(buf + 1, "X%dY%dX%dY%d", &x, &y, &tx, &ty);
			nfccDisplayClearScreenByCoordinates(&ra8875, x, y, tx, ty);
			break;

		case COMCOM_DISPLAY_TIME:
			RA8875_setTextColor(&ra8875, RA8875_WHITE, RA8875_DARK_BLUE); // cannot use transparent, clock digits remain visible
#if CURRENT_TARGET == CNS_BUSCONSOLE
					RA8875_setFontScale(&ra8875, 1);
					x = 600;
					y = 60;
#elif CURRENT_TARGET == CNS_TOPUP || CURRENT_TARGET == CNS_HANDHELD || CURRENT_TARGET == CNS_VALIDATOR
					RA8875_setFontScale(&ra8875, 1);
					x = 20;
					y = 330;
#endif
			RA8875_textPosition(&ra8875, x, y, true);
			RA8875_textWrite(&ra8875, buf + 1, 0);
			RA8875_setTextColorForeground(&ra8875, RA8875_WHITE); // transparent background text only
			RA8875_textPosition(&ra8875, x + 1, y + 1, true);
			RA8875_textWrite(&ra8875, buf + 1, 0);
			break;

		case COMCOM_DISPLAY_DATE:
			RA8875_setTextColor(&ra8875, RA8875_LIGHT_GREY,
			RA8875_DARK_BLUE); // shadow transparent background text only
#if CURRENT_TARGET == CNS_BUSCONSOLE
			RA8875_setFontScale(&ra8875, 1);
			x = 580;
			y = 20;

#elif CURRENT_TARGET == CNS_TOPUP || CURRENT_TARGET == CNS_HANDHELD || CURRENT_TARGET == CNS_VALIDATOR
			RA8875_setFontScale(&ra8875, 1);
			x = 20;
			y = 20;
#endif
			RA8875_textPosition(&ra8875, x, y, true);
			RA8875_textWrite(&ra8875, buf + 1, 0);
			RA8875_textPosition(&ra8875, x + 1, y + 1, true);
			RA8875_setTextColorForeground(&ra8875, RA8875_WHITE); // transparent background text only
			RA8875_textWrite(&ra8875, buf + 1, 0);
			break;

		case COMCOM_DISPLAY_LOGGING_START:
			log_Y = LOG_START_PIXEL;
			log_X = 0;

			nfccDisplayClearScreenByCoordinates(&ra8875, 0, 0, 799, 479);
			break;
		case COMCOM_DISPLAY_LOGGING:
			//
			// 'l': Log the following message to the screen
			//
			buf[0] = '>';
			RA8875_setFontScale(&ra8875, 0);
			RA8875_setTextColor(&ra8875, RA8875_BLACK, RA8875_WHITE);
			if (deviceStatus.rotation) {
				RA8875_textPosition(&ra8875, log_Y, log_X, true);
			} else {
				RA8875_textPosition(&ra8875, log_X, log_Y, true);
			}
			for (int i = 0; i < DISPLAY_BUFFER_SIZE; i++) {
				if (buf[i] == '\0') {
					buf[i] = ' ';
					buf[i + 1] = '\0';
				}
			}
			RA8875_setFontScale(&ra8875, 0);
			buf[30] = '\0';
			RA8875_textWrite(&ra8875, buf, 30);
			RA8875_textWrite(&ra8875, "\n", 1);
			int16_t j;
			RA8875_getCursorFast(&ra8875, &j, &log_Y);
			if (log_Y > 450) {
				log_Y = 2;
				log_X += 250;
				if (log_X > 700) {
					log_X = 0;
				}
			}
			if (deviceStatus.rotation) {
				RA8875_textPosition(&ra8875, log_Y, log_X, true);
			} else {
				RA8875_textPosition(&ra8875, log_X, log_Y, true);
			}
			RA8875_textWrite(&ra8875, "......                                 ",
					30);
			RA8875_textPosition(&ra8875, log_X, log_Y, true);
			break;

		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(
					deviceStatus.displayTask);
			sprintf(stbuf, "!DIWM%ld", uxHighWaterMark);
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_DISPLAY;
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;

		}
	}
}

/**  @} */
