/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef _RA8875COLORPRESETS_H_
#define _RA8875COLORPRESETS_H_

// Colors preset (RGB565)
#define	RA8875_BLACK             0x0000
#define 	RA8875_WHITE         0xFFFF

#define	RA8875_RED               0xF800
#define	RA8875_GREEN             0x07E0
#define RA8875_DARK_GREEN         0x0540
#define	RA8875_BLUE              0x001F

#define 	RA8875_CYAN              0x07FF
#define 	RA8875_MAGENTA          0xF81F
#define 	RA8875_YELLOW            0xFFE0
#define 	RA8875_LIGHT_GREY 		 0xB5B2
#define 	RA8875_LIGHT_ORANGE 	 0xFC80
#define 	RA8875_DARK_ORANGE 		 0xFB60
#define 	RA8875_PINK 			 0xFCFF
#define 	RA8875_PURPLE 			 0x8017
#define 	RA8875_GRAYSCALE 		 2113

#define 	RA8875_DARK_GREY 		 0x39C7
#define 	RA8875_GREY 		     0xA554
#define     RA8875_GRANITE	         0x0841
#define     RA8875_DARK_BLUE         0x0004
#endif
