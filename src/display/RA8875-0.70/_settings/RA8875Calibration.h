/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef _RA8875CAL_H_
#define _RA8875CAL_H_


/* Calibration Data --------------------------------*/
/* ------------- TOUCH SCREEN CALIBRATION (how to) -----------
Touch Screen are not all the same and needs a calibration, you will see
yourself by load touchPaint.ino example, try to paint all over the screen!
If you have space on one or more side you prolly need to calibrate values  
    ---- perform calibration ----
To perform the touch screen calibration, load libTouchSCalibration.ino and open serial terminal:
(you have to put data inside RA8875/_utility/RA8875Calibration.h)
1) the lowest value of x by touch the top/left angle of your tft, put value in TOUCSRCAL_XLOW
2) you can get the lowest value of y in the same time, put value in TOUCSRCAL_YLOW
3) the highest value of x by touching the lower/bottom corner of your tft, put the value in TOUCSRCAL_XHIGH
4) in the same manner you get the max value of y, put that value in TOUCSRCAL_XHIGH
*/
#define TOUCSRCAL_XLOW	0//62
#define TOUCSRCAL_YLOW	0//153
#define TOUCSRCAL_XHIGH	0//924
#define TOUCSRCAL_YHIGH	0//917



#endif
