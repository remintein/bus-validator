/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef _ST7735_FONTS_H
	#define _ST7735_FONTS_H

#define __PRGMTAG_ /**/
#define bool uint8_t

	#include <stdio.h>
/*
	#if defined(_FORCE_PROGMEM__)

		typedef struct PROGMEM {
				const uint8_t 	*data;
				uint8_t 		image_width;
				int				image_datalen;
		} tImage;

		typedef struct {
				uint8_t 		char_code;
				const tImage 	*image;
		} tChar;

		typedef struct {
				uint8_t 		length;
				const tChar 	*chars;
				uint8_t			font_width;
				uint8_t			font_height;
				bool 			rle;
		} tFont;
	#else
		typedef struct {
				const uint8_t 	*data;
				uint8_t 		image_width;
				int				image_datalen;
		} tImage;

		typedef struct {
				uint8_t 		char_code;
				const tImage 	*image;
		} tChar;

		typedef struct {
				uint8_t 		length;
				const tChar 	*chars;
				uint8_t			font_width;
				uint8_t			font_height;
				bool 			rle;
		} tFont;
	#endif
	
*/
	typedef struct __PRGMTAG_ {
			const uint8_t 	*data;
			uint8_t 		image_width;
			int				image_datalen;
	} tImage;

	typedef struct {
		    u_int32_t 		char_code;
			const tImage 	*image;
	} tChar;

	typedef struct {
		    u_int32_t 		length;
			const tChar 	*chars;
			uint8_t			font_width;
			uint8_t			font_height;
			bool 			rle;
	} tFont;
	
#endif
