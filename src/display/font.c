/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "font.h"

/**
 * @file
 * @brief Elements of the Vietnam fonts.
 *
 */

/**
 * @addtogroup display
 *
 * @{
 */


#define MAX_FONT_CHARACTERS 10
/**
 * @brief Character font file
 */

const uint8_t myFont[MAX_FONT_CHARACTERS][16] = {

		// Char 0
		{
				0b00000000,
				0b00000000,
				0b00000000,
				0b00001000,
				0b00010100,
				0b00100010,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000 },
		// Char 1
		{
				0b00000100,
				0b00001000,
				0b00010000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000 },
		// Char 2
		{
				0b00010000,
				0b00001000,
				0b00000100,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000 },
		// Char 3
		{
				0b00000000,
				0b00011000,
				0b00100100,
				0b00000100,
				0b00001000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000 },
		// Char 4
		{
				0b00000000,
				0b00000000,
				0b00010000,
				0b00101010,
				0b00000100,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000 },
		// Char 5
		{
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00001000 },
		// Char 6
		{
				0b00000000,
				0b00000000,
				0b00000000,
				0b00100010,
				0b00011100,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000 },
		// Char 7
		{
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000001,
				0b00000001,
				0b00000010,
				0b00000010,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000 },
		// Char 8
				{
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00001111,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000 },
		// Char 9
				{
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00001111,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000,
				0b00000000 }
 };

/**
 * @brief upload the fonts in this file
 * @param ra8875 - The ra8875 structure
 */
void RA8875_uploadFonts(RA8875_struct *ra8875) {
	for (int i = 0; i < MAX_FONT_CHARACTERS; i++) {
		RA8875_uploadUserChar(ra8875, myFont[i], i);
	}
}

/** @}*/
