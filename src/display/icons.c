/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "Display.h"
#include "RA8875-0.70/RA8875.h"
#include "icons.h"
#include "math.h"
#include <stdlib.h>

/**
 * @file
 * @brief Contains the functions for the icons
 *
 */

/**
 * @addtogroup display
 *
 * @{
 */

/**
 * @brief Scales the input according the scale; 240 = 1:1
 *
 * @param val - input
 * @param scale - Scale
 */
static int doScale(int val, int scale) {
	// scale 240 = 1:1
	unsigned out = val * scale;
	out /= 240;
	return out;
}
/**
 * Static integers to use in functions. Static means less overhead, and the current position is retained.
 */
static uint16_t curX, curY;

/**
 * @brief set cursor to the designated coordinates
 *
 * @param ra8873 - RA8875 struct pointer
 * @param x - X position
 * @param y - Y position
 * @param tx - X offset
 * @param ty - Y offset
 * @param scale - Scaling factor
 */
void moveTo(RA8875_struct *ra8875, float x, float y, uint16_t tx, uint16_t ty, int scale) {
	curX = tx + doScale((uint16_t) round(x), scale);
	curY = ty + doScale((uint16_t) round(y), scale);
	RA8875_setXY(ra8875, curX, curY);
}

/**
 * @brief Draw a line to the coordinates
 *
 * @param ra8873 - RA8875 struct pointer
 * @param x - X position
 * @param y - Y position
 * @param col - Color
 * @param tx - X offset
 * @param ty - Y offset
 * @param scale - Scaling factor
 */
void lineTo(RA8875_struct *ra8875, float x, float y, uint16_t col, uint16_t tx, uint16_t ty, int scale) {
	RA8875_drawLine(ra8875, curX, curY, tx + doScale((uint16_t) round(x), scale),
			ty + doScale((uint16_t) round(y), scale), col);
	curX = tx + doScale((uint16_t) round(x), scale);
	curY = ty + doScale((uint16_t) round(y), scale);
}

void DisplayGradientFilledCircle(RA8875_struct *ra8875, unsigned int x, unsigned int y, unsigned int radius,
		unsigned int color1, unsigned int color2, unsigned int scale) {
//
//	Draw a filled circle with radius using vertical direction gradient color between color1 and color 2
//
	uint8_t x1;
	uint8_t y1;
	uint16_t color;

	for (uint8_t i = 0; i < 2 * radius; i++) {
		y1 = abs(radius - i);
		x1 = sqrt((radius * radius) - (y1 * y1));
		color = RA8875_colorInterpolation(color1, color2, i, 2 * radius);
		if (i < radius) {
			RA8875_drawFastHLine(ra8875, x - doScale(x1, scale), y - doScale(y1, scale), doScale(x1 * 2, scale), color);
		} else {
			RA8875_drawFastHLine(ra8875, x - doScale(x1, scale), y + doScale(y1, scale), doScale(x1 * 2, scale), color);
		}
	}
}

/**
 * @brief Display a led
 *
 * @param ra8873 - RA8875 struct pointer
 * @param x - X position
 * @param y - Y position
 * @param radius - Radius of the LED
 * @param color - the color of the LED
 * @param scale - Scaling factor
 */
void DisplayLED(RA8875_struct *ra8875, unsigned int x, unsigned int y, unsigned int radius, unsigned int color,
		unsigned int scale) {
	uint16_t gcol;

	x = doScale(x, scale);
	y = doScale(y, scale);
	radius = doScale(radius, scale);
	// draw metal edge
	RA8875_fillCircle(ra8875, x, y, radius, RA8875_BLACK);
	DisplayGradientFilledCircle(ra8875, x, y, radius - radius / 30, RA8875_LIGHT_GREY, RA8875_BLACK, scale);
	// fill inner
	for (int i = 0; i < 5; i++) {
		// from black to color in i steps
		gcol = RA8875_colorInterpolation(RA8875_BLACK, color, i * 20, 100);
		RA8875_fillCircle(ra8875, x, y, radius - radius / 7 - i, gcol); // shades of color from black
	}
	// top glow light, use colors between color1 and white
	for (int i = 0; i < 4; i++) {
		// from color to white in i steps
		gcol = RA8875_colorInterpolation(color, RA8875_WHITE, i * 25, 100);
		RA8875_fillEllipse(ra8875, x, y - (uint16_t) (radius / 1.7) - i, (uint16_t) (radius / 2.6) - i /*radiusx*/,
				(uint16_t) (radius / 4.1) - 1.5 * i /*radiusy*/, gcol); //
	}
}

/**
 * @brief Draw a star
 *
 * @param ra8873 - RA8875 struct pointer
 * @param x - X position
 * @param y - Y position
 * @param scale - Scaling factor
 * @param color - the color
 */
static void drawStar(RA8875_struct *ra8875, unsigned int x, unsigned int y, unsigned int scale, unsigned int color) {
	RA8875_fillTriangle(ra8875, x, y + doScale(62, scale), x + doScale(106, scale), y + doScale(62, scale),
			x + doScale(139, scale), y + doScale(162, scale), color);
	RA8875_fillTriangle(ra8875, x + doScale(86, scale), y, x + doScale(119, scale), y + doScale(101, scale),
			x + doScale(33, scale), y + doScale(163, scale), color);
	RA8875_fillTriangle(ra8875, x + doScale(33, scale), y + doScale(163, scale), x + doScale(66, scale),
			y + doScale(62, scale), x + doScale(172, scale), y + doScale(62, scale), color);
}

/**
 * @brief Draw a star
 *
 * @param ra8873 - RA8875 struct pointer
 * @param x - X position
 * @param y - Y position
 * @param scale - Scaling factor
 * @param sh - Do we want shadow
 */
void DisplayCNSLogo(RA8875_struct *ra8875, unsigned int x, unsigned int y, unsigned int scale, unsigned int sh) {
#define CNSblue 0x01EE
	// big C
	// shadow +x +y if sh
	if (sh != 0) {
		// shadow
		RA8875_fillRect(ra8875, x + doScale(114 + sh, scale), y + doScale(sh, scale), doScale(57, scale),
				doScale(57, scale),
				RA8875_DARK_GREY);
		RA8875_fillRect(ra8875, x + doScale(114 + sh, scale), y + doScale(164 + sh, scale), doScale(65, scale),
				doScale(57, scale), RA8875_DARK_GREY);
		RA8875_drawArc(ra8875, x + doScale(114 + sh, scale), y + doScale(110 + sh, scale), doScale(111, scale),
				doScale(57, scale), 180.0, 0.0, RA8875_DARK_GREY);
		// end shadow
	}
	RA8875_fillRect(ra8875, x + doScale(114, scale), y, doScale(57, scale), doScale(57, scale), CNSblue);
	RA8875_fillRect(ra8875, x + doScale(114, scale), y + doScale(23, scale), doScale(57, scale), doScale(6, scale),
			RA8875_WHITE);
	RA8875_fillRect(ra8875, x + doScale(114, scale), y + doScale(164, scale), doScale(66, scale), doScale(57, scale),
			CNSblue);
	RA8875_drawArc(ra8875, x + doScale(114, scale), y + doScale(110, scale), doScale(111, scale), doScale(57, scale),
			180.0, 0.0, CNSblue);
	RA8875_drawArc(ra8875, x + doScale(114, scale), y + doScale(110, scale), doScale(88, scale), doScale(6, scale),
			180.0, 0.0, RA8875_WHITE);
	// big N
	if (sh != 0) {
		// shadow
		RA8875_fillTriangle(ra8875, x + doScale(179 + sh, scale), y + doScale(sh, scale), x + doScale(242 + sh, scale),
				y + doScale(sh, scale), x + doScale(299 + sh, scale), y + doScale(221 + sh, scale), RA8875_DARK_GREY);
		RA8875_fillTriangle(ra8875, x + doScale(242 + sh, scale), y + doScale(sh, scale), x + doScale(299 + sh, scale),
				y + doScale(221 + sh, scale), x + doScale(360 + sh, scale), y + doScale(221 + sh, scale),
				RA8875_DARK_GREY);
		RA8875_fillRect(ra8875, x + doScale(179 + sh, scale), y + doScale(sh, scale), doScale(57, scale),
				doScale(221, scale),
				RA8875_DARK_GREY);
		RA8875_fillRect(ra8875, x + doScale(305 + sh, scale), y + doScale(sh, scale), doScale(57, scale),
				doScale(221, scale),
				RA8875_DARK_GREY);
		// end shadow
	}
	RA8875_fillTriangle(ra8875, x + doScale(179, scale), y, x + doScale(242, scale), y, x + doScale(299, scale),
			y + doScale(221, scale), CNSblue);
	RA8875_fillTriangle(ra8875, x + doScale(242, scale), y, x + doScale(299, scale), y + doScale(221, scale),
			x + doScale(360, scale), y + doScale(221, scale), CNSblue);
	RA8875_fillRect(ra8875, x + doScale(179, scale), y, doScale(57, scale), doScale(221, scale), CNSblue);
	RA8875_fillRect(ra8875, x + doScale(205, scale), y + doScale(29, scale), doScale(6, scale), doScale(164, scale),
			RA8875_WHITE);
	RA8875_fillRect(ra8875, x + doScale(305, scale), y, doScale(57, scale), doScale(221, scale), CNSblue);
	RA8875_fillRect(ra8875, x + doScale(331, scale), y, doScale(6, scale), doScale(192, scale), RA8875_WHITE);
	RA8875_fillTriangle(ra8875, x + doScale(179, scale), y + doScale(154, scale), x + doScale(179, scale),
			y + doScale(160, scale), x + doScale(227, scale), y + doScale(220, scale), RA8875_WHITE);
	RA8875_fillTriangle(ra8875, x + doScale(179, scale), y + doScale(154, scale), x + doScale(233, scale),
			y + doScale(220, scale), x + doScale(227, scale), y + doScale(220, scale), RA8875_WHITE);
	for (int i = 0; i < doScale(7, scale); i++) {
		RA8875_drawLine(ra8875, x + i + doScale(216, scale), y + doScale(29, scale), x + i + doScale(320, scale),
				y + doScale(192, scale), RA8875_WHITE);
	}
	RA8875_drawArc(ra8875, x + doScale(213, scale), y + doScale(29, scale), doScale(8, scale), doScale(6, scale), 270.0,
			90.0, RA8875_WHITE);
	RA8875_drawArc(ra8875, x + doScale(328, scale), y + doScale(192, scale), doScale(8, scale), doScale(6, scale), 90.0,
			270.0, RA8875_WHITE);

	// white connect to C
	RA8875_fillRect(ra8875, x + doScale(114, scale), y + doScale(193, scale), doScale(94, scale), doScale(6, scale),
			RA8875_WHITE);

	// big S
	RA8875_fillRect(ra8875, x + doScale(367, scale), y + doScale(164, scale), doScale(120, scale), doScale(57, scale),
			RA8875_RED);
	RA8875_fillRect(ra8875, x + doScale(440, scale), y, doScale(100, scale), doScale(57, scale), RA8875_RED);

	RA8875_fillTriangle(ra8875, x + doScale(387, scale), y + doScale(100, scale), x + doScale(428, scale),
			y + doScale(62, scale), x + doScale(520, scale), y + doScale(107, scale), RA8875_RED);

	RA8875_fillTriangle(ra8875, x + doScale(387, scale), y + doScale(100, scale), x + doScale(481, scale),
			y + doScale(150, scale), x + doScale(520, scale), y + doScale(107, scale), RA8875_RED);

	RA8875_drawArc(ra8875, x + doScale(440, scale), y + doScale(64, scale), doScale(65, scale), doScale(57, scale),
			235.0, 0.0, RA8875_RED);
	RA8875_drawArc(ra8875, x + doScale(477, scale), y + doScale(156, scale), doScale(65, scale), doScale(57, scale),
			35.0, 180.0, RA8875_RED);
	RA8875_drawArc(ra8875, x + doScale(440, scale), y + doScale(58, scale), doScale(36, scale), doScale(6, scale),
			235.0, 0.0, RA8875_WHITE);
	RA8875_drawArc(ra8875, x + doScale(477, scale), y + doScale(157, scale), doScale(36, scale), doScale(6, scale),
			35.0, 180.0, RA8875_WHITE);

	RA8875_fillRect(ra8875, x + doScale(367, scale), y + doScale(187, scale), doScale(120, scale), doScale(6, scale),
			RA8875_WHITE);
	RA8875_fillRect(ra8875, x + doScale(440, scale), y + doScale(23, scale), doScale(100, scale), doScale(6, scale),
			RA8875_WHITE);

	for (int i = 0; i < doScale(10, scale); i++) {
		RA8875_drawLine(ra8875, x + i + doScale(407, scale), y + doScale(74, scale), x + i + doScale(497, scale),
				y + doScale(135, scale), RA8875_WHITE);
	}
	if (sh != 0) {
		// shadow stars
		drawStar(ra8875, x + doScale(143 + sh, scale), y + doScale(240 + sh, scale), 40, RA8875_DARK_GREY);
		drawStar(ra8875, x + doScale(208 + sh, scale), y + doScale(240 + sh, scale), 40, RA8875_DARK_GREY);
		drawStar(ra8875, x + doScale(273 + sh, scale), y + doScale(240 + sh, scale), 40, RA8875_DARK_GREY);
		drawStar(ra8875, x + doScale(338 + sh, scale), y + doScale(240 + sh, scale), 40, RA8875_DARK_GREY);
		drawStar(ra8875, x + doScale(403 + sh, scale), y + doScale(240 + sh, scale), 40, RA8875_DARK_GREY);
	}
	// red stars
	drawStar(ra8875, x + doScale(143, scale), y + doScale(240, scale), 40, RA8875_RED);
	drawStar(ra8875, x + doScale(208, scale), y + doScale(240, scale), 40, RA8875_RED);
	drawStar(ra8875, x + doScale(273, scale), y + doScale(240, scale), 40, RA8875_RED);
	drawStar(ra8875, x + doScale(338, scale), y + doScale(240, scale), 40, RA8875_RED);
	drawStar(ra8875, x + doScale(403, scale), y + doScale(240, scale), 40, RA8875_RED);
}

/** @} */
