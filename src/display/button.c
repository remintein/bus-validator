/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "RA8875-0.70/RA8875.h"
#include "button.h"
#include "../_definition/buttonTypes.h"
#include "icons.h"

/**
 * @file
 * @brief Contains the functions for the display
 *
 */

/**
 * @addtogroup display
 *
 * @{
 */

/**
 * @brief Draws a button according the input buffer
 *
 * @param ra8875 - Pointer to the display struct
 * @param buf - Communicated message with the definition of the button
 */
void displayDrawButton(RA8875_struct *ra8875, char *buf) {
	uint16_t color;
	uint16_t txtColor, txtShadowColor, buttonShadowColor, edgeColor, buttonType;
	int x, y, xsize, ysize, tx, ty, glowTop;
	int fontScale = 2;
	char c;
	char stbuf[DISPLAY_BUFFER_SIZE];
	stbuf[0] = '\0';

	sscanf(buf + 1, "T%huX%dY%ux%uy%dC%cL%[^\t]", &buttonType, &x, &y, &xsize, &ysize, &c, stbuf);

	switch (c) {
	case 'R':
		color = RA8875_RED;
		txtColor = RA8875_WHITE;
		txtShadowColor = RA8875_DARK_GREY;
		edgeColor = RA8875_DARK_GREY;
		buttonShadowColor = 0x0002;
		glowTop = 1;
		break;
	case 'G':
		color = RA8875_DARK_GREEN;
		txtColor = RA8875_WHITE;
		txtShadowColor = RA8875_DARK_GREY;
		edgeColor = RA8875_DARK_GREY;
		buttonShadowColor = 0x0002;
		glowTop = 1;
		break;
	case 'Y':
		color = RA8875_YELLOW;
		txtColor = RA8875_BLACK;
		txtShadowColor = RA8875_DARK_GREY;
		edgeColor = RA8875_DARK_GREY;
		buttonShadowColor = 0x0002;
		glowTop = 1;
		break;
	case 'B':
		color = RA8875_BLUE;
		txtColor = RA8875_YELLOW;
		txtShadowColor = RA8875_DARK_GREY;
		edgeColor = RA8875_DARK_GREY;
		buttonShadowColor = 0x0002;
		glowTop = 1;
		break;
	case 'W':
		color = 0xAD55;
		txtColor = RA8875_DARK_GREY;
		txtShadowColor = RA8875_BLACK;
		edgeColor = RA8875_DARK_GREY;
		buttonShadowColor = RA8875_LIGHT_GREY;
		glowTop = 1;
		break;
	case 'D': // disabled
		color = RA8875_DARK_BLUE;
		txtColor = RA8875_DARK_BLUE;
		txtShadowColor = RA8875_BLACK;
		buttonShadowColor = 0x0002;
		edgeColor = RA8875_DARK_GREY;
		glowTop = 1;
		break;
	case 'S': // standby
		color = RA8875_DARK_BLUE;
		txtColor = RA8875_WHITE;
		txtShadowColor = RA8875_DARK_GREY;
		buttonShadowColor = 0x0002;
		edgeColor = RA8875_DARK_GREY;
		glowTop = 1;
		break;
	case 'A': // active
		color = 0x0002;
		txtColor = RA8875_GREEN;
		txtShadowColor = RA8875_DARK_GREY;
		buttonShadowColor = 0x0001;
		edgeColor = RA8875_GREEN;
		glowTop = 0;
		break;
	case 'H': // hover
		color = 0x0002;
		txtColor = RA8875_YELLOW;
		txtShadowColor = RA8875_LIGHT_GREY;
		buttonShadowColor = 0x0002;
		edgeColor = RA8875_YELLOW;
		glowTop = 0;
		break;
	}

	if (stbuf[0] != '\0') {
			// draw button label
			if (c == 'W') {
				RA8875_setFontScale(ra8875, fontScale);
			} else {
				RA8875_setFontScale(ra8875, fontScale - 1);
			}
			tx = x + xsize / 2; // use CENTER feature of RA8875 lib, just calculate center of button
			if (buttonType == WINDOW_WITH_TEXT || buttonType == WINDOW_WITH_TEXT_NOCLEAR) {
				ty = y + 25;
			} else {
				ty = y + ysize / 2; // and use setCursor
			}
			// shadow text
			RA8875_setTextColorForeground(ra8875, txtShadowColor); // transparent background text only
			if (ra8875->_rotation) { // only rotation 1, ToDo: other rotations
				RA8875_setCursor(ra8875, ty + 1, tx + 1, true);
			} else {
				RA8875_setCursor(ra8875, tx + 1, ty + 1, true);
			}
			RA8875_textWrite(ra8875, stbuf, 0);

			if (ra8875->_rotation) {
				RA8875_setCursor(ra8875, ty, tx, true);
			} else {
				RA8875_setCursor(ra8875, tx, ty, true);
			}
			RA8875_setTextColorForeground(ra8875, txtColor); // transparent background text only
			RA8875_textWrite(ra8875, stbuf, 0);
		}

	switch (buttonType) {
	case BUTTON_NORMAL:
	case BUTTON_SMALLTEXT:
	case WINDOW_WITH_TEXT:
	case WINDOW_WITH_TEXT_RED:
	case WINDOW_WITH_TEXT_GREEN:
	case WINDOW_WITH_TEXT_NOCLEAR:
		// draw a 3D button in button state
		RA8875_fillRoundRect(ra8875, x, y, xsize, ysize, 10, edgeColor);
		RA8875_fillRoundRect(ra8875, x + 2, y + 2, xsize - 5, ysize - 5, 8, color);
		// glow line
		if (glowTop == 1) {
			RA8875_drawLine(ra8875, x + 10, y + 6, x + 8, y + 8,
			RA8875_LIGHT_GREY);
			RA8875_drawLine(ra8875, x + 10, y + 7, x + 8, y + 8,
			RA8875_LIGHT_GREY);
			RA8875_fillRoundRect(ra8875, x + 10, y + 6, xsize - 20, 2, 1,
			RA8875_LIGHT_GREY);
			RA8875_drawLine(ra8875, x + xsize - 10, y + 6, x + xsize - 6, y + 11, RA8875_LIGHT_GREY);
			RA8875_drawFastVLine(ra8875, x + xsize - 6, y + 11, ysize - 22,
			RA8875_LIGHT_GREY);
			RA8875_drawLine(ra8875, x + xsize - 6, y + ysize - 11, x + xsize - 8, y + ysize - 9,
			RA8875_LIGHT_GREY);
		} else {
			RA8875_drawFastVLine(ra8875, x + 5, y + 12, ysize - 20,
			RA8875_LIGHT_GREY);
			RA8875_drawLine(ra8875, x + 5, y + 12, x + 7, y + 10,
			RA8875_LIGHT_GREY);
			RA8875_drawLine(ra8875, x + 5, y + ysize - 8, x + 10, y + ysize - 5,
			RA8875_LIGHT_GREY);
			RA8875_drawFastHLine(ra8875, x + 10, y + ysize - 5, xsize - 20,
			RA8875_LIGHT_GREY);
			RA8875_drawLine(ra8875, x + xsize - 10, y + ysize - 5, x + xsize - 8, y + ysize - 7,
			RA8875_LIGHT_GREY);
		}
		// shadow
		if (glowTop == 1) {
			RA8875_fillRect(ra8875, x + 10, y + ysize - 9, xsize - 22, 3,
			RA8875_BLACK);
			RA8875_fillRoundRect(ra8875, x + 8, y + ysize - 15, xsize - 16, 6, 5, buttonShadowColor);
			RA8875_fillRoundRect(ra8875, x + 6, y + 10, 5, ysize - 20, 3, buttonShadowColor);
		} else {
			RA8875_fillRect(ra8875, x + 10, y + 6, xsize - 20, 3, buttonShadowColor);
			RA8875_fillRect(ra8875, x + xsize - 10, y + 10, 3, ysize - 18, buttonShadowColor);
		}

		break;
	case BUTTON_LED:
		DisplayLED(ra8875, x + xsize / 2, y + ysize / 2, xsize / 2, color, 240);
		break;
	case BUTTON_LEDCIRCLE:
		DisplayLED(ra8875, x + xsize / 2, y + ysize / 2, xsize / 2, color, 240);

		DisplayGradientFilledCircle(ra8875, x + xsize / 2, y + ysize / 2, 40, RA8875_DARK_GREY, RA8875_BLACK, 240);
//				RA8875_setFontScale(ra8875, 1);
//
//		RA8875_setTextColorForeground(ra8875, RA8875_LIGHT_GREY);
//		RA8875_setCursor(ra8875, x + 171, 271, true);
		break;
	case LOGO_CNS:
	case LOGO_CNS_NOCLEAR:
		DisplayCNSLogo(ra8875, x, y, ysize, 0);
		break;
	case WINDOW_WITH_TEXT_NOBACKGROUND:
		RA8875_fillRect(ra8875, x, y, xsize, ysize, RA8875_BLACK);
		break;
	}
	if (stbuf[0] != '\0') {
		// draw button label
		if (c == 'W') {
			RA8875_setFontScale(ra8875, fontScale);
		} else {
			RA8875_setFontScale(ra8875, fontScale - 1);
		}
		tx = x + xsize / 2; // use CENTER feature of RA8875 lib, just calculate center of button
		if (buttonType == WINDOW_WITH_TEXT || buttonType == WINDOW_WITH_TEXT_NOCLEAR) {
			ty = y + 25;
		} else {
			ty = y + ysize / 2; // and use setCursor
		}
		// shadow text
		RA8875_setTextColorForeground(ra8875, txtShadowColor); // transparent background text only
		if (ra8875->_rotation) { // only rotation 1, ToDo: other rotations
			RA8875_setCursor(ra8875, ty + 1, tx + 1, true);
		} else {
			RA8875_setCursor(ra8875, tx + 1, ty + 1, true);
		}
		RA8875_textWrite(ra8875, stbuf, 0);

		if (ra8875->_rotation) {
			RA8875_setCursor(ra8875, ty, tx, true);
		} else {
			RA8875_setCursor(ra8875, tx, ty, true);
		}
		RA8875_setTextColorForeground(ra8875, txtColor); // transparent background text only
		RA8875_textWrite(ra8875, stbuf, 0);
	}
}

/**
 * @brief Adds a button according the input buffer
 *
 * @param ra8875 - Pointer to the display struct
 * @param buf - Communicated message with the definition of the button
 */
void displayAddButton(RA8875_struct *ra8875, char *buf) {
	uint16_t txtColor, txtShadowColor,   buttonType;
	int x, y, xsize, ysize, tx, ty;
	int line;
	int fontScale = 2;
	char c;
	char stbuf[DISPLAY_BUFFER_SIZE];
	stbuf[0] = '\0';

	sscanf(buf + 1, "T%huX%dY%ux%uy%dC%cl%dL%[^\t]", &buttonType, &x, &y, &xsize, &ysize, &c, &line, stbuf);

	if (buttonType == BUTTON_SMALLTEXT) {
		fontScale = 1;
	}

	switch (c) {
	case 'R':
		txtColor = RA8875_WHITE;
		txtShadowColor = RA8875_DARK_GREY;
		break;
	case 'G':
		txtColor = RA8875_WHITE;
		txtShadowColor = RA8875_DARK_GREY;
		break;
	case 'Y':
		txtColor = RA8875_BLACK;
		txtShadowColor = RA8875_DARK_GREY;
		break;
	case 'B':
		txtColor = RA8875_YELLOW;
		txtShadowColor = RA8875_DARK_GREY;
		break;
	case 'W':
		txtColor = RA8875_DARK_GREY;
		txtShadowColor = RA8875_BLACK;
		break;
	case 'D': // disabled
		txtColor = RA8875_DARK_BLUE;
		txtShadowColor = RA8875_BLACK;
		break;
	case 'S': // standby
		txtColor = RA8875_WHITE;
		txtShadowColor = RA8875_DARK_GREY;
		break;
	case 'A': // active
		txtColor = RA8875_GREEN;
		txtShadowColor = RA8875_DARK_GREY;
		break;
	case 'H': // hover
		txtColor = RA8875_YELLOW;
		txtShadowColor = RA8875_LIGHT_GREY;
		break;
	}

	// draw button label
	if (c == 'W') {
		RA8875_setFontScale(ra8875, fontScale);
	} else {
		RA8875_setFontScale(ra8875, fontScale - 1);
	}
	tx = x + xsize / 2; // use CENTER feature of RA8875 lib, just calculate center of button
	if (buttonType == WINDOW_WITH_TEXT || buttonType == WINDOW_WITH_TEXT_NOCLEAR) {
		ty = y + 25 * line;
	} else {
		ty = y + ysize / 2; // and use setCursor
	}
	// shadow text
	RA8875_setTextColorForeground(ra8875, txtShadowColor); // transparent background text only
	if (ra8875->_rotation) { // only rotation 1, ToDo: other rotations
		RA8875_setCursor(ra8875, ty + 1, tx + 1, true);
	} else {
		RA8875_setCursor(ra8875, tx + 1, ty + 1, true);
	}
	RA8875_textWrite(ra8875, stbuf, 0);

	if (ra8875->_rotation) {
		RA8875_setCursor(ra8875, ty, tx, true);
	} else {
		RA8875_setCursor(ra8875, tx, ty, true);
	}
	RA8875_setTextColorForeground(ra8875, txtColor); // transparent background text only
	RA8875_textWrite(ra8875, stbuf, 0);
}

/** @} */
