/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef SDIO_H_
#define SDIO_H_

BaseType_t nfccSdioInit(UBaseType_t uxPriority);
void nfccSdio(void *pvParameters);

/* These types MUST be 64 bit */
typedef uint64_t DWORD;

typedef enum {
	Initializing, Busy, Ready
} SdioStatus;

/* Results of Disk Functions */
typedef enum {
	RES_OK = 0, /* 0: Successful */
	RES_ERROR, /* 1: R/W Error */
	RES_WRPRT, /* 2: Write Protected */
	RES_NOTRDY, /* 3: Not Ready */
	RES_PARERR /* 4: Invalid Parameter */
} DRESULT;

/* Results of Disk Functions */
/* Verify the step in this enum is 32 */
typedef enum {
	FIRST_BLOCKFILE = 0,
	WIFI_FILE = 32,
	PRINTER_FILE	 = 64,
	FARE_FILE = 96,
	BLACKLIST_FILE = 160,
	LOGFILE = 192
} DnfcFileNo;

typedef enum {
	EMPTY, // Starts here
	READING, // Data in block is not valid, reading..
	READY_READ, // Data in block is valid, sector as seen
	DIRTY_WRITE, // Data in block is valid, but not written to SD-card
	READY_WRITE // Data in block is valid, and written to SD-card
} sdioBlockStatus;

typedef struct {
	DWORD sectorOffsetInFile;
	DnfcFileNo file;
	uint16_t readErrors;
	uint16_t writeErrors;

	sdioBlockStatus readStatus;
	sdioBlockStatus writeStatus;

	uint8_t data[512];
} SdioBlockInfo;

typedef struct {
	char signature[10];
	char tmp1[22];
//		WIFI_FILE = 32,
	char wifiVersion[4];
	char wifiNewVersion[4];
	uint8_t wifiStartsector[8];
	uint8_t wifiNewStartsector[8];
	uint8_t wifiLength[4];
	uint8_t wifiNewLength[4];
//		PRINTER_FILE = 64,
	char printerVersion[4];
	char printerNewVersion[4];
	uint8_t printerStartsector[8];
	uint8_t printerNewStartsector[8];
	uint8_t printerLength[4];
	uint8_t printerNewLength[4];
//		FARE_FILE = 96,
	char fareVersion[4];
	char fareNewVersion[4];
	uint8_t fareStartsector[8];
	uint8_t fareNewStartsector[8];
	uint8_t fareLength[4];
	uint8_t fareNewLength[4];
//		Not present_FILE = 128,
	char xxxVersion[4];
	char xxxNewVersion[4];
	uint8_t xxxStartsector[8];
	uint8_t xxxNewStartsector[8];
	uint8_t xxxLength[4];
	uint8_t xxxNewLength[4];
//		BLACKLIST_FILE = 140,
	char blckVersion[4];
	char blckNewVersion[4];
	uint8_t blckStartsector[8];
	uint8_t blckNewStartsector[8];
	uint8_t blckLength[4];
	uint8_t blckNewLength[4];
//		LOGFILE = 192
	char logVersion[4];
	char logNewVersion[4];
	uint8_t logStartsector[8];
	uint8_t logNewStartsector[8];
	uint8_t logProcessing[8];

} firstBlockData;
void intDnfccInitializeFileTransfer(char *buf);
void intDnfccRequestNextBlock();
void intDnfccInitializeFile(char *buf);
int intDnfccGetDataForFile(char *buf);
void intDnfccStoreFileAsActive();

#endif
