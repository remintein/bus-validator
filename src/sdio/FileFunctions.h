/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef SDIO_FILEFUNCTIONS_H_
#define SDIO_FILEFUNCTIONS_H_

extern SD_HandleTypeDef hsd;
extern HAL_SD_CardInfoTypedef SDCardInfo;
//
extern SdioBlockInfo sdioFirstBlockDataPointer;
extern SdioBlockInfo sdioFareDataPointer;
extern SdioBlockInfo sdioPrinterDataPointer;
extern SdioBlockInfo sdioWifiDataPointer;
extern SdioBlockInfo sdioBlacklistDataPointer;
extern SdioBlockInfo sdioLogfileDataPointer;
extern SdioBlockInfo sdioLogfileDataReadPointer;

#define OFFSET_C_VERSION 0
#define OFFSET_N_VERSION 4
#define OFFSET_C_STARTSECTOR 8
#define OFFSET_N_STARTSECTOR 16
// For Logging, here's the processing pointer
#define OFFSET_C_PROCESSING 16
#define OFFSET_C_LENGTH 24
// For logging not in use
#define OFFSET_N_LENGTH 28

void apiDnfccInitSdioBlockInfo(SdioBlockInfo *p, DnfcFileNo fileno);
void apiDnfccReadSector(SdioBlockInfo *p, DWORD sectorOffset);
void apiDnfccInitSector(SdioBlockInfo *p, DWORD sector);
void apiapiDnfccInitSectorForWriteByRead(SdioBlockInfo *p, DWORD sector);
void apiDnfccFlushWrite(SdioBlockInfo *p);
void apiDnfccSetWriteData(SdioBlockInfo *p, uint32_t pos, uint8_t *data,
		uint16_t len);
void apiDnfccAllocateFile(SdioBlockInfo *p, char *version, uint32_t size);
void apiDnfccPublishFile(SdioBlockInfo *p);
void apiDnfccWriteLogfile128Bytes(char *data);
void apiDnfccFormatCardIfNeeded();
void apiDnfccUpdateDeviceStructVersions();
uint8_t apiDnfccGetNextMessageFromLogging(char *buf);

#endif /* SDIO_FILEFUNCTIONS_H_ */
