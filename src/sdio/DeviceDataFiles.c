/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "bsp_driver_sd.h"
#include "Sdio.h"
#include "FileFunctions.h"
#include <string.h>
#include "DeviceDataFiles.h"

static uint32_t longRouteFare[10];
static uint32_t shortRouteFare[10];
static char curRouteType = 'L';

/**
 * @file
 * @brief Routines for the getting the correct fare amount
 */
/**
 * @addtomodule fare
 * @{
 */

/**
 * @brief match route/fare/length with line
 *
 * @param route - the route to find
 * @param service - the service to find
 * @param routeType - the route type  'L'/'S'
 * @param s - the String to search in
 * @return 0 = not found, 1 = found
 */
uint8_t intDnfccCheckLineForMatchRST(char *route, char *service,
		uint8_t routeType, char *s) {
	uint8_t i;
	uint8_t searchPos = 0;

	// Match route
	for (i = 0; route[i] != '\0'; i++) {

		if (s[searchPos] == ',' || s[searchPos] == '\0') {
			break;
		}
		if (s[searchPos] != route[i]) {
			return 0;
		}
		searchPos++;
	}
// Skip till next ,
	while (s[searchPos] != ',') {
		if (s[searchPos] == '\0') {
			return 0;
		}
		searchPos++;
	}
	searchPos++; // And move after this
	// Match service
	for (i = 0; service[i] != '\0'; i++) {

		if (s[searchPos] == ',' || s[searchPos] == '\0') {
			break;
		}
		if (s[searchPos] != service[i]) {
			return 0;
		}
		searchPos++;
	}
	// Skip till next ,
	while (s[searchPos] != ',') {
		if (s[searchPos] == '\0') {
			return 0;
		}
		searchPos++;
	}
	searchPos++; // And move after this
// Verify route type
	if (s[searchPos] == ',' || s[searchPos] == routeType) {
		return 1;
	}
	return 0;
}
/**
 * @brief, skip till after next comma, or leave at '\0'
 *
 * @param p = input pointer
 * @return new pointer
 */
char *intDnfccSkipTillAfterComma(char *p) {
	while (*p != ',' && *p != '\0') {
		p++;
	}
	if (*p == ',') {
		p++;
	}
	return p;
}
/**
 * @brief Get the values for the tickets and put them in the array
 *
 * @param inputString - The input sting, with 3x a comma, and then 10 values separated with a comma
 * @param outputArray - The output array
 */
void apiDnfccParseLineToArray(char *inputString, uint32_t *outputArray) {
	char *p = inputString;
	int i;

	p = intDnfccSkipTillAfterComma(p);
	p = intDnfccSkipTillAfterComma(p);
	p = intDnfccSkipTillAfterComma(p);

	for (i = 0; i < 10; i++) {
		sscanf(p, "%lu", &outputArray[i]);
		p = intDnfccSkipTillAfterComma(p);
	}
}
/**
 * @brief read the fare table, and set the internal values according the values provided
 *
 * The fare table consists of 3 + 10 columns separated by ,
 * 1: value route should start with
 * 2: value service should start with
 * 3: L or S for long or short routes
 * 0..9, ticket type 0..9 the amount
 *
 * Route -1 = Top up array [0..9 max elements]
 * Route -2 = Handheld array[penalty 0..9, but only 0 is used]
 */
void apiDnfccSetRouteAndFare(uint32_t route, uint32_t service) {
	uint8_t longSet = 0;
	uint8_t shortSet = 0;
	uint32_t filePtr;
	uint32_t fileLen;
	uint8_t sptr;
	char s[100];
	int i;
	char routeSearch[10], serviceSearch[10];
	sprintf(routeSearch, "%lu", route);
	sprintf(serviceSearch, "%lu", service);
	for (i = 0; i < 10; i++) {
		longRouteFare[i] = shortRouteFare[i] = 1234;
	}

	filePtr = 0;

	memcpy(&fileLen,
			sdioFirstBlockDataPointer.data + FARE_FILE + OFFSET_C_LENGTH, 4);
	// While get lines
	while (filePtr < fileLen) {
		sptr = 0;
		while (filePtr < fileLen) {
			if (filePtr % 512 == 0) {
				apiDnfccReadSector(&sdioFareDataPointer, filePtr / 512);
			}
			s[sptr] = sdioFareDataPointer.data[filePtr % 512];
			filePtr++;
			s[sptr + 1] = '\0';
			if (s[sptr] == '\n') {
				break;
			}
			sptr++;
		}
		if (longSet == 0
				&& intDnfccCheckLineForMatchRST(routeSearch, serviceSearch, 'L',
						s)) {
			apiDnfccParseLineToArray(s, longRouteFare);
			longSet = 1;
		}
		if (shortSet == 0
				&& intDnfccCheckLineForMatchRST(routeSearch, serviceSearch, 'S',
						s)) {
			apiDnfccParseLineToArray(s, shortRouteFare);
			shortSet = 1;
		}
	}
}

/**
 * @brief Get the ticket fare for the ticket given
 *
 * @param ticketType : 0..9 for ticket 0..9
 * @return The fare according the kind of route (short or long), which is set with apiDnfccSetRouteType(..)
 */
uint32_t apiDnfccGetTicketFare(uint8_t ticketType) {
	if (curRouteType == 'S') {
		return shortRouteFare[ticketType];
	}
	return longRouteFare[ticketType];

}

/**
 * @brief set the current route type (Long or Short)
 *
 * This call sets the current route type value, to short route or long route. It can be either 'S' or 'L'
 * @param purseFare - 'S' or 'L' for short or long route
 */
void apiDnfccSetRouteType(char routeType) {
	curRouteType = routeType;

}
/** @} */
