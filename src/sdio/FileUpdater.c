/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "Sdio.h"
#include "FileFunctions.h"
#include <string.h>

/**
 * @file
 * @brief Defines the file update functions
 */

/** \addtogroup sdio
 *  @{
 */


typedef struct updateInfo {
	char sha1[41]; /**< Sha1 verification of the file */
	uint32_t fileLength; /**< Length of the file */
	long filePtr; /**< pointer to where in the file the update is done */
	char fileVersion[5]; /**< Version of the file to update */
	char fileToDo[5]; /**< Which file to update */
} FileUpdateInfo; /**< Structure to hold the information for updating a file. This will have info on the current file to update */

/**
 * @brief holds the current update data
 */
static FileUpdateInfo fileUpdateInfo;

extern SdioBlockInfo sdioFareDataPointer;
extern SdioBlockInfo sdioPrinterDataPointer;
extern SdioBlockInfo sdioWifiDataPointer;
extern SdioBlockInfo sdioBlacklistDataPointer;

/**
 * @brief Initializes the fileUpdateInfo, with the info from the buffer supplied
 *
 * @param buf - the buffer of the file
 */
void intDnfccInitializeFileTransfer(char *buf) {
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];

	strncpy(fileUpdateInfo.fileToDo, buf + 1, 4);
	fileUpdateInfo.fileToDo[4] = '\0';
	strncpy(fileUpdateInfo.fileVersion, buf + 5, 4);
	fileUpdateInfo.fileVersion[4] = '\0';

	fileUpdateInfo.fileLength = 0;
	fileUpdateInfo.filePtr = 0;
	fileUpdateInfo.sha1[0] = '\0';
	stbuf[0] = COMCOM_DATADISTRIBUTOR_INIT_FILE;
	strncpy(stbuf + 1, buf + 1, 8);
	queueSendToDataDistributor(stbuf, portMAX_DELAY);
}

/**
 * @brief Requests the next block from the backend through the data distributor
 */
void intDnfccRequestNextBlock() {
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	stbuf[0] = COMCOM_DATADISTRIBUTOR_GET_DATA_FROM_FILE;
	memcpy(stbuf + 1, fileUpdateInfo.fileToDo, 4);
	memcpy(stbuf + 5, fileUpdateInfo.fileVersion, 4);
	uint32_t len = BLOCKLENGTH_FILEDATA;
	if (fileUpdateInfo.filePtr + len > fileUpdateInfo.fileLength) {
		len = fileUpdateInfo.fileLength - fileUpdateInfo.filePtr;
	}
	sprintf(stbuf + 9, "%lu,%lu", fileUpdateInfo.filePtr, len);
	queueSendToDataDistributor(stbuf, portMAX_DELAY);
}

/**
 * @brief initializes a file on SD card (eg. create a next version, allocates etc)
 * This routine checks what file is to be done.
 */
void intDnfccInitializeFile(char *buf) {
	// STORE the length etc.
	strncpy(fileUpdateInfo.sha1, buf + 18, 41);
	sscanf(buf + 9, "%ld", &fileUpdateInfo.fileLength);
	fileUpdateInfo.filePtr = 0;
	strncpy(fileUpdateInfo.fileVersion, buf + 5, 4);
	fileUpdateInfo.fileVersion[4] = '\0';
	strncpy(fileUpdateInfo.fileToDo, buf + 1, 4);
	fileUpdateInfo.fileToDo[4] = '\0';

	if (strncmp(buf + 1, NAME_FILE_BLACKLIST, 4) == 0) {
		apiDnfccAllocateFile(&sdioBlacklistDataPointer, fileUpdateInfo.fileVersion,
				fileUpdateInfo.fileLength);
	} else if (strncmp(buf + 1, NAME_FILE_FARE, 4) == 0) {
		apiDnfccAllocateFile(&sdioFareDataPointer, fileUpdateInfo.fileVersion,
				fileUpdateInfo.fileLength);
	} else if (strncmp(buf + 1, NAME_FILE_PRINTER, 4) == 0) {
		apiDnfccAllocateFile(&sdioPrinterDataPointer, fileUpdateInfo.fileVersion,
				fileUpdateInfo.fileLength);
	} else if (strncmp(buf + 1, NAME_FILE_WIFI, 4) == 0) {
		apiDnfccAllocateFile(&sdioWifiDataPointer, fileUpdateInfo.fileVersion,
				fileUpdateInfo.fileLength);
	}
}
/**
 * @brief Processes a new data buffer from the backend.
 *
 * @param - The data retrieved from the backend
 */
int intDnfccGetDataForFile(char *buf) {
	uint32_t length;
	uint32_t offset;
	int i;
	sscanf(buf + 5, "%lu", &offset);
	sscanf(buf + 13, "%lu", &length);
	uint8_t cnvBuf[SDIO_BUFFER_SIZE / 2];
// TODO: Check if MD5sum is correct
	for (i = 0; i < length; i++) {
		sscanf(buf + 50 + i * 2, "%2hhx", cnvBuf + i);
	}

	if (strncmp(fileUpdateInfo.fileToDo, NAME_FILE_FARE, 4) == 0) {
		apiDnfccSetWriteData(&sdioFareDataPointer, offset, cnvBuf, length);
	} else if (strncmp(fileUpdateInfo.fileToDo, NAME_FILE_BLACKLIST, 4) == 0) {
		apiDnfccSetWriteData(&sdioBlacklistDataPointer, offset, cnvBuf, length);
	} else if (strncmp(fileUpdateInfo.fileToDo, NAME_FILE_PRINTER, 4) == 0) {
		apiDnfccSetWriteData(&sdioPrinterDataPointer, offset, cnvBuf, length);
	} else if (strncmp(fileUpdateInfo.fileToDo, NAME_FILE_WIFI, 4) == 0) {
		apiDnfccSetWriteData(&sdioWifiDataPointer, offset, cnvBuf, length);
	}

	fileUpdateInfo.filePtr += length;

	char bbuf[100];
	sprintf(bbuf, "%4.4s - %ld %ld", buf + 1, fileUpdateInfo.filePtr,
			fileUpdateInfo.fileLength);
	logUploadString(bbuf);

	if (fileUpdateInfo.filePtr < fileUpdateInfo.fileLength) {
		return 1;
	} else {
		return 0;
	}
}

/**
 * @brief Set the current begin uplaoded file to active
 */
void intDnfccStoreFileAsActive() {
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE];
// TODO: Check if sha1 is correct
	if (strncmp(fileUpdateInfo.fileToDo, NAME_FILE_FARE, 4) == 0) {
		apiDnfccPublishFile(&sdioFareDataPointer);
	} else if (strncmp(fileUpdateInfo.fileToDo, NAME_FILE_PRINTER, 4) == 0) {
		apiDnfccPublishFile(&sdioPrinterDataPointer);
	} else if (strncmp(fileUpdateInfo.fileToDo, NAME_FILE_BLACKLIST, 4) == 0) {
		apiDnfccPublishFile(&sdioBlacklistDataPointer);
	} else if (strncmp(fileUpdateInfo.fileToDo, NAME_FILE_WIFI, 4) == 0) {
		apiDnfccPublishFile(&sdioWifiDataPointer);
	}

	// So backend will know the new file version.
	stbuf[0] = COMCOM_DATADISTRIBUTOR_KEEPALIVE;
	queueSendToDataDistributor(stbuf, 0);
}
/** @} */
