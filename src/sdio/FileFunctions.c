/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "bsp_driver_sd.h"
#include "Sdio.h"
#include "FileFunctions.h"
#include <string.h>

void intDnfccInitSectorForWriteByReadRaw(SdioBlockInfo *p, DWORD sector);
void apiDnfccReadSectorRaw(SdioBlockInfo *p, DWORD sectorOffset);
void intDnfccFlushWriteRaw(SdioBlockInfo *p);

/**
 * @file
 * @brief Defines the file functions
 */

/** \addtogroup sdio
 *  @{
 */

/**
 * @brief Initializes a SdioBlockInfo structure
 *
 * @param p - Pointer to the structure
 * @param fileno - The fileno, as defined in the macro (must be a multiple of 32)
 */
void apiDnfccInitSdioBlockInfo(SdioBlockInfo *p, DnfcFileNo fileno) {
	p->file = fileno;
	p->readStatus = EMPTY;
	p->writeStatus = EMPTY;
	p->readErrors = 0;
	p->writeErrors = 0;
}

/**
 * @brief update the versions of the file and the device ID in the deviceStatus
 */
void apiDnfccUpdateDeviceStructVersions() {
	deviceStatus.blckVersion = sdioFirstBlockDataPointer.data + BLACKLIST_FILE
			+ OFFSET_C_VERSION;
	deviceStatus.fareVersion = sdioFirstBlockDataPointer.data + FARE_FILE
			+ OFFSET_C_VERSION;
	deviceStatus.printerVersion = sdioFirstBlockDataPointer.data + PRINTER_FILE
			+ OFFSET_C_VERSION;
	deviceStatus.wifiVersion = sdioFirstBlockDataPointer.data + WIFI_FILE
			+ OFFSET_C_VERSION;
	memcpy(deviceStatus.deviceId, sdioFirstBlockDataPointer.data + 10, 4);
}
/**
 * @brief Read one sector of a file; using semaphores to avoid conflicts
 *
 * @param p - Pointer to the file structure
 * @param sectorOffset - Offset of the sector with respect to the beginning of the file
 */
void apiDnfccReadSector(SdioBlockInfo *p, DWORD sectorOffset) {
	if (xSemaphoreTake(deviceStatus.sdioSemaphore, portMAX_DELAY) == pdTRUE) {
		deviceStatus.sdioStatus = Busy;
		apiDnfccReadSectorRaw(p, sectorOffset);
		deviceStatus.sdioStatus = Ready;
		xSemaphoreGive(deviceStatus.sdioSemaphore);
	}
}

/**
 * @brief Read one sector of a file which is to be written
 *
 * @param p - Pointer to the file structure
 * @param sectorOffset - Offset of the sector with respect to the beginning of the file
 */
void apiapiDnfccInitSectorForWriteByRead(SdioBlockInfo *p, DWORD sectorOffset) {
	if (xSemaphoreTake(deviceStatus.sdioSemaphore, portMAX_DELAY) == pdTRUE) {
		deviceStatus.sdioStatus = Busy;
		intDnfccInitSectorForWriteByReadRaw(p, sectorOffset);
		deviceStatus.sdioStatus = Ready;
		xSemaphoreGive(deviceStatus.sdioSemaphore);
	}
}
/**
 * @brief Flush one sector to SD card
 *
 * @param p - Pointer to the file structure
 */
void apiDnfccFlushWrite(SdioBlockInfo *p) {
	if (xSemaphoreTake(deviceStatus.sdioSemaphore, portMAX_DELAY) == pdTRUE) {
		deviceStatus.sdioStatus = Busy;
		intDnfccFlushWriteRaw(p);
		deviceStatus.sdioStatus = Ready;
		xSemaphoreGive(deviceStatus.sdioSemaphore);
	}
}
/**
 * @brief - Read one sector from SD card. When the file is the first block, ignore the offset (must be 0) and if the file block must be written, do this first.
 *
 * @param p - Pointer to the file structure
 * @param sectorOffset - Sector offset in the file
 */
void apiDnfccReadSectorRaw(SdioBlockInfo *p, DWORD sectorOffset) {
	if (p->file == FIRST_BLOCKFILE && sectorOffset != 0) { // Not allowed
		return;
	}
	DWORD sectorAbsolute;
	uint32_t length;

	memcpy(&sectorAbsolute,
			sdioFirstBlockDataPointer.data + p->file + OFFSET_C_STARTSECTOR, 8);
	memcpy(&length, sdioFirstBlockDataPointer.data + p->file + OFFSET_C_LENGTH,
			4);
	sectorAbsolute += sectorOffset;
	if (sectorOffset == p->sectorOffsetInFile && p->readStatus == READY_READ) {
		return;
	}
	if (p->file == FIRST_BLOCKFILE) {
		sectorAbsolute = 0;
	} else if (length == 0) {
		return;
	}
	if (p->writeStatus == DIRTY_WRITE) {
		apiDnfccFlushWrite(p);
	}
	p->readStatus = READING;
	uint8_t res;
	do {
		vTaskDelay(1);
		apiDnfccDisplayLedControlRed(1);
		res = BSP_SD_GetStatus();
		memset(p->data, 0xff, 512);
		res = BSP_SD_ReadBlocks((uint32_t *) (p->data), sectorAbsolute * 512,
				512, 1);
		apiDnfccDisplayLedControlRed(0);
		if (res == MSD_ERROR) {
			p->readErrors++;
			vTaskDelay(1);
		}

	} while (res == MSD_ERROR);
	p->sectorOffsetInFile = sectorOffset;
	p->readStatus = READY_READ;
}
/**
 * @brief - Init a new sector for write with all 0; flushing write if the current block is in write.
 *
 * @param p - Pointer to the file structure
 * @param sectorOffset - offset of the sector in the file
 */
void apiDnfccInitSector(SdioBlockInfo *p, DWORD sectorOffset) {
	if (p->writeStatus == DIRTY_WRITE) {
		apiDnfccFlushWrite(p);
	}
	p->writeStatus = DIRTY_WRITE;
	p->sectorOffsetInFile = sectorOffset;
	for (int i = 0; i < 512; i++) {
		p->data[i] = 0;
	}
}
/**
 * @brief Init a sector for writing by reading the sector first
 *
 * @param p - Pointer to the file structure
 * @param sectorOffset - offset of the sector in the file
 */
void intDnfccInitSectorForWriteByReadRaw(SdioBlockInfo *p, DWORD sectorOffset) {
	if (p->writeStatus == DIRTY_WRITE) {
		apiDnfccFlushWrite(p);
	}
	DWORD sectorAbsolute;
	memcpy(&sectorAbsolute,
			sdioFirstBlockDataPointer.data + p->file + OFFSET_C_STARTSECTOR, 8);
	p->writeStatus = EMPTY;
	sectorAbsolute += sectorOffset;
	if (p->file == FIRST_BLOCKFILE) {
		sectorAbsolute = 0;
	}
	if (sectorOffset == p->sectorOffsetInFile) {
		p->writeStatus = READY_WRITE;
	} else {

		DRESULT res;
		do {
			vTaskDelay(1);
			apiDnfccDisplayLedControlRed(1);
			res = BSP_SD_ReadBlocks((uint32_t *) (p->data),
					sectorAbsolute * 512, 512, 1);
			apiDnfccDisplayLedControlRed(0);
			if (res == MSD_ERROR) {
				p->readErrors++;
				vTaskDelay(1);
			}
		} while (res == MSD_ERROR);
		p->writeStatus = READY_WRITE;
	}
	p->sectorOffsetInFile = sectorOffset;
}
/**
 * @brief flush the current block writing
 *
 * @paaram p - Pointer to the file structure
 */
void intDnfccFlushWriteRaw(SdioBlockInfo *p) {
	if (p->writeStatus != DIRTY_WRITE) {
		return;
	}
	DWORD sectorAbsolute;
	memcpy(&sectorAbsolute,
			sdioFirstBlockDataPointer.data + p->file + OFFSET_N_STARTSECTOR, 8);
	sectorAbsolute += p->sectorOffsetInFile;
	if (p->file == FIRST_BLOCKFILE) {
		sectorAbsolute = 0;
	} else {
		if (sectorAbsolute == 0) { // Should not happen when the data is clean
			p->writeStatus = READY_WRITE;
			return;
		}
	}
	DRESULT res;

	// Wait for send status
//	res = HAL_SD_CardStatusTypedef
//	cardStatus;
//	HAL_SD_GetCardStatus(&hsd, &cardStatus);
	do {
		apiDnfccDisplayLedControlRed(1);

		res = BSP_SD_WriteBlocks((uint32_t *) (p->data), sectorAbsolute * 512,
				512, 1);
		apiDnfccDisplayLedControlRed(0);
		if (res == MSD_ERROR) {
			p->writeErrors++;
			vTaskDelay(1);
		}

	} while (res == MSD_ERROR);

	p->writeStatus = READY_WRITE;
}
/**
 * @brief set data in the file as for writing
 *
 * @param p - Pointer to the file structure
 * @param pos - Position in the file
 * @param data - Data to write
 * @param len - The length of the data
 */
void apiDnfccSetWriteData(SdioBlockInfo *p, uint32_t pos, uint8_t *data,
		uint16_t len) {
	DWORD sectorAbsolute = 0;
	if (p->file == FIRST_BLOCKFILE) {
		sectorAbsolute = 0;
	} else if (p->file == LOGFILE) {
		memcpy(&sectorAbsolute,
				sdioFirstBlockDataPointer.data + p->file + OFFSET_C_STARTSECTOR,
				8);
	} else {
		memcpy(&sectorAbsolute,
				sdioFirstBlockDataPointer.data + p->file + OFFSET_N_STARTSECTOR,
				8);
	}
	sectorAbsolute += pos / 512;
	if (p->writeStatus == DIRTY_WRITE && p->sectorOffsetInFile != (pos / 512)) {
		apiDnfccFlushWrite(p);
		p->sectorOffsetInFile = (pos / 512);
	}
	p->writeStatus = DIRTY_WRITE;
	uint16_t wdone = 0;

	while (wdone < len && (pos % 512) + wdone < 512) {
		p->data[(pos % 512) + wdone] = data[wdone];
		wdone++;
	}
	// Clear rest of the data with '\0', but only when not in first block!
	if (p->file != FIRST_BLOCKFILE) {
		while ((pos % 512) + wdone < 512) {
			p->data[(pos % 512) + wdone] = '\0';
			wdone++;
		}
	}
}
/**
 * @brief allocate a new version of the file
 *
 * @param p - Pointer to the file structure
 * @param version - the new version
 * @param size - the size of the new file
 */
void apiDnfccAllocateFile(SdioBlockInfo *p, char *version, uint32_t size) {
	if (p->file == FIRST_BLOCKFILE || p->file == LOGFILE) {
		return; // No need for allocating these; are fixed
	}
// Find first free sector block needed
	DWORD numSectors = (size + 511) / 512;

	DWORD checkSector = 1; // Checking starts from this sector.
	for (int fil = 32; fil < 173; fil += 32) {
		uint64_t f1Start, f1End, f2Start, f2End;
		uint32_t f1Length, f2Length;
		memcpy(&f1Start,
				sdioFirstBlockDataPointer.data + fil + OFFSET_C_STARTSECTOR, 8);
		memcpy(&f1Length,
				sdioFirstBlockDataPointer.data + fil + OFFSET_C_LENGTH, 4);
		memcpy(&f2Start,
				sdioFirstBlockDataPointer.data + fil + OFFSET_N_STARTSECTOR, 8);
		memcpy(&f2Length,
				sdioFirstBlockDataPointer.data + fil + OFFSET_N_LENGTH, 4);
		f1End = (f1Length + 511) / 512 + f1Start;
		f2End = (f2Length + 511) / 512 + f2Start;
		if (f1Start >= checkSector && f1Start <= checkSector + numSectors) {
			checkSector = f1End + 1;
			fil = 0;
			continue;
		}
		if (f2Start >= checkSector && f2Start <= checkSector + numSectors) {
			checkSector = f2End + 1;
			fil = 0;
			continue;
		}
	}
	// Now register
	apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
			p->file + OFFSET_N_STARTSECTOR, (uint8_t *) &checkSector, 8);
	apiDnfccSetWriteData(&sdioFirstBlockDataPointer, p->file + OFFSET_N_LENGTH,
			(uint8_t *) &size, 4);
	apiDnfccSetWriteData(&sdioFirstBlockDataPointer, p->file + OFFSET_N_VERSION,
			(uint8_t *) version, 4);
	apiDnfccFlushWrite(&sdioFirstBlockDataPointer);
	p->readStatus = EMPTY;
	p->writeStatus = EMPTY;
}
/**
 * @brief Publish the file (set from new to active)
 *
 * @param p - pointer to the file structure
 */
void apiDnfccPublishFile(SdioBlockInfo *p) {
	if (p->file == FIRST_BLOCKFILE || p->file == LOGFILE) {
		return; // No need for allocating these; are fixed
	}
	apiDnfccFlushWrite(p);

	apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
			p->file + OFFSET_C_STARTSECTOR,
			sdioFirstBlockDataPointer.data + p->file + OFFSET_N_STARTSECTOR, 8);

	apiDnfccSetWriteData(&sdioFirstBlockDataPointer, p->file + OFFSET_C_LENGTH,
			sdioFirstBlockDataPointer.data + p->file + OFFSET_N_LENGTH, 4);

	apiDnfccSetWriteData(&sdioFirstBlockDataPointer, p->file + OFFSET_C_VERSION,
			sdioFirstBlockDataPointer.data + p->file + OFFSET_N_VERSION, 4);

	uint8_t *nullBytes = (uint8_t *) "\0\0\0\0\0\0\0\0";
	uint8_t *nullVersion = (uint8_t *) "00nv";

	apiDnfccSetWriteData(&sdioFirstBlockDataPointer, p->file + OFFSET_N_VERSION,
			nullVersion, 4); // Sector start next file
	apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
			p->file + OFFSET_N_STARTSECTOR, nullBytes, 8); // Sector start next file
	apiDnfccSetWriteData(&sdioFirstBlockDataPointer, p->file + OFFSET_N_LENGTH,
			nullBytes, 4); // Length of current file

	p->readStatus = EMPTY;
	p->writeStatus = EMPTY;
	sdioFirstBlockDataPointer.writeStatus = DIRTY_WRITE;
	apiDnfccFlushWrite(&sdioFirstBlockDataPointer);
}

/**
 * @brief size of a log record
 *
 * The size of a log record. Must be a dividor of 512. (512, 256, 128,...)
 */
#define LOG_RECORD_SIZE 128
/**
 * @brief write a log record to the log file.
 *
 * @param data - 128 bytes to write
 */
void apiDnfccWriteLogfile128Bytes(char *data) {
	DWORD sectorAbsolute;
	DWORD curLength;
	memcpy(&sectorAbsolute,
			sdioFirstBlockDataPointer.data + sdioLogfileDataPointer.file
					+ OFFSET_C_STARTSECTOR, 8);
	memcpy(&curLength,
			sdioFirstBlockDataPointer.data + sdioLogfileDataPointer.file
					+ OFFSET_C_LENGTH, 8);

	sectorAbsolute += curLength / 512;
	if (sdioLogfileDataPointer.writeStatus == DIRTY_WRITE
			&& sdioLogfileDataPointer.sectorOffsetInFile != curLength / 512) {
		apiDnfccFlushWrite(&sdioLogfileDataPointer);
	}
	apiDnfccReadSector(&sdioLogfileDataPointer, curLength / 512);
	sdioLogfileDataPointer.writeStatus = DIRTY_WRITE;
	uint16_t wdone = 0;
	while (wdone < LOG_RECORD_SIZE && (curLength % 512) + wdone < 512) {
		sdioLogfileDataPointer.data[(curLength % 512) + wdone] = data[wdone];
		wdone++;
	}
	curLength += LOG_RECORD_SIZE;
	uint8_t *cl;
	cl = (uint8_t *) (&curLength);

	apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
			sdioLogfileDataPointer.file + OFFSET_C_LENGTH, cl, 8);
}

/**
 * @brief Verify if the SD card needs to be formatted
 *
 * This function will determin if the card is to be formatted. It will do so after examining the first 10 bytes. If they do not match the signature and version, formatting will commence.
 * Note: All data on SD-card will be lost afterwards.
 */
void apiDnfccFormatCardIfNeeded() {
#define SIGNATURE     "DNFCCCNS"
#define SDCARD_VERSION  "04"

	apiDnfccReadSectorRaw(&sdioFirstBlockDataPointer, 0);
	if (memcmp(SIGNATURE, sdioFirstBlockDataPointer.data, 8) != 0
			|| memcmp(SDCARD_VERSION, sdioFirstBlockDataPointer.data + 8,
					strlen(SDCARD_VERSION)) != 0) {
		memset(sdioFirstBlockDataPointer.data, '\0', 512);
		apiDnfccSetWriteData(&sdioFirstBlockDataPointer, 0, (uint8_t *) SIGNATURE,
				strlen(SIGNATURE));
		apiDnfccSetWriteData(&sdioFirstBlockDataPointer, 8,
				(uint8_t *) SDCARD_VERSION, strlen(SDCARD_VERSION));
		apiDnfccSetWriteData(&sdioFirstBlockDataPointer, 10, (uint8_t *) "????",
				4);

		uint8_t *startBytes = (uint8_t *) "\1\0\0\0\0\0\0\0\0";
		uint8_t *startNulls = (uint8_t *) "\0\0\0\0\0\0\0\0\0";
		uint8_t *nullVersion = (uint8_t *) "V000";
		for (int fil = 32; fil < 193; fil += 32) {
			apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
					fil + OFFSET_C_VERSION, nullVersion, 4); // Current file version
			apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
					fil + OFFSET_N_VERSION, nullVersion, 4); // Next file version
			apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
					fil + OFFSET_C_STARTSECTOR, startBytes, 8); // Sector start current file
			startBytes[0]++;
			apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
					fil + OFFSET_N_STARTSECTOR, startNulls, 8); // Sector start next file, or position of transmitting the logging.
			apiDnfccSetWriteData(&sdioFirstBlockDataPointer, fil + OFFSET_C_LENGTH,
					startNulls, 4); // Length of current file
			apiDnfccSetWriteData(&sdioFirstBlockDataPointer, fil + OFFSET_N_LENGTH,
					startNulls, 4); // Length of next file
		}
		uint8_t *logStart = (uint8_t *) "\0\x09\x3d\0\0\0\0\0"; // == 4.000.000 blocks  * 512 = 2.048.000.000 = 2GB
		apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
				LOGFILE + OFFSET_C_STARTSECTOR, logStart, 8); // Sector start log file
		apiDnfccSetWriteData(&sdioFirstBlockDataPointer,
				LOGFILE + OFFSET_C_PROCESSING, startNulls, 8); // Sector processing log file

		intDnfccFlushWriteRaw(&sdioFirstBlockDataPointer);
	}
	deviceStatus.sdioStatus = Ready;
}
/**
 * @brief Get the next message from logging, for sending it over to the backend
 *
 * @param buf - the buffer to put the information in
 */
uint8_t apiDnfccGetNextMessageFromLogging(char *buf) {
	DWORD curLength;
	DWORD curProcessed;
	memcpy(&curLength,
			sdioFirstBlockDataPointer.data + sdioLogfileDataPointer.file
					+ OFFSET_C_LENGTH, 8);
	memcpy(&curProcessed,
			sdioFirstBlockDataPointer.data + sdioLogfileDataPointer.file
					+ OFFSET_C_PROCESSING, 8);
	if (curProcessed >= curLength) {
		return 1; // No next message
	}

	if (sdioLogfileDataPointer.sectorOffsetInFile == curProcessed / 512
			&& sdioLogfileDataPointer.readStatus == READY_READ) {
		memcpy(buf, sdioLogfileDataPointer.data + curProcessed % 512,
		LOG_RECORD_SIZE);
	} else {
		apiDnfccReadSector(&sdioLogfileDataReadPointer, curProcessed / 512);
		memcpy(buf, sdioLogfileDataReadPointer.data + (curProcessed % 512),
		LOG_RECORD_SIZE);
	}
	curProcessed += LOG_RECORD_SIZE;
	memcpy(
			sdioFirstBlockDataPointer.data + sdioLogfileDataPointer.file
					+ OFFSET_C_PROCESSING, &curProcessed, 8);
	return 0; // Has next message
}
/** @} */
