/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#define SDIO_COMMUNICATION
#include "bsp_driver_sd.h"
#include "Sdio.h"
#include "FileFunctions.h"

/**
 * @file
 * @brief Defines the SDIO functions
 */

/** \addtogroup sdio
 *  @{
 */

#define STACK_SIZE 2000

SD_HandleTypeDef hsd;
HAL_SD_CardInfoTypedef SDCardInfo;
//
SdioBlockInfo sdioFirstBlockDataPointer;
SdioBlockInfo sdioFareDataPointer;
SdioBlockInfo sdioWifiDataPointer;
SdioBlockInfo sdioPrinterDataPointer;
SdioBlockInfo sdioGsmDataPointer;
SdioBlockInfo sdioBlacklistDataPointer;
SdioBlockInfo sdioBlacklistDataReadPointer;
SdioBlockInfo sdioLogfileDataPointer;
SdioBlockInfo sdioLogfileDataReadPointer;
SdioBlockInfo sdioLanguagePackDataPointer;

void nfccSdio(void *pvParameters);

firstBlockData *pFirstBlock;

#define OFFSET_C_VERSION 0
#define OFFSET_N_VERSION 4
#define OFFSET_C_STARTSECTOR 8
#define OFFSET_N_STARTSECTOR 16
#define OFFSET_C_LENGTH 24
#define OFFSET_N_LENGTH 28
/**
 * @brief Initialization routine for SD-card
 */
static void MX_SDIO_SD_Init(void) {
	hsd.Instance = SDIO;
	hsd.Init.ClockEdge = SDIO_CLOCK_EDGE_RISING;
	hsd.Init.ClockBypass = SDIO_CLOCK_BYPASS_DISABLE;
	hsd.Init.ClockPowerSave = SDIO_CLOCK_POWER_SAVE_DISABLE;
	hsd.Init.BusWide = SDIO_BUS_WIDE_1B;
	hsd.Init.HardwareFlowControl = SDIO_HARDWARE_FLOW_CONTROL_DISABLE;
	hsd.Init.ClockDiv = 40;
}

static unsigned short SdioPINS[] = { SDCARD_PIN_CMD, SDCARD_PIN_CK,
SDCARD_PIN_D0,
SDCARD_PIN_D1, SDCARD_PIN_D2, SDCARD_PIN_D3 };
static GPIO_TypeDef * SdioGPIOS[] = { SDCARD_GPIO_CMD, SDCARD_GPIO_CK,
SDCARD_GPIO_D0,
SDCARD_GPIO_D1, SDCARD_GPIO_D2, SDCARD_GPIO_D3 };

/**
 * HAL routine for initializing the SD-card
 */
void HAL_SD_MspInit(SD_HandleTypeDef* hsd) {
	GPIO_InitTypeDef GPIO_InitStruct;
	int pin;

	if (hsd->Instance == SDIO) {
		/*
		 * First pull all pins high, by initializing them as a GPIO-normal pin
		 */
		for (pin = 0; pin < 6; pin++) {
			GPIO_InitStruct.Pin = SdioPINS[pin];
			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
			GPIO_InitStruct.Pull = GPIO_PULLUP;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
			GPIO_InitStruct.Alternate = 0;
			HAL_GPIO_Init(SdioGPIOS[pin], &GPIO_InitStruct);
			HAL_GPIO_WritePin(SdioGPIOS[pin], SdioPINS[pin], GPIO_PIN_SET);
		}
		for (pin = 0; pin < 6; pin++) {
			HAL_GPIO_WritePin(SdioGPIOS[pin], SdioPINS[pin], GPIO_PIN_SET);
		}

		/* Peripheral clock enable */
		__HAL_RCC_SDIO_CLK_ENABLE()
		;

		/*
		 * Set all pins to the alternate function
		 */
		for (pin = 0; pin < 6; pin++) {
			GPIO_InitStruct.Pin = SdioPINS[pin];
			GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
			GPIO_InitStruct.Alternate = GPIO_AF12_SDIO;
			HAL_GPIO_Init(SdioGPIOS[pin], &GPIO_InitStruct);
		}
	}
}

/**
 * @brief initialize the SDIO task
 *
 * @param uxPriority - the FreeRTOS priority
 */
BaseType_t nfccSdioInit(UBaseType_t uxPriority) {
	deviceStatus.sdioRxQueue = xQueueCreate(SDIO_QUEUE_LENGTH, SDIO_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.sdioRxQueue, "Sdio");
	setWaitForSystemReadyThisTask(TASK_BIT_SDIO);
	return xTaskCreate(nfccSdio, "Sdio", STACK_SIZE, NULL, uxPriority, &deviceStatus.sdioTask);
}

/**
 * @brief the sdio main loop
 */
void nfccSdio(void *pvParameters) {
	char buf[SDIO_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;


	vTaskDelay(500); // Wait a bit, display needs to be first

	MX_SDIO_SD_Init();
	HAL_SD_MspInit(&hsd);

	pFirstBlock =(firstBlockData *) sdioFirstBlockDataPointer.data;
	BSP_SD_Init();
	vTaskDelay(1000);
	// Initialize files...
	apiDnfccInitSdioBlockInfo(&sdioFirstBlockDataPointer, FIRST_BLOCKFILE);
	apiDnfccInitSdioBlockInfo(&sdioFareDataPointer, FARE_FILE);
	apiDnfccInitSdioBlockInfo(&sdioPrinterDataPointer, PRINTER_FILE);
	apiDnfccInitSdioBlockInfo(&sdioWifiDataPointer, WIFI_FILE);
	apiDnfccInitSdioBlockInfo(&sdioBlacklistDataPointer, BLACKLIST_FILE);
	apiDnfccInitSdioBlockInfo(&sdioBlacklistDataReadPointer, BLACKLIST_FILE);
	apiDnfccInitSdioBlockInfo(&sdioLogfileDataPointer, LOGFILE);
	apiDnfccInitSdioBlockInfo(&sdioLogfileDataReadPointer, LOGFILE);

	apiDnfccReadSector(&sdioFirstBlockDataPointer, 0);	// Initialize local file handlers (filesystem)
	apiDnfccFormatCardIfNeeded();

	apiDnfccReadSector(&sdioFirstBlockDataPointer, 0);	// Initialize local file handlers (filesystem)
	apiDnfccUpdateDeviceStructVersions();

	/**** For Timing ****/
//#define T/IMING
#ifdef TIMING
	TickType_t start = xTaskGetTickCount();
	int loop = 100;
	while (1) {
		start = xTaskGetTickCount();
		while (loop > 0) {
			apiDnfccReadSector(&sdioBlacklistDataReadPointer, 0);
			apiDnfccReadSector(&sdioBlacklistDataReadPointer, 1);
			apiDnfccReadSector(&sdioBlacklistDataReadPointer, 2);
			loop--;
		}
		TickType_t end = xTaskGetTickCount() - start;  // 15 ms per run
		loop = 100;
	}

#endif

	setTaskIsSystemReady(TASK_BIT_SDIO);
	waitForSystemReady();

	for (;;) {
		buf[0] = '\0';
		if (xQueueReceive(deviceStatus.sdioRxQueue, buf, 4000) != pdPASS) {
			// If no data was received, just save all open files for now
			apiDnfccFlushWrite(&sdioFirstBlockDataPointer);
			apiDnfccFlushWrite(&sdioWifiDataPointer);
			apiDnfccFlushWrite(&sdioBlacklistDataPointer);
			apiDnfccFlushWrite(&sdioFareDataPointer);
			apiDnfccFlushWrite(&sdioGsmDataPointer);
			apiDnfccFlushWrite(&sdioLanguagePackDataPointer);
			apiDnfccFlushWrite(&sdioLogfileDataPointer);
			vTaskDelay(1);

			continue;
		}
		char stbuf[DATADISTRIBUTOR_BUFFER_SIZE];
		switch (buf[0]) {
		case COMCOM_SDIO_INITIALIZE_FARE:
		case COMCOM_SDIO_INITIALIZE_PRINTER:
		case COMCOM_SDIO_INITIALIZE_BLACKLIST:
		case COMCOM_SDIO_INITIALIZE_WIFI:
			intDnfccInitializeFileTransfer(buf);
			break;
		case COMCOM_SDIO_INIT_FILE:
			intDnfccInitializeFile(buf);	//TODO: If already in progress, ignore...
			intDnfccRequestNextBlock();
			break;
		case COMCOM_SDIO_DATA_FOR_FILE:
			if (intDnfccGetDataForFile(buf) == 1) {
				intDnfccRequestNextBlock();
			} else {
				intDnfccStoreFileAsActive();
				apiDnfccInitSdioBlockInfo(&sdioBlacklistDataReadPointer, BLACKLIST_FILE);
				stbuf[0] = COMCOM_DATADISTRIBUTOR_KEEPALIVE;
				queueSendToDataDistributor(stbuf, 0);
			}
			break;
		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(deviceStatus.soundTask);
			sprintf(stbuf, "%c%cDWM%ld",
			COMCOM_DATADISTRIBUTOR_SELFTESTRESULT, COMSELFTEST_SDIO, uxHighWaterMark);
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
		}
	}
}
/** @} */
