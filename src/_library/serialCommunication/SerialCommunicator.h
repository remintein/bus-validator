/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef LIBRARY_SERIALCOMMUNICATION_SERIALCOMMUNICATOR_H_
#define LIBRARY_SERIALCOMMUNICATION_SERIALCOMMUNICATOR_H_

#include "../serialEngine/serialEngine.h"
/**
 * @file
 * @brief Serial communication processor
 *
 * @addtogroup SerialCommunicator
 * @{
 */

enum action {
	VERIFY_DATA, /**< Verify the data is present in the received string */
	VERIFY_DATA_WITH_CALLBACK, /**< Verify by means of a callback that the data is present in the received string */
	VERIFY_DATA_AND_PROCESS, /**< Verify data and process it */
	VERIFY_DATA_AND_WAIT_FOR_RETURN, /**< Verify data and wait for return */
	EXECUTE_CALLBACK_WITH_PARAMETER, /**< Execute the callback with the string and int parameter  */
	SEND_LITERAL_TEXT, /**< Send the literal string through */
	SEND_PROCESSED_TEXT, /**< Send processed text  through */
	VALIDATE_CALLBACK, /**< Perform callback and verify */
	VERIFY_TIMEOUT_HAS_PASSED /**< Verify the int32_t timeout (ms) has passed */
};/** Action to perform */


typedef uint8_t (*SerialControllerCallback)(char *data, int32_t intData); /**< Prototype of callback routine */


typedef struct SerialControllerFlow {
	unsigned int thisStep; /**< Step number */
	int stepToRun; /**< This is the code to send to the callback routine */
	char * data; /**< Data to use in this step */
	int32_t intData; /**< Int-data to use in this step */
	unsigned int nextStep; /**< Step when outcome of the codeToPerform == 0 */
} SerialControllerFlow_type; /**< One step in the flow */

typedef struct {
	SerialEngineStruct *engine; /**< To communicate with the engine */
	SerialControllerFlow_type *flow; /**< Flow to use */
	uint16_t flowSize; /**< Size of the flow */
	unsigned int currentStep; /**< Current step */
	unsigned int uartNr; /**< UART */
	SerialControllerCallback callbackFunction; /**< In EXECUTE_CALLBACK_WITH_PARAMETER function */
	char *jsonReceiveData; /**< JSon data received */
	char *jsonRequestData; /**< JSon data requested */
	char *jsonEndPoint; /**< JSon endpoint */
} SerialCommunicatorHandler_type; /**< The flow pointer */

void apiDnfccSerialCommunicationAdvance(SerialCommunicatorHandler_type *serialFlowDescriptor);
void apiDnfccSerialCommunicationInitialize(SerialCommunicatorHandler_type *serialFlowDescriptor,
		SerialControllerFlow_type *flow, int flowSize, int uart
		//, SendFunction, ResetFunction, StringBuilderFunction, StringProcessFunction, HasWorkFunction
		);
int apiDnfccSerialCommunicationVerifyStringPresentInReceiveBuffer(SerialCommunicatorHandler_type *serialFlowDescriptor,
		char *string);

/** @} */
#endif /* LIBRARY_SERIALCOMMUNICATION_SERIALCOMMUNICATOR_H_ */
