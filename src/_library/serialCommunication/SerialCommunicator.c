/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../../project_includes.h"

#include "SerialCommunicator.h"

/**
 * @file
 * @brief Serial communication processor
 *
 * @addtogroup SerialCommunicator
 * @{
 */
typedef uint8_t (*SendReceiveFunction)(void *handel, uint8_t *command, uint16_t commandLength, uint8_t *response,
		uint16_t *pResponseLength);

typedef int (*FlowControllerCallback)(SerialCommunicatorHandler_type *handel, int codeToPerform, char **step);

/**
 * @brief Verify string is present
 *
 * @param serialFlowDescriptor - The flow Descriptor to search through
 * @param string - The string to find
 */
int apiDnfccSerialCommunicationVerifyStringPresentInReceiveBuffer(SerialCommunicatorHandler_type *serialFlowDescriptor,
		char *string) {
	return apiDnfccSerialEngineVerifyStringPresent(string, serialFlowDescriptor->uartNr);
}

/**
 * @brief Initialize the flow description
 *
 * @param serialFlowDescriptor - The flow Descriptor to search through
 * @param Flow - the flow to add
 * @param flowSize - The length of the flow
 * @param uart - The uart to have this flow work on
 */
void apiDnfccSerialCommunicationInitialize(SerialCommunicatorHandler_type *serialFlowDescriptor,
		SerialControllerFlow_type *flow, int flowSize, int uart
		//, SendFunction, ResetFunction, StringBuilderFunction, StringProcessFunction, HasWorkFunction
		) {
	serialFlowDescriptor->flow = flow;
	serialFlowDescriptor->currentStep = flow[0].thisStep;
	serialFlowDescriptor->flowSize = flowSize;
	serialFlowDescriptor->uartNr = uart;
}

/**
 * @brief Advance one go
 *
 * @param serialFlowDescriptor - The flow Descriptor to search through
 */
void apiDnfccSerialCommunicationAdvance(SerialCommunicatorHandler_type *serialFlowDescriptor) {
	int didExecuteALine = 0;
	int verifyReturnValue;
	int curStep = serialFlowDescriptor->currentStep;
	uint8_t ior;

	for (int i = 0; i < serialFlowDescriptor->flowSize; i++) {
		if (serialFlowDescriptor->flow[i].thisStep != serialFlowDescriptor->currentStep) {
			continue;
		}

		switch (serialFlowDescriptor->flow[i].stepToRun) {
		case (int) VERIFY_DATA:
			if ((verifyReturnValue = apiDnfccSerialCommunicationVerifyStringPresentInReceiveBuffer(serialFlowDescriptor,
					serialFlowDescriptor->flow[i].data)) >= 0) {
				serialFlowDescriptor->currentStep = serialFlowDescriptor->flow[i].nextStep;
				serialFlowDescriptor->engine->receiveProcessedTill = verifyReturnValue;
				didExecuteALine = 1;
			}
			break;

		case VERIFY_DATA_WITH_CALLBACK:
			if ((verifyReturnValue = apiDnfccSerialCommunicationVerifyStringPresentInReceiveBuffer(serialFlowDescriptor,
					serialFlowDescriptor->flow[i].data)) >= 0) {

				serialFlowDescriptor->callbackFunction(serialFlowDescriptor->flow[i].data,
						serialFlowDescriptor->flow[i].intData);

				serialFlowDescriptor->currentStep = serialFlowDescriptor->flow[i].nextStep;
				serialFlowDescriptor->engine->receiveProcessedTill = verifyReturnValue;
				didExecuteALine = 1;
			}
			break;

		case VERIFY_TIMEOUT_HAS_PASSED:

			if ((xTaskGetTickCount() - serialFlowDescriptor->engine->lastCommunication)
					> serialFlowDescriptor->flow[i].intData) {
				if (serialFlowDescriptor->flow[i].intData == 4999){
					vTaskDelay(1000);
					vTaskDelay(1000);

				}
				serialFlowDescriptor->currentStep = serialFlowDescriptor->flow[i].nextStep;
				didExecuteALine = 1;
			}
			break;

		case EXECUTE_CALLBACK_WITH_PARAMETER:
			// Callback
			serialFlowDescriptor->callbackFunction(serialFlowDescriptor->flow[i].data,
					serialFlowDescriptor->flow[i].intData);

			serialFlowDescriptor->currentStep = serialFlowDescriptor->flow[i].nextStep;

			didExecuteALine = 1;
			break;

		case VERIFY_DATA_AND_WAIT_FOR_RETURN:
			break;

		case SEND_LITERAL_TEXT:
			apiDnfccSerialEngineTransmit(serialFlowDescriptor->flow[i].data, serialFlowDescriptor->uartNr);
			serialFlowDescriptor->currentStep = serialFlowDescriptor->flow[i].nextStep;

			didExecuteALine = 1;
			break;

		case SEND_PROCESSED_TEXT:
			// StringBuilderFunction
			break;

		case VALIDATE_CALLBACK:
			ior = serialFlowDescriptor->callbackFunction(serialFlowDescriptor->flow[i].data,
					serialFlowDescriptor->flow[i].intData);
			if (ior) {
				serialFlowDescriptor->currentStep = serialFlowDescriptor->flow[i].nextStep;
				didExecuteALine = 1;
			}
			break;
		}
		if (didExecuteALine) {
			break;
		}
	}
	if (serialFlowDescriptor->currentStep != curStep) {
		serialFlowDescriptor->engine->lastCommunication = xTaskGetTickCount();
	}

}

/** @} */
