/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"

/**
 * @file
 * @brief Defines the functions for the LED control library
 *
 * These functions are exposed as API functions.
 */

/** \addtogroup LED_control
 *  @{
 */
/**
 * Switch the RED LED on or off
 *
 * @param on - When true, switch on, otherwise off
 */
void apiDnfccDisplayLedControlRed(uint8_t on) {
	if (on) {
		HAL_GPIO_WritePin(DISPLAY_GPIO_LED_RED, DISPLAY_PIN_LED_RED,
				GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(DISPLAY_GPIO_LED_RED, DISPLAY_PIN_LED_RED,
				GPIO_PIN_SET);
	}
}

/**
 * Switch the GREEN LED on or off
 *
 * @param on - When true, switch on, otherwise off
 */
void apiDnfccDisplayLedControlGreen(uint8_t on) {
	if (on) {
		HAL_GPIO_WritePin(DISPLAY_GPIO_LED_GREEN, DISPLAY_PIN_LED_GREEN,
				GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(DISPLAY_GPIO_LED_GREEN, DISPLAY_PIN_LED_GREEN,
				GPIO_PIN_SET);
	}
}

/**
 * Switch the BLUE LED on or off
 *
 * @param on - When true, switch on, otherwise off
 */
void apiDnfccDisplayLedControlBlue(uint8_t on) {
	if (on) {
		HAL_GPIO_WritePin(DISPLAY_GPIO_LED_BLUE, DISPLAY_PIN_LED_BLUE,
				GPIO_PIN_RESET);
	} else {
		HAL_GPIO_WritePin(DISPLAY_GPIO_LED_BLUE, DISPLAY_PIN_LED_BLUE,
				GPIO_PIN_SET);
	}
}

/** @} */
