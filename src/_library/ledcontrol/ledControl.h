/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef LIBRARY_LEDCONTROL_LEDCONTROL_H_
#define LIBRARY_LEDCONTROL_LEDCONTROL_H_

/**
 * @file
 * @brief Defines the functions for the Led control library
 */

/**
 * Switch the red LED on or off;
 *
 * @param on - 0 = set to off, otherwise is set to on
 */
void apiDnfccDisplayLedControlRed(uint8_t on);
/**
 * Switch the red GREEN on or off
 *
 * @param on - 0 = set to off, otherwise is set to on
 */
void apiDnfccDisplayLedControlGreen(uint8_t on);
/**
 * Switch the red BLUE on or off
 *
 * @param on - 0 = set to off, otherwise is set to on
 */
void apiDnfccDisplayLedControlBlue(uint8_t on);


#endif /* LIBRARY_LEDCONTROL_LEDCONTROL_H_ */
