/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../../project_includes.h"
#include "random.h"
#include <stdlib.h>

/**
 * @file
 *
 * As data will keep the information in there, it can be used as the seed for rand (but only if the processor is not ready to provide a random number)
 */

/**
 * @addtogroup Random
 * @{
 */
RNG_HandleTypeDef hrng;
static uint32_t data;

void MX_RNG_Init() {
    hrng.Instance = RNG;
    if (HAL_RNG_Init(&hrng) != HAL_OK) {
    }
}

/**
 * @brief Get random number, 1 byte, as uint8_t
 *
 * Get a uint8_t random number
 */
uint8_t apiDnfccGetRandomUint8() {
    return apiDnfccGetRandomUint32() & 0xff;
}

/**
 * @brief Get random number, 2 bytes, as uint16_t
 *
 * Get a uint16_t random number
 */
uint16_t apiDnfccGetRandomUint16() {
    return apiDnfccGetRandomUint32() & 0xffff;
}

/**
 * @brief Get random number, 4 byte, as uint32_t
 *
 * Get a uint32_t random number
 */
uint32_t apiDnfccGetRandomUint32() {
    if (hrng.State != HAL_RNG_STATE_READY) {
    	srand(data);
        data = rand();
    } else {
        HAL_RNG_GenerateRandomNumber(&hrng, &data);
    }
    return data ;
}

/**
 * @brief Fill an array of chars with random information
 *
 * @param *p - Pointer to the array
 * @param cnt - the number of bytes to full
 */
void apiDnfccFillRandomBytes(uint8_t *p, uint8_t cnt) {
    while (cnt > 4) {
        apiDnfccGetRandomUint32();
        *(p++) = data;
        *(p++) = data >> 8;
        *(p++) = data >> 16;
        *(p++) = data >> 24;
        cnt -= 4;
    }
    if (cnt > 0) {
        apiDnfccGetRandomUint32();
        *(p++) = data;
        if (cnt > 1) {
            *(p++) = data >> 8;
            if (cnt > 2) {
                *(p++) = data >> 16;
                if (cnt > 3) {
                    *(p++) = data >> 24;
                }
            }
        }
    }
}

/** @} */
