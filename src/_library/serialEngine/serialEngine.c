/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"
#include "serialEngine.h"
#include <string.h>
// Serial Engine data 1,..6

/**
 * @file
 * @brief Serial communication processor
 *
 * @addtogroup SerialCommunicator
 * @{
 */

static void intDnfccSerialEngineHandleInterrupt(int pointer);
static SerialEngineStruct serialEngines[6];

/**
 * @Brief Initialize the serial engines
 *
 * Initialize the serial engines. There are 6 (0..5) for UART 1..6
 * Each UART has one
 */
void apiDnfccInitializeSerialEngineStruct() {
// Empty out and clear all of these
	for (int i = 0; i < 6; i++) {
		serialEngines[i].lastCommunication = 0;
		serialEngines[i].curReceiveByte = 0;
		serialEngines[i].transmitPointer = 0;
		serialEngines[i].transmitPointerTill = 0;
		serialEngines[i].receivePointer = 0;
		serialEngines[i].receiveProcessedTill = 0;
	}
}
/**
 * @brief Start listening to the interface
 *
 * Starts the interface and starts listening. This is the start of the listen-code
 * @param huart - the UART to use
 * @param uart - the number of the uart (1..6)
 * @param receiveBuffer - the buffer in which the receive data will be stored
 * @param receiveBufferLength - the length of this buffer
 * @param transmitBuffer - the buffer in which the transmit data will be stored
 * @param transmitBufferLength - the length of this buffer
 */
SerialEngineStruct *apiDnfccSerialEngineStartListening(UART_HandleTypeDef *huart,
		int uart, uint8_t *receiveBuffer, uint16_t receiveBufferLength,
		uint8_t *transmitBuffer, uint16_t transmitBufferLength) {
	int pointer = uart - 1;
	serialEngines[pointer].huart = huart;
	if (transmitBufferLength > 0) {
		serialEngines[pointer].transmitBuffer = transmitBuffer;
		serialEngines[pointer].transmitBufferLength = transmitBufferLength;
	}
	if (receiveBufferLength > 0) {
		serialEngines[pointer].receiveBuffer = receiveBuffer;
		serialEngines[pointer].receiveBufferLength = receiveBufferLength;
	}
	if (serialEngines[pointer].huart->RxState == HAL_UART_STATE_READY) {
		HAL_UART_Receive_IT(serialEngines[pointer].huart,
				&(serialEngines[pointer].curReceiveByte), 1);
	}
	return &serialEngines[pointer];
}

/**
 * @brief Verify if the string is present in the receive buffer of the uart
 *
 * @param string - The string to find
 * @param uart - the number of the uart (1..6)
 */
int apiDnfccSerialEngineVerifyStringPresent(char *string, int uart) {
	uint16_t cur;
	uint16_t len = strlen(string);
	int i;

	if ((serialEngines[uart - 1].receivePointer
			+ serialEngines[uart - 1].receiveBufferLength
			- serialEngines[uart - 1].receiveProcessedTill)
			% serialEngines[uart - 1].receiveBufferLength < len) {
		return -1;
	}

	for (cur = serialEngines[uart - 1].receiveProcessedTill;
			cur != serialEngines[uart - 1].receivePointer;
			cur = (cur + 1) % serialEngines[uart - 1].receiveBufferLength) {
		for (i = 0; i < len; i++) {
			if (string[i]
					!= serialEngines[uart - 1].receiveBuffer[(cur + i)
							% serialEngines[uart - 1].receiveBufferLength]) {
				break;
			}
		}
		if (i == len) {
			return (cur + len) % serialEngines[uart - 1].receiveBufferLength;
		}
	}
	return -1;
}

/**
 * @brief Transmit a string on an uart
 *
 * @param s - The string to send
 * @param uart - The uart to send with (1..6)
 */
uint8_t apiDnfccSerialEngineTransmit(char *s, int uart) {
	int pointer = uart - 1;

	for (int i = 0; i < strlen(s); i++) {
		int nwp = (serialEngines[pointer].transmitPointerTill + 1)
				% serialEngines[pointer].transmitBufferLength;
		serialEngines[pointer].transmitBuffer[serialEngines[pointer].transmitPointerTill] =
				s[i];
		serialEngines[pointer].transmitPointerTill = nwp;
	}

	if (serialEngines[uart - 1].transmitBufferLength > 0
			&& serialEngines[pointer].huart->gState == HAL_UART_STATE_READY) {
		if (serialEngines[pointer].transmitPointer
				!= serialEngines[pointer].transmitPointerTill) {
			HAL_UART_Transmit_IT(serialEngines[pointer].huart,
					&(serialEngines[pointer].transmitBuffer[serialEngines[pointer].transmitPointer]),
					1);
			serialEngines[pointer].transmitPointer =
					(serialEngines[pointer].transmitPointer + 1)
							% serialEngines[pointer].transmitBufferLength;
		}
	}

	while (serialEngines[pointer].transmitPointerTill
			!= serialEngines[pointer].transmitPointer) {
		vTaskDelay(1);
	}

	return 1;
}

/**
 * @briefTransmit a binary array on an uart
 *
 * @param s - The data to send
 * @param len - the length of the data
 * @param uart - The uart to send with (1..6)
 */
uint8_t apiDnfccSerialEngineTransmitBinary(uint8_t *s, uint16_t len, int uart) {
	int pointer = uart - 1;
	if (serialEngines[uart - 1].transmitBufferLength > 0) {
		for (int i = 0; i < len; i++) {
			int nwp = (serialEngines[pointer].transmitPointerTill + 1)
					% serialEngines[pointer].transmitBufferLength;
			serialEngines[pointer].transmitBuffer[serialEngines[pointer].transmitPointerTill] =
					s[i];
			serialEngines[pointer].transmitPointerTill = nwp;
		}

		intDnfccSerialEngineHandleInterrupt(uart - 1);
	}
	return 1;
}

/** @} */

/**
 * @Brief Internal interrupt handler
 *
 * @param pointer - the uart number (1..6)
 */
static void intDnfccSerialEngineHandleInterrupt(int pointer) {
	if (serialEngines[pointer].huart == 0) {
		return;
	}
	HAL_UART_IRQHandler(serialEngines[pointer].huart);

	// Check if something was received
	if (serialEngines[pointer].receiveBufferLength > 0
			&& serialEngines[pointer].huart->RxXferCount == 0) {
		serialEngines[pointer].receiveBuffer[serialEngines[pointer].receivePointer] =
				serialEngines[pointer].curReceiveByte;
		serialEngines[pointer].receivePointer =
				(serialEngines[pointer].receivePointer + 1)
						% serialEngines[pointer].receiveBufferLength;
		serialEngines[pointer].receiveBuffer[serialEngines[pointer].receivePointer] =
				'\0';
		serialEngines[pointer].lastCommunication = xTaskGetTickCount();
	}

	// Check if uart is ready and something needs to be transmitted
	if (serialEngines[pointer].transmitBufferLength > 0
			&& serialEngines[pointer].huart->gState == HAL_UART_STATE_READY) {
		if (serialEngines[pointer].transmitPointer
				!= serialEngines[pointer].transmitPointerTill) {
			HAL_UART_Transmit_IT(serialEngines[pointer].huart,
					&(serialEngines[pointer].transmitBuffer[serialEngines[pointer].transmitPointer]),
					1);
			serialEngines[pointer].transmitPointer =
					(serialEngines[pointer].transmitPointer + 1)
							% serialEngines[pointer].transmitBufferLength;
		}
	}

	// Restart receive if UART Rx is ready
	if (serialEngines[pointer].huart->RxState == HAL_UART_STATE_READY) {
		HAL_UART_Receive_IT(serialEngines[pointer].huart,
				&(serialEngines[pointer].curReceiveByte), 1);
	}
}

#if SAM_UART_NR != 1
void USART1_IRQHandler(void) {
	intDnfccSerialEngineHandleInterrupt(0);
}
#endif

#if SAM_UART_NR != 2
void USART2_IRQHandler(void) {
	intDnfccSerialEngineHandleInterrupt(1);
}
#endif

#if SAM_UART_NR != 3
void USART3_IRQHandler(void) {
	intDnfccSerialEngineHandleInterrupt(2);
}
#endif

#if SAM_UART_NR != 4
void UART4_IRQHandler(void) {
	intDnfccSerialEngineHandleInterrupt(3);
}
#endif

#if SAM_UART_NR != 5
void UART5_IRQHandler(void) {
	intDnfccSerialEngineHandleInterrupt(4);
}
#endif

#if SAM_UART_NR != 6
void USART6_IRQHandler(void) {
	intDnfccSerialEngineHandleInterrupt(5);
}
#endif
