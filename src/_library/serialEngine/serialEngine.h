/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef SERIAL_ENGINE_H_
#define SERIAL_ENGINE_H_

typedef struct {
    TickType_t lastCommunication; // Last tick some receive communication was done

    uint8_t curReceiveByte; // Internal byte for receiving something

    uint16_t receivePointer; // Pointer of first empty byte in receive buffer
    uint16_t receiveProcessedTill; // Pointer till which the receive buffer was handled
    uint8_t *receiveBuffer; // Pointer to the receive buffer
    uint16_t receiveBufferLength; // Length of the receive buffer

    uint16_t transmitPointer; // Pointer to the first byte to transmit
    uint16_t transmitPointerTill; // Pointer to first empty byte for transmission
    uint8_t *transmitBuffer; // Pointer to the transmit buffer
    uint16_t transmitBufferLength; // Length of the transmit buffer
    UART_HandleTypeDef *huart;
} SerialEngineStruct;

void apiDnfccInitializeSerialEngineStruct(); // Initialize this library
SerialEngineStruct * apiDnfccSerialEngineStartListening(UART_HandleTypeDef *huart, int uart,
        uint8_t *receiveBuffer, uint16_t receiveBufferLength, uint8_t *transmitBuffer,
        uint16_t transmitBufferLength); // Start the engine to listen - and initialize this specific receiver
int apiDnfccSerialEngineVerifyStringPresent(char *string, int uart); // Verify if a string is present in the uart receive buffer
uint8_t apiDnfccSerialEngineTransmit(char *s, int uart); // Transmit a string ending with \0
uint8_t apiDnfccSerialEngineTransmitBinary(uint8_t *s, uint16_t len, int uart); // Transmit a sequence of char with len len

#endif /* SERIAL_ENGINE_H_ */
