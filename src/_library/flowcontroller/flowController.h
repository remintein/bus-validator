/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef FLOWCONTROLLER_H_
#define FLOWCONTROLLER_H_

/**
 * @file
 * @brief Defines the functions for the clock module
 */

/**
 * Status of the flow controller
 */
typedef enum {
	FLOWCONTROLLER_STATUS_UNDEFINED /** Status not defined */,
	FLOWCONTROLLER_STATUS_INITIALIZED /** Status is initialized */,
	FLOWCONTROLLER_STATUS_ERROR /** Status is some error */
} flowController_status;

/**
 * Default step, need to go to the next step, i.s.o. the number provided.
 */
#define FC_NEXT_STEP 0xffff

/**
 * The following callback function needs to be defined when used, it will get back the handel, as given by the init-function and the code to perform (int, or cast it to the flow's enum)
 * @param handel  Link to the data structure provided
 * @param codeToPerform  number to give to the callback function
 * @param step pointer to a string pointer which can be pointing to a string, for debug
 */
typedef int (*FlowControllerCallback)(void *handel, int codeToPerform,
		char **step);

/**
 * Step in the flow. It contains definition of 1 step
 */
typedef struct flowControllerflow {
	unsigned int thisStep /** Step number of the current step */;
	int codeToPerform /** This is the code to send to the callback routine */;
	unsigned int okStep /** Step when outcome of the codeToPerform == 1 */;
	unsigned int errStep /** Step when outcome of the codeToPerform == 0 */;
} flowControllerflow_type;

/**
 * Defines the flow with the current status
 */
typedef struct flowController {
	flowController_status status /** The current status of the flow controller */;
	unsigned int currentStep /** At which step is it now */;
	flowControllerflow_type *theFlow /** Pointer to the flow (a list of enums) */;
	unsigned int numSteps /** The max number of steps in the file */;
	FlowControllerCallback callbackFunction /** Function to call to verify whether to go to the OK or the ERR step */;
	void *handel; /** Handler to give back to the callback routine */
	;
	int debug; /** 1 = Send logging to the display */
	;
} flowController_type;

/**
 * Function to initialize the flow
 *
 * @param controller - pointer to the flow controller (will be initialized with the parameters)
 * @param flow - pointer to the flow
 * @param callbackRoutine - Pointer to the routine to call for evaluating the step
 * @param handel - pointer to some data structure
 * @param flowLength, the number of steps in the flow
 */
int apiDnfccFlowController_initFlow(flowController_type *controller,
		flowControllerflow_type *flow, FlowControllerCallback callbackRoutine,
		void *handel, unsigned int flowLength);
/**
 * Function which will run a step and set the internal status to the next step
 *
 * @param controller - pointer to the flow controller
 */
unsigned int apiDnfccFlowController_runNextStep(flowController_type *controller);

/**
 * Function to enable debug mode
 *
 * @param controller - pointer to the flow controller
 */
void apiDnfccFlowController_enableDebug(flowController_type *controller);

/**
 * Function to disable debug mode
 *
 * @param controller - pointer to the flow controller
 */
void apiDnfccFlowController_disableDebug(flowController_type *controller);

#endif /* FLOWCONTROLLER_H_ */
