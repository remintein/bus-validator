/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include <string.h>
#include <strings.h>
#include "./flowController.h"

#include <stdio.h>

void apiDnfccLogFlowcontrollerString(char *s);

/**
 * @file
 * @brief function to determin if the step is present in the flow - used during initialization.
 *
 * @param flow - pointer to all steps in the flow
 * @param flowLength - the length of the flow
 * @param step - the step to verify
 */
static int flowController_hasStep(flowControllerflow_type *flow,
		unsigned int flowLength, unsigned int step) {
	if (step == FC_NEXT_STEP) {
		return 1;
	}
	for (unsigned int i = 0; i < flowLength; i++) {
		if (flow[i].thisStep == step) {
			return 1;
		}
	}
	return 0;
}

/** \addtogroup FlowController
 *  @{
 */

/**
 * @brief Initialize flow controller with a flow
 *
 * @param controller - Pointer to controller to connect to the flow
 * @param flow - Pointer to the complete flow
 * @param callbackRoutine - Pointer to call when running a step
 * @param handel - Pointer to user object called with the callbackRoutine
 * @param flowLength - number of steps in the flow (can be calculated from sizeof(flow) / sizeof(flow element pointer)
 */
int apiDnfccFlowController_initFlow(flowController_type *controller,
		flowControllerflow_type *flow, FlowControllerCallback callbackRoutine,
		void *handel, unsigned int flowLength) {

	controller->currentStep = 0;
	controller->theFlow = flow;
	controller->numSteps = flowLength;
	controller->callbackFunction = callbackRoutine;
	controller->handel = handel;
	controller->debug = 0;

	controller->status = FLOWCONTROLLER_STATUS_INITIALIZED;

	// Verify flow
	unsigned int stp;
	for (stp = 0; stp < flowLength; stp++) {
		if (!flowController_hasStep(flow, flowLength, flow[stp].okStep)) {
			controller->status = FLOWCONTROLLER_STATUS_ERROR;
		}
		if (!flowController_hasStep(flow, flowLength, flow[stp].errStep)) {
			controller->status = FLOWCONTROLLER_STATUS_ERROR;
		}
	}
	if (flow[flowLength - 1].okStep == FC_NEXT_STEP
			|| flow[flowLength - 1].errStep == FC_NEXT_STEP) {
		controller->status = FLOWCONTROLLER_STATUS_ERROR;
	}

	return controller->status == FLOWCONTROLLER_STATUS_ERROR;
}

/**
 * @brief Perform one step, calling the callbackRoutine with the right parameters.
 *
 * Parameters to call the callbackRoutine with are handel, codeToPerform (from current step) and a pointer to a string-pointer to put the step name in (for debug)
 * Will go to the OK step if the outcome of the callback != 0, otherwise go to the errStep.
 * Also if the debugging it on it will send the current-step, current-step-field and the string step name to the display.
 *
 * @param controller - Pointer to controller to connect to the flow
 */
unsigned int apiDnfccFlowController_runNextStep(flowController_type *controller) {
	char outcome;

	char *step = "";

	unsigned int nextStep = 0;

	outcome = controller->callbackFunction(controller->handel,
			controller->theFlow[controller->currentStep].codeToPerform, &step);

	if (outcome == 0) {
		nextStep = controller->theFlow[controller->currentStep].errStep;
	} else {
		nextStep = controller->theFlow[controller->currentStep].okStep;
	}
	if (nextStep == FC_NEXT_STEP) {
		controller->currentStep++;
	} else {
		for (unsigned int i = 0; i < controller->numSteps; i++) {
			if (controller->theFlow[i].thisStep == nextStep) {
				controller->currentStep = i;
				break;
			}
		}
	}
	if (controller->debug) {
		char buf[100];

		if (outcome == 0) {
			sprintf(buf, "E => %d-%d %s", controller->currentStep,
					controller->theFlow[controller->currentStep].thisStep,
					step);
		} else {
			sprintf(buf, "  => %d-%d %s", controller->currentStep,
					controller->theFlow[controller->currentStep].thisStep,
					step);
		}
		apiDnfccLogFlowcontrollerString(buf);
	}
	return controller->currentStep;
}

/**
 * @brief function to switch on the debug logging on flow controller
 *
 * @param controller: the controller to set the debug on
 */
void apiDnfccFlowController_enableDebug(flowController_type *controller) {
	controller->debug = 1;
}

/**
 * @brief function to switch off the debug logging on flow controller
 *
 * @param controller: the controller to set the debug off
 */
void apiDnfccFlowController_disableDebug(flowController_type *controller) {
	controller->debug = 0;
}

/** @} */
