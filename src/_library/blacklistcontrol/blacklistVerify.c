/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../../project_includes.h"
#include <string.h>

#include "blacklistVerify.h"

/**
 * @file
 * @brief Defines the functions for the blacklist library
 */

static int intDnfccProcessIndexBlock(unsigned char * cardId,
		unsigned char *block);
static int intDnfccProcessDataBlock(unsigned char * cardId,
		unsigned char *block);

/**
 * Pointer to the Sd-file containing the blacklist data Read pointer
 */
extern SdioBlockInfo sdioBlacklistDataReadPointer;
/**
 * Pointer to the Sd-file containing the first block pointer
 */
extern SdioBlockInfo sdioFirstBlockDataPointer;

/** \addtogroup Blacklist
 *  @{
 */

/**
 * This routine will verify whether a card is in the blacklist data file
 *
 * It will return 1 if the card is NOT in the blacklist and 0 if it is
 *
 * First it will wait to verify that the first data pointer is read
 * Then it will read the first sector of the blacklist, and depending on wether it is a index or a data block
 * verify whether the card ID is in there, or which block to read
 *
 * @param Card ID (10 bytes BCD coded)
 */
int apiDnfccVerifyCardIsNotInBlacklist(unsigned char * cardId) {
	uint8_t *block;
	DWORD blockPointer = 0;
	int loops = 0;
	block = sdioBlacklistDataReadPointer.data;
	while (sdioFirstBlockDataPointer.readStatus == EMPTY) {
		vTaskDelay(10);
	}
	apiDnfccReadSector(&sdioBlacklistDataReadPointer, blockPointer);
// When status is not READY_READ (this can happen if no file is present yet) all card are not in the blacklist
	while (sdioBlacklistDataReadPointer.readStatus == READY_READ) {
		if ((block[0] & 0xff) == 0xff) {
			return intDnfccProcessDataBlock(cardId, block);
		} else {
			blockPointer = intDnfccProcessIndexBlock(cardId, block);
			if (blockPointer == -1) {
				return 1;
			}
			if (blockPointer == -2) {
				return 0;
			}
			apiDnfccReadSector(&sdioBlacklistDataReadPointer, blockPointer);
			if (++loops > 5) {
				break;
			}
		}
	}

	return 1;
}

/** @}  */


/**
 * Internal routine to verify if a card ID is in the data block provided in the argument
 *
 *  @param Card ID (10 bytes BCD coded)
 *  @param block - pointer to the block of data
 *
 *  @return 1 if the card is not in the list
 *  @return 0 if the card is in the list.
 */
static int intDnfccProcessDataBlock(unsigned char * cardId,
		unsigned char *block) {
	int length = block[1];
	int i;
	int mc;

	if (memcmp(&block[2], cardId, length) != 0) {
		return 1;
	}

	for (i = 2 + length; i < 512; i += (10 - length)) {
		if ((mc = memcmp(&block[i], &cardId[length], (10 - length))) == 0) {
			return 0;
		}
		if (mc > 0) {
			return 1;
		}
	}
	return 1;
}

/**
 * Internal routine to verify if a card ID is in the index block provided in the argument
 *
 *  @param Card ID (10 bytes BCD coded)
 *  @param block - pointer to the block of data
 *
 *  @return -2 if the card is in the index list
 *  @return -1 if the card is not to be found with this index file (eg. is before)
 *  @return block to read next
 */
static int intDnfccProcessIndexBlock(unsigned char * cardId,
		unsigned char *block) {
	int i, lower;
	lower = 2;
	for (i = 2; i < 512; i += 10) {
		if (memcmp(&block[i], cardId, 10) >= 0) {
			if (memcmp(&block[i], cardId, 10) == 0) {
				return -2;
			}
			break;
		}
		lower = i;
	}
	if (i == 2) {
		if (memcmp(&block[i], cardId, 10) > 0) {
			return -1;
		}
	}
	return block[0] * 256 + block[1] + lower / 10;
}

