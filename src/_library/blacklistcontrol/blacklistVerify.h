/*
 * blacklistVerify.h
 *
 *  Created on: 16 jul. 2018
 *      Author: aart
 */

#ifndef LIBRARY_BLACKLISTCONTROL_BLACKLISTVERIFY_H_
#define LIBRARY_BLACKLISTCONTROL_BLACKLISTVERIFY_H_

int apiDnfccVerifyCardIsNotInBlacklist(unsigned char * cardId);
void apiDnfccReadSector(SdioBlockInfo *p, DWORD sectorOffset);

#endif /* LIBRARY_BLACKLISTCONTROL_BLACKLISTVERIFY_H_ */
