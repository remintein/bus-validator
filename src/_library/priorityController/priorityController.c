/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"


/**
 * The current priority settings. Default IDLE
 */
static PriorityState currentPriority = PRIORITY_IDLE;
/**
 * When the last time the priority was set. After 20 seconds it will go from CARD_HANDLING to IDLE.
 */
static TickType_t lastSet = 0;

/** \addtogroup Priority
 * @{
 */
/**
 * @brief set priority state
 *
 * Set the priority state to Card Handling, Idle or Get data from backend
 * @param state - the state to set
 */
void apiDnfccSetPriorityTo(PriorityState state) {
    lastSet = xTaskGetTickCount();
    currentPriority = state;

    switch (state) {
    case PRIORITY_CARD_HANDLING:
        apiDnfccDisplayLedControlBlue(1);
        break;
    case PRIORITY_IDLE:
        apiDnfccDisplayLedControlBlue(0);
        break;
    case PRIORITY_GET_DATA_FROM_BACKEND:
        apiDnfccDisplayLedControlBlue(0);
        break;
    }
}

/**
 * @brief get priority state
 *
 * Get the current priority. The priority will default to IDLE, when the CARD-Handling is last done 20 seconds or more ago
 */
PriorityState apiDnfccGetPriority() {
    if (currentPriority == PRIORITY_CARD_HANDLING) {
        TickType_t curTick = xTaskGetTickCount();
        if (curTick - lastSet > 20000) {
            apiDnfccSetPriorityTo(PRIORITY_IDLE);
        }
    }
    return currentPriority;
}

/** @} */
