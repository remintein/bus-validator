/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"

#define SMARTCARDIF_COMMUNICATION
#include "SmartcardIf.h"
#include <phNfcLib.h>
#include "DesfireFunctions.h"
#include "CardHandler.h"
#include "flowController/flowController.h"
#include "../_definition/deviceStatus.h"
#include "../_library/flowcontroller/flowController.h"


/**
 * @file
 *
 * This file contains a authenticate function
 *
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */
static DesfireHandlingStruct desfireHandlingStruct;


extern flowControllerflow_type theMainFlow[];
extern  int theMainFlowLength;

#define STACK_SIZE 3000

int flowController_callBack(void *handel, int codeToPerform, char **step);

/**
 * @brief Init routine for the smartcard interface
 *
 * @param uxPriority - The priority of the smartcard-task
 */
BaseType_t nfccSmartcardIfInit(UBaseType_t uxPriority) {

	deviceStatus.smartcardRxQueue = xQueueCreate(SMARTCARDIF_QUEUE_LENGTH,
			SMARTCARDIF_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.smartcardRxQueue, "SmartCard");
	setWaitForSystemReadyThisTask(TASK_BIT_SMARTCARD);
	return xTaskCreate(nfccSmartcardIf, "smartcardIf", STACK_SIZE, NULL,
			uxPriority, &deviceStatus.smartcardTask);
}

/**
 * @brief Main loop of the smartcard interface
 *
 * @param pvParameters - parameters gotten from FreeRTOS
 */
void nfccSmartcardIf(void *pvParameters) {
	flowController_type mainFlow;
	char buf[SMARTCARDIF_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;

	CardHandlerInitializer();

	apiDnfccFlowController_initFlow(&mainFlow, theMainFlow, flowController_callBack,
			&desfireHandlingStruct, theMainFlowLength);

	setTaskIsSystemReady(TASK_BIT_SMARTCARD);
	waitForSystemReady();

	while (1) {
		unsigned int nextStep = apiDnfccFlowController_runNextStep(&mainFlow);
		if (nextStep == 0) {
			vTaskDelay(5);
		}
		if (xQueueReceive(deviceStatus.smartcardRxQueue, buf, 1) == pdPASS) {
			switch (buf[0]) {
			case '?':
				uxHighWaterMark = uxTaskGetStackHighWaterMark(
						deviceStatus.smartcardTask);
				sprintf(stbuf, "!SWM%ld", uxHighWaterMark);

				stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
				stbuf[1] = COMSELFTEST_SMARTCARDIF;
				queueSendToDataDistributor(stbuf, portMAX_DELAY);
				break;
			}
		}
	}
}

/** @} */
