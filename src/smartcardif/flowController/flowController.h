/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_FLOWCONTROLLER_H_
#define SMARTCARDIF_FLOWCONTROLLER_H_

#include "../DesfireFunctions.h"
#include "../CardHandler.h"
#include "../../_configuration/deviceSelector.h"

typedef enum {
// START_DEFINE_OF_ENUM Leave this for auto-update by script
#if CURRENT_TARGET == CNS_BUSCONSOLE
FC_STEP_BEGIN,
FC_STEP_CARD_IS_NOT_IN_BLACKLIST,
FC_STEP_CARD_READ_INFO,
FC_STEP_DETECT_CARD,
FC_STEP_DISPLAY_REJECT,
FC_STEP_DISPLAY_TRY_AGAIN,
FC_STEP_END_CARDCOMMUNICATION,
FC_STEP_GET_NEXT_CARD_DESFIRE,
FC_STEP_INITIALIZE,
FC_STEP_INIT_ENCRYPTION,
FC_STEP_IS_NOT_CARD_IGNORED,
FC_STEP_MENU_MODE_BUSCONSOLE_MAY_HANDLE_CARD,
FC_STEP_RATS,
FC_STEP_SAM_RCINIT,
FC_STEP_SET_CARD_STATUS_P2A,
FC_STEP_SET_IGNORE_CARD,
FC_STEP_SYSTEM_IS_LOGGED_IN,
FC_STEP_TICKET_READ_INFO,
FC_STEP_UNSET_IGNORE_CARD,
FC_STEP_VERIFY_CARD_ACTIVE,
FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED,
FC_STEP_VERIFY_CARD_STATUS_A,
FC_STEP_VERIFY_CARD_STATUS_P,
FC_STEP_VERIFY_TICKET_IS_NOT_EXPIRED,
FC_STEP_WRITE_TICKET_LOG_FILE,
FLOWCONTROLLER_STEP_BUSDRIVER_DISPLAY_OK,
FLOWCONTROLLER_STEP_BUSDRIVER_DISPLAY_OK_LOW_BALANCE,
FLOWCONTROLLER_STEP_DISPLAY_OK,
FLOWCONTROLLER_STEP_DISPLAY_OK_LOW_BALANCE,
FLOWCONTROLLER_STEP_DISPLAY_START_SCREEN,
FLOWCONTROLLER_STEP_INCREASE_CARD_TXN_COUNT,
FLOWCONTROLLER_STEP_NULL,
FLOWCONTROLLER_STEP_PRINT_RECEIPT,
FLOWCONTROLLER_STEP_PURSE_BALANCE_SELECT,
FLOWCONTROLLER_STEP_PURSE_LOW_BALANCE,
FLOWCONTROLLER_STEP_PURSE_PAY,
FLOWCONTROLLER_STEP_PURSE_READ_INFO,
FLOWCONTROLLER_STEP_PURSE_REJECT,
FLOWCONTROLLER_STEP_TICKET_BALANCE_SELECT,
FLOWCONTROLLER_STEP_TICKET_LOW_BALANCE,
FLOWCONTROLLER_STEP_TICKET_PAY,
FLOWCONTROLLER_STEP_TICKET_REJECT,
FLOWCONTROLLER_STEP_VERIFY_PURSE_EXPIRED_DATE,
FLOWCONTROLLER_STEP_VERIFY_PURSE_STATUS,
FLOWCONTROLLER_STEP_VERIFY_TICKET_STATUS,
FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_1,
FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_2,
FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_6,
FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_9,
FLOWCONTROLLER_STEP_WRITE_PURSE_LOG_FILE,
#endif
#if CURRENT_TARGET == CNS_VALIDATOR
//FC_STEP_BEGIN,
//FC_STEP_CARD_IS_NOT_IN_BLACKLIST,
//FC_STEP_CARD_READ_INFO,
//FC_STEP_DETECT_CARD,
//FC_STEP_DISPLAY_REJECT,
//FC_STEP_DISPLAY_TRY_AGAIN,
//FC_STEP_END_CARDCOMMUNICATION,
//FC_STEP_GET_NEXT_CARD_DESFIRE,
//FC_STEP_INITIALIZE,
//FC_STEP_INIT_ENCRYPTION,
//FC_STEP_IS_NOT_CARD_IGNORED,
//FC_STEP_MENU_MODE_VALIDATOR_MAY_HANDLE_CARD,
//FC_STEP_RATS,
//FC_STEP_SET_CARD_STATUS_P2A,
//FC_STEP_SET_CARD_STATUS_A2P,
//FC_STEP_SET_IGNORE_CARD,
//FC_STEP_SYSTEM_IS_LOGGED_IN,
//FC_STEP_TICKET_READ_INFO,
//FC_STEP_UNSET_IGNORE_CARD,
//FC_STEP_VERIFY_CARD_ACTIVE,
//FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED,
//FC_STEP_VERIFY_CARD_STATUS_A,
//FC_STEP_VERIFY_CARD_STATUS_P,
//FC_STEP_VERIFY_TICKET_IS_NOT_EXPIRED,
//FC_STEP_WRITE_TICKET_LOG_FILE,
//FLOWCONTROLLER_STEP_BUSDRIVER_DISPLAY_OK,
//FLOWCONTROLLER_STEP_BUSDRIVER_DISPLAY_OK_LOW_BALANCE,
//FLOWCONTROLLER_STEP_BUSDRIVER_DISPLAY_REJECT,
//FLOWCONTROLLER_STEP_DISPLAY_OK,
//FLOWCONTROLLER_STEP_DISPLAY_OK_LOW_BALANCE,
//FLOWCONTROLLER_STEP_DISPLAY_START_SCREEN,
//FLOWCONTROLLER_STEP_INCREASE_CARD_TXN_COUNT,
//FLOWCONTROLLER_STEP_NULL,
//FLOWCONTROLLER_STEP_PRINT_RECEIPT,
//FLOWCONTROLLER_STEP_PURSE_BALANCE_SELECT,
//FLOWCONTROLLER_STEP_PURSE_LOW_BALANCE,
//FLOWCONTROLLER_STEP_PURSE_PAY,
//FLOWCONTROLLER_STEP_PURSE_READ_INFO,
//FLOWCONTROLLER_STEP_PURSE_REJECT,
//FLOWCONTROLLER_STEP_TICKET_BALANCE_SELECT,
//FLOWCONTROLLER_STEP_TICKET_LOW_BALANCE,
//FLOWCONTROLLER_STEP_TICKET_PAY,
//FLOWCONTROLLER_STEP_TICKET_REJECT,
//FLOWCONTROLLER_STEP_VERIFY_PURSE_EXPIRED_DATE,
//FLOWCONTROLLER_STEP_VERIFY_PURSE_STATUS,
//FLOWCONTROLLER_STEP_VERIFY_TICKET_STATUS,
//FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_1,
//FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_2,
//FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_6,
//FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_9,
//FLOWCONTROLLER_STEP_WRITE_PURSE_LOG_FILE,
FC_STEP_BEGIN,
FC_STEP_INITIALIZE,
FC_STEP_CARD_READ_INFO,
FC_STEP_DETECT_CARD,
FC_STEP_DISPLAY_REJECT,
FC_STEP_DISPLAY_TRY_AGAIN,
FC_STEP_END_CARDCOMMUNICATION,
FC_STEP_GET_NEXT_CARD_DESFIRE,
FC_STEP_MENU_MODE_VALIDATOR_MAY_HANDLE_CARD,
FC_STEP_RATS,
FLOWCONTROLLER_STEP_DISPLAY_OK,
FLOWCONTROLLER_STEP_DISPLAY_START_SCREEN,
FLOWCONTROLLER_STEP_NULL,
#endif
#if CURRENT_TARGET == CNS_HANDHELD
FC_STEP_BEGIN,
FC_STEP_CARD_IS_NOT_IN_BLACKLIST,
FC_STEP_CARD_READ_INFO,
FC_STEP_CLEAR_PENALTY_PAY,
FC_STEP_DETECT_CARD,
FC_STEP_DISPLAY_HANDHELD_LOG_FILE,
FC_STEP_DISPLAY_REJECT,
FC_STEP_DISPLAY_TRY_AGAIN,
FC_STEP_END_CARDCOMMUNICATION,
FC_STEP_GET_NEXT_CARD_DESFIRE,
FC_STEP_HANDHELD_DISPLAY_CARD_IS_FAIL,
FC_STEP_HANDHELD_DISPLAY_CARD_IS_PASS,
FC_STEP_HANDHELD_PAY_PENALTY,
FC_STEP_HANDHELD_READ_LOGFILE,
FC_STEP_HANDHELD_VERIFY_LOGFILE,
FC_STEP_HANDHELD_VERIFY_TICKET_BALANCE_ENOUGH_FOR_PENALTY,
FC_STEP_INITIALIZE,
FC_STEP_INIT_ENCRYPTION,
FC_STEP_IS_NOT_CARD_IGNORED,
FC_STEP_IS_PAY_CARD_SELECTED,
FC_STEP_IS_PAY_CASH_SELECTED,
FC_STEP_JUMP_TO_START_SCREEN,
FC_STEP_LOGIN_SYSTEM,
FC_STEP_MENU_MODE_HANDHELD_MAY_HANDLE_CARD,
FC_STEP_RATS,
FC_STEP_SET_CARD_STATUS_P2A,
FC_STEP_SET_IGNORE_CARD,
FC_STEP_SYSTEM_IS_LOGGED_IN,
FC_STEP_UNSET_IGNORE_CARD,
FC_STEP_VERIFY_CARD_ACTIVE,
FC_STEP_VERIFY_CARD_IS_LOGINTYPE,
FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED,
FC_STEP_VERIFY_CARD_STATUS_A,
FC_STEP_VERIFY_CARD_STATUS_P,
FC_STEP_VERIFY_TICKET_IS_NOT_EXPIRED,
FC_STEP_WRITE_HANDHELD_LOG_FILE,
FC_STEP_WRITE_TICKET_LOG_FILE,
#endif
#if CURRENT_TARGET == CNS_TOPUP
FC_STEP_BEGIN,
FC_STEP_CARD_IS_NOT_IN_BLACKLIST,
FC_STEP_CARD_READ_INFO,
FC_STEP_DETECT_CARD,
FC_STEP_DISPLAY_REJECT,
FC_STEP_DISPLAY_TRY_AGAIN,
FC_STEP_END_CARDCOMMUNICATION,
FC_STEP_GET_NEXT_CARD_DESFIRE,
FC_STEP_INCREASE_TICKET_VALUE,
FC_STEP_INITIALIZE,
FC_STEP_INIT_ENCRYPTION,
FC_STEP_IS_NOT_CARD_IGNORED,
FC_STEP_LOGIN_SYSTEM,
FC_STEP_MENU_MODE_TOPUP_MAY_HANDLE_CARD,
FC_STEP_PRINT_BILL,
FC_STEP_RATS,
FC_STEP_SET_CARD_STATUS_P2A,
FC_STEP_SET_IGNORE_CARD,
FC_STEP_SET_TICKET_VALUE_TO_0,
FC_STEP_SYSTEM_IS_LOGGED_IN,
FC_STEP_TICKET_READ_INFO,
FC_STEP_TOPUP_VALUE_IS_SELECTED,
FC_STEP_TOP_UP_DISPLAY_SCREEN,
FC_STEP_UNSET_IGNORE_CARD,
FC_STEP_VERIFY_CARD_ACTIVE,
FC_STEP_VERIFY_CARD_IS_LOGINTYPE,
FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED,
FC_STEP_VERIFY_CARD_STATUS_A,
FC_STEP_VERIFY_CARD_STATUS_P,
FC_STEP_VERIFY_TICKET_IS_NOT_EXPIRED,
FC_STEP_WRITE_TOPUP_LOG_FILE,
#endif
// END_DEFINE_OF_ENUM Leave this for auto-update by script
} flowControllerStep_enum;

// START_DEFINE_OF_FUNCTIONS Leave this for auto-update by script
uint8_t flowControllerBegin(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerapiDnfccVerifyCardIsNotInBlacklist(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerReadCardInfo(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerDetectCard(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerDisplayReject(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerStepTryAgain(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerEndCardCommunication(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerGetNextCardDesfire(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerInitialize(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerInitEncrytion(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerIsNotCardIgnored(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerIsMenuBusconsoleMayHandleCard(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerRats(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerSamRcInit(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerSetCardFromPtoA(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerSetCardFromAtoP(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerIgnoreCard(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerSystemIsLoggedIn(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerReadTicketInfo(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerUnIgnoreCard(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerStepVerifyCardActive(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyIfCardIsNotExpired(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyCardStatusA(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyCardStatusP(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyTicketIsNotExpired(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerWriteTicketLog(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerDisplayBusDriverOK(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerDisplayBusDriverOKLowBalance(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerDisplayOK(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerDisplayOKLowBalance(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerDisplayStartScreen(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerIncreaseCardTxnCount(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerStepNull(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerPrintReceipt(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerPurseBalanceSelect(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerPurseLowBalance(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerPursePay(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerReadPurseInfo(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerPurseReject(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerTicketBalanceSelect(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerTicketLowBalance(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerTicketPay(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerTicketReject(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyPurseExpired_Date(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyPurseStatus(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyTicketStatus(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyTicketType1(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyTicketType2(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyTicketType6(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerVerifyTicketType9(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerWritePurseLog(char **step, DesfireHandlingStruct *controller);
uint8_t flowControllerIsTopupValueEnabled(char **step, DesfireHandlingStruct *controller) ;
uint8_t flowControllerSetTicketValueNul(char **step,DesfireHandlingStruct *controller);
uint8_t flowControllerDisplayTicketValueInTopup(char **step, DesfireHandlingStruct *controller) ;
uint8_t flowControllerLogTopup(char **step, DesfireHandlingStruct *controller) ;
uint8_t flowControllerPrintBill(char **step, DesfireHandlingStruct *controller) ;
uint8_t flowControllerIsMenuTopupMayHandleCard(char **step, DesfireHandlingStruct *controller) ;
uint8_t flowControllerLoginSystem(char **step, DesfireHandlingStruct *controller) ;
uint8_t flowControllerIncrementTicketValue(char **step, DesfireHandlingStruct *controller) ;
uint8_t flowControllerCheckCardIsLoginType(char **step, DesfireHandlingStruct *controller) ;

// END_DEFINE_OF_FUNCTIONS Leave this for auto-update by script
#endif /* SMARTCARDIF_FLOWCONTROLLER_H_ */
