/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include <string.h>
#include <strings.h>
#include "../../_library/flowcontroller/flowController.h"
#include "flowController.h"
#include <semphr.h>

#include "../../project_includes.h"
extern CnsCardInterfaceInfo apiInterface;

void SamPerformReset();

/**
 * @file
 * @brief Contains definitions about the files
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

/**
 * @brief Callback routine, which is called by the flow controller. Will process only the correct messages for the device - to make the image smaller.
 * The compiler/linker will only add used calls to the image.
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
int flowController_callBack(DesfireHandlingStruct *handel, int codeToPerform,
		char **step) {
	uint16_t outcome;
	char displMsg[DISPLAY_BUFFER_SIZE + 1];

	strcpy(apiInterface.previousReason, apiInterface.reason);
	apiInterface.reason[0] = '\0';
	vTaskDelay(1);
	if (deviceStatus.samNeedReset) {
		SamPerformReset();
		codeToPerform = FC_STEP_INITIALIZE;
	}

	*step = "?";
	switch (codeToPerform) {
	// START_DEFINE_OF_CASE Leave this for auto-update by script
#if CURRENT_TARGET == CNS_BUSCONSOLE
	case FC_STEP_BEGIN : outcome = flowControllerBegin(step, handel); break;
	case FC_STEP_CARD_IS_NOT_IN_BLACKLIST : outcome = flowControllerapiDnfccVerifyCardIsNotInBlacklist(step, handel); break;
	case FC_STEP_CARD_READ_INFO : outcome = flowControllerReadCardInfo(step, handel); break;
	case FC_STEP_DETECT_CARD : outcome = flowControllerDetectCard(step, handel); break;
	case FC_STEP_DISPLAY_REJECT : outcome = flowControllerDisplayReject(step, handel); break;
	case FC_STEP_DISPLAY_TRY_AGAIN : outcome = flowControllerStepTryAgain(step, handel); break;
	case FC_STEP_END_CARDCOMMUNICATION : outcome = flowControllerEndCardCommunication(step, handel); break;
	case FC_STEP_GET_NEXT_CARD_DESFIRE : outcome = flowControllerGetNextCardDesfire(step, handel); break;
	case FC_STEP_INITIALIZE : outcome = flowControllerInitialize(step, handel); break;
	case FC_STEP_INIT_ENCRYPTION : outcome = flowControllerInitEncrytion(step, handel); break;
	case FC_STEP_IS_NOT_CARD_IGNORED : outcome = flowControllerIsNotCardIgnored(step, handel); break;
	case FC_STEP_MENU_MODE_BUSCONSOLE_MAY_HANDLE_CARD : outcome = flowControllerIsMenuBusconsoleMayHandleCard(step, handel); break;
	case FC_STEP_RATS : outcome = flowControllerRats(step, handel); break;
	case FC_STEP_SAM_RCINIT : outcome = flowControllerSamRcInit(step, handel); break;
	case FC_STEP_SET_CARD_STATUS_P2A : outcome = flowControllerSetCardFromPtoA(step, handel); break;
	case FC_STEP_SET_IGNORE_CARD : outcome = flowControllerIgnoreCard(step, handel); break;
	case FC_STEP_SYSTEM_IS_LOGGED_IN : outcome = flowControllerSystemIsLoggedIn(step, handel); break;
	case FC_STEP_TICKET_READ_INFO : outcome = flowControllerReadTicketInfo(step, handel); break;
	case FC_STEP_UNSET_IGNORE_CARD : outcome = flowControllerUnIgnoreCard(step, handel); break;
	case FC_STEP_VERIFY_CARD_ACTIVE : outcome = flowControllerStepVerifyCardActive(step, handel); break;
	case FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED : outcome = flowControllerVerifyIfCardIsNotExpired(step, handel); break;
	case FC_STEP_VERIFY_CARD_STATUS_A : outcome = flowControllerVerifyCardStatusA(step, handel); break;
	case FC_STEP_VERIFY_CARD_STATUS_P : outcome = flowControllerVerifyCardStatusP(step, handel); break;
	case FC_STEP_VERIFY_TICKET_IS_NOT_EXPIRED : outcome = flowControllerVerifyTicketIsNotExpired(step, handel); break;
	case FC_STEP_WRITE_TICKET_LOG_FILE : outcome = flowControllerWriteTicketLog(step, handel); break;
	case FLOWCONTROLLER_STEP_BUSDRIVER_DISPLAY_OK : outcome = flowControllerDisplayBusDriverOK(step, handel); break;
	case FLOWCONTROLLER_STEP_BUSDRIVER_DISPLAY_OK_LOW_BALANCE : outcome = flowControllerDisplayBusDriverOKLowBalance(step, handel); break;
	case FLOWCONTROLLER_STEP_DISPLAY_OK : outcome = flowControllerDisplayOK(step, handel); break;
	case FLOWCONTROLLER_STEP_DISPLAY_OK_LOW_BALANCE : outcome = flowControllerDisplayOKLowBalance(step, handel); break;
	case FLOWCONTROLLER_STEP_DISPLAY_START_SCREEN : outcome = flowControllerDisplayStartScreen(step, handel); break;
	case FLOWCONTROLLER_STEP_INCREASE_CARD_TXN_COUNT : outcome = flowControllerIncreaseCardTxnCount(step, handel); break;
	case FLOWCONTROLLER_STEP_NULL : outcome = flowControllerStepNull(step, handel); break;
	case FLOWCONTROLLER_STEP_PRINT_RECEIPT : outcome = flowControllerPrintReceipt(step, handel); break;
	case FLOWCONTROLLER_STEP_PURSE_BALANCE_SELECT : outcome = flowControllerPurseBalanceSelect(step, handel); break;
	case FLOWCONTROLLER_STEP_PURSE_LOW_BALANCE : outcome = flowControllerPurseLowBalance(step, handel); break;
	case FLOWCONTROLLER_STEP_PURSE_PAY : outcome = flowControllerPursePay(step, handel); break;
	case FLOWCONTROLLER_STEP_PURSE_READ_INFO : outcome = flowControllerReadPurseInfo(step, handel); break;
	case FLOWCONTROLLER_STEP_PURSE_REJECT : outcome = flowControllerPurseReject(step, handel); break;
	case FLOWCONTROLLER_STEP_TICKET_BALANCE_SELECT : outcome = flowControllerTicketBalanceSelect(step, handel); break;
	case FLOWCONTROLLER_STEP_TICKET_LOW_BALANCE : outcome = flowControllerTicketLowBalance(step, handel); break;
	case FLOWCONTROLLER_STEP_TICKET_PAY : outcome = flowControllerTicketPay(step, handel); break;
	case FLOWCONTROLLER_STEP_TICKET_REJECT : outcome = flowControllerTicketReject(step, handel); break;
	case FLOWCONTROLLER_STEP_VERIFY_PURSE_EXPIRED_DATE : outcome = flowControllerVerifyPurseExpired_Date(step, handel); break;
	case FLOWCONTROLLER_STEP_VERIFY_PURSE_STATUS : outcome = flowControllerVerifyPurseStatus(step, handel); break;
	case FLOWCONTROLLER_STEP_VERIFY_TICKET_STATUS : outcome = flowControllerVerifyTicketStatus(step, handel); break;
	case FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_1 : outcome = flowControllerVerifyTicketType1(step, handel); break;
	case FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_2 : outcome = flowControllerVerifyTicketType2(step, handel); break;
	case FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_6 : outcome = flowControllerVerifyTicketType6(step, handel); break;
	case FLOWCONTROLLER_STEP_VERIFY_TICKET_TYPE_9 : outcome = flowControllerVerifyTicketType9(step, handel); break;
	case FLOWCONTROLLER_STEP_WRITE_PURSE_LOG_FILE : outcome = flowControllerWritePurseLog(step, handel); break;
#endif
#if CURRENT_TARGET == CNS_VALIDATOR
	case FC_STEP_BEGIN : outcome = flowControllerBegin(step, handel); break;

	case FC_STEP_CARD_READ_INFO : outcome = flowControllerReadCardInfo(step, handel); break;
	case FC_STEP_DETECT_CARD : outcome = flowControllerDetectCard(step, handel); break;
	case FC_STEP_DISPLAY_REJECT : outcome = flowControllerDisplayReject(step, handel); break;
	case FC_STEP_DISPLAY_TRY_AGAIN : outcome = flowControllerStepTryAgain(step, handel); break;
	case FC_STEP_END_CARDCOMMUNICATION : outcome = flowControllerEndCardCommunication(step, handel); break;
	case FC_STEP_GET_NEXT_CARD_DESFIRE : outcome = flowControllerGetNextCardDesfire(step, handel); break;
	case FC_STEP_INITIALIZE : outcome = flowControllerInitialize(step, handel); break;
	case FC_STEP_RATS : outcome = flowControllerRats(step, handel); break;
	case FLOWCONTROLLER_STEP_DISPLAY_OK : outcome = flowControllerDisplayOK(step, handel); break;
	case FLOWCONTROLLER_STEP_DISPLAY_START_SCREEN : outcome = flowControllerDisplayStartScreen(step, handel); break;
	case FLOWCONTROLLER_STEP_NULL : outcome = flowControllerStepNull(step, handel); break;
#endif
#if CURRENT_TARGET == CNS_HANDHELD
	case FC_STEP_BEGIN:
	outcome = flowControllerBegin(step, handel);
	break;
	case FC_STEP_CARD_IS_NOT_IN_BLACKLIST:
	outcome = flowControllerapiDnfccVerifyCardIsNotInBlacklist(step,
			handel);
	break;
	case FC_STEP_CARD_READ_INFO:
	outcome = flowControllerReadCardInfo(step, handel);
	break;
	case FC_STEP_CLEAR_PENALTY_PAY:
	outcome = flowControllerClearPenaltyPay(step, handel);
	break;
	case FC_STEP_DETECT_CARD:
	outcome = flowControllerDetectCard(step, handel);
	break;
	case FC_STEP_DISPLAY_HANDHELD_LOG_FILE:
	outcome = flowControllerHandheldDisplayLogFile(step, handel);
	break;
	case FC_STEP_DISPLAY_REJECT:
	outcome = flowControllerDisplayReject(step, handel);
	break;
	case FC_STEP_DISPLAY_TRY_AGAIN:
	outcome = flowControllerStepTryAgain(step, handel);
	break;
	case FC_STEP_END_CARDCOMMUNICATION:
	outcome = flowControllerEndCardCommunication(step, handel);
	break;
	case FC_STEP_GET_NEXT_CARD_DESFIRE:
	outcome = flowControllerGetNextCardDesfire(step, handel);
	break;
	case FC_STEP_HANDHELD_DISPLAY_CARD_IS_FAIL:
	outcome = flowControllerHandheldDisplayFail(step, handel);
	break;
	case FC_STEP_HANDHELD_DISPLAY_CARD_IS_PASS:
	outcome = flowControllerHandheldDisplayPass(step, handel);
	break;
	case FC_STEP_HANDHELD_PAY_PENALTY:
	outcome = flowControllerHandheldPayPenalty(step, handel);
	break;
	case FC_STEP_HANDHELD_READ_LOGFILE:
	outcome = flowControllerHandheldReadLogfile(step, handel);
	break;
	case FC_STEP_HANDHELD_VERIFY_LOGFILE:
	outcome = flowControllerHandheldVerifyLogfile(step, handel);
	break;
	case FC_STEP_HANDHELD_VERIFY_TICKET_BALANCE_ENOUGH_FOR_PENALTY:
	outcome = flowControllerIsTicketBalanceEnoughForPenalty(step, handel);
	break;
	case FC_STEP_INITIALIZE:
	outcome = flowControllerInitialize(step, handel);
	break;
	case FC_STEP_INIT_ENCRYPTION:
	outcome = flowControllerInitEncrytion(step, handel);
	break;
	case FC_STEP_IS_NOT_CARD_IGNORED:
	outcome = flowControllerIsNotCardIgnored(step, handel);
	break;
	case FC_STEP_IS_PAY_CARD_SELECTED:
	outcome = flowControllerIsPayCardSelected(step, handel);
	break;
	case FC_STEP_IS_PAY_CASH_SELECTED:
	outcome = flowControllerIsPayCashSelected(step, handel);
	break;
	case FC_STEP_JUMP_TO_START_SCREEN:
	outcome = flowControllerJumpToStartScreen(step, handel);
	break;
	case FC_STEP_LOGIN_SYSTEM:
	outcome = flowControllerLoginSystem(step, handel);
	break;
	case FC_STEP_MENU_MODE_HANDHELD_MAY_HANDLE_CARD:
	outcome = flowControllerIsMenuHandheldMayHandleCard(step, handel);
	break;
	case FC_STEP_RATS:
	outcome = flowControllerRats(step, handel);
	break;
	case FC_STEP_SET_CARD_STATUS_P2A:
	outcome = flowControllerSetCardFromPtoA(step, handel);
	break;
	case FC_STEP_SET_IGNORE_CARD:
	outcome = flowControllerIgnoreCard(step, handel);
	break;
	case FC_STEP_SYSTEM_IS_LOGGED_IN:
	outcome = flowControllerSystemIsLoggedIn(step, handel);
	break;
	case FC_STEP_UNSET_IGNORE_CARD:
	outcome = flowControllerUnIgnoreCard(step, handel);
	break;
	case FC_STEP_VERIFY_CARD_ACTIVE:
	outcome = flowControllerStepVerifyCardActive(step, handel);
	break;
	case FC_STEP_VERIFY_CARD_IS_LOGINTYPE:
	outcome = flowControllerCheckCardIsLoginType(step, handel);
	break;
	case FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED:
	outcome = flowControllerVerifyIfCardIsNotExpired(step, handel);
	break;
	case FC_STEP_VERIFY_CARD_STATUS_A:
	outcome = flowControllerVerifyCardStatusA(step, handel);
	break;
	case FC_STEP_VERIFY_CARD_STATUS_P:
	outcome = flowControllerVerifyCardStatusP(step, handel);
	break;
	case FC_STEP_VERIFY_TICKET_IS_NOT_EXPIRED:
	outcome = flowControllerVerifyTicketIsNotExpired(step, handel);
	break;
	case FC_STEP_WRITE_HANDHELD_LOG_FILE:
	outcome = flowControllerHandheldLogFile(step, handel);
	break;
	case FC_STEP_WRITE_TICKET_LOG_FILE:
	outcome = flowControllerWriteTicketLog(step, handel);
	break;
#endif
#if CURRENT_TARGET == CNS_TOPUP
	case FC_STEP_BEGIN:
		outcome = flowControllerBegin(step, handel);
		break;
	case FC_STEP_CARD_IS_NOT_IN_BLACKLIST:
		outcome = flowControllerapiDnfccVerifyCardIsNotInBlacklist(step,
				handel);
		break;
	case FC_STEP_CARD_READ_INFO:
		outcome = flowControllerReadCardInfo(step, handel);
		break;
	case FC_STEP_DETECT_CARD:
		outcome = flowControllerDetectCard(step, handel);
		break;
	case FC_STEP_DISPLAY_REJECT:
		outcome = flowControllerDisplayReject(step, handel);
		break;
	case FC_STEP_DISPLAY_TRY_AGAIN:
		outcome = flowControllerStepTryAgain(step, handel);
		break;
	case FC_STEP_END_CARDCOMMUNICATION:
		outcome = flowControllerEndCardCommunication(step, handel);
		break;
	case FC_STEP_GET_NEXT_CARD_DESFIRE:
		outcome = flowControllerGetNextCardDesfire(step, handel);
		break;
	case FC_STEP_INCREASE_TICKET_VALUE:
		outcome = flowControllerIncrementTicketValue(step, handel);
		break;
	case FC_STEP_INITIALIZE:
		outcome = flowControllerInitialize(step, handel);
		break;
	case FC_STEP_INIT_ENCRYPTION:
		outcome = flowControllerInitEncrytion(step, handel);
		break;
	case FC_STEP_IS_NOT_CARD_IGNORED:
		outcome = flowControllerIsNotCardIgnored(step, handel);
		break;
	case FC_STEP_LOGIN_SYSTEM:
		outcome = flowControllerLoginSystem(step, handel);
		break;
	case FC_STEP_MENU_MODE_TOPUP_MAY_HANDLE_CARD:
		outcome = flowControllerIsMenuTopupMayHandleCard(step, handel);
		break;
	case FC_STEP_PRINT_BILL:
		outcome = flowControllerPrintBill(step, handel);
		break;
	case FC_STEP_RATS:
		outcome = flowControllerRats(step, handel);
		break;
	case FC_STEP_SET_CARD_STATUS_P2A:
		outcome = flowControllerSetCardFromPtoA(step, handel);
		break;
	case FC_STEP_SET_IGNORE_CARD:
		outcome = flowControllerIgnoreCard(step, handel);
		break;
	case FC_STEP_SET_TICKET_VALUE_TO_0:
		outcome = flowControllerSetTicketValueNul(step, handel);
		break;
	case FC_STEP_SYSTEM_IS_LOGGED_IN:
		outcome = flowControllerSystemIsLoggedIn(step, handel);
		break;
	case FC_STEP_TICKET_READ_INFO:
		outcome = flowControllerReadTicketInfo(step, handel);
		break;
	case FC_STEP_TOPUP_VALUE_IS_SELECTED:
		outcome = flowControllerIsTopupValueEnabled(step, handel);
		break;
	case FC_STEP_TOP_UP_DISPLAY_SCREEN:
		outcome = flowControllerDisplayTicketValueInTopup(step, handel);
		break;
	case FC_STEP_UNSET_IGNORE_CARD:
		outcome = flowControllerUnIgnoreCard(step, handel);
		break;
	case FC_STEP_VERIFY_CARD_ACTIVE:
		outcome = flowControllerStepVerifyCardActive(step, handel);
		break;
	case FC_STEP_VERIFY_CARD_IS_LOGINTYPE:
		outcome = flowControllerCheckCardIsLoginType(step, handel);
		break;
	case FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED:
		outcome = flowControllerVerifyIfCardIsNotExpired(step, handel);
		break;
	case FC_STEP_VERIFY_CARD_STATUS_A:
		outcome = flowControllerVerifyCardStatusA(step, handel);
		break;
	case FC_STEP_VERIFY_CARD_STATUS_P:
		outcome = flowControllerVerifyCardStatusP(step, handel);
		break;
	case FC_STEP_VERIFY_TICKET_IS_NOT_EXPIRED:
		outcome = flowControllerVerifyTicketIsNotExpired(step, handel);
		break;
	case FC_STEP_WRITE_TOPUP_LOG_FILE:
		outcome = flowControllerLogTopup(step, handel);
		break;
#endif
		// END_DEFINE_OF_CASE Leave this for auto-update by script

	}
	sprintf(displMsg, "S:%d->%d - %10.10s", codeToPerform, outcome, *step);
	apiDnfccLogFlowcontrollerString(displMsg);
	return outcome;
}

/** @} */
