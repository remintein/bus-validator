/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"

#include "../DesfireFunctions.h"
#include "../CnsCardInterface.h"
#include "../CnsCardInterfaceInternal.h"

/**
 * @file
 * @brief Contains definitions about the files
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */
#if CURRENT_TARGET == CNS_TOPUP
extern CnsCardInterfaceInfo apiInterface;

void sendBalancesOk(DesfireHandlingStruct *controller);

/**
 * @brief Verify if the card needs to be polled
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerIsMenuTopupMayHandleCard(char **step,
		DesfireHandlingStruct *controller) {
	*step = "TU-TopSel";

	return deviceStatus.curMenu != &menu_topup_valueselect
			&& deviceStatus.curMenu != &menu_topup_confirm;
}

/**
 * @brief Set the ticket value to 0
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */

uint8_t flowControllerSetTicketValueNul(char **step,
		DesfireHandlingStruct *controller) {
	*step = "Tick->0";

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);

	CnsCardInterfaceInternalDebit(&apiInterface, controller, 6,
			apiInterface.file6);

	apiInterface.file6 = 0;

	return 1;
}

/**
 * @brief Verify if there is a value to top-up
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerIsTopupValueEnabled(char **step,
		DesfireHandlingStruct *controller) {
	*step = "DoTopup?";

	return deviceStatus.topupValue > 0;
}

/**
 * @brief Perform the topup
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */

uint8_t flowControllerIncrementTicketValue(char **step,
		DesfireHandlingStruct *controller) {
	*step = "Top>0";
	char buf[MENU_BUFFER_SIZE];
	CnsCardInterfaceCreditTicketBalance(&apiInterface, controller,
			deviceStatus.topupValue);

	deviceStatus.topupWith = deviceStatus.topupValue;
	deviceStatus.topupValue = 0;

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 5);
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);

	deviceStatus.topupBalance = apiInterface.file6;

	fileTicketInformationStruct cardInfo;
	CopyStreamToTicketInformation(&cardInfo, apiInterface.file5);

	// TODO: Add 3 months to file ticketEffectiveDate

	char date[10];

	apiDnfccClockFetchDate((char *) date);

	CnsCardInterfaceInternalBcdDate(date, cardInfo.ticketEffectiveDate);
	CnsCardInterfaceInternalBcdToDate((char *) cardInfo.ticketEffectiveDate,
			(uint8_t *) date);
	CopyTicketInformationsToStream(apiInterface.file5, &cardInfo);
	CnsCardInterfaceInternalWriteFile(&apiInterface, controller, 5);

	sprintf(buf, "%c%ld,%8.8s", COMCOM_MENU_TOPUP_NEWBALACE, apiInterface.file6,
			date);

	queueSendToMenu(buf, 10000);

	return 1;
}

/**
 * @brief Write the ticket log file through the datadistributor
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerLogTopup(char **step, DesfireHandlingStruct *controller) {
	*step = "TLOG";

	char buf[DATADISTRIBUTOR_BUFFER_SIZE];
	buf[0] = COMCOM_DATADISTRIBUTOR_TOPUP_PERFORMED;
	queueSendToPrinter(buf, 5000);
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);

	sprintf((char *) (buf + 15), "V%luT%lu", deviceStatus.topupBalance,
			deviceStatus.topupWith);

	queueSendToDataDistributor(buf, 1000);
	return 1;
}

/**
 * @brief Start printing a bill
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerPrintBill(char **step, DesfireHandlingStruct *controller) {
	char buf[PRINTER_BUFFER_SIZE];
	buf[0] = COMCOM_PRINT_TICKET;
	queueSendToPrinter(buf, 5000);

	return 1;
}

/**
 * @brief VDisplay the ticket value in top-up
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerDisplayTicketValueInTopup(char **step,
		DesfireHandlingStruct *controller) {
	*step = "DISP:ERR";
	char buf[MENU_BUFFER_SIZE];
	// TODO: Get the files and display the info on the screen
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 5);
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);

	fileTicketInformationStruct cardInfo;
	CopyStreamToTicketInformation(&cardInfo, apiInterface.file5);

	uint8_t b[4];
	memcpy(b, cardInfo.ticketEffectiveDate, 4);
	CnsCardInterfaceInternalAddDaysToBcdDate(b, cardInfo.ticketExpireDays);

	uint8_t date[10];
	CnsCardInterfaceInternalBcdToDate((char *) b, date);
	sprintf(buf, "%c%ld,%8.8s", COMCOM_MENU_TOPUP_NEWBALACE, apiInterface.file6,
			(char *) date);
	queueSendToMenu(buf, 10000);

	sendBalancesOk(controller);

	return 1;
}

/** @} */
#endif
