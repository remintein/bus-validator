/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"

#include "../DesfireFunctions.h"
#include "../CnsCardInterface.h"
#include "../CnsCardInterfaceInternal.h"

extern CnsCardInterfaceInfo apiInterface;
void sendBalancesOk(DesfireHandlingStruct *controller);

void sendBalancesError(DesfireHandlingStruct *controller);

/**
 * @file
 * @brief Contains definitions about the files
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

/**
 * @brief Check if the bus console may handle card
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerIsMenuHandheldMayHandleCard(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH-TopSel";
#if RC663_LOG == 1
	char t[DISPLAY_BUFFER_SIZE];
	t[0]=COMCOM_DISPLAY_LOGGING_START;
	queueSendToDisplay(t, 1000);
	vTaskDelay(1000);
#endif
	return deviceStatus.curMenu != &menu_handheld_selectpenaltypay;
}

/**
 * @brief Check if pay by card is selected
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerIsPayCardSelected(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH-TopSel";

	return deviceStatus.penaltyCard > 0;
}

/**
 * @brief Check if Pay by cash is selected
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerIsPayCashSelected(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH-TopSel";

	return deviceStatus.penaltyCash > 0;
}

/**
 * @brief Check if the card holds enough balance for paying penalty
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerIsTicketBalanceEnoughForPenalty(char **step,
		DesfireHandlingStruct *controller) {
	*step = "Tick->0";

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);

	return apiInterface.file6 >= deviceStatus.penaltyCard;
}

/**
 * @brief Pay penalty with the current card
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldPayPenalty(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:PP";
	uint8_t ior;

	ior = CnsCardInterfaceDebitTicketBalance(&apiInterface, controller,
			deviceStatus.penaltyCard);
// TODO: Also pay
	return ior == 0;
}

/**
 * @brief Clear the penalty values
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerClearPenaltyPay(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:CP";
	deviceStatus.penaltyCard = deviceStatus.penaltyCash = 0;
	return 1;
}

/**
 * @brief write the logfile to the PICC
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldLogFile(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:LGF";

	char dateTime[15];
	apiDnfccClockFetchDateTime(dateTime);
	uint8_t ior = CnsCardInterfaceTicketTransaction(&apiInterface, controller);

	strcpy(apiInterface.reason, apiInterface.previousReason); // Passthrough the reason
	return ior == 0;

	return 1;
}

/**
 * @brief Display logfile to the Menu
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldDisplayLogFile(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:ST";
	char mbuf[MENU_BUFFER_SIZE];
	mbuf[0] = COMCOM_MENU_HANDHELD_LOGDATA;

	fileTicketLogStruct ticketlog;

	CopyStreamToTicketLog(&ticketlog, apiInterface.file7);

	sprintf(mbuf, "%c%02x-%02x-%02x%02x %02x:%02x:%02x\n%ld",
			COMCOM_MENU_HANDHELD_LOGDATA,
			ticketlog.ticketTransactionDateTime[3],
			ticketlog.ticketTransactionDateTime[2],
			ticketlog.ticketTransactionDateTime[0],
			ticketlog.ticketTransactionDateTime[1],
			ticketlog.ticketTransactionDateTime[4],
			ticketlog.ticketTransactionDateTime[5],
			ticketlog.ticketTransactionDateTime[6], ticketlog.ticketValue);

	queueSendToMenu(mbuf, portMAX_DELAY);

	return 1;
}

/**
 * @brief Read the logfile from the SD card
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldReadLogfile(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "HH:ReadLogF";

	// Logfile
	// Busline
	memset(apiInterface.file7, 0x55, SIZE_TICKET_LOG);
	ior = CnsCardInterfaceInternalReadFile(&apiInterface, controller, 7);
	return ior == 0;
}

/**
 * @brief Verify logfile if a ticket has been bought today
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldVerifyLogfile(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:VTK";
	fileTicketLogStruct ticketlog;

	CopyStreamToTicketLog(&ticketlog, apiInterface.file7);
	char myDate[12];
	apiDnfccClockFetchDate(myDate);
	uint8_t myBcdDate[10];
	CnsCardInterfaceInternalBcdDate(myDate, myBcdDate);

	// All checks done here in a row, return 0 if fail

	if (strncmp((char *) ticketlog.ticketTransactionDateTime,
			(char *) myBcdDate, 4) != 0) {
		return 0;
	}
	return 1;
}

/**
 * @brief Send the menu that the card is PASS
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldDisplayPass(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH-PASS";
	char dbuf[MENU_BUFFER_SIZE];
	dbuf[0] = COMCOM_MENU_HANDHELD_CARD_PASS;
	queueSendToMenu(dbuf, 2000);

	sendBalancesOk(controller);
	return 1;
}

/**
 * @brief Send the menu that the card is FAIL
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */

uint8_t flowControllerHandheldDisplayFail(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH-FAIL";
	char dbuf[MENU_BUFFER_SIZE];
	dbuf[0] = COMCOM_MENU_HANDHELD_CARD_FAIL;
	queueSendToMenu(dbuf, 2000);
	sendBalancesError(controller);
	return 1;
}

/** @} */
