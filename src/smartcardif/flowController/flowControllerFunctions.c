/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "flowController.h"
#include <string.h>
#include <semphr.h>

#include "../../project_includes.h"
#include "../CnsCardInterface.h"
#include "../CnsCardInterfaceInternal.h"
#include "../DesfireFunctions.h"
#include "../../sdio/DeviceDataFiles.h"

CnsCardInterfaceInfo apiInterface;

/**
 * @file
 * @brief Contains definitions about the files
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

/**
 * @brief Send the Card Ok to the datadistributor, with parameters
 *
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
void sendBalancesOk(DesfireHandlingStruct *controller) {
	char buf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	memset(buf, 0x00, DATADISTRIBUTOR_BUFFER_SIZE);
// 0 = message
	buf[0] = COMCOM_DATADISTRIBUTOR_OK_CARD;
// 1..14 = uid
	for (int i = 0; i < 7; i++) {
		sprintf(buf + 1 + i * 2, "%02x", apiInterface.cardUid[i]);
	}
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 1);
	fileCardInformationStruct fil1;
	CopyStreamToCardInformation(&fil1, apiInterface.file1);
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 9);
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 13);
// 15..24 = Ticket Balance
	sprintf(buf + 15, "%10.10ld", apiInterface.file6);
// 25..34 = Purse Balance
	sprintf(buf + 25, "%10.10ld", apiInterface.file9);
// 35..44 = Card Transaction
	sprintf(buf + 35, "%10.10ld", apiInterface.file13);
// 45..64 = CardId
	for (int i = 0; i < 10; i++) {
		sprintf(buf + 45 + i * 2, "%02x", fil1.originalCard_ID[i]);
	}
// 65..79 = date/time
	apiDnfccClockFetchDateTime(buf + 65);

	queueSendToDataDistributor(buf, portMAX_DELAY);
}

/**
 * @brief Send the Card ERROR to the datadistributor with parameters
 *
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
void sendBalancesError(DesfireHandlingStruct *controller) {
	char buf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	memset(buf, 0x00, DATADISTRIBUTOR_BUFFER_SIZE);
// 0 = message
	buf[0] = COMCOM_DATADISTRIBUTOR_ERROR_CARD;
// 1..14 = uid
	for (int i = 0; i < 7; i++) {
		sprintf(buf + 1 + i * 2, "%02x", apiInterface.cardUid[i]);
	}
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 1);
	fileCardInformationStruct fil1;
	CopyStreamToCardInformation(&fil1, apiInterface.file1);
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 9);
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 13);
// 15..24 = Ticket Balance
	sprintf(buf + 15, "%10.10ld", apiInterface.file6);
// 25..34 = Purse Balance
	sprintf(buf + 25, "%10.10ld", apiInterface.file9);
// 35..44 = Card Transaction
	sprintf(buf + 35, "%10.10ld", apiInterface.file13);
// 45..64 = CardId
	for (int i = 0; i < 10; i++) {
		sprintf(buf + 45 + i * 2, "%02x", fil1.originalCard_ID[i]);
	}
// 65..79 = date/time
	apiDnfccClockFetchDateTime(buf + 65);
	// 80.. = cause
	strncpy(buf + 80, apiInterface.reason, 10);
	queueSendToDataDistributor(buf, portMAX_DELAY);
}

/**
 * @brief Send the Card Ok to the menu and sound
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerDisplayOK(char **step, DesfireHandlingStruct *controller) {
	char buf[DISPLAY_BUFFER_SIZE + 1];
	*step = "DISP:OK";
	memset(buf, 0x00, DISPLAY_BUFFER_SIZE);

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);

	sprintf(buf, "I%ld", apiInterface.file6);
	// put validator message on CAN bus only if I am a validator
#if CURRENT_TARGET == CNS_VALIDATOR
	buf[0] = COMCOM_CANBUS_VALID_IN;
	queueSendToCanbus(buf, portMAX_DELAY);
#endif
	// and send to menu
	buf[0] = COMCOM_MENU_CARD_ENTER_SUCCESSFULL;
	queueSendToMenu(buf, portMAX_DELAY);

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 4);
	fileCardHolderInformationStruct holderInfo;
	CopyStreamToCardHolderInformation(&holderInfo, apiInterface.file4);
	// send message with gender and language info (futureUse[0]) to buzzer

	sendBalancesOk(controller);

	sprintf(buf, "I%d", holderInfo.futureUse[0]);
	if (holderInfo.gender == 0) { // Female = 0, see CNS spec
		buf[0] = COMCOM_SOUND_VALID_IN_FEMALE;
	} else {
		buf[0] = COMCOM_SOUND_VALID_IN;
	}

	queueSendToSound(buf, portMAX_DELAY);

	return 1;
}

/**
 * @brief Send the Card Ok -low balance- to the menu and sound
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerDisplayOKLowBalance(char **step,
		DesfireHandlingStruct *controller) {
	char buf[MENU_BUFFER_SIZE + 1];
	*step = "DISP:OKLB";
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);

	sprintf(buf + 1, "%ld", apiInterface.file6);
	// put validator message on CAN bus only if I am a validator
#if CURRENT_TARGET == CNS_VALIDATOR
	buf[0] = COMCOM_CANBUS_VALID_IN_LOWVAL;
	queueSendToCanbus(buf, portMAX_DELAY);
#endif
	buf[0] = COMCOM_MENU_CARD_ENTER_SUCCESSFULL_WARNING;
	queueSendToMenu(buf, portMAX_DELAY);

	fileCardHolderInformationStruct holderInfo;
	CopyStreamToCardHolderInformation(&holderInfo, apiInterface.file4);
	// send message with gender and language info (futureUse[0]) to buzzer
	sprintf(buf, "L%d", holderInfo.futureUse[0]);

	sendBalancesOk(controller);

	buf[0] = COMCOM_SOUND_VALID_IN_LOW;
	queueSendToSound(buf, portMAX_DELAY);

	return 1;
}

/**
 * @brief Send the bus driver Card Ok to the menu and sound
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerDisplayBusDriverOK(char **step,
		DesfireHandlingStruct *controller) {
	char buf[DISPLAY_BUFFER_SIZE + 1];
	*step = "DISPDR:OK";
	memset(buf, 0x00, DISPLAY_BUFFER_SIZE);

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 9);

	sprintf(buf, "B%ld", apiInterface.file9);

	buf[0] = COMCOM_MENU_DRIVERCARD_ENTER_SUCCESSFULL;
	queueSendToMenu(buf, portMAX_DELAY);

	sendBalancesOk(controller);

	return 1;
}

/**
 * @brief Send the bus driver Card Ok - low balance - to the menu and sound
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerDisplayBusDriverOKLowBalance(char **step,
		DesfireHandlingStruct *controller) {
	char buf[MENU_BUFFER_SIZE + 1];
	*step = "DISPDR:OKLB";
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 9);

	sprintf(buf + 1, "%ld", apiInterface.file9);
	buf[0] = COMCOM_MENU_DRIVERCARD_ENTER_SUCCESSFULL_WARNING;
	queueSendToMenu(buf, portMAX_DELAY);

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 4);
	fileCardHolderInformationStruct holderInfo;
	CopyStreamToCardHolderInformation(&holderInfo, apiInterface.file4);
	// send message with language info (futureUse[0]) to buzzer
	sprintf(buf, "L%d", holderInfo.futureUse[0]);

	sendBalancesOk(controller);

	buf[0] = COMCOM_SOUND_VALID_IN_LOW;
	queueSendToSound(buf, portMAX_DELAY);

	return 1;
}

/**
 * @brief Increase the card transaction count
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerIncreaseCardTxnCount(char **step,
		DesfireHandlingStruct *controller) {
	*step = "CardTxn++";
	strcpy(apiInterface.reason, apiInterface.previousReason); // Pass through the reason
	uint8_t ior = CnsCardInterfaceCardTransaction(&apiInterface, controller);

	return ior == 0;
}

/**
 * @brief Read the purse info
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerReadPurseInfo(char **step,
		DesfireHandlingStruct *controller) {
	*step = "ReadTicketInfo";

	uint8_t ior = CnsCardInterfaceReadPurseInfo(&apiInterface, controller);

	return ior == 0;
}

/**
 * @brief Write the purse log
 *
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerWritePurseLog(char **step,
		DesfireHandlingStruct *controller) {
	char dateTime[15];
	apiDnfccClockFetchDateTime(dateTime);
	uint8_t ior = CnsCardInterfaceWritePurseLog(&apiInterface, controller,
			dateTime);
	ior = CnsCardInterfacePurseTransaction(&apiInterface, controller);
	return ior == 0;
}

/**
 * @brief Pay ticket
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerTicketPay(char **step, DesfireHandlingStruct *controller) {
	uint8_t ior;
	uint32_t ticketFare;
	*step = "PayTicket";
	// get ticket type and Fare value for that type
	ior = CnsCardInterfaceInternalReadFile(&apiInterface, controller, 5);
	if (ior != 0) {
		return ior;
	}
	fileTicketInformationStruct str;
	CopyStreamToTicketInformation(&str, apiInterface.file5);
	ticketFare = apiDnfccGetTicketFare(str.ticketType);
	ior = CnsCardInterfaceDebitTicketBalance(&apiInterface, controller,
			ticketFare);
	if (ior == 0) {
		apiInterface.removedTicket = ticketFare;
	} else {
		strcpy(apiInterface.reason, LOCALE_ERROR_TICKET_LOW);
	}
	return ior == 0;
}

/**
 * @brief Pay purse
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerPursePay(char **step, DesfireHandlingStruct *controller) {
	uint8_t ior;
	uint32_t purseFare;
	*step = "PayPurse";
	purseFare = apiDnfccGetTicketFare(2);
	ior = CnsCardInterfaceDebitPurseBalance(&apiInterface, controller,
			purseFare);
	if (ior == 0) {
		apiInterface.removedPurse = purseFare;
		deviceStatus.purseTransactionCount++;
	} else {
		strcpy(apiInterface.reason, LOCALE_ERROR_PURSE_LOW);
	}
	return ior == 0;
}
/** @} */
