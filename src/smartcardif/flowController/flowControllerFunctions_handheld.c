/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

/**
 * @file
 * @brief Contains definitions about the files
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */
#include "../../project_includes.h"

#include "../DesfireFunctions.h"
#include "../CnsCardInterface.h"
#include "../CnsCardInterfaceInternal.h"
extern CnsCardInterfaceInfo apiInterface;

/**
 * @brief Verify if the card needs to be polled
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldCheckLogin(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:LGIN";

	return CnsCardInterfaceCardIsOfLogintype(&apiInterface, controller,
	LOGINTYPE_HANDHELD);
}


/**
 * @brief Jump to the start screen
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerDisplayHandheldStartScreen(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:ST";
	char mbuf[MENU_BUFFER_SIZE];
	mbuf[0] = COMCOM_MENU_STARTSCREEN;
	queueSendToMenu(mbuf, portMAX_DELAY);

	return 1;
}

/**
 * @brief Jump to the penalty screen
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerDisplayPenaltyScreen(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:ST";
	char mbuf[MENU_BUFFER_SIZE];
	mbuf[0] = COMCOM_MENU_PENALTY_SCREEN;
	queueSendToMenu(mbuf, portMAX_DELAY);

	return 1;
}

/**
 * @brief Pay by card, but insufficient balance
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldInsufficientBalance(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:IB";

	char mbuf[MENU_BUFFER_SIZE];
	int32_t balance;
	CnsCardInterfaceGetTicketBalance(&apiInterface, controller, &balance);

	sprintf(mbuf, "%c%s\n%ld\n", COMCOM_MENU_HANDHELD_LOGDATA,
	LOCALE_ERROR_TICKET_LOW, balance);

	queueSendToMenu(mbuf, portMAX_DELAY);

	return 1;
}

/**
 * @brief Report card is expired
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldCardExpired(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:IBCE";
	char mbuf[MENU_BUFFER_SIZE];

	sprintf(mbuf, "%c%s", COMCOM_MENU_HANDHELD_LOGDATA,
	LOCALE_ERROR_CARD_EXPIRED);
	queueSendToMenu(mbuf, portMAX_DELAY);

	return 1;
}


/**
 * @brief Verify that pay by cash is selected
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerMustPayPenaltyWithCard(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:PCD";
	return deviceStatus.penaltyCard != 0;
}


/**
 * @brief Verify that pay by card is selected
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerDidPayPenaltyWithCash(char **step,
		DesfireHandlingStruct *controller) {
	*step = "HH:PCH";
	return deviceStatus.penaltyCash != 0;
}

/**
 * @brief Verify the ticket balance
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerHandheldVerifyTicketBalance(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "HH:TB";

	ior = CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);
	if (ior != 0) {
		return 0;
	}
	fileTicketValueStruct str;
	CopyTicketValueToStream((uint8_t *) &(apiInterface.file6), &str);

	return str.ticketValue >= deviceStatus.penaltyCard;
}

/** @} */
