/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"

#include "../DesfireFunctions.h"
#include "../../sam/SamControlCommands.h"
#include "../CnsCardInterface.h"
#include "../CnsCardInterfaceInternal.h"

/**
 * @file
 * @brief Contains definitions about the files
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

/**
 * @brief Check if the bus console may handle card
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerIsMenuBusconsoleMayHandleCard(char **step,
		DesfireHandlingStruct *controller) {
	*step = "MayDoCard";
	if (deviceStatus.curMenu != &menu_busconsole_main
			&& deviceStatus.curMenu != &menu_routeselect
			&& deviceStatus.curMenu != &menu_serviceselect) {
		if (!deviceStatus.isFieldOn) {
			DnfccSamRcRFControl(0x0101);
			vTaskDelay(300);
			deviceStatus.isFieldOn = 1;
		}
		return 1;
	}
	if (deviceStatus.isFieldOn) {
		DnfccSamRcRFControl(0);
		deviceStatus.isFieldOn = 0;
	}
	vTaskDelay(500);
	return 0;
}
/** @} */
