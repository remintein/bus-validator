/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"

#include "phNfcLib.h"

#include "../../_configuration/cardInformation.h"
#include "NxpLibFunctions.h"
#include "../../sam/SamControlCommands.h"



/**
 * @brief Placeholder function for library initialization
 *
 * @return true
 */
uint8_t fcNxpLibBegin() {
	return 1;
}

/**
 * @brief Detect card, depending on the cardNeeds Wupa (Wake-up Activate) or Atqa, run the correct one
 * @return true if call was done correct
 */
uint8_t fcNxpLibDetectCard() {
	uint16_t ior;
	uint8_t atqa[2];
	if (deviceStatus.cardNeedsWupa) {
		ior = DnfccSamIso14443_3RequestWakeup(0x52, (uint16_t *) atqa);
		deviceStatus.cardNeedsWupa=0;
	} // 26 = REQA
	else {
		ior = DnfccSamIso14443_3RequestWakeup(0x26, (uint16_t *) atqa);
	} // 26 = REQA
	return ior == 0x9000;
}

/**
 * @brief Placeholdr function, should always return 0
 *
 * @return 0
 */

int fcNxpLibInitEncryption() {
	return 0;
}

