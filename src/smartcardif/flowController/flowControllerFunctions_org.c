/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

/**
 * @file
 * @brief Contains definitions about the files
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */
#include "flowController.h"
#include <string.h>
#include <semphr.h>

#include "../../project_includes.h"
#include "../../_configuration/queueDefinitions.h"
#include "../../_definition/deviceStatus.h"
#include "../../_definition/communicationCommands.h"
#include "../../_configuration/cardInformation.h"
#include "../../_library/priorityController/priorityController.h"
#include "../CnsCardInterface.h"
#include "../DesfireFunctions.h"
#include "../CnsCardInterfaceInternal.h"

extern CnsCardInterfaceInfo apiInterface;

/**
 * @brief Increase card transaction count
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerIncreaseCardTransactionCount(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "Increase card TXN cnt";
	ior = CnsCardInterfaceCardTransaction(&apiInterface, controller);
	return ior == 0;
}

/**
 * @brief Verify ticket status is A
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerVerifyTicketStatus(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "Status=A?";
	ior = CnsCardInterfaceTicketStatus(&apiInterface, controller, 'A');
	return ior == 0;
}

/**
 * @brief Verify ticket type = 1
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerVerifyTicketType1(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "Type=1?";
	ior = CnsCardInterfaceTicketType(&apiInterface, controller, 1);
	return ior == 0;
}

/**
 * @brief Verify ticket type = 2
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerVerifyTicketType2(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "Type=2?";
	ior = CnsCardInterfaceTicketType(&apiInterface, controller, 2);
	return ior == 0;
}

/**
 * @brief Verify purse status = 'A'
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerVerifyPurseStatus(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "PurseStat?";
	ior = CnsCardInterfacePurseStatus(&apiInterface, controller, 'A');
	return ior == 0;
}

/**
 * @brief Verify purse expired date not to be expired
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerVerifyPurseExpired_Date(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	char date[9];
	apiDnfccClockFetchDate(date);
	*step = "ExpireDate?";
	ior = CnsCardInterfacePurseExpire(&apiInterface, controller, date);
	return ior == 0;
}

/**
 * @brief Message pritner to print ticket
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerPrintReceipt(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "PrintRec";
	char buf[PRINTER_BUFFER_SIZE];
	buf[0] = COMCOM_PRINT_TICKET;
	queueSendToPrinter(buf, 5000);
	ior = 1;
	return ior == 0;
}

/**
 * @brief Verify if the purse balance is below 2000
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerPurseLowBalance(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "LowBalance";

	ior = CnsCardInterfaceCheckLowPurseBalance(&apiInterface, controller, 2000);
	return ior == 0;
}

/**
 * @brief Placeholder for purse reject - returns 0
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerPurseReject(char **step,
		DesfireHandlingStruct *controller) {
	*step = "P-reject";

	strcpy(apiInterface.reason, apiInterface.previousReason); // Passthrough
	return 0;
}

/**
 * @brief Verify ticket type = 6
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerVerifyTicketType6(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "Type=6?";
	ior = CnsCardInterfaceTicketType(&apiInterface, controller, 6);
	return ior == 0;
}

/**
 * @brief Verify ticket type  = 9
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerVerifyTicketType9(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "Type=9?";
	ior = CnsCardInterfaceTicketType(&apiInterface, controller, 9);
	return ior == 0;
}

/**
 * @brief Verify ticket balance > 2000
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerTicketLowBalance(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	*step = "DisLowB";
	ior = CnsCardInterfaceCheckLowTicketBalance(&apiInterface, controller,
			2000);
	if (ior != 0) {
		strcpy(apiInterface.reason, LOCALE_ERROR_TICKET_LOW);
	}
	return ior == 0;
}

/**
 * @brief Placeholder for reject ticket
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerTicketReject(char **step,
		DesfireHandlingStruct *controller) {
	*step = "TicketReject";
	strcpy(apiInterface.reason, apiInterface.previousReason);
	return 0;
}

/**
 * @brief Jump to start screen and log if == bus-driver console
 *
 * @param set - pointer to a pointer char for debug
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
uint8_t flowControllerDisplayStartScreen(char **step,
		DesfireHandlingStruct *controller) {
	char dBuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];

	*step = "DisplayStartScreen";
#if CURRENT_TARGET == CNS_BUSCONSOLE
	// login driver and  get start counter of purse transactions
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 9);
	deviceStatus.startPurseTransactionCount = apiInterface.file9;
	// read busdriver ID
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 1);
	fileCardHolderInformationStruct holderInfo;
	CopyCardHolderInformationsStructToStream
	(apiInterface.file4, &holderInfo);
	strncpy((char *)deviceStatus.loginId, (char *)holderInfo.cardHolder_ID, 10);

	sprintf(dBuf, "%c%02x%02x%02x%02x%02x%02x%02x%02x%02x%02xT=",
	COMCOM_DATADISTRIBUTOR_START_SHIFT, deviceStatus.loginId[0],
			deviceStatus.loginId[1], deviceStatus.loginId[2],
			deviceStatus.loginId[3], deviceStatus.loginId[4],
			deviceStatus.loginId[5], deviceStatus.loginId[6],
			deviceStatus.loginId[7], deviceStatus.loginId[8],
			deviceStatus.loginId[9]);
	apiDnfccClockFetchDateTime(dBuf + 23); // add time of this event count length above
	queueSendToDataDistributor(dBuf, portMAX_DELAY);
	// make validators aware driver logged in
#endif
	dBuf[0] = COMCOM_MENU_STARTSCREEN;
	queueSendToMenu(dBuf, portMAX_DELAY);
	return 1;
}

uint8_t flowControllerPurseBalanceSelect(char **step,
		DesfireHandlingStruct *controller) {
	*step = "EndCommuncation";

	return 1;
}

uint8_t flowControllerTicketBalanceSelect(char **step,
		DesfireHandlingStruct *controller) {
	*step = "EndCommuncation";

	return 1;
}
/**  @} */
