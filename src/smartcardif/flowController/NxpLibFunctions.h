/*
 * NxpLibFunctions.h
 *
 *  Created on: 14 jul. 2018
 *      Author: aart
 */

#ifndef SMARTCARDIF_FLOWCONTROLLER_NXPLIBFUNCTIONS_H_
#define SMARTCARDIF_FLOWCONTROLLER_NXPLIBFUNCTIONS_H_


uint8_t fcNxpLibBegin();
uint8_t fcNxpLibDetectCard() ;
int fcNxpLibInitEncryption();


#endif /* SMARTCARDIF_FLOWCONTROLLER_NXPLIBFUNCTIONS_H_ */
