/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"

#include "flowController.h"
#include <string.h>
#include <semphr.h>

#include "../../_configuration/queueDefinitions.h"
#include "../../_definition/deviceStatus.h"
#include "../../_definition/communicationCommands.h"
#include "../../_configuration/cardInformation.h"
#include "../../_library/priorityController/priorityController.h"
#include "../../_library/blacklistcontrol/blacklistVerify.h"
#include "../CnsCardInterface.h"
#include "../CnsCardInterfaceInternal.h"
#include "NxpLibFunctions.h"
#include "../../sam/SamControlCommands.h"
#include "../CardHandler.h"

extern CnsCardInterfaceInfo apiInterface;

void sendBalancesError(DesfireHandlingStruct *controller);

/**
 * @brief Initialize system. This functions is called only when starting up the system.
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerInitialize(char **step, DesfireHandlingStruct *controller) {
	*step = "initialize"; // Changed to 1 tick = 1 ms.

	return 1;
}

/**
 * @brief Perform Sam RC Init
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerSamRcInit(char **step, DesfireHandlingStruct *controller) {
	uint8_t reg;
	uint16_t ior;
	reg = 0;
	ior = DnfccSamRcReadSingleRegister(0x7f, &reg);

	if (ior != 0x9000 || reg == 0) {
		SamPerformReset();
	}

	return 1;
}

/**
 * @brief Begins the loop for reading one card. This function is called every time the handling of the card was done
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerBegin(char **step, DesfireHandlingStruct *controller) {
	*step = "begin";

	return fcNxpLibBegin();
}

/**
 * @brief  Send WUPA to the card, and gives back 1 if a card did react
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerDetectCard(char **step, DesfireHandlingStruct *controller) {
	*step = "detectCard";
	return fcNxpLibDetectCard();
}

/**
 * @brief Perform cascade to get the next card in the field
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerGetNextCardDesfire(char **step,
		DesfireHandlingStruct *controller) {
	*step = "getNextCard";
	uint8_t uid[10];

	CnsCardInterfaceInitialize(&apiInterface);
	// Here the cascade should be run AGMS
	if (CnsCardInterfaceStartCard(&apiInterface, controller,
			uid) != CNS_CARDINTERFACE_RETURN_OK) {
		return 0;
	}

	return 1;
}


/**
 * @brief Perform no  action
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerStepNull(char **step, DesfireHandlingStruct *controller) {
	*step = "Null";
	strcpy(apiInterface.reason, apiInterface.previousReason);
	return 1;
}

/**
 * @brief Verify that the card is not set to ignore
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerIsNotCardIgnored(char **step,
		DesfireHandlingStruct *controller) {
	*step = "IsNotCardIgnored";
	return CardHandlerContinueWith(controller) != CARDHANDLER_IGNORE;
}

/**
 * @brief Set the card to ignore so that it is not used in the next round
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerIgnoreCard(char **step, DesfireHandlingStruct *controller) {
	*step = "IgnoreCard";
	apiDnfccDisplayLedControlGreen(1);
	CardHandlerSetToIgnore(controller);
	return 1;
}

/**
 * @brief Unset the card ignore so it can be used in the next cycle
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerUnIgnoreCard(char **step,
		DesfireHandlingStruct *controller) {
	*step = "UnIgnoreCard";
	apiDnfccDisplayLedControlGreen(1);
	CardHandlerSetToNotIgnore(controller);
	return 1;
}

/**
 * @brief Initialize the encryption
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerInitEncrytion(char **step,
		DesfireHandlingStruct *controller) {
	*step = "InitEncrypt";
	return fcNxpLibInitEncryption(controller) == 0;
}

/**
 * @brief Perform RATS
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerRats(char **step, DesfireHandlingStruct *controller) {
	uint16_t ior;
	*step = "RATS";
	uint8_t cardinfo[20];
//	ior = DnfccSamAuthenticateHost(0, 0, 0);

	//ior = DnfccSamIso14443_3ActivateIdle(1, 01, 0, 0, cardinfo); // 90, will get uid in data (1..7), 0=sak
	cardinfo[0] = 0x93;
	cardinfo[1] = 0x95;
	cardinfo[2] = 0x97;

	ior = DnfccSamIso14443_3AntiCollisionSelect(0x97, cardinfo); // 90, will get uid in data (1..7), 0=sak
	if (ior != 0x9000) {
		return 0;
	}
	memcpy(controller->cardId, cardinfo + 1, 7);

	ior = DnfccSamIso14443_4RatsPps(0, 0, 0, cardinfo); // Was 0,3,3

	return ior == 0x9000;
}

/**
 * @brief This step will read the file(s) necessary for handling the card information functions
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerReadCardInfo(char **step,
		DesfireHandlingStruct *controller) {
	*step = "ReadCardInfo";

	uint8_t ior = CnsCardInterfaceReadCardInfo(&apiInterface, controller);

	return ior == 0;
}

/**
 * @brief Verify if the card is not in the blacklist
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerapiDnfccVerifyCardIsNotInBlacklist(char **step,
		DesfireHandlingStruct *controller) {
	*step = "blckl";
	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 1);
	fileCardInformationStruct str;
	CopyStreamToCardInformation(&str, apiInterface.file1);
	uint8_t ret = apiDnfccVerifyCardIsNotInBlacklist(str.originalCard_ID);
	if (ret != 1) {
#if CURRENT_TARGET == CNS_HANDHELD
		strcpy(apiInterface.reason, "\n\n");
		strcpy(apiInterface.reason+2, LOCALE_REPORT_BLACKLIST);
#else
		strcpy(apiInterface.reason, LOCALE_REPORT_BLACKLIST);
#endif
	}
	return ret;
}

/**
 * @brief Verify that the card is active
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerStepVerifyCardActive(char **step,
		DesfireHandlingStruct *controller) {
	*step = "ReadCardInfo";

	uint8_t ior = CnsCardInterfaceVerifyCardActive(&apiInterface, controller);

	return ior == 0;
}

/**
 * @brief Verify if the card has the status as defined in the last char
 * @param step - String to note the step
 * @param controller - Controller  to use
 * @param status - the status to check
 */
uint8_t flowControllerVerifyCardStatus(char **step,
		DesfireHandlingStruct *controller, char status) {
	uint8_t ior;
	*step = "VERIFY_CARD_STATUS";
	ior = CnsCardInterfaceCardStatus(&apiInterface, controller, status);
	return ior == 0;
}

/**
 * @brief Verify if the card has the status A
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerVerifyCardStatusA(char **step,
		DesfireHandlingStruct *controller) {
	return flowControllerVerifyCardStatus(step, controller, 'A');
}

/**
 * @brief Verify if the card has the status P
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerVerifyCardStatusP(char **step,
		DesfireHandlingStruct *controller) {
	return flowControllerVerifyCardStatus(step, controller, 'P');
}

/**
 * @brief Verify if the card is not expired
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerVerifyIfCardIsNotExpired(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	char date[9];
	apiDnfccClockFetchDate(date);
	*step = "Check exp.dt";

	ior = CnsCardInterfaceCardIsCardNotExpired(&apiInterface, controller, date);
	return ior == 0;
}

/**
 * @brief Set the cad from status A to P
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerSetCardFromPtoA(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior = CnsCardInterfaceSetCardFromPtoA(&apiInterface, controller);
	return ior == 0;
}

/** TONY
 * @brief Set the cad from status P to A
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerSetCardFromAtoP(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior = CnsCardInterfaceSetCardFromAtoP(&apiInterface, controller);
	return ior == 0;
}

/**
 * @brief Verify that the system is logged in
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerSystemIsLoggedIn(char **step,
		DesfireHandlingStruct *controller) {
	*step = "Syslogin?";

	return deviceStatus.isLoggedIn;
}

/**
 * @brief Verify that the card is not expired
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerVerifyTicketIsNotExpired(char **step,
		DesfireHandlingStruct *controller) {
	uint8_t ior;
	char date[9];
	apiDnfccClockFetchDate(date);
	*step = "Expired?";
	ior = CnsCardInterfaceTicketExpire(&apiInterface, controller, date);
	return ior == 0;
}

/**
 * @brief Notify Try Again
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerStepTryAgain(char **step,
		DesfireHandlingStruct *controller) {
	char buf[MENU_BUFFER_SIZE + 1];
	*step = "DISP:RETR";

	// put validator message on CAN bus, note that device will not respond/get it's own messages
	// put validator message on CAN bus only if I am a validator
#if CURRENT_TARGET == CNS_VALIDATOR
	buf[0] = COMCOM_CANBUS_CARD_ENTER_RETRY;
	queueSendToCanbus(buf, portMAX_DELAY);
#endif

	buf[0] = COMCOM_MENU_CARD_ENTER_RETRY;

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 6);

	sprintf(buf + 1, "%ld", apiInterface.file6);
	queueSendToMenu(buf, portMAX_DELAY);

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 4);
	fileCardHolderInformationStruct holderInfo;
	CopyStreamToCardHolderInformation(&holderInfo, apiInterface.file4);
// send message with gender and language info (futureUse[0]) to buzzer
	sprintf(buf, "T%d", holderInfo.futureUse[0]);

	buf[0] = COMCOM_SOUND_TRYAGAIN;
	queueSendToSound(buf, portMAX_DELAY);
// short delay to allow sound to finish, otherwise too fast card handling causing sound loop
	vTaskDelay(100); // sound duration ~1 sec. this delay is sufficient
	CardHandlerSetToNotIgnore(controller);
	// Set that the next try is WUPA, not REQA
	deviceStatus.cardNeedsWupa = 1;
	return 1;
}

/**
 * @brief PErform the things to do when ending communication
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerEndCardCommunication(char **step,
		DesfireHandlingStruct *controller) {
	*step = "EndCommuncation";

	uint16_t ior = DnfccSamRcFreeCID(0xff);
	apiDnfccDisplayLedControlGreen(0);

	return ior = 0x9000;
}

/**
 * @brief Check if the card is still in the field
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerIsCardInField(char **step,
		DesfireHandlingStruct *controller) {
	*step = "EndCommuncation";
	return DnfccSamPresenceCheck() == 0x9000;
}

/**
 * @brief Read the ticket info file
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerReadTicketInfo(char **step,
		DesfireHandlingStruct *controller) {
	*step = "ReadTicketInfo";

	uint8_t ior = CnsCardInterfaceReadTicketInfo(&apiInterface, controller);

	return ior == 0;
}

/**
 * @brief Check if the card is of the correct login type
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerCheckCardIsLoginType(char **step,
		DesfireHandlingStruct *controller) {
	*step = "DISP:OKLB";
	uint8_t loginType =
#if CURRENT_TARGET == CNS_HANDHELD
			LOGINTYPE_HANDHELD
#elif CURRENT_TARGET == CNS_BUSCONSOLE
			LOGINTYPE_BUSDRIVER
#elif CURRENT_TARGET == CNS_VALIDATOR
	LOGINTYPE_BUSDRIVER
#elif CURRENT_TARGET == CNS_TOPUP
	LOGINTYPE_TOPUP
#endif
	;
	return CnsCardInterfaceCardIsOfLogintype(&apiInterface, controller,
			loginType);
}

/**
 * @brief Login the system and write log file
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerLoginSystem(char **step,
		DesfireHandlingStruct *controller) {
	char dBuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	*step = "Log-in";

	uint8_t ior = CnsCardInterfaceInternalReadFile(&apiInterface, controller,
			4);
	if (ior != 0) {
		return 0;
	}
	fileCardHolderInformationStruct cardInfo;
	CopyStreamToCardHolderInformation(&cardInfo, apiInterface.file4);
	strncpy((char *) deviceStatus.loginId, (char *) cardInfo.cardHolder_ID, 10);

	sprintf(dBuf, "%c%02x%02x%02x%02x%02x%02x%02x%02x%02x%02xT=",
	COMCOM_DATADISTRIBUTOR_LOGIN, deviceStatus.loginId[0],
			deviceStatus.loginId[1], deviceStatus.loginId[2],
			deviceStatus.loginId[3], deviceStatus.loginId[4],
			deviceStatus.loginId[5], deviceStatus.loginId[6],
			deviceStatus.loginId[7], deviceStatus.loginId[8],
			deviceStatus.loginId[9]);
	apiDnfccClockFetchDateTime(dBuf + 23); // add time of this event count length above
	queueSendToDataDistributor(dBuf, portMAX_DELAY);

	dBuf[0] = COMCOM_MENU_STARTSCREEN;
	queueSendToMenu(dBuf, portMAX_DELAY);
	return 1;
}

/**
 * @brief Display card reject
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerDisplayReject(char **step,
		DesfireHandlingStruct *controller) {
	*step = "DISP:ERR";
	char buf[MENU_BUFFER_SIZE + 1];
	// put validator message on CAN bus, note that device will not respond/get it's own messages
	buf[0] = COMCOM_CANBUS_REJECT_IN;
	queueSendToCanbus(buf, portMAX_DELAY);

	buf[0] = COMCOM_MENU_CARD_ENTER_REJECT;
	strcpy(buf + 1, apiInterface.previousReason);
	queueSendToMenu(buf, portMAX_DELAY);

	CnsCardInterfaceInternalReadFile(&apiInterface, controller, 4);
	fileCardHolderInformationStruct holderInfo;
	CopyStreamToCardHolderInformation(&holderInfo, apiInterface.file4);

	sendBalancesError(controller);

	buf[0] = COMCOM_SOUND_INVALID_IN;
	queueSendToSound(buf, portMAX_DELAY);

	return 1;
}

/**
 * @brief Write to the ticket log file
 * @param step - String to note the step
 * @param controller - Controller  to use
 */

uint8_t flowControllerWriteTicketLog(char **step,
		DesfireHandlingStruct *controller) {
	char dateTime[15];
	apiDnfccClockFetchDateTime(dateTime);
	uint8_t ior = CnsCardInterfaceTicketTransaction(&apiInterface, controller);
	ior = CnsCardInterfaceWriteTicketLog(&apiInterface, controller, dateTime);

	strcpy(apiInterface.reason, apiInterface.previousReason); // Passthrough the reason
	return ior == 0;
}

/**
 * @brief Jump to the start screen
 * @param step - String to note the step
 * @param controller - Controller  to use
 */
uint8_t flowControllerJumpToStartScreen(char **step,
		DesfireHandlingStruct *controller) {
	*step = "JSS";
	char dBuf[DISPLAY_BUFFER_SIZE];
	dBuf[0] = COMCOM_MENU_STARTSCREEN;
	queueSendToMenu(dBuf, portMAX_DELAY);
	return 1;
}

/** @} */
