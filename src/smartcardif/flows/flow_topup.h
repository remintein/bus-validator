/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_FLOWS_FLOW_TOPUP_H_
#define SMARTCARDIF_FLOWS_FLOW_TOPUP_H_

#include "../../_library/flowcontroller/flowController.h"
#include "../flowController/flowController.h"

 flowControllerflow_type theMainFlow[] = {

		// StepNr, Action, Next if successful   , Next if unsuccessful

		{ 0, FC_STEP_INITIALIZE, FC_NEXT_STEP, FC_NEXT_STEP }, /* 0 */

		{ 1000, FC_STEP_BEGIN, FC_NEXT_STEP, FC_NEXT_STEP }, /* 1 */

		{ 1500, FC_STEP_MENU_MODE_TOPUP_MAY_HANDLE_CARD, FC_NEXT_STEP, 1500 }, /* 2 */

		{ 2000, FC_STEP_DETECT_CARD, FC_NEXT_STEP, 1000 }, /* 3 */

		{ 2999, FC_STEP_MENU_MODE_TOPUP_MAY_HANDLE_CARD, FC_NEXT_STEP, 1500 }, /* 2 */
		{ 3000, FC_STEP_GET_NEXT_CARD_DESFIRE, FC_NEXT_STEP, 1000 }, /* 4 */

		{ 3005, FC_STEP_IS_NOT_CARD_IGNORED, FC_NEXT_STEP, 9999 }, /* 5 */

		{ 3010, FC_STEP_RATS, FC_NEXT_STEP, 9999 }, /* 6 */

		{ 3015, FC_STEP_SET_IGNORE_CARD, FC_NEXT_STEP, 9999 }, /* 7 */

		{ 3500, FC_STEP_INIT_ENCRYPTION, FC_NEXT_STEP, 9999 }, /* 8 */

		// CARD APPLICATION
		{ 4000, FC_STEP_CARD_READ_INFO, FC_NEXT_STEP, 18030 }, /* 9 */

		{ 4010, FC_STEP_CARD_IS_NOT_IN_BLACKLIST, FC_NEXT_STEP, 18020 }, /* 10 */

		{ 4020, FC_STEP_VERIFY_CARD_ACTIVE, FC_NEXT_STEP, 4100 }, /* 11 */

		{ 4030, FC_STEP_VERIFY_CARD_STATUS_A, FC_NEXT_STEP, 4100 }, /* 12 */

		{ 4040, FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED, 14900, 18020 }, /* 13 */

		{ 4100, FC_STEP_VERIFY_CARD_STATUS_P, FC_NEXT_STEP, 18020 }, /* 14 */

		{ 4110, FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED, FC_NEXT_STEP, 18020 }, /* 15 */

		{ 4120, FC_STEP_SET_CARD_STATUS_P2A, 14900, 18020 }, /* 16 */

		{ 14900, FC_STEP_SYSTEM_IS_LOGGED_IN, 15000, FC_NEXT_STEP }, /* 17 */

		{ 14910, FC_STEP_VERIFY_CARD_IS_LOGINTYPE, FC_NEXT_STEP, 9999 }, /*  18 */

		{ 14920, FC_STEP_LOGIN_SYSTEM, 9999, 9999 }, /* 19 */

		// TOPUP APPLICATION
		{ 15000, FC_STEP_TICKET_READ_INFO, FC_NEXT_STEP, 18030 }, /* 20 */

		{ 15010, FC_STEP_VERIFY_TICKET_IS_NOT_EXPIRED, 15030, FC_NEXT_STEP }, /*  21 */

		{ 15020, FC_STEP_SET_TICKET_VALUE_TO_0, FC_NEXT_STEP, FC_NEXT_STEP }, /* 22 */

		{ 15030, FC_STEP_TOPUP_VALUE_IS_SELECTED, FC_NEXT_STEP, 15060 }, /* 23 */

		{ 15040, FC_STEP_INCREASE_TICKET_VALUE, FC_NEXT_STEP, FC_NEXT_STEP }, /* 24 */

		{ 15050, FC_STEP_WRITE_TOPUP_LOG_FILE, FC_NEXT_STEP, FC_NEXT_STEP },/* 25 */

		{ 15055, FC_STEP_PRINT_BILL, FC_NEXT_STEP, FC_NEXT_STEP },/* 25 */

		{ 15060, FC_STEP_TOP_UP_DISPLAY_SCREEN, 9999, 9999 }, /* 26 */

		// END TOPUP APPLICATION

		///////////////////////////////////////////////////
		// DISPLAY & SOUND
		///////////////////////////////////////////////////
		{ 18020, FC_STEP_DISPLAY_REJECT, 9999, 9999 }, /* 27 */

		{ 18030, FC_STEP_DISPLAY_TRY_AGAIN, 18031, 18031 }, /* 28 */
		{ 18031, FC_STEP_UNSET_IGNORE_CARD, 9999, 9999 }, /* 28 */

		{ 9999, FC_STEP_END_CARDCOMMUNICATION, 1500, 1500 }, /* 29 */
};

 int theMainFlowLength = sizeof(theMainFlow) / sizeof(theMainFlow[0]);

#endif /* SMARTCARDIF_FLOWS_FLOW_TOPUP_H_ */
