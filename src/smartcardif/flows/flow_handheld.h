/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_FLOWS_FLOW_HANDHELD_H_
#define SMARTCARDIF_FLOWS_FLOW_HANDHELD_H_

#include "../../_library/flowcontroller/flowController.h"
#include "../flowController/flowController.h"

flowControllerflow_type theMainFlow[] =
		{

// StepNr, Action, Next if successful   , Next if unsuccessful
				{ 0, FC_STEP_INITIALIZE, FC_NEXT_STEP, FC_NEXT_STEP }, /* 0 */

				{ 1000, FC_STEP_BEGIN, FC_NEXT_STEP, FC_NEXT_STEP }, /* 1 */

				{ 1500, FC_STEP_MENU_MODE_HANDHELD_MAY_HANDLE_CARD,
						FC_NEXT_STEP, 1500 }, /*Penalty_pay_screen, WUPA*/

				{ 2000, FC_STEP_DETECT_CARD, FC_NEXT_STEP, 1000 }, /*WUPA*/

				{ 2999, FC_STEP_MENU_MODE_HANDHELD_MAY_HANDLE_CARD,
						FC_NEXT_STEP, 1500 }, /*Penalty_pay_screen, WUPA*/
				{ 3000, FC_STEP_GET_NEXT_CARD_DESFIRE, FC_NEXT_STEP, 1000 }, /* 5 */
				{ 3001, FC_STEP_IS_NOT_CARD_IGNORED, FC_NEXT_STEP, 9999 }, /* 6 */
				{ 3002, FC_STEP_RATS, FC_NEXT_STEP, 9999 }, /* 7 */
				{ 3004, FC_STEP_SET_IGNORE_CARD, FC_NEXT_STEP, 9999 }, /* 8 */

				{ 3500, FC_STEP_INIT_ENCRYPTION, FC_NEXT_STEP, 7003 }, /* 9 */

				// CHECK CARD
				{ 4000, FC_STEP_CARD_READ_INFO, FC_NEXT_STEP, 8030 }, /****NEW READ CARD INFO< TRY AGAIN*/ //

				{ 4010, FC_STEP_CARD_IS_NOT_IN_BLACKLIST, FC_NEXT_STEP, 7003 }, /* 10 */
				{ 4020, FC_STEP_VERIFY_CARD_ACTIVE, FC_NEXT_STEP, 4050 }, //
				{ 4030, FC_STEP_VERIFY_CARD_STATUS_A, FC_NEXT_STEP, 4050 }, /* CARD STATUS IS A – P?*/
				{ 4040, FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED, 4900, 7003 }, /* EXPIRED DATA IS OK – REJECT*/
				{ 4050, FC_STEP_VERIFY_CARD_STATUS_P, 4070, 7003 }, /*CARD STATUS IS A – P?*/
				{ 4060, FC_STEP_VERIFY_CARD_IS_NOT_EXPIRED, FC_NEXT_STEP, 7003 }, /* EXPIRED DATA IS OK - REJECT*/
				{ 4070, FC_STEP_SET_CARD_STATUS_P2A, FC_NEXT_STEP, 7003 }, /*SET STATUS FROM P TO A – REJECT*/

				// LOGIN
				{ 4900, FC_STEP_SYSTEM_IS_LOGGED_IN, 5000, FC_NEXT_STEP }, /*LOGIN is DONE OR JUMP TO LOGIN PROCESS */
				{ 4910, FC_STEP_VERIFY_CARD_IS_LOGINTYPE, FC_NEXT_STEP, 9999 }, /*SUCCESS ==> HD_DISPLAY - !SUCCESS => START SCREEN */
				{ 4920, FC_STEP_LOGIN_SYSTEM, 9999, 9999 }, /*SUCCESS ==> HD_DISPLAY - !SUCCESS => START SCREEN */

				// HANDHELD_DISPLAY
				// CARD_PAY
				{ 5000, FC_STEP_IS_PAY_CARD_SELECTED, FC_NEXT_STEP, 5100 }, /*CARD_PAY - CASH_PAY*/
				{ 5010, FC_STEP_VERIFY_TICKET_IS_NOT_EXPIRED, FC_NEXT_STEP, 7003 }, /* !Expired - Expired */
				{ 5020, FC_STEP_HANDHELD_VERIFY_TICKET_BALANCE_ENOUGH_FOR_PENALTY,
						FC_NEXT_STEP, 7003 }, /*CHECK TICKET VALUE*/
				{ 5030, FC_STEP_HANDHELD_PAY_PENALTY, 5200, 5200 }, /*DECREMENT TICKET_VALUE 10,000VND*/
				//CASH_PAY
				{ 5100, FC_STEP_IS_PAY_CASH_SELECTED, 5200, 5300 }, /*Has customer paid with cash (and is in penalty pay mode)*/
				//CONTROL
				{ 5300, FC_STEP_HANDHELD_READ_LOGFILE, FC_NEXT_STEP, 26010 }, /*READ Transaction FILE-TRY AGAIN*/
				{ 5310, FC_STEP_HANDHELD_VERIFY_LOGFILE, 5320, 5330 }, /*Correct-Penalty*/
//TONY, see flow
				{ 5320, FC_STEP_HANDHELD_DISPLAY_CARD_IS_PASS, 5230, 5230 }, /*  CORRECT_BT=GREEN - GO_TO_HANDHELD_DISPLAY_SCREEN*/
				{ 5330, FC_STEP_HANDHELD_DISPLAY_CARD_IS_FAIL, 5230, 5230 }, /*  GO_TO_PENALTY_PAY_SCREEN*/
//
				//WRITE TICKET LOG
				{ 5200, FC_STEP_WRITE_TICKET_LOG_FILE, FC_NEXT_STEP,
						FC_NEXT_STEP },/**** TICKET LOG FILE & TICKET TRANS. COUNT*/
				{ 5210, FC_STEP_CLEAR_PENALTY_PAY, FC_NEXT_STEP, FC_NEXT_STEP },/*CLEAR CARD+PAY,CASH_PAY*/
				{ 5220, FC_STEP_JUMP_TO_START_SCREEN, FC_NEXT_STEP, FC_NEXT_STEP }, /*SUCCESS ==> HD_DISPLAY - !SUCCESS => START SCREEN */

				{ 5230, FC_STEP_WRITE_HANDHELD_LOG_FILE, FC_NEXT_STEP,
						FC_NEXT_STEP },/**** TICKET LOG FILE & TICKET TRANS. COUNT*/
				{ 5240, FC_STEP_DISPLAY_HANDHELD_LOG_FILE, 9999, 9999 },/**** TICKET LOG FILE & TICKET TRANS. COUNT*/

				{ 26010, FC_STEP_JUMP_TO_START_SCREEN, 9999, 9999 }, /*  GO_TO_HANDHELD_DISPLAY_SCREEN*/

				// END HANDHELD APPLICATION
				///////////////////////////////////////////////////
				// DISPLAY & SOUND
				///////////////////////////////////////////////////
				{ 8030, FC_STEP_DISPLAY_TRY_AGAIN, 8031, 8031 }, /*NEW  HANDHELD_DISPLAY -TRY AGAIN*/
				{ 8031, FC_STEP_UNSET_IGNORE_CARD, 9999, 9999 }, /*NEW  HANDHELD_DISPLAY -TRY AGAIN*/
				{ 7003, FC_STEP_DISPLAY_REJECT, 9999, 9999 }, /*NEW HANDHELD_DISPLAY -REJECT*/
				{ 9999, FC_STEP_END_CARDCOMMUNICATION, 1500, 1500 }, /* We are ready with this card */
		};

int theMainFlowLength = sizeof(theMainFlow) / sizeof(theMainFlow[0]);

#endif /* SMARTCARDIF_FLOWS_FLOW_HANDHELD_H_ */
