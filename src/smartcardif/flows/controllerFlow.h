#include "../../_configuration/deviceSelector.h"

#if CURRENT_TARGET == CNS_VALIDATOR
#include "flow_validator.h"
#endif

#if CURRENT_TARGET == CNS_BUSCONSOLE
#include "flow_busconsole.h"
#endif

#if CURRENT_TARGET == CNS_HANDHELD
#include "flow_handheld.h"
#endif

#if CURRENT_TARGET == CNS_TOPUP
#include "flow_topup.h"
#endif

