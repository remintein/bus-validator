/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../_configuration/deviceSelector.h"
#include "../../_library/flowcontroller/flowController.h"
#include "../flowController/flowController.h"

/**
 * @file
 * @brief Includes the right flow.
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

#if CURRENT_TARGET == CNS_VALIDATOR
#include "flow_validator.h"
#endif

#if CURRENT_TARGET == CNS_BUSCONSOLE
#include "flow_busconsole.h"
#endif

#if CURRENT_TARGET == CNS_HANDHELD
#include "flow_handheld.h"
#endif

#if CURRENT_TARGET == CNS_TOPUP
#include "flow_topup.h"
#endif


/** @} */
