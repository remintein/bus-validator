/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_FLOWS_FLOW_VALIDATOR_H_
#define SMARTCARDIF_FLOWS_FLOW_VALIDATOR_H_

#include "../../_library/flowcontroller/flowController.h"
#include "../flowController/flowController.h"

flowControllerflow_type theMainFlow[] =
		{

// StepNr, Action										, Next if successfull		, Next if unsuccessfull

				{ 0, FC_STEP_INITIALIZE, FC_NEXT_STEP, FC_NEXT_STEP }, /* 0 */

				{ 1000, FC_STEP_BEGIN, FC_NEXT_STEP, FC_NEXT_STEP }, /* 1 */

				{ 1500, FC_STEP_MENU_MODE_VALIDATOR_MAY_HANDLE_CARD,
				FC_NEXT_STEP, 1500 }, /* 2 */

				{ 2000, FC_STEP_DETECT_CARD, FC_NEXT_STEP, 1000 }, /* 4 */

				{ 3010, FC_STEP_RATS, FC_NEXT_STEP, 9999 }, /* 7 */
// CARD APPLICATION

				{ 4000, FC_STEP_CARD_READ_INFO, FC_NEXT_STEP, 8030 }, /****NEW READ CARD INFO< TRY AGAIN*/ //	{   4000, FLOWCONTROLLER_STEP_CARD_TRANSACTION_COUNT, 4001, 9999}, /* CARD_TRANSACTION_COUNT,*/
				{ 7003, FLOWCONTROLLER_STEP_NULL, 4030, 4030 }, /* CARD REJECT */


///////////////////////////////////////////////////
// DISPLAY & SOUND
///////////////////////////////////////////////////

				{ 8000, FLOWCONTROLLER_STEP_DISPLAY_OK, 9999, 9999 }, /*  DISPLAY OK*/


				{ 8020, FC_STEP_DISPLAY_REJECT, 9999, 9999 }, /*  DISPLAY REJECT*/

				{ 8030, FC_STEP_DISPLAY_TRY_AGAIN, 8031, 8031 }, /****NEW  DISPLAY TRY AGAIN*/

				{ 8040, FLOWCONTROLLER_STEP_DISPLAY_START_SCREEN, 9999, 9999 }, /****NEW  DISPLAY START SCREEN */

				{ 9999, FC_STEP_END_CARDCOMMUNICATION, 2000, 2000 }, /* We are ready with this card */

		};

int theMainFlowLength = sizeof(theMainFlow) / sizeof(theMainFlow[0]);

#endif /* SMARTCARDIF_FLOWS_FLOW_VALIDATOR_H_ */
