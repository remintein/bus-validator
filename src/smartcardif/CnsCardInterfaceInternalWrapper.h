/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_CNSCARDINTERFACEINTERNALWRAPPER_H_
#define SMARTCARDIF_CNSCARDINTERFACEINTERNALWRAPPER_H_

#include "phNfcLib.h"

uint8_t CnsCardInterfaceInternalReadData(uint8_t bOption, uint8_t bFileNo,
		uint8_t * pOffset, uint8_t * pLength, uint8_t ** ppRxdata,
		uint16_t * pRxdataLen);

uint8_t CnsCardInterfaceInternalWriteData(uint8_t bCommOption, uint8_t bFileNo,
		uint8_t * pOffset, uint8_t * pData, uint8_t * pDataLen);

uint8_t CnsCardInterfaceInternalWriteRecord(uint8_t bCommOption,
		uint8_t bFileNo, uint8_t * pOffset, uint8_t * pData, uint8_t * pDataLen);

uint8_t CnsCardInterfaceInternalReadRecords(uint8_t bCommOption,
		uint8_t bFileNo, uint8_t * pOffset, uint8_t * pNumRec,
		uint8_t * pRecSize, uint8_t ** ppRxdata, uint16_t * pRxdataLen);

uint16_t CnsCardInterfaceInternalGetValue(uint8_t bCommOption, uint8_t bFileNo,
		uint8_t * pValue);

uint16_t CnsCardInterfaceInternal_Sw_CommitTransaction();

uint16_t CnsCardInterfaceInternal_Sw_Debit(uint8_t bCommOption, uint8_t bFileNo,
		uint8_t * pValue);

uint16_t CnsCardInterfaceInternal_Sw_Credit(uint8_t bCommOption,
		uint8_t bFileNo, uint8_t * pValue);

uint16_t CnsCardInterfaceInternal_Sw_SelectApplication(uint8_t * pAppId);

uint16_t CnsCardInterfaceInternal_Sw_FormatPICC();

uint16_t CnsCardInterfaceInternal_Sw_CreateApplication(uint8_t *pAid,
		uint8_t bKeySettings1, uint8_t bKeySettings2);

phStatus_t CnsCardInterfaceSam_Sw_IsoAuthenticate(uint16_t wKeyNo,
		uint16_t wKeyVer, uint8_t bKeyNoCard);

uint16_t CnsCardInterfaceInternal_Sw_CreateNormalFile(uint8_t fileno,
		uint8_t fileType, uint8_t comset, uint16_t accessRights,
		uint16_t filesize);

uint16_t CnsCardInterfaceInternal_Sw_CreateValueFile(uint8_t fileno,
		uint8_t fileType, uint8_t comset, uint16_t accessRights, uint32_t low,
		uint32_t high, uint32_t start, uint8_t limited);

uint16_t CnsCardInterfaceInternal_Sw_CreateRecordsFile(uint8_t fileno,
		uint8_t fileType, uint8_t comset, uint16_t accessRights,
		uint16_t recordSize, uint16_t numRecords);

#endif /* SMARTCARDIF_CNSCARDINTERFACEINTERNALWRAPPER_H_ */
