/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "stm32f4xx.h"
#include <string.h>
#include <stdio.h>
#include "../project_includes.h"
#include "../_configuration/cardInformation.h"
#include "CnsCardInterface.h"
#include "CnsCardInterfaceInternal.h"
#include "CnsCardInterfaceInternalWrapper.h"
#include "DesfireFunctions.h"
#include "phalMfdf_Int.h"

#include "phNfcLib.h"

/**
 * @file
 * @brief Contains definitions about the files
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */
int desfireAuthenticateAES(uint8_t keyNo, uint8_t keyNoCard, uint8_t keyVers);

uint8_t CnsCardInterfaceInternalFileNumber[] = {
FILE_CARD_INFORMATION,
FILE_CARD_CONFIGURATION,
FILE_CARD_STATUS,
FILE_CARD_HOLDER_INFORMATION,
FILE_TICKET_INFORMATION,
FILE_TICKET_VALUE,
FILE_TICKET_LOG,
FILE_PURSE_INFORMATION,
FILE_PURSE_VALUE,
FILE_PURSE_LOG,
FILE_TICKET_TRANSACTION_COUNT,
FILE_PURSE_TRANSACTION_COUNT,
FILE_CARD_TRANSACTION_COUNT };

uint8_t CnsCardInterfaceInternalFileSize[] = {
SIZE_CARD_INFORMATION,
SIZE_CARD_CONFIGURATION,
SIZE_CARD_STATUS,
SIZE_CARD_HOLDER_INFORMATION,
SIZE_TICKET_INFORMATION,
SIZE_TICKET_VALUE,
SIZE_TICKET_LOG,
SIZE_PURSE_INFORMATION,
SIZE_PURSE_VALUE,
SIZE_PURSE_LOG,
SIZE_TICKET_TRANSACTION_COUNT,
SIZE_PURSE_TRANSACTION_COUNT,
SIZE_CARD_TRANSACTION_COUNT };

uint8_t CnsCardInterfaceInternalFileSizeRead[] = {
SIZE_CARD_INFORMATION_READ,
SIZE_CARD_CONFIGURATION_READ,
SIZE_CARD_STATUS_READ,
SIZE_CARD_HOLDER_INFORMATION_READ,
SIZE_TICKET_INFORMATION_READ,
SIZE_TICKET_VALUE_READ,
SIZE_TICKET_LOG_READ,
SIZE_PURSE_INFORMATION_READ,
SIZE_PURSE_VALUE_READ,
SIZE_PURSE_LOG_READ,
SIZE_TICKET_TRANSACTION_COUNT_READ,
SIZE_PURSE_TRANSACTION_COUNT_READ,
SIZE_CARD_TRANSACTION_COUNT_READ };

uint8_t CnsCardInterfaceInternalFileKind[] = {
KIND_CARD_INFORMATION,
KIND_CARD_CONFIGURATION,
KIND_CARD_STATUS,
KIND_CARD_HOLDER_INFORMATION,
KIND_TICKET_INFORMATION,
KIND_TICKET_VALUE,
KIND_TICKET_LOG,
KIND_PURSE_INFORMATION,
KIND_PURSE_VALUE,
KIND_PURSE_LOG,
KIND_TICKET_TRANSACTION_COUNT,
KIND_PURSE_TRANSACTION_COUNT,
KIND_CARD_TRANSACTION_COUNT };

uint32_t CnsCardInterfaceInternalApplications[] = {
APPLICATION_CARD_INFORMATION,
APPLICATION_CARD_CONFIGURATION,
APPLICATION_CARD_STATUS,
APPLICATION_CARD_HOLDER_INFORMATION,
APPLICATION_TICKET_INFORMATION,
APPLICATION_TICKET_VALUE,
APPLICATION_TICKET_LOG,
APPLICATION_PURSE_INFORMATION,
APPLICATION_PURSE_VALUE,
APPLICATION_PURSE_LOG,
APPLICATION_TICKET_TRANSACTION_COUNT,
APPLICATION_PURSE_TRANSACTION_COUNT,
APPLICATION_CARD_TRANSACTION_COUNT };

uint32_t CnsCardInterfaceInternalAuthenticateKey[] = {
AUTHENTICATIONKEY_CARD_INFORMATION,
AUTHENTICATIONKEY_CARD_CONFIGURATION,
AUTHENTICATIONKEY_CARD_STATUS,
AUTHENTICATIONKEY_CARD_HOLDER_INFORMATION,
AUTHENTICATIONKEY_TICKET_INFORMATION,
AUTHENTICATIONKEY_TICKET_VALUE,
AUTHENTICATIONKEY_TICKET_LOG,
AUTHENTICATIONKEY_PURSE_INFORMATION,
AUTHENTICATIONKEY_PURSE_VALUE,
AUTHENTICATIONKEY_PURSE_LOG,
AUTHENTICATIONKEY_TICKET_TRANSACTION_COUNT,
AUTHENTICATIONKEY_PURSE_TRANSACTION_COUNT,
AUTHENTICATIONKEY_CARD_TRANSACTION_COUNT };

uint32_t CnsCardInterfaceInternalCardKey[] = {
CARDKEY_CARD_INFORMATION,
CARDKEY_CARD_CONFIGURATION,
CARDKEY_CARD_STATUS,
CARDKEY_CARD_HOLDER_INFORMATION,
CARDKEY_TICKET_INFORMATION,
CARDKEY_TICKET_VALUE,
CARDKEY_TICKET_LOG,
CARDKEY_PURSE_INFORMATION,
CARDKEY_PURSE_VALUE,
CARDKEY_PURSE_LOG,
CARDKEY_TICKET_TRANSACTION_COUNT,
CARDKEY_PURSE_TRANSACTION_COUNT,
CARDKEY_CARD_TRANSACTION_COUNT };

/**
 * @brief converts a string date to a BCD date
 * @param date - the String date
 * @param out - the BCD date
 */
void CnsCardInterfaceInternalBcdDate(char *date, uint8_t *out) {
	out[0] = ((date[0] - '0') << 4) | (date[1] - '0');
	out[1] = ((date[2] - '0') << 4) | (date[3] - '0');
	out[2] = ((date[4] - '0') << 4) | (date[5] - '0');
	out[3] = ((date[6] - '0') << 4) | (date[7] - '0');
}

/**
 * @brief converts a BCD date to a String date
 * @param date - the BCD date
 * @param out - the String date
 */
void CnsCardInterfaceInternalBcdToDate(char *date, uint8_t *out) {
	out[0] = (date[0] >> 4) + '0';
	out[1] = (date[0] & 0xf) + '0';
	out[2] = (date[1] >> 4) + '0';
	out[3] = (date[1] & 0xf) + '0';
	out[4] = (date[2] >> 4) + '0';
	out[5] = (date[2] & 0xf) + '0';
	out[6] = (date[3] >> 4) + '0';
	out[7] = (date[3] & 0xf) + '0';
	out[8] = '\0';
}

/**
 * @brief converts a string date/time to a BCD date/time
 * @param date - the String date/time
 * @param out - the BCD date/time
 */
void CnsCardInterfaceInternalBcdDateTime(char *date, uint8_t *out) {
	out[0] = (date[0] - '0') << 4 | (date[1] - '0');
	out[1] = (date[2] - '0') << 4 | (date[3] - '0');
	out[2] = (date[4] - '0') << 4 | (date[5] - '0');
	out[3] = (date[6] - '0') << 4 | (date[7] - '0');
	out[4] = (date[8] - '0') << 4 | (date[9] - '0');
	out[5] = (date[10] - '0') << 4 | (date[11] - '0');
	out[6] = (date[12] - '0') << 4 | (date[13] - '0');
}

/**
 * @brief add days to a string date
 * @param date - the string date
 * @param days - the number of days
 */
void CnsCardInterfaceInternalAddDaysToDate(char *date, uint16_t days) {
	int year, month, day;

	sscanf(date, "%04d%02d%02d", &year, &month, &day);

	while (days > 0) {
		day++;
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 8:
		case 10:
		case 12:
			if (day > 31) {
				day -= 31;
				month++;
				if (month > 12) {
					month = 1;
					year++;
				}
			}
			break;
		case 2:
			if (day > 28) {
				day -= 28;
				month++;
			} else if (day > 29 && year % 4 == 0) {
				days -= 29;
				month++;
			}
			break;
		case 4:
		case 6:
		case 7:
		case 9:
		case 11:
			if (day > 30) {
				day -= 30;
				month++;
			}
			break;
		}
		days--;
	}
	char b[9];
	sprintf(b, "%04d%02d%02d", year, month, day);
	strncpy(date, b, 8);
}

/**
 * @brief add days to a BCD date
 * @param date - the BCD date
 * @param days - the number of days
 */
void CnsCardInterfaceInternalAddDaysToBcdDate(uint8_t *date, uint16_t days) {
	int year, month, day;
	year = ((date[0] >> 4) & 0x0f) * 1000;
	year += (date[0] & 0x0f) * 100;
	year += ((date[1] >> 4) & 0x0f) * 10;
	year += date[1] & 0x0f;

	month = ((date[2] >> 4) & 0x0f) * 10;
	month += date[2] & 0x0f;

	day = ((date[3] >> 4) & 0x0f) * 10;
	day += date[3] & 0x0f;

	while (days > 0) {
		day++;
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 8:
		case 10:
		case 12:
			if (day > 31) {
				day -= 31;
				month++;
				if (month > 12) {
					month = 1;
					year++;
				}
			}
			break;
		case 2:
			if (day > 28) {
				day -= 28;
				month++;
			} else if (day > 29 && year % 4 == 0) {
				day = 29;
				month++;
			}
			break;
		case 4:
		case 6:
		case 7:
		case 9:
		case 11:
			if (day > 30) {
				day -= 30;
				month++;
			}
			break;
		}
		days--;
	}
	char b[9];
	sprintf(b, "%04d%02d%02d", year, month, day);
	CnsCardInterfaceInternalBcdDate(b, date);
}

/**
 * @brief Select application
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param aid - the application id
 * @param applicationKey - the applicationkey
 * @param cardKey - the card key
 * @return Status
 */
uint8_t CnsCardInterfaceInternalSelectApplication(
		CnsCardInterfaceInfo *apiPointer, void *handel, uint32_t aid,
		uint32_t applicationKey, uint32_t cardKey) {
	uint16_t ior;
	uint8_t aidBytes[5];

	if (aid != apiPointer->curApplication) {
		aidBytes[0] = aid & 0xff;
		aidBytes[1] = (aid >> 8) & 0xff;
		aidBytes[2] = (aid >> 16) & 0xff;
		ior = CnsCardInterfaceInternal_Sw_SelectApplication(aidBytes);
		if (ior != CNS_CARDINTERFACE_RETURN_OK) {
			return ior;
		}

		apiPointer->curApplication = aid;
		apiPointer->curApplicationKey = -1;
	}
	if (applicationKey != apiPointer->curApplicationKey) {
// Authenticate now
		memcpy(apiPointer->cardUid, ((DesfireHandlingStruct *) handel)->cardId,
				7);
		ior = desfireAuthenticateAES(applicationKey, cardKey, 0x00);
		if (ior != CNS_CARDINTERFACE_RETURN_OK) {
			return CNS_CARDINTERFACE_RETURN_AUTHENTICATION_ERROR;
		}

		apiPointer->curApplicationKey = applicationKey;
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Debet an amount of a file
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param fileno - the filenumber
 * @param value - the amount to debet
 * @return Status
 */
uint8_t CnsCardInterfaceInternalDebit(CnsCardInterfaceInfo *apiPointer,
		void *handel, int fileno, uint32_t value) {
	if (fileno < 1 || fileno > CNS_CARDINTERFACE_MAXFILES) {
		return CNS_CARDINTERFACE_INTERNAL_ERROR;
	}
	if (CnsCardInterfaceInternalFileKind[fileno - 1] != VALUE_FILE) {
		return CNS_CARDINTERFACE_INTERNAL_ERROR;
	}

	uint16_t ior;

	ior = CnsCardInterfaceInternalSelectApplication(apiPointer, handel,
			CnsCardInterfaceInternalApplications[fileno - 1],
			CnsCardInterfaceInternalAuthenticateKey[fileno - 1],
			CnsCardInterfaceInternalCardKey[fileno - 1]);

	if (ior != CNS_CARDINTERFACE_RETURN_OK) {
		return ior;
	}
	uint8_t pValue[4];
	memcpy(pValue, &value, 4);
	ior = CnsCardInterfaceInternal_Sw_Debit(
	PHAL_MFDF_COMMUNICATION_PLAIN,
			CnsCardInterfaceInternalFileNumber[fileno - 1], pValue);

	if (ior != CNS_CARDINTERFACE_RETURN_OK) {
		return ior;
	}

	ior = CnsCardInterfaceInternal_Sw_CommitTransaction();

	if (ior != 0) {
		return ior;
	}

	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Credit an amount to a file
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param fileno - the filenumber
 * @param value - the amount to credit
 * @return Status
 */
uint8_t CnsCardInterfaceInternalCredit(CnsCardInterfaceInfo *apiPointer,
		void *handel, int fileno, uint32_t value) {
	if (fileno < 1 || fileno > CNS_CARDINTERFACE_MAXFILES) {
		return CNS_CARDINTERFACE_INTERNAL_ERROR;
	}
	if (CnsCardInterfaceInternalFileKind[fileno - 1] != VALUE_FILE) {
		return CNS_CARDINTERFACE_INTERNAL_ERROR;
	}

	int ior = CnsCardInterfaceInternalSelectApplication(apiPointer, handel,
			CnsCardInterfaceInternalApplications[fileno - 1],
			CnsCardInterfaceInternalAuthenticateKey[fileno - 1],
			CnsCardInterfaceInternalCardKey[fileno - 1]);
	if (ior != CNS_CARDINTERFACE_RETURN_OK) {
		return ior;
	}

	ior = CnsCardInterfaceInternal_Sw_Credit(
	PHAL_MFDF_COMMUNICATION_PLAIN,
			CnsCardInterfaceInternalFileNumber[fileno - 1], (uint8_t *) &value);

	if (ior != CNS_CARDINTERFACE_RETURN_OK) {
		return ior;
	}
	ior = CnsCardInterfaceInternal_Sw_CommitTransaction();

	if (ior != 0) {
		return ior;
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Read a file from the PICC
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param fileno - the filenumber
 * @return Status
 */
uint8_t CnsCardInterfaceInternalReadFile(CnsCardInterfaceInfo *apiPointer,
		void *handel, int fileno) {
	uint8_t len[3];
	uint8_t offset[3];
	uint8_t numrec[3];
	uint8_t recsize[3];
	uint8_t value[4];
	uint16_t lengthRead;
	uint16_t position;
	uint16_t readLength;
	if (fileno < 1 || fileno > CNS_CARDINTERFACE_MAXFILES) {
		return CNS_CARDINTERFACE_INTERNAL_ERROR;
	}

	if (apiPointer->fileIsRead[fileno - 1] == 1) {
		return CNS_CARDINTERFACE_RETURN_OK;
	}
	uint8_t ior;

	ior = CnsCardInterfaceInternalSelectApplication(apiPointer, handel,
			CnsCardInterfaceInternalApplications[fileno - 1],
			CnsCardInterfaceInternalAuthenticateKey[fileno - 1],
			CnsCardInterfaceInternalCardKey[fileno - 1]);
	if (ior != CNS_CARDINTERFACE_RETURN_OK) {
		return ior;
	}
	uint8_t *sendTo;
	uint8_t *pRecBuf;
	int32_t *sendTo32;
	switch (fileno) {
	case 1:
		sendTo = apiPointer->file1;
		break;
	case 2:
		sendTo = apiPointer->file2;
		break;
	case 3:
		sendTo = apiPointer->file3;
		break;
	case 4:
		sendTo = apiPointer->file4;
		break;
	case 5:
		sendTo = apiPointer->file5;
		break;
	case 6:
		sendTo32 = &(apiPointer->file6);
		break;
	case 7:
		sendTo = apiPointer->file7;
		break;
	case 8:
		sendTo = apiPointer->file8;
		break;
	case 9:
		sendTo32 = &(apiPointer->file9);
		break;
	case 10:
		sendTo = apiPointer->file10;
		break;
	case 11:
		sendTo32 = &(apiPointer->file11);
		break;
	case 12:
		sendTo32 = &(apiPointer->file12);
		break;
	case 13:
		sendTo32 = &(apiPointer->file13);
		break;
	}
	switch (CnsCardInterfaceInternalFileKind[fileno - 1]) {

	case VALUE_FILE:
		ior = CnsCardInterfaceInternalGetValue(
		PHAL_MFDF_COMMUNICATION_PLAIN,
				CnsCardInterfaceInternalFileNumber[fileno - 1], value);
		if (ior != CNS_CARDINTERFACE_RETURN_OK) {
			return ior;
		}
		*sendTo32 = value[3];
		*sendTo32 = *sendTo32 << 8;
		*sendTo32 += value[2];
		*sendTo32 = *sendTo32 << 8;
		*sendTo32 += value[1];
		*sendTo32 = *sendTo32 << 8;
		*sendTo32 += value[0];
		break;

	case DATA_FILE:
	case BACKUP_DATA_FILE:

		for (position = 0;
				position < CnsCardInterfaceInternalFileSizeRead[fileno - 1];
				position += 32) {
			readLength = CnsCardInterfaceInternalFileSizeRead[fileno - 1]
					- position;
			if (readLength > 32) {
				readLength = 32;
			}
			len[2] = 0;
			len[1] = 0;
			len[0] = readLength;
			offset[2] = '\0';
			offset[1] = (position >> 8) & 0xff;
			offset[0] = position & 0xff;
			lengthRead = 0;
			ior = CnsCardInterfaceInternalReadData(
			PHAL_MFDF_COMMUNICATION_PLAIN,
					CnsCardInterfaceInternalFileNumber[fileno - 1], offset, len,
					&pRecBuf, &lengthRead);
			if (ior == 0 && lengthRead > 0) {
				memcpy(sendTo + position, pRecBuf, lengthRead);
			} else {
				return 1;
			}
		}
		break;

	case CYCLIC_FILE:
		len[2] = (CnsCardInterfaceInternalFileSize[fileno - 1] >> 16) & 0xff;
		len[1] = (CnsCardInterfaceInternalFileSize[fileno - 1] >> 8) & 0xff;
		len[0] = CnsCardInterfaceInternalFileSize[fileno - 1] & 0xff;
		offset[0] = '\0';
		offset[1] = '\0';
		offset[2] = '\0';
		numrec[0] = '\1';
		numrec[1] = '\0';
		numrec[2] = '\0';
		recsize[0] = CnsCardInterfaceInternalFileSize[fileno - 1];
		recsize[1] = CnsCardInterfaceInternalFileSize[fileno - 1] >> 8;
		recsize[2] = CnsCardInterfaceInternalFileSize[fileno - 1] >> 16;
		lengthRead = 0;

		ior = CnsCardInterfaceInternalReadRecords(
		PHAL_MFDF_COMMUNICATION_PLAIN,
				CnsCardInterfaceInternalFileNumber[fileno - 1], offset, numrec,
				recsize, &pRecBuf, &lengthRead);

		if (ior == 0 && lengthRead > 0) {
			memcpy(sendTo, pRecBuf, lengthRead);
		}

		if (ior != CNS_CARDINTERFACE_RETURN_OK) {
			return ior;
		}
		break;
	}
	apiPointer->fileIsRead[fileno - 1] = 1;
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Write a file to the PICC
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param fileno - the filenumber
 * @return Status
 */
uint8_t CnsCardInterfaceInternalWriteFile(CnsCardInterfaceInfo *apiPointer,
		void *handel, int fileno) {
	uint8_t len[3];
	uint8_t offset[3];
	if (fileno < 1 || fileno > CNS_CARDINTERFACE_MAXFILES) {
		return CNS_CARDINTERFACE_INTERNAL_ERROR;
	}

	uint8_t ior;

	ior = CnsCardInterfaceInternalSelectApplication(apiPointer, handel,
			CnsCardInterfaceInternalApplications[fileno - 1],
			CnsCardInterfaceInternalAuthenticateKey[fileno - 1],
			CnsCardInterfaceInternalCardKey[fileno - 1]);
	if (ior != CNS_CARDINTERFACE_RETURN_OK) {
		return ior;
	}
	uint8_t *sendTo;
	switch (fileno) {
	case 1:
		sendTo = apiPointer->file1;
		break;
	case 2:
		sendTo = apiPointer->file2;
		break;
	case 3:
		sendTo = apiPointer->file3;
		break;
	case 4:
		sendTo = apiPointer->file4;
		break;
	case 5:
		sendTo = apiPointer->file5;
		break;
	case 7:
		sendTo = apiPointer->file7;
		break;
	case 8:
		sendTo = apiPointer->file8;
		break;
	case 10:
		sendTo = apiPointer->file10;
		break;
	default:
		return -1; // Not permitted to set a value file
	}
	switch (CnsCardInterfaceInternalFileKind[fileno - 1]) {
	case CYCLIC_FILE:
		len[2] = (CnsCardInterfaceInternalFileSizeRead[fileno - 1] >> 16)
				& 0xff;
		len[1] = (CnsCardInterfaceInternalFileSizeRead[fileno - 1] >> 8) & 0xff;
		len[0] = CnsCardInterfaceInternalFileSizeRead[fileno - 1] & 0xff;
		offset[0] = '\0';
		offset[1] = '\0';
		offset[2] = '\0';

		ior = CnsCardInterfaceInternalWriteRecord(
		PHAL_MFDF_COMMUNICATION_PLAIN,
				CnsCardInterfaceInternalFileNumber[fileno - 1], offset, sendTo,
				len);

		if (ior != CNS_CARDINTERFACE_RETURN_OK) {
			return ior;
		}
		ior = CnsCardInterfaceInternal_Sw_CommitTransaction();

		if (ior != 0) {
			return ior;
		}

		break;
	case VALUE_FILE:
		break;
	case DATA_FILE:
		len[2] = (CnsCardInterfaceInternalFileSizeRead[fileno - 1] >> 16)
				& 0xff;
		len[1] = (CnsCardInterfaceInternalFileSizeRead[fileno - 1] >> 8) & 0xff;
		len[0] = CnsCardInterfaceInternalFileSizeRead[fileno - 1] & 0xff;
		offset[0] = '\0';
		offset[1] = '\0';
		offset[2] = '\0';

		ior = CnsCardInterfaceInternalWriteData(
		PHAL_MFDF_COMMUNICATION_PLAIN,
				CnsCardInterfaceInternalFileNumber[fileno - 1], offset, sendTo,
				len);
		if (ior != CNS_CARDINTERFACE_RETURN_OK) {
			return ior;
		}
		ior = CnsCardInterfaceInternal_Sw_CommitTransaction();

		if (ior != 0) {
			return ior;
		}
		break;
	case BACKUP_DATA_FILE:
		len[2] = (CnsCardInterfaceInternalFileSizeRead[fileno - 1] >> 16)
				& 0xff;
		len[1] = (CnsCardInterfaceInternalFileSizeRead[fileno - 1] >> 8) & 0xff;
		len[0] = CnsCardInterfaceInternalFileSizeRead[fileno - 1] & 0xff;
		offset[0] = '\0';
		offset[1] = '\0';
		offset[2] = '\0';

		ior = CnsCardInterfaceInternalWriteData(
		PHAL_MFDF_COMMUNICATION_PLAIN,
				CnsCardInterfaceInternalFileNumber[fileno - 1], offset, sendTo,
				len);
		if (ior != CNS_CARDINTERFACE_RETURN_OK) {
			return ior;
		}
		ior = CnsCardInterfaceInternal_Sw_CommitTransaction();

		if (ior != 0) {
			return ior;
		}
		break;

	}

	if (CnsCardInterfaceInternalFileKind[fileno - 1] == VALUE_FILE) {
		// Not possible, or should it be debit/credit to the right value? Not in use at this moment
	}
	apiPointer->fileIsRead[fileno - 1] = 1;
	return CNS_CARDINTERFACE_RETURN_OK;
}
/** @} */
