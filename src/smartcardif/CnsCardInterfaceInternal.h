/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_CNSCARDINTERFACEINTERNAL_H_
#define SMARTCARDIF_CNSCARDINTERFACEINTERNAL_H_

void CnsCardInterfaceInternalBcdDate(char *date, uint8_t *out);
void CnsCardInterfaceInternalBcdDateTime(char *date, uint8_t *out);
void CnsCardInterfaceInternalBcdToDate(char *date, uint8_t *out);
void CnsCardInterfaceInternalAddDaysToBcdDate(uint8_t *date, uint16_t days);
void CnsCardInterfaceInternalAddDaysToDate(char *date, uint16_t days);

uint8_t CnsCardInterfaceInternalSelectApplication(
		CnsCardInterfaceInfo *apiPointer, void *handel, uint32_t aid,
		uint32_t applicationKey, uint32_t cardKey);
uint8_t CnsCardInterfaceInternalReadFile(CnsCardInterfaceInfo *apiPointer,
		void *handel, int fileno);
uint8_t CnsCardInterfaceInternalWriteFile(CnsCardInterfaceInfo *apiPointer,
		void *handel, int fileno);
uint8_t CnsCardInterfaceInternalCredit(CnsCardInterfaceInfo *apiPointer,
		void *handel, int fileno, uint32_t value);
uint8_t CnsCardInterfaceInternalDebit(CnsCardInterfaceInfo *apiPointer,
		void *handel, int fileno, uint32_t value);

#endif /* SMARTCARDIF_CNSCARDINTERFACEINTERNAL_H_ */
