/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

//#include "DesfireFunctions.h"
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "SmartcardIf.h"
#include <phpalMifare.h>
#include <phalMfdf.h>
#include "phalMfdf_Int.h"
#include <ph_RefDefs.h>
#include "phCryptoSym.h"
#include "../project_includes.h"

#include "CnsCardInterface.h"

/**
 * @file
 *
 * This file contains a authenticate function
 *
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

uint16_t DnfccSamDesfireAuthenticatePicc(uint8_t keySam, uint8_t keyPicc,
		uint8_t *pDivInp, uint8_t pDivLength, uint8_t *results,
		uint16_t *length);

extern CnsCardInterfaceInfo apiInterface;

#define PH_CRYPTOSYM_SW_NUM_AES_ROUNDS_128 10U  /**< AES rounds for 128 bit key. */
#define PH_CRYPTOSYM_SW_NUM_AES_ROUNDS_192 12U  /**< AES rounds for 192 bit key. */
#define PH_CRYPTOSYM_SW_NUM_AES_ROUNDS_256 14U  /**< AES rounds for 256 bit key. */

/**
 * @brief authenticare AES with the PICC
 * @param keyNo - the key number
 * @param keyNoCard - the keuy number on the card
 * @param keyVers - the version of the key
 * @return 0 = ok, otherwise is an error
 */
int desfireAuthenticateAES(uint8_t keyNo, uint8_t keyNoCard, uint8_t keyVers) {
#define DIV_MODE PHAL_MFDF_DIV_METHOD_ENCR
	uint8_t divKey[16];
	memset(divKey, '\0', 16);
	divKey[0] = apiInterface.cardUid[0];
	divKey[1] = apiInterface.cardUid[1];
	divKey[2] = apiInterface.cardUid[2];
	divKey[3] = apiInterface.cardUid[3];
	divKey[4] = apiInterface.cardUid[4];
	divKey[5] = apiInterface.cardUid[5];
	divKey[6] = apiInterface.cardUid[6];

#define DIV_INPUT divKey
#define DIV_LENGTH 16
	uint16_t responselength;
	uint8_t response[20];
	uint16_t ior = DnfccSamDesfireAuthenticatePicc(keyNo, keyNoCard, DIV_INPUT,
	DIV_LENGTH, response, &responselength);
	if (ior != 0x9000) {
		return CNS_CARDINTERFACE_RETURN_AUTHENTICATION_ERROR;
	}

	return 0;
}

/** @} */
