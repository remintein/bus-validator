/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "stm32f4xx.h"
#include <string.h>
#include <stdio.h>
#include "../project_includes.h"
#include "../_configuration/cardInformation.h"
#include "CnsCardInterface.h"
#include "CnsCardInterfaceInternal.h"

#include "CnsCardInterfaceInternalWrapper.h"
#include "../sam/SamControlCommands.h"
#include "../sam/CommunicationCommands.h"

#include "../sam/phhalHw_SamAV2_Cmd.h"
#include "phalMfdf_Int.h"
static uint8_t readBuf[256];

extern phalMfdf_Sw_DataParams_t * palMFDF; /* Pointer to AL MFDF data-params */

/**
 * @file
 *
 * This file contains the communication commands with the PICC, and the read/write/commit and other instructions
 *
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

/**
 * @Brief Issue the SAM ReadX command
 *
 * This command does the ReadX command using SAM
 *
 * @param bInputCommand - the command to run (excluding data)
 * @param bCommandLength - The length of the command
 * @param expectedLength - The expected data length (0 if not known)
 * @param bppRxdata - Pointer to the pointer of data (this is set to a specific location)
 * @param pRxdataLen - Length of the data read
 */
static uint16_t intDnfccReadX(uint8_t *bInputCommand, uint8_t bCommandLength,
		uint8_t pExpectedLength, uint8_t ** ppRxdata, uint16_t * pRxdataLen) {
	uint8_t bCmdBuff[40];
	uint16_t lengthReceived;
	uint8_t wCmdLen = 0;

	bCmdBuff[wCmdLen++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	bCmdBuff[wCmdLen++] = PHHAL_HW_SAMAV2_CMD_DESFIRE_READ_X_INS;
	bCmdBuff[wCmdLen++] = 0x00; // P1
	bCmdBuff[wCmdLen++] = 0x30; // P2 (Bit4/5 = 11 : Encrypted)
	bCmdBuff[wCmdLen++] = bCommandLength + 3; // This is the length of the command + 3 bytes for expected length
// Expected length
	bCmdBuff[wCmdLen++] = pExpectedLength;
	bCmdBuff[wCmdLen++] = 0;
	bCmdBuff[wCmdLen++] = 0;

	memcpy(bCmdBuff + wCmdLen, bInputCommand, bCommandLength);
	wCmdLen += bCommandLength;

	bCmdBuff[wCmdLen++] = 0x00; // Le

	*ppRxdata = readBuf;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 1;
	}
	SamTransmitCommand(bCmdBuff, wCmdLen);
	SamReceiveEnvelope(readBuf, &lengthReceived, 10000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 2;
	}
	uint16_t result = readBuf[lengthReceived - 2] * 256
			+ readBuf[lengthReceived - 1];
	if (result != 0x9000) {
		if (result == 0x90df) {
			return readBuf[0];
		}
		return result;
	}
	*pRxdataLen = lengthReceived - 2;

	return 0;
}

/**
 * @Brief Internal Read data command
 *
 * This command reads the internal data
 *
 * @param bOption - Not used
 * @param bFileNo - The file to read
 * @param pOffset - Offset in file to read
 * @param pLength - The length of the data to read
 * @param ppRxdata - Pointer to pointer where the data is received
 * @param pRxdataLen - Pointer to uint16_t where the length of the received data is stored
 */
uint8_t CnsCardInterfaceInternalReadData(uint8_t bOption, uint8_t bFileNo,
		uint8_t * pOffset, uint8_t * pLength, uint8_t ** ppRxdata,
		uint16_t * pRxdataLen) {
	uint8_t wCmdLen = 0;
	uint8_t bCmdBuff[8];

	bCmdBuff[wCmdLen++] = PHAL_MFDF_CMD_READ_DATA;
	bCmdBuff[wCmdLen++] = bFileNo;
	memcpy(&bCmdBuff[wCmdLen], pOffset, 3);
	wCmdLen += 3U;
	memcpy(&bCmdBuff[wCmdLen], pLength, 3);
	wCmdLen += 3U;

	uint16_t wDataLen = pLength[0] + pLength[1] * 256 + pLength[2] * 256 * 256;

	return intDnfccReadX(bCmdBuff, wCmdLen, wDataLen, ppRxdata, pRxdataLen);
}

/**
 * @brief Get value from value file
 *
 * @param bCommOption - Not used
 * @param bFileNo - The file to read
 * @param pValue - Pointer to 4 bytes to store the value
 */
uint16_t CnsCardInterfaceInternalGetValue(uint8_t bCommOption, uint8_t bFileNo,
		uint8_t * pValue) {
	uint8_t wCmdLen = 0;
	uint8_t bCmdBuff[20];
	uint8_t *pRxdata;
	uint16_t pRxdataLen;

	bCmdBuff[wCmdLen++] = PHAL_MFDF_CMD_GET_VALUE;
	bCmdBuff[wCmdLen++] = bFileNo;

	uint16_t ior = intDnfccReadX(bCmdBuff, wCmdLen, 4, &pRxdata, &pRxdataLen);
	if (ior == 0) {
		memcpy(pValue, pRxdata, 4);
	}
	return ior;
}

/**
 * @Brief Internal Read records command
 *
 * This command reads records from a record file
 *
 * @param bCommOption - Not used
 * @param bFileNo - The file to read
 * @param pOffset - Offset in record to read
 * @param pNumRec - Number of records (should be set to 1)
 * @param pRecSize - The size of the record
 * @param ppRxdata - Pointer to pointer where the data is received
 * @param pRxdataLen - Pointer to uint16_t where the length of the received data is stored
 */
uint8_t CnsCardInterfaceInternalReadRecords(uint8_t bCommOption,
		uint8_t bFileNo, uint8_t * pOffset, uint8_t * pNumRec,
		uint8_t * pRecSize, uint8_t ** ppRxdata, uint16_t * pRxdataLen) {
	uint16_t wCmdLen = 0;
	uint16_t wRecLen = 0;
	uint16_t wNumRec;
	uint8_t bCmdBuff[20];

	bCmdBuff[wCmdLen++] = PHAL_MFDF_CMD_READ_RECORDS;
	bCmdBuff[wCmdLen++] = bFileNo;
	/* Offset */
	(void) memcpy(&bCmdBuff[wCmdLen], pOffset, 3);
	wCmdLen += 3U;

	/* Length */
	(void) memcpy(&bCmdBuff[wCmdLen], pNumRec, 3);
	wCmdLen += 3U;

	wRecLen = (uint16_t) pRecSize[1]; /* MSB */
	wRecLen <<= 8U;
	wRecLen |= pRecSize[0]; /* LSB */

	wNumRec = (uint16_t) pNumRec[1]; /* MSB */
	wNumRec <<= 8U;
	wNumRec |= pNumRec[0]; /* LSB */
	/* Total number of bytes to read */
	wRecLen = (uint16_t) wRecLen * wNumRec;
	return intDnfccReadX(bCmdBuff, wCmdLen, wRecLen, ppRxdata, pRxdataLen);
}

/**
 * @Brief Issue the SAM WriteX command
 *
 * This command does the writeX command using SAM
 *
 * @param bInputCommand - the command to run (excluding data)
 * @param bCommandLength - The length of the command
 * @param pData - The data to write
 * @param bDataLength - The length of the data
 */
static uint16_t intDnfccWritedX(uint8_t *bInputCommand, uint8_t bCommandLength,
		uint8_t * pData, uint16_t bDataLength) {
	uint8_t bCmdBuff[40];
	uint16_t lengthReceived;
	uint8_t wCmdLen = 0;

	bCmdBuff[wCmdLen++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	bCmdBuff[wCmdLen++] = PHHAL_HW_SAMAV2_CMD_DESFIRE_WRITE_X_INS;
	bCmdBuff[wCmdLen++] = 0x00; // P1
	bCmdBuff[wCmdLen++] = 0x30 + bCommandLength; // P2 (Bit4/5 = 11 : Encrypted)
	bCmdBuff[wCmdLen++] = bCommandLength + bDataLength;

	memcpy(bCmdBuff + wCmdLen, bInputCommand, bCommandLength);
	wCmdLen += bCommandLength;
	memcpy(bCmdBuff + wCmdLen, pData, bDataLength);
	wCmdLen += bDataLength;

	bCmdBuff[wCmdLen++] = 0x00; // Le

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 1;
	}
	SamTransmitCommand(bCmdBuff, wCmdLen);
	SamReceiveEnvelope(readBuf, &lengthReceived, 10000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 2;
	}
	uint16_t result = readBuf[lengthReceived - 2] * 256
			+ readBuf[lengthReceived - 1];
	if (result != 0x9000) {
		if (result == 0x90df) {
			return readBuf[0];
		}
		return result;
	}

	return 0;
}

/**
 * @Brief Issue the debit command
 *
 * This command does debit a file
 *
 * @param bCommOption - not used
 * @param bFileNo - the file number to debit
 * @param pValue - The amount to debit
 */
uint16_t CnsCardInterfaceInternal_Sw_Debit(uint8_t bCommOption, uint8_t bFileNo,
		uint8_t * pValue) {
	uint8_t wCmdLen = 0;
	uint8_t bCmdBuff[2];
	bCmdBuff[wCmdLen++] = PHAL_MFDF_CMD_DEBIT;
	bCmdBuff[wCmdLen++] = bFileNo;

	return intDnfccWritedX(bCmdBuff, wCmdLen, pValue, 4);
}

/**
 * @Brief Issue the credit command
 *
 * This command does credit a file
 *
 * @param bCommOption - not used
 * @param bFileNo - the file number to credit
 * @param pValue - The amount to credit
 */
uint16_t CnsCardInterfaceInternal_Sw_Credit(uint8_t bCommOption,
		uint8_t bFileNo, uint8_t * pValue) {
	uint8_t wCmdLen = 0;
	uint8_t bCmdBuff[2];
	bCmdBuff[wCmdLen++] = PHAL_MFDF_CMD_CREDIT;
	bCmdBuff[wCmdLen++] = bFileNo;

	return intDnfccWritedX(bCmdBuff, wCmdLen, pValue, 4);
}

/**
 * @brief Write data
 *
 * This command writes data to the PICC
 *
 * @param bCommOption - not used
 * @param bFileNo - File to write
 * @param pOffset - offset in file to write
 * @param pData - pointer to data to write
 * @param pDataLen - Length of data to write
 */
uint8_t CnsCardInterfaceInternalWriteData(uint8_t bCommOption, uint8_t bFileNo,
		uint8_t * pOffset, uint8_t * pData, uint8_t * pDataLen) {
	uint8_t wCmdLen = 0;
	uint8_t bCmdBuff[256];
	uint16_t wDataLen = pDataLen[0] + pDataLen[1] * 256
			+ pDataLen[2] * 256 * 256;

	bCmdBuff[wCmdLen++] = PHAL_MFDF_CMD_WRITE_DATA;
	bCmdBuff[wCmdLen++] = bFileNo;
	(void) memcpy(&bCmdBuff[wCmdLen], pOffset, 3);
	wCmdLen += 3U;
	(void) memcpy(&bCmdBuff[wCmdLen], pDataLen, 3);
	wCmdLen += 3U;

	return intDnfccWritedX(bCmdBuff, wCmdLen, pData, wDataLen);
}

/**
 * @brief Write record
 *
 * This command writes a record to the PICC
 *
 * @param bCommOption - not used
 * @param bFileNo - File to write
 * @param pOffset - offset in file to write
 * @param pData - pointer to data to write
 * @param pDataLen - Length of data to write
 */
uint8_t CnsCardInterfaceInternalWriteRecord(uint8_t bCommOption,
		uint8_t bFileNo, uint8_t * pOffset, uint8_t * pData, uint8_t * pDataLen) {
	uint8_t wCmdLen = 0;
	uint8_t bCmdBuff[256];
	uint16_t wDataLen = 0;

	bCmdBuff[wCmdLen++] = PHAL_MFDF_CMD_WRITE_RECORD;
	bCmdBuff[wCmdLen++] = bFileNo;
	(void) memcpy(&bCmdBuff[wCmdLen], pOffset, 3);
	wCmdLen += 3U;
	(void) memcpy(&bCmdBuff[wCmdLen], pDataLen, 3);
	wCmdLen += 3U;

	/* Assuming here that the size can never go beyond FFFF.
	 In fact it can never go beyond 8092 (1F9C) bytes */
	wDataLen = (uint16_t) pDataLen[1];
	wDataLen = wDataLen << 8U;
	wDataLen |= pDataLen[0];

	return intDnfccWritedX(bCmdBuff, wCmdLen, pData, wDataLen);
}

/**
 * @brief Commit transaction
 *
 * This command commits a transaction on the PICC
 *
 */
uint16_t CnsCardInterfaceInternal_Sw_CommitTransaction() {
	uint8_t wCmdLen = 0;
	uint8_t bCmdBuff[2];
	uint16_t dataLen;
	bCmdBuff[wCmdLen++] = PHAL_MFDF_CMD_COMMIT_TXN;
	uint16_t ior = DnfccSamIso14443_4Exchange(bCmdBuff, wCmdLen, readBuf,
			&dataLen);
	if (ior != 0x9000) {
		return 1;
	}

	return readBuf[0];
}

/**
 * @brief Select application
 *
 * This command selects an application on the PICC
 *
 * @param pAppId - Pointer to 3 bytes of application number
 */
uint16_t CnsCardInterfaceInternal_Sw_SelectApplication(uint8_t * pAppId) {
	uint16_t ior;
	uint8_t aidBytes[5];
	uint16_t responselength;
	aidBytes[0] = PHAL_MFDF_CMD_SELECT_APPLN;
	memcpy(aidBytes + 1, pAppId, 3);
	ior = DnfccSamIso14443_4Exchange(aidBytes, 4, readBuf, &responselength);
	if (ior != 0x9000) {
		return 1;
	}
	if (responselength != 1) {
		return 2;
	}
	return readBuf[0];
}

/**
 * @brief Format the PICC
 */
uint16_t CnsCardInterfaceInternal_Sw_FormatPICC() {
	uint16_t ior;
	uint8_t aidBytes[5];
	uint16_t responselength;
	aidBytes[0] = PHAL_MFDF_CMD_FORMAT_PICC;

	ior = DnfccSamIso14443_4Exchange(aidBytes, 1, readBuf, &responselength);
	if (ior != 0x9000) {
		return 1;
	}

	return readBuf[0];
}

/**
 * @brief PErform a IsoAuthenticate
 * @param wKeyNo - the key to authenticate with
 * @param wKeyVer - the version of the key
 * @param bKeyNoCard - the key in the card
 */
phStatus_t CnsCardInterfaceSam_Sw_IsoAuthenticate(uint16_t wKeyNo,
		uint16_t wKeyVer, uint8_t bKeyNoCard) {
	/**
	 * The key type can be DES, 3DES, 3K3DES.
	 * Random numbers can be 8 or 16 bytes long
	 * Init vector can be 8 or 16 bytes long
	 * Session key max size is 24 bytes if 3k3DES keys are used.
	 *
	 */

	uint8_t PH_MEMLOC_REM bRndA[PH_CRYPTOSYM_AES_BLOCK_SIZE];
	uint8_t PH_MEMLOC_REM bRndB[PH_CRYPTOSYM_AES_BLOCK_SIZE + 1U];
	phStatus_t PH_MEMLOC_REM statusTmp;
	uint8_t PH_MEMLOC_REM bKey[PH_CRYPTOSYM_3K3DES_KEY_SIZE];
	uint16_t PH_MEMLOC_REM wKeyType;
	uint16_t PH_MEMLOC_REM wRxlen = 0;
	uint8_t PH_MEMLOC_REM bCmdBuff[33];
	uint8_t PH_MEMLOC_REM bWorkBuffer[PH_CRYPTOSYM_AES_BLOCK_SIZE + 1U];
	uint16_t PH_MEMLOC_REM status;
	uint16_t PH_MEMLOC_REM wCmdLen = 0;
	uint8_t PH_MEMLOC_REM bRndLen;
	uint8_t PH_MEMLOC_REM bIvSize;
	uint8_t PH_MEMLOC_REM bIv_bak[PH_CRYPTOSYM_DES_BLOCK_SIZE];
//	uint8_t * PH_MEMLOC_REM pRecv = NULL;
	uint8_t respBuff[100];

	if (bKeyNoCard > 0x0dU) {
		return PH_ADD_COMPCODE_FIXED(PH_ERR_INVALID_PARAMETER, PH_COMP_AL_MFDF);
	}

	/* Get Key out of the key store object */
	memset(bKey, 0x00, PH_CRYPTOSYM_3K3DES_KEY_SIZE);
	wKeyType = PH_KEYSTORE_KEY_TYPE_2K3DES;

	switch (wKeyType) {
	case PH_KEYSTORE_KEY_TYPE_DES:
		bRndLen = PH_CRYPTOSYM_DES_BLOCK_SIZE;
		bIvSize = PH_CRYPTOSYM_DES_BLOCK_SIZE;
		(void) memcpy(&bKey[8], bKey, 8);
		wKeyType = PH_KEYSTORE_KEY_TYPE_2K3DES;
		break;

	case PH_KEYSTORE_KEY_TYPE_2K3DES:
		bRndLen = PH_CRYPTOSYM_DES_BLOCK_SIZE;
		bIvSize = PH_CRYPTOSYM_DES_BLOCK_SIZE;
		break;

	case PH_KEYSTORE_KEY_TYPE_3K3DES:
		bRndLen = 2u * PH_CRYPTOSYM_DES_BLOCK_SIZE;
		bIvSize = PH_CRYPTOSYM_DES_BLOCK_SIZE;
		break;

	default:
		/* Wrong key type specified. Auth. will not work */
		return PH_ADD_COMPCODE_FIXED(PH_ERR_KEY, PH_COMP_AL_MFDF);
	}

	/* load key */
	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoSym_LoadKeyDirect(palMFDF->pCryptoDataParamsEnc, bKey,
					wKeyType));

	/* Initialize the init vector to all zeors */
	(void) memset(bIv_bak, 0x00, bIvSize);

	/* Send the cmd and receive the encrypted RndB */
	bCmdBuff[wCmdLen++] = PHAL_MFDF_CMD_AUTHENTICATE_ISO;
	bCmdBuff[wCmdLen++] = bKeyNoCard; /* key number card */

	status = DnfccSamIso14443_4Exchange(bCmdBuff, wCmdLen, respBuff, &wRxlen);

	if (status != 0x9000 || respBuff[0] != 0xaf) {
		return PH_ADD_COMPCODE_FIXED(PH_ERR_PROTOCOL_ERROR, PH_COMP_AL_MFDF);
	}

	if (wRxlen - 1 != bRndLen) {
		return PH_ADD_COMPCODE_FIXED(PH_ERR_PROTOCOL_ERROR, PH_COMP_AL_MFDF);
	}

	/* Store the unencrypted RndB */
	(void) memcpy(bRndB, respBuff + 1, bRndLen);

	/* Store the IV to be used for encryption later */
	(void) memcpy(bIv_bak, &bRndB[bRndLen - bIvSize], bIvSize);

	/* Reset IV for the first crypto operation */
	(void) memset(palMFDF->bIv, 0x00, bIvSize);

	/* Load Iv.*/
	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoSym_LoadIv(palMFDF->pCryptoDataParamsEnc, palMFDF->bIv,
					bIvSize));

	/* Decrypt the RndB received from PICC */
	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoSym_Decrypt( palMFDF->pCryptoDataParamsEnc, PH_CRYPTOSYM_CIPHER_MODE_CBC | PH_EXCHANGE_BUFFER_CONT, bRndB, bRndLen, bRndB ));

	/* Generate RndA */
	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoRng_Seed(palMFDF->pCryptoRngDataParams, bRndB, bRndLen));

	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoRng_Rnd(palMFDF->pCryptoRngDataParams, bRndLen, bRndA));

	/* Concat RndA and RndB' */
	bCmdBuff[0] = PHAL_MFDF_RESP_ADDITIONAL_FRAME;
	(void) memcpy(&bCmdBuff[1], bRndA, bRndLen);
	(void) memcpy(&bCmdBuff[bRndLen + 1U], &bRndB[1], bRndLen - 1);
	bCmdBuff[2U * bRndLen] = bRndB[0]; /* RndB left shifted by 8 bits */

	(void) memcpy(palMFDF->bIv, bIv_bak, bIvSize);

	/* Load Iv */
	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoSym_LoadIv(palMFDF->pCryptoDataParamsEnc, palMFDF->bIv,
					bIvSize));

	/* Encrypt RndA + RndB' */
	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoSym_Encrypt(palMFDF->pCryptoDataParamsEnc, PH_CRYPTOSYM_CIPHER_MODE_CBC | PH_EXCHANGE_BUFFER_CONT, &bCmdBuff[1], 2U * bRndLen, &bCmdBuff[1] ));

	/* Update command length */
	wCmdLen = (2u * bRndLen) + 1U;

	/* Update Iv */
	(void) memcpy(palMFDF->bIv, &bCmdBuff[wCmdLen - bIvSize], bIvSize);

	/* Get the encrypted RndA' into bWorkBuffer */
	status = DnfccSamIso14443_4Exchange(bCmdBuff, wCmdLen, respBuff, &wRxlen);

//	PH_CHECK_SUCCESS_FCT(status,
//			phalMfdf_ExchangeCmd(palMFDF, palMFDF->pPalMifareDataParams,
//					palMFDF->bWrappedMode, bCmdBuff, wCmdLen, &pRecv,
//					&wRxlen));
	if (wRxlen - 1 != bRndLen) {
		return PH_ADD_COMPCODE_FIXED(PH_ERR_PROTOCOL_ERROR, PH_COMP_AL_MFDF);
	}

	(void) memcpy(bWorkBuffer, respBuff + 1, wRxlen - 1);

	/* bWorkBuffer now has the encrypted RndA' */
	/* Decrypt the received RndA' */
	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoSym_LoadIv(palMFDF->pCryptoDataParamsEnc, palMFDF->bIv,
					bIvSize));

	/* Decrypt RndA'*/
	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoSym_Decrypt( palMFDF->pCryptoDataParamsEnc, PH_CRYPTOSYM_CIPHER_MODE_CBC | PH_EXCHANGE_BUFFER_CONT, bWorkBuffer, bRndLen, &bCmdBuff[1] ));

	/* Using bCmdBuff for storage of decrypted RndA */
	bCmdBuff[0] = bCmdBuff[bRndLen]; /* right shift to get back RndA */

	/* Compare RndA and buff */
	if (memcmp(bCmdBuff, bRndA, bRndLen) != 0) {
		/* Authentication failed */
		return PH_ADD_COMPCODE_FIXED(PH_ERR_AUTH_ERROR, PH_COMP_AL_MFDF);
	}

	/* Generate the session key */
	/*
	 DES - 8 byte
	 2K3DES - 16 bytes
	 3K3DES - 24 bytes session key
	 */
	(void) memcpy(palMFDF->bSessionKey, bRndA, 4);
	(void) memcpy(&palMFDF->bSessionKey[4], bRndB, 4);
	palMFDF->bCryptoMethod = PH_CRYPTOSYM_KEY_TYPE_DES;

	/*
	 If first half of bKey is same as the second half it is a single
	 DES Key.
	 the session key generated is different.
	 RndA 1st half + Rnd b 1st half + RndA1st half + RndB 1st half
	 */

	if (wKeyType == PH_KEYSTORE_KEY_TYPE_2K3DES) {
		if (memcmp(bKey, &bKey[PH_CRYPTOSYM_DES_KEY_SIZE],
		PH_CRYPTOSYM_DES_KEY_SIZE) == 0) {
			(void) memcpy(&palMFDF->bSessionKey[8], bRndA, 4);
			(void) memcpy(&palMFDF->bSessionKey[12], bRndB, 4);
		} else {
			(void) memcpy(&palMFDF->bSessionKey[8], &bRndA[4], 4);
			(void) memcpy(&palMFDF->bSessionKey[12], &bRndB[4], 4);
		}
		palMFDF->bCryptoMethod = PH_CRYPTOSYM_KEY_TYPE_2K3DES;
	}
	if (wKeyType == PH_KEYSTORE_KEY_TYPE_3K3DES) {
		(void) memcpy(&palMFDF->bSessionKey[8], &bRndA[6], 4);
		(void) memcpy(&palMFDF->bSessionKey[12], &bRndB[6], 4);

		(void) memcpy(&palMFDF->bSessionKey[16], &bRndA[12], 4);
		(void) memcpy(&palMFDF->bSessionKey[20], &bRndB[12], 4);
		palMFDF->bCryptoMethod = PH_CRYPTOSYM_KEY_TYPE_3K3DES;
	}

	/* Session key is generated */
	palMFDF->bAuthMode = PHAL_MFDF_AUTHENTICATEISO;
	palMFDF->bKeyNo = bKeyNoCard;

	/* IV is reset to zero as per the impl. hints document */
	(void) memset(palMFDF->bIv, 0x00, (size_t) sizeof(palMFDF->bIv));

	/* Load the Session key which is valid for this authentication */
	PH_CHECK_SUCCESS_FCT(statusTmp,
			phCryptoSym_LoadKeyDirect(palMFDF->pCryptoDataParamsEnc,
					palMFDF->bSessionKey, palMFDF->bCryptoMethod));

	/* Need to set the IV on */
	return phCryptoSym_SetConfig(palMFDF->pCryptoDataParamsEnc,
	PH_CRYPTOSYM_CONFIG_KEEP_IV,
	PH_CRYPTOSYM_VALUE_KEEP_IV_ON);
}

/**
 * @brief Create a normal file on the card
 * @param fileno - the number of the file
 * @param fileType - the type of the file
 * @param comset - the communication settings
 * @param accessRights - the access right
 * @param filesize - the size of the file
 * @return the return value of the PICC (or 1 if the SAM does not work)
 */
uint16_t CnsCardInterfaceInternal_Sw_CreateNormalFile(uint8_t fileno,
		uint8_t fileType, uint8_t comset, uint16_t accessRights,
		uint16_t filesize) {

	uint16_t ior;
	uint8_t aidBytes[20];
	uint8_t comLen = 0;
	uint16_t responselength;
	switch (fileType) {
	case DATA_FILE:
		aidBytes[comLen++] = PHAL_MFDF_CMD_CREATE_STD_DATAFILE;
		break;
	case BACKUP_DATA_FILE:
		aidBytes[comLen++] = PHAL_MFDF_CMD_CREATE_BKUP_DATAFILE;
		break;
	default:
		return 0xffff;
	}
	aidBytes[comLen++] = fileno;
	aidBytes[comLen++] = comset;
	aidBytes[comLen++] = accessRights >> 8;
	aidBytes[comLen++] = accessRights & 0xff;
	aidBytes[comLen++] = filesize & 0xff;
	aidBytes[comLen++] = (filesize >> 8) & 0xff;
	aidBytes[comLen++] = 0;

	ior = DnfccSamIso14443_4Exchange(aidBytes, comLen, readBuf,
			&responselength);
	if (ior != 0x9000) {
		return 1;
	}

	return readBuf[0];
}

/**
 * @brief Create a value file on the card
 * @param fileno - the number of the file
 * @param fileType - the type of the file
 * @param comset - the communication settings
 * @param accessRights - the access right
 * @param low - the lowest possible value
 * @param high - the highest possible value
 * @param start - the initial value
 * @param limited - whether limited change is set
 * @return the return value of the PICC (or 1 if the SAM does not work)
 */
uint16_t CnsCardInterfaceInternal_Sw_CreateValueFile(uint8_t fileno,
		uint8_t fileType, uint8_t comset, uint16_t accessRights, uint32_t low,
		uint32_t high, uint32_t start, uint8_t limited) {

	uint16_t ior;
	uint8_t aidBytes[20];
	uint8_t comLen = 0;
	uint16_t responselength;
	switch (fileType) {
	case VALUE_FILE:
		aidBytes[comLen++] = PHAL_MFDF_CMD_CREATE_VALUE_FILE;
		break;
	default:
		return 0xffff;
	}
	aidBytes[comLen++] = fileno;
	aidBytes[comLen++] = comset;
	aidBytes[comLen++] = accessRights >> 8;
	aidBytes[comLen++] = accessRights & 0xff;

	aidBytes[comLen++] = low & 0xff;
	aidBytes[comLen++] = (low >> 8) & 0xff;
	aidBytes[comLen++] = (low >> 16) & 0xff;
	aidBytes[comLen++] = (low >> 24) & 0xff;

	aidBytes[comLen++] = high & 0xff;
	aidBytes[comLen++] = (high >> 8) & 0xff;
	aidBytes[comLen++] = (high >> 16) & 0xff;
	aidBytes[comLen++] = (high >> 24) & 0xff;

	aidBytes[comLen++] = start & 0xff;
	aidBytes[comLen++] = (start >> 8) & 0xff;
	aidBytes[comLen++] = (start >> 16) & 0xff;
	aidBytes[comLen++] = (start >> 24) & 0xff;

	aidBytes[comLen++] = limited;

	ior = DnfccSamIso14443_4Exchange(aidBytes, comLen, readBuf,
			&responselength);
	if (ior != 0x9000) {
		return 1;
	}

	return readBuf[0];
}

/**
 * @brief Create a records file on the card
 * @param fileno - the number of the file
 * @param fileType - the type of the file
 * @param comset - the communication settings
 * @param accessRights - the access right
 * @param recordsize - the size of a record in the file
 * @param numRecords - the amount of records
 * @return the return value of the PICC (or 1 if the SAM does not work)
 */
uint16_t CnsCardInterfaceInternal_Sw_CreateRecordsFile(uint8_t fileno,
		uint8_t fileType, uint8_t comset, uint16_t accessRights,
		uint16_t recordSize, uint16_t numRecords) {

	uint16_t ior;
	uint8_t aidBytes[20];
	uint8_t comLen = 0;
	uint16_t responselength;
	switch (fileType) {
	case CYCLIC_FILE:
		aidBytes[comLen++] = PHAL_MFDF_CMD_CREATE_CYCLIC_RECFILE;
		break;
	default:
		return 0xffff;
	}
	aidBytes[comLen++] = fileno;
	aidBytes[comLen++] = comset;
	aidBytes[comLen++] = accessRights >> 8;
	aidBytes[comLen++] = accessRights & 0xff;
	aidBytes[comLen++] = recordSize & 0xff;
	aidBytes[comLen++] = (recordSize >> 8) & 0xff;
	aidBytes[comLen++] = 0;
	aidBytes[comLen++] = numRecords & 0xff;
	aidBytes[comLen++] = (numRecords >> 8) & 0xff;
	aidBytes[comLen++] = 0;

	ior = DnfccSamIso14443_4Exchange(aidBytes, comLen, readBuf,
			&responselength);
	if (ior != 0x9000) {
		return 1;
	}

	return readBuf[0];
}

/**
 * @brief Create an application on the card
 * @param pAid - the application id
 * @param bKeySettings1 - the keysettings 1
 * @param bKeySettings2 - the keysettings 2
 * @return the return value of the PICC (or 1 if the SAM does not work)
 */
uint16_t CnsCardInterfaceInternal_Sw_CreateApplication(uint8_t *pAid,
		uint8_t bKeySettings1, uint8_t bKeySettings2) {
	uint16_t ior;
	uint8_t aidBytes[7];
	uint16_t responselength;
	aidBytes[0] = PHAL_MFDF_CMD_CREATE_APPLN;
	aidBytes[1] = pAid[0];
	aidBytes[2] = pAid[1];
	aidBytes[3] = pAid[2];
	aidBytes[4] = bKeySettings1;
	aidBytes[5] = bKeySettings2;

	ior = DnfccSamIso14443_4Exchange(aidBytes, 6, readBuf, &responselength);
	if (ior != 0x9000) {
		return 1;
	}

	return readBuf[0];
}

/** @} */
