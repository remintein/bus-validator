/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include <string.h>
#include "../project_includes.h"
#include "../_configuration/cardInformation.h"

/**
 * @file
 * @brief Contains file functions for the card interface
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

#define COPY_FROM_STRUCT(STRU, DATA) memcpy(&DATA[pos], &STRU, sizeof(STRU)); pos += sizeof(STRU);
#define COPY_TO_STRUCT(STRU, DATA)   memcpy(&STRU, &DATA[pos], sizeof(STRU));pos += sizeof(STRU);

/**
 * @brief copy file 1 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToCardInformation(fileCardInformationStruct *out, uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->fileFormatVersion, data);
	COPY_TO_STRUCT(out->originalCard_ID, data);
	COPY_TO_STRUCT(out->recycleCard_ID, data);
	COPY_TO_STRUCT(out->cardVersion, data);
	COPY_TO_STRUCT(out->cardCompany, data);
	COPY_TO_STRUCT(out->cardFormatAmount, data);
	COPY_TO_STRUCT(out->depositAmount, data);
	COPY_TO_STRUCT(out->chipType, data);
	COPY_TO_STRUCT(out->isPerso, data);
	COPY_TO_STRUCT(out->cardIssueDate, data);
	COPY_TO_STRUCT(out->cardExpiredDate, data);
	COPY_TO_STRUCT(out->loginType, data);
	COPY_TO_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 1 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyCardInformationToStream(uint8_t *data, fileCardInformationStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->fileFormatVersion, data);
	COPY_FROM_STRUCT(out->originalCard_ID, data);
	COPY_FROM_STRUCT(out->recycleCard_ID, data);
	COPY_FROM_STRUCT(out->cardVersion, data);
	COPY_FROM_STRUCT(out->cardCompany, data);
	COPY_FROM_STRUCT(out->cardFormatAmount, data);
	COPY_FROM_STRUCT(out->depositAmount, data);
	COPY_FROM_STRUCT(out->chipType, data);
	COPY_FROM_STRUCT(out->isPerso, data);
	COPY_FROM_STRUCT(out->cardIssueDate, data);
	COPY_FROM_STRUCT(out->cardExpiredDate, data);
	COPY_FROM_STRUCT(out->loginType, data);
	COPY_FROM_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 2 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToCardConfiguration(fileCardConfigurationStruct *out,
		uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->fileFormatVersion, data);
	COPY_TO_STRUCT(out->isActive, data);
	COPY_TO_STRUCT(out->isPurseEnable, data);
	COPY_TO_STRUCT(out->topUpEnable, data);
	COPY_TO_STRUCT(out->isAutoLoadEnable, data);
	COPY_TO_STRUCT(out->isReturnEnable, data);
	COPY_TO_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 2 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyCardConfigurationToStream(uint8_t *data,
		fileCardConfigurationStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->fileFormatVersion, data);
	COPY_FROM_STRUCT(out->isActive, data);
	COPY_FROM_STRUCT(out->isPurseEnable, data);
	COPY_FROM_STRUCT(out->topUpEnable, data);
	COPY_FROM_STRUCT(out->isAutoLoadEnable, data);
	COPY_FROM_STRUCT(out->isReturnEnable, data);
	COPY_FROM_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 3 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToCardStatus(fileCardStatusStruct *out, uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->fileFormatVersion, data);
	COPY_TO_STRUCT(out->cardStatus, data);
	COPY_TO_STRUCT(out->lockDate, data);
	COPY_TO_STRUCT(out->unLockDate, data);
	COPY_TO_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 3 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyCardStatusToStream(uint8_t *data, fileCardStatusStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->fileFormatVersion, data);
	COPY_FROM_STRUCT(out->cardStatus, data);
	COPY_FROM_STRUCT(out->lockDate, data);
	COPY_FROM_STRUCT(out->unLockDate, data);
	COPY_FROM_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 4 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToCardHolderInformation(fileCardHolderInformationStruct *out,
		uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->fileFormatVersion, data);
	COPY_TO_STRUCT(out->cardHolder_ID, data);
	COPY_TO_STRUCT(out->fullName, data);
	COPY_TO_STRUCT(out->accountType, data);
	COPY_TO_STRUCT(out->gender, data);
	COPY_TO_STRUCT(out->DOB, data);
	COPY_TO_STRUCT(out->orgCode, data);
	COPY_TO_STRUCT(out->orgName, data);
	COPY_TO_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 4 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyCardHolderInformationsStructToStream(uint8_t *data,
		fileCardHolderInformationStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->fileFormatVersion, data);
	COPY_FROM_STRUCT(out->cardHolder_ID, data);
	COPY_FROM_STRUCT(out->fullName, data);
	COPY_FROM_STRUCT(out->accountType, data);
	COPY_FROM_STRUCT(out->gender, data);
	COPY_FROM_STRUCT(out->DOB, data);
	COPY_FROM_STRUCT(out->orgCode, data);
	COPY_FROM_STRUCT(out->orgName, data);
	COPY_FROM_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 5 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToTicketInformation(fileTicketInformationStruct *out,
		uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->fileFormatVersion, data);
	COPY_TO_STRUCT(out->ticket_ID, data);
	COPY_TO_STRUCT(out->ticketType, data);
	COPY_TO_STRUCT(out->ticketEffectiveDate, data);
	COPY_TO_STRUCT(out->ticketExpireDays, data);
	COPY_TO_STRUCT(out->isRefundEnable, data);
	COPY_TO_STRUCT(out->passengerType, data);
	COPY_TO_STRUCT(out->signature, data);
	COPY_TO_STRUCT(out->ticketStatus, data);
	COPY_TO_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 5 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyTicketInformationsToStream(uint8_t *data,
		fileTicketInformationStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->fileFormatVersion, data);
	COPY_FROM_STRUCT(out->ticket_ID, data);
	COPY_FROM_STRUCT(out->ticketType, data);
	COPY_FROM_STRUCT(out->ticketEffectiveDate, data);
	COPY_FROM_STRUCT(out->ticketExpireDays, data);
	COPY_FROM_STRUCT(out->isRefundEnable, data);
	COPY_FROM_STRUCT(out->passengerType, data);
	COPY_FROM_STRUCT(out->signature, data);
	COPY_FROM_STRUCT(out->ticketStatus, data);
	COPY_FROM_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 6 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToTicketValue(fileTicketValueStruct *out, uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->ticketValue, data);
}

/**
 * @brief copy file 6 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyTicketValueToStream(uint8_t *data, fileTicketValueStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->ticketValue, data);
}

/**
 * @brief copy file 7 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToTicketLog(fileTicketLogStruct *out, uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->fileFormatVersion, data);
	COPY_TO_STRUCT(out->ticketTransactionsSubType, data);
	COPY_TO_STRUCT(out->ticketTransactionDateTime, data);
	COPY_TO_STRUCT(out->ticketBeforeCount, data);
	COPY_TO_STRUCT(out->ticketUsedCount, data);
	COPY_TO_STRUCT(out->ticketValue, data);
	COPY_TO_STRUCT(out->ticketSAM_ID, data);
	COPY_TO_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 7 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyTicketLogToStream(uint8_t *data, fileTicketLogStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->fileFormatVersion, data);
	COPY_FROM_STRUCT(out->ticketTransactionsSubType, data);
	COPY_FROM_STRUCT(out->ticketTransactionDateTime, data);
	COPY_FROM_STRUCT(out->ticketBeforeCount, data);
	COPY_FROM_STRUCT(out->ticketUsedCount, data);
	COPY_FROM_STRUCT(out->ticketValue, data);
	COPY_FROM_STRUCT(out->ticketSAM_ID, data);
	COPY_FROM_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 8 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToPurseInformation(filePurseInformationStruct *out,
		uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->fileFormatVersion, data);
	COPY_TO_STRUCT(out->purse_ID, data);
	COPY_TO_STRUCT(out->purseType, data);
	COPY_TO_STRUCT(out->purseEffectiveDate, data);
	COPY_TO_STRUCT(out->purseExpiredDate, data);
	COPY_TO_STRUCT(out->purseStatus, data);
	COPY_TO_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 8 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyPurseInformationsToStream(uint8_t *data,
		filePurseInformationStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->fileFormatVersion, data);
	COPY_FROM_STRUCT(out->purse_ID, data);
	COPY_FROM_STRUCT(out->purseType, data);
	COPY_FROM_STRUCT(out->purseEffectiveDate, data);
	COPY_FROM_STRUCT(out->purseExpiredDate, data);
	COPY_FROM_STRUCT(out->purseStatus, data);
	COPY_FROM_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 9 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToPurseValue(filePurseValueStruct *out, uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->purseValue, data);
}

/**
 * @brief copy file 9 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyPurseValueToStream(uint8_t *data, filePurseValueStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->purseValue, data);
}

/**
 * @brief copy file 10 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToPurseLog(filePurseLogStruct *out, uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->fileFormatVersion, data);
	COPY_TO_STRUCT(out->purseTransactionsSubType, data);
	COPY_TO_STRUCT(out->purseTransactionDateTime, data);
	COPY_TO_STRUCT(out->purseBeforeCount, data);
	COPY_TO_STRUCT(out->purseUsedCount, data);
	COPY_TO_STRUCT(out->purseValue, data);
	COPY_TO_STRUCT(out->purseSAM_ID, data);
	COPY_TO_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 10 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyPurseLogToStream(uint8_t *data, filePurseLogStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->fileFormatVersion, data);
	COPY_FROM_STRUCT(out->purseTransactionsSubType, data);
	COPY_FROM_STRUCT(out->purseTransactionDateTime, data);
	COPY_FROM_STRUCT(out->purseBeforeCount, data);
	COPY_FROM_STRUCT(out->purseUsedCount, data);
	COPY_FROM_STRUCT(out->purseValue, data);
	COPY_FROM_STRUCT(out->purseSAM_ID, data);
	COPY_FROM_STRUCT(out->futureUse, data);
}

/**
 * @brief copy file 11 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToTicketTransactionCount(fileTicketTransactionCountStruct *out,
		uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->ticketTransactionCount, data);
}

/**
 * @brief copy file 11 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyTicketTransactionCountToStream(uint8_t *data,
		fileTicketTransactionCountStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->ticketTransactionCount, data);
}

/**
 * @brief copy file 12 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToPurseTransactionCount(filePurseTransactionCountStruct *out,
		uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->purseTransactionCount, data);
}

/**
 * @brief copy file 12 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyPurseTransactionCountToStream(uint8_t *data,
		filePurseTransactionCountStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->purseTransactionCount, data);
}

/**
 * @brief copy file 13 from stream to struct
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyStreamToCardTransactionCount(fileCardTransactionCountStruct *out,
		uint8_t *data) {
	int pos = 0;
	COPY_TO_STRUCT(out->cardTransactionCount, data);
}

/**
 * @brief copy file 13 struct to stream
 * @param out - Struct with the fields
 * @param data - input data
 */
void CopyCardTransactionCountToStream(uint8_t *data,
		fileCardTransactionCountStruct *out) {
	int pos = 0;
	COPY_FROM_STRUCT(out->cardTransactionCount, data);
}

/** @} */
