/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_CNSCARDINTERFACE_H_
#define SMARTCARDIF_CNSCARDINTERFACE_H_

#define CNS_CARDINTERFACE_MAXFILES 13

#include "../_configuration/cardInformation.h"

// Typedef's used
typedef uint8_t (*SendReceiveFunction)(void *handel, uint8_t *command,
		uint16_t commandLength, uint8_t *response, uint16_t *pResponseLength);

typedef struct {
	uint8_t isInitialized;
	uint32_t curApplication;
	uint32_t curApplicationKey;
	uint8_t isCardInitialized;
	uint32_t removedTicket;
	uint32_t removedPurse;
	char reason[40];
	char previousReason[40];
	uint8_t fileIsRead[CNS_CARDINTERFACE_MAXFILES];
	uint8_t file1[SIZE_CARD_INFORMATION];
	uint8_t file2[SIZE_CARD_CONFIGURATION];
	uint8_t file3[SIZE_CARD_STATUS];
	uint8_t file4[SIZE_CARD_HOLDER_INFORMATION];
	uint8_t file5[SIZE_TICKET_INFORMATION];
	int32_t file6;
	uint8_t file7[SIZE_TICKET_LOG];
	uint8_t file8[SIZE_PURSE_INFORMATION];
	int32_t file9;
	uint8_t file10[SIZE_PURSE_LOG];
	int32_t file11;
	int32_t file12;
	int32_t file13;
	uint8_t cardUid[7];

	//Addition for Delivery

	uint8_t DFBooking_Log[240];
} CnsCardInterfaceInfo;

typedef struct {
	char name[71];
} CnsCardInterfaceUserInformation;

// Return types
#define CNS_CARDINTERFACE_RETURN_OK  0
#define CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED 1
#define CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS 2
#define CNS_CARDINTERFACE_RETURN_CARD_NOT_ACTIVE 3
#define CNS_CARDINTERFACE_INSUFFICIENT_BALANCE 4
#define CNS_CARDINTERFACE_INTERNAL_ERROR 5
#define CNS_CARDINTERFACE_TICKET_TYPE_ERROR 6
#define CNS_CARDINTERFACE_CARD_STATUS_ERROR 7
#define CNS_CARDINTERFACE_RETURN_CARD_IS_EXPIRED 8
#define CNS_CARDINTERFACE_RETURN_TICKET_IS_EXPIRED 9
#define CNS_CARDINTERFACE_RETURN_TICKET_NOT_ACTIVE 10
#define CNS_CARDINTERFACE_TICKET_STATUS_ERROR 11
#define CNS_CARDINTERFACE_TICKET_LOW_BALANCE 12
#define CNS_CARDINTERFACE_RETURN_PURSE_IS_EXPIRED 13
#define CNS_CARDINTERFACE_PURSE_STATUS_ERROR 14
#define CNS_CARDINTERFACE_PURSE_INSUFFICIENT_BALANCE 15
#define CNS_CARDINTERFACE_RETURN_PURSE_BALANCE_ERROR 16
#define CNS_CARDINTERFACE_RETURN_PURSE_EFFECTIVE_DATE_ERROR 17
#define CNS_CARDINTERFACE_PURSE_LOW_BALANCE 18
#define CNS_CARDINTERFACE_RETURN_AUTHENTICATION_ERROR 19

// Function prototypes
uint8_t CnsCardInterfaceInitialize(CnsCardInterfaceInfo *apiPointer);

uint8_t CnsCardInterfaceStartCard(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint8_t *cardId);

uint8_t CnsCardInterfaceGetUserInformation(CnsCardInterfaceInfo *apiPointer,
		void *handel, CnsCardInterfaceUserInformation *information);

uint8_t CnsCardInterfaceCardIsEnabled(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date);

uint8_t CnsCardInterfaceGetPurseBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, int32_t *value);

uint8_t CnsCardInterfaceDebitPurseBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t value);

uint8_t CnsCardInterfaceGetTicketBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, int32_t *value);

uint8_t CnsCardInterfaceDebitTicketBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t amount);

uint8_t CnsCardInterfaceTicketTransactionCount(CnsCardInterfaceInfo *apiPointer,
		void *handel, int32_t amount);

uint8_t CnsCardInterfaceCardExpireDate(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date);

uint8_t CnsCardInterfacePurseExpire(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date);

uint8_t CnsCardInterfaceTicketExpire(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date);

uint8_t CnsCardInterfaceGetPurseEffectiveDate(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date);

uint8_t CnsCardInterfaceCardStatus(CnsCardInterfaceInfo *apiPointer,
		void *handel, char cardstatus);

uint8_t CnsCardInterfaceSetCardFromPtoA(CnsCardInterfaceInfo *apiPointer,
		void *handel);

uint8_t CnsCardInterfaceCardIsCardNotExpired(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date);

uint8_t CnsCardInterfaceTicketIsEnabled(CnsCardInterfaceInfo *apiPointer, // 24-07-2017 TONY&KHOA
		void *handel, char *date);

uint8_t CnsCardInterfaceTicketType(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint8_t tickettype);

uint8_t CnsCardInterfacePerformPurseLogging(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *datetime, uint16_t busId, uint16_t deviceId,
		uint16_t routeId);

uint8_t CnsCardInterfaceTicketStatus(CnsCardInterfaceInfo *apiPointer,
		void *handel, char ticketstatus);

uint8_t CnsCardInterfacePurseStatus(CnsCardInterfaceInfo *apiPointer,
		void *handel, char pursestatus);

uint8_t CnsCardInterfaceCheckLowTicketBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t belowIsLow);

uint8_t CnsCardInterfaceCheckLowPurseBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t belowIsLow);

uint8_t CnsCardInterfaceTicketTransaction(CnsCardInterfaceInfo *apiPointer,
		void *handel);

uint8_t CnsCardInterfacePurseTransaction(CnsCardInterfaceInfo *apiPointer,
		void *handel);

uint8_t CnsCardInterfaceCardTransaction(CnsCardInterfaceInfo *apiPointer,
		void *handel);

uint8_t CnsCardInterfaceReadCardInfo(CnsCardInterfaceInfo *apiPointer,
		void *handel);

uint8_t CnsCardInterfaceVerifyCardActive(CnsCardInterfaceInfo *apiPointer,
		void *handel);

uint8_t CnsCardInterfaceWriteTicketLog(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *dateTime);

uint8_t CnsCardInterfaceCardIsOfLogintype(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint8_t loginType);

uint8_t CnsCardInterfaceReadTicketInfo(CnsCardInterfaceInfo *apiPointer,
		void *handel);

uint8_t CnsCardInterfaceCreditTicketBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t amount);

uint8_t CnsCardInterfaceReadPurseInfo(CnsCardInterfaceInfo *apiPointer,
		void *handel);

uint8_t CnsCardInterfaceWritePurseLog(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *dateTime);

#endif /* SMARTCARDIF_CNSCARDINTERFACE_H_ */
