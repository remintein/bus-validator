/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_DESFIREFUNCTIONS_H_
#define SMARTCARDIF_DESFIREFUNCTIONS_H_

#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "phCryptoSym.h"
#include "phCryptoRng.h"
#include "phKeyStore.h"
#include "CnsCardInterface.h"


#define CNS_FILE_CARD_INFORMATION 17
#define CNS_FILE_WITH_VALUE 3
#define CNS_FILE_ACTIVE_TICKET 19
#define CNS_FILE_PAYPLANS 20
#define CNS_FILE_TRANSACTION_LOG 21

typedef struct {
	uint8_t cardId[10]; /**< Card ID  as read by CASCADE etc */
	uint8_t cardIdLength; /**<  Length of card id */
	uint8_t bcc; /**<  BCC bit count check */
	uint8_t bUidLength; /**<  Current length of Uid */
	uint8_t bUidComplete; /**<  Is uid complete? */
	uint8_t bSessionKey[24]; /**< Session key for this authentication */
	uint8_t bKeyNo; /**< key number against which this authentication is done */
	uint8_t bIv[16]; /**< Max size of IV can be 16 bytes */
	uint8_t bAuthMode; /**< Authenticate (0x0A), AuthISO (0x1A), AuthAES (0xAA) */
	uint8_t pAid[3]; /**< Aid of the currently selected application */
	uint8_t bCryptoMethod; /**< DES,3DES, 3K3DES or AES */
	uint8_t bWrappedMode; /**< Wrapped APDU mode. All native commands need to be sent wrapped in ISO 7816 APDUs. */
	uint16_t wCrc; /**< 2 Byte CRC initial value in Authenticate mode. */
	uint32_t dwCrc; /**< 4 Byte CRC initial value in 0x1A, 0xAA mode. */
	uint16_t wAdditionalInfo; /**< Specific error codes for Desfire generic errors. */
	uint16_t wPayLoadLen; /**< Amount of data to be read. Required for Enc read to verify CRC. */
	uint8_t bLastBlockBuffer[16]; /**< Buffer to store last Block of encrypted data in case of chaining. */
	uint8_t bLastBlockIndex; /**< Last Block Buffer Index. */

	uint8_t bCidSupported; /**< Cid Support indicator; Unequal '0' if supported. */
	uint8_t bCid; /**< Card Identifier; Ignored if bCidSupported is equal '0'. */

	uint8_t bNadSupported; /**< Nad Support indicator; Unequal '0' if supported. */
	uint8_t transmitBlock; /**<Transmit block, will alternate */

	CnsCardInterfaceInfo cnsData;
} DesfireHandlingStruct;

uint8_t resetBitsAfterByte(uint8_t *p, unsigned int firstBitToClear,
		unsigned int totBits);

int desfireGetFreeMemory(DesfireHandlingStruct *pDesfire, uint32_t *response);

void initDesfireHandlingStruct(DesfireHandlingStruct *pDesfire);



#endif /* SMARTCARDIF_DESFIREFUNCTIONS_H_ */

/** ReqA Command code */
#define PHPAL_I14443P3A_REQUEST_CMD         0x26U

/** WupA Command code */
#define PHPAL_I14443P3A_WAKEUP_CMD          0x52U

/** HltA Command code */
#define PHPAL_I14443P3A_HALT_CMD            0x50U

/** Cascade Tag (CT) value */
#define PHPAL_I14443P3A_CASCADE_TAG         0x88U

///** Cascade Level 1 code for ISO14443-3A anticollision/select command. */
////#define PHPAL_I14443P3A_CASCADE_LEVEL_1     0x93
//
///** Cascade Level 2 code for ISO14443-3A anticollision/select command. */
//#define PHPAL_I14443P3A_CASCADE_LEVEL_2     0x95
//
///** Cascade Level 3 code for ISO14443-3A anticollision/select command. */
//#define PHPAL_I14443P3A_CASCADE_LEVEL_3     0x97

#define PH_CRYPTOSYM_CONFIG_KEY_TYPE    0x0000U
#define PH_CRYPTOSYM_CONFIG_KEY_SIZE    0x0001U /**< Key Size of currently loaded key. Read-only.  */
#define PH_CRYPTOSYM_CONFIG_BLOCK_SIZE  0x0002U /**< Block Size of currently loaded key. Read-only. */
/**
 * Keep init vector. Either #PH_CRYPTOSYM_VALUE_KEEP_IV_OFF or #PH_CRYPTOSYM_VALUE_KEEP_IV_ON.\n
 * This flag has to be used in combination with the option flag in the Encrypt/Decrypt/CalculateMac\n
 * function: If either the option in the function or this flag is set, the IV will be updated before\n
 * returning of the function.\n
 * R/W access possible.
 */
#define PH_CRYPTOSYM_CONFIG_KEEP_IV     0x0003U
/*@}*/

/**
 * \name Supported IV Updated Behaviour Modes
 */
/*@{*/
#define PH_CRYPTOSYM_VALUE_KEEP_IV_OFF  0x0000U /**< Switch off Keep-IV behaviour. */
#define PH_CRYPTOSYM_VALUE_KEEP_IV_ON   0x0001U /**< Switch on Keep-IV behaviour. */

/**
 * \name Supported Padding Modes.
 */
//#define PH_CRYPTOSYM_PADDING_MODE_1     00U /**< Pad with all zeros */
//#define PH_CRYPTOSYM_PADDING_MODE_2     01U /**< Pad with a one followed by all zeros */
/*@}*/

/**
 * \name Supported Cipher Modes
 */
/*@{*/
#define PH_CRYPTOSYM_CIPHER_MODE_ECB        0x00U   /**< ECB Cipher Mode. */
#define PH_CRYPTOSYM_CIPHER_MODE_CBC        0x01U   /**< CBC Cipher Mode. */
#define PH_CRYPTOSYM_CIPHER_MODE_CBC_DF4    0x02U   /**< CBC Cipher Mode for DF4. */
/*@}*/

/**
 * \name Supported Mac Modes
 */
/*@{*/
#define PH_CRYPTOSYM_MAC_MODE_CMAC      0x00U   /**< CMAC MAC Mode. */
#define PH_CRYPTOSYM_MAC_MODE_CBCMAC    0x01U   /**< CBCMAC MAC Mode. */
/*@}*/

/**
 * \name Supported Key Types to be used in key loading functionality.
 */
/*@{*/
#define PH_CRYPTOSYM_KEY_TYPE_AES128    0x0000U /**< AES 128 Key [16 Bytes]. */
#define PH_CRYPTOSYM_KEY_TYPE_AES192    0x0001U /**< AES 192 Key [24 Bytes]. */
#define PH_CRYPTOSYM_KEY_TYPE_AES256    0x0002U /**< AES 256 Key [32 Bytes]. */
#define PH_CRYPTOSYM_KEY_TYPE_DES       0x0003U /**< DES Single Key [8 Bytes].   */
#define PH_CRYPTOSYM_KEY_TYPE_2K3DES    0x0004U /**< 2 Key Triple Des [16 Bytes]. */
#define PH_CRYPTOSYM_KEY_TYPE_3K3DES    0x0005U /**< 3 Key Triple Des [24 Bytes]. */

#define PH_CRYPTOSYM_KEY_TYPE_INVALID   0xFFFFU /**< Invalid Key*/

#define PH_CRYPTOSYM_KEY_TYPE_MIFARE    0x0006U /**< MIFARE (R) Key. */
/*@}*/

/**
 * \name Supported Diversification Types
 */
/*@{*/
#define PH_CRYPTOSYM_DIV_MODE_DESFIRE       0x0000U /**< DESFire Key Diversification. */
#define PH_CRYPTOSYM_DIV_MODE_MIFARE_PLUS   0x0001U /**< MIFARE Plus Key Diversification. */
#define PH_CRYPTOSYM_DIV_MODE_MASK          0x00FFU /**< Bitmask for diversification Mode. */
#define PH_CRYPTOSYM_DIV_OPTION_2K3DES_FULL 0x0000U /**< Option for 2K3DES full-key diversification (only with #PH_CRYPTOSYM_DIV_MODE_DESFIRE). */
#define PH_CRYPTOSYM_DIV_OPTION_2K3DES_HALF 0x8000U /**< Option for 2K3DES half-key diversification (only with #PH_CRYPTOSYM_DIV_MODE_DESFIRE). */
/*@}*/

/**
 * \name General DES Defines.
 */
/*@{*/
#define PH_CRYPTOSYM_DES_BLOCK_SIZE      8U /**< Block size in DES algorithm */
#define PH_CRYPTOSYM_DES_KEY_SIZE        8U /**< Key size in DES algorithm for 56 bit key*/
#define PH_CRYPTOSYM_2K3DES_KEY_SIZE    16U /**< Key size in AES algorithm for 112 bit key*/
#define PH_CRYPTOSYM_3K3DES_KEY_SIZE    24U /**< Key size in AES algorithm for 168 bit key*/
/*@}*/

/**
 * \name General AES Defines.
 */
#define PH_CRYPTOSYM_AES_BLOCK_SIZE     16U /**< Block size in AES algorithm */
#define PH_CRYPTOSYM_AES128_KEY_SIZE    16U /**< Key size in AES algorithm for 128 bit key*/
#define PH_CRYPTOSYM_AES192_KEY_SIZE    24U /**< Key size in AES algorithm for 192 bit key*/
#define PH_CRYPTOSYM_AES256_KEY_SIZE    32U /**< Key size in AES algorithm for 256 bit key*/
/*@}*/

#define PH_CRYPTOSYM_KEYSCHEDULE_ENCRYPTION 0x0F
#define PH_CRYPTOSYM_KEYSCHEDULE_DECRYPTION 0xC0
#define PH_CRYPTOSYM_KEYSCHEDULE_DECRYPTION_PREPARE 0x30
