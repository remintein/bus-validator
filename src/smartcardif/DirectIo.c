/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "DirectIo.h"
#include <FreeRTOS.h>
#include <semphr.h>

#include "../project_includes.h"

#include <phNfcLib.h>
#include "phhalHw_Rc663_Reg.h"

/**
 * @file
 *
 * This file contains the low level commands to the RC663
 *
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

SPI_HandleTypeDef nfccSmartcardIf_HandleStructure;
/**
 * @brief Read a register from the Rc663
 *
 * @param reg - Register to read
 * @return - The value in the register
 */
uint8_t Rc663ReadRegister(uint8_t reg) {
	uint8_t cmd[2];
	uint8_t ret[2];

	cmd[0] = (reg << 1);
	cmd[0] |= 1;
	cmd[1] = 0x00;
	HAL_GPIO_WritePin(CLRC663_GPIO_NSS, CLRC663_PIN_NSS, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(&nfccSmartcardIf_HandleStructure, cmd, ret, 2,
	HAL_MAX_DELAY);
	HAL_GPIO_WritePin(CLRC663_GPIO_NSS, CLRC663_PIN_NSS, GPIO_PIN_SET);
	return ret[1];
}

/**
 * @brief Write a register of the Rc663
 *
 * @param reg - Register to write
 * @param val - The value to write
 * @return - Return of the transmission
 */
uint8_t Rc663WriteRegister(uint8_t reg, uint8_t val) {
	uint8_t cmd[2];
	uint8_t ret[2];

	cmd[0] = (reg << 1);
	cmd[1] = val;
	HAL_GPIO_WritePin(CLRC663_GPIO_NSS, CLRC663_PIN_NSS, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(&nfccSmartcardIf_HandleStructure, cmd, ret, 2,
	HAL_MAX_DELAY);
	HAL_GPIO_WritePin(CLRC663_GPIO_NSS, CLRC663_PIN_NSS, GPIO_PIN_SET);
	return ret[0];
}

/**
 * @brief Write a register of the Rc663
 *
 * @param reg - Register to write
 * @param val - The values to write
 * @param size - The number of values to write
 * @return - Return of the transmission
 */
uint8_t Rc663MultiWriteRegister(uint8_t reg, uint8_t* val, uint8_t size) {
	uint8_t cmd[257];
	uint8_t ret[257];

	cmd[0] = (reg << 1);
	memcpy(cmd + 1, val, size);
	HAL_GPIO_WritePin(CLRC663_GPIO_NSS, CLRC663_PIN_NSS, GPIO_PIN_RESET);
	HAL_SPI_TransmitReceive(&nfccSmartcardIf_HandleStructure, cmd, ret,
			1 + size,
			HAL_MAX_DELAY);
	HAL_GPIO_WritePin(CLRC663_GPIO_NSS, CLRC663_PIN_NSS, GPIO_PIN_SET);
	return ret[0];
}

/** @} */
