/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_CARDHANDLER_H_
#define SMARTCARDIF_CARDHANDLER_H_
#include "stm32f4xx.h"
//#include "stm32f4_discovery.h"
#include "FreeRTOS.h"
#include "DesfireFunctions.h"

void CardHandlerInitializer();
uint8_t CardHandlerContinueWith(DesfireHandlingStruct *card);
void CardHandlerSetToIgnore(DesfireHandlingStruct *card) ;
void CardHandlerSetToNotIgnore(DesfireHandlingStruct *cardStruct);

#define CARDHANDLER_IGNORE 1
#define CARDHANDLER_CONTINUE 0
#define CARDHANDLER_ENTERCARD 2
#define CARDHANDLER_LEAVECARD 3
#define CARDHANDLER_REJECTCARD 4


typedef struct {
	uint8_t status;
	uint8_t found[7];
	TickType_t time;
	uint8_t farePerc;
	int fare;
} CardHandlerInfo;
#endif /* SMARTCARDIF_CARDHANDLER_H_ */
