/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include <string.h>

#include "stm32f4xx.h"

#include "../project_includes.h"
#include "CnsCardInterface.h"
#include "CnsCardInterfaceInternal.h"

/**
 * @file
 * @brief Contains  functions for the card interface
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

/**
 * @brief Initialize the card interface
 *
 * @param apiPointer - the pointer to this API
 */
uint8_t CnsCardInterfaceInitialize(CnsCardInterfaceInfo *apiPointer) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}

	apiPointer->isInitialized = 1;
	apiPointer->reason[0] = '\0';
	apiPointer->previousReason[0] = '\0';
	apiPointer->isCardInitialized = 0;
	apiPointer->curApplication = -1;
	apiPointer->curApplicationKey = -1;
	for (int i = 0; i < CNS_CARDINTERFACE_MAXFILES; i++) {
		apiPointer->fileIsRead[i] = 0;
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Start with the card
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param cardId  -Ifd of the card to work on
 * @return Status
 */
uint8_t CnsCardInterfaceStartCard(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint8_t *cardId) {
	if (cardId == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}

	for (int i = 0; i < CNS_CARDINTERFACE_MAXFILES; i++) {
		apiPointer->fileIsRead[i] = 0;
	}
	apiPointer->isCardInitialized = 1;
	apiPointer->curApplication = -1;
	apiPointer->reason[0] = '\0';
	memcpy(apiPointer->cardUid, cardId, 7);
	apiPointer->removedPurse = 0;
	apiPointer->removedTicket = 0;
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Get the user information from the file on the card
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceGetUserInformation(CnsCardInterfaceInfo *apiPointer,
		void *handel, CnsCardInterfaceUserInformation *information) {

	if (information == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 4);
	if (ior != 0) {
		return ior;
	}
	fileCardHolderInformationStruct str;
	CopyStreamToCardHolderInformation(&str, apiPointer->file1);
	memcpy(information->name, str.fullName, sizeof(str.fullName));
	information->name[sizeof(str.fullName)] = '\0';
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Check if the card is of a certain login type
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param loginType - the login type to check (bit)
 * @return Status
 */
uint8_t CnsCardInterfaceCardIsOfLogintype(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint8_t loginType) {
	CnsCardInterfaceInternalReadFile(apiPointer, handel, 1);

	fileCardInformationStruct cardInfo;
	CopyStreamToCardInformation(&cardInfo, apiPointer->file1);

	return (cardInfo.loginType & loginType) != 0;
}

/**
 * @brief Check if the card is enabled
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceCardIsEnabled(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date) {
	if (date == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 1);
	if (ior != 0) {
		return ior;
	}
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 2);
	if (ior != 0) {
		return ior;
	}
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 3);
	if (ior != 0) {
		return ior;
	}
	// check card status P, the first time use the card!!!
	uint8_t theBcdDate[4];
	CnsCardInterfaceInternalBcdDate(date, theBcdDate);
// local structures needed
	fileCardInformationStruct file1;
	fileCardConfigurationStruct file2;
	fileCardStatusStruct file3;
// copy data to struct
	CopyStreamToCardInformation(&file1, apiPointer->file1);
	CopyStreamToCardConfiguration(&file2, apiPointer->file2);
	CopyStreamToCardStatus(&file3, apiPointer->file3);
	if (file2.isActive == 0 && file3.cardStatus == 'P') {
		if (memcmp(file1.cardExpiredDate, theBcdDate, 4) > 0) {
			file2.isActive = 1;
			file3.cardStatus = 'A';
			// Copy data back to buffer
			CopyCardConfigurationToStream(apiPointer->file2, &file2);
			CopyCardStatusToStream(apiPointer->file3, &file3);
			ior = CnsCardInterfaceInternalWriteFile(apiPointer, handel, 2);
			if (ior != 0) {
				return ior;
			}
			ior = CnsCardInterfaceInternalWriteFile(apiPointer, handel, 3);
			if (ior != 0) {
				return ior;
			}
		}
	}
	ior = CnsCardInterfaceInternalWriteFile(apiPointer, handel, 2);
	if (ior != 0) {
		return ior;
	}
	ior = CnsCardInterfaceInternalWriteFile(apiPointer, handel, 3);
	if (ior != 0) {
		return ior;
	}
	if (file2.isActive != 1 || file3.cardStatus != 'A') {
		return CNS_CARDINTERFACE_RETURN_CARD_NOT_ACTIVE; //24-07-2017 TONY&KHOA
	}
	if (memcmp(file1.cardExpiredDate, theBcdDate, 4) <= 0) {
		return CNS_CARDINTERFACE_RETURN_CARD_IS_EXPIRED;  //24-07-2017 TONY&KHOA
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Check if the card is of a certain ticket type
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param ticketType - Type of ticket to verify
 * @return Status
 */
uint8_t CnsCardInterfaceTicketType(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint8_t tickettype) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 5);
	if (ior != 0) {
		return ior;
	}
	fileTicketInformationStruct str;
	CopyStreamToTicketInformation(&str, apiPointer->file5);
	if (str.ticketType == tickettype) {
		return CNS_CARDINTERFACE_RETURN_OK;
	}

	return CNS_CARDINTERFACE_TICKET_TYPE_ERROR;
}

/**
 * @brief Set the card status from P to A
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceSetCardFromPtoA(CnsCardInterfaceInfo *apiPointer,
		void *handel) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 3);
	fileCardStatusStruct file3;
	CopyStreamToCardStatus(&file3, apiPointer->file3);

	if (file3.cardStatus != 'P') {
		return CNS_CARDINTERFACE_CARD_STATUS_ERROR;
	}

	file3.cardStatus = 'A';
	CopyCardStatusToStream(apiPointer->file3, &file3);
	CnsCardInterfaceInternalWriteFile(apiPointer, handel, 3);
	if (ior != 0) {
		return ior;
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/** TONY
 * @brief Set the card status from A to P
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceSetCardFromAtoP(CnsCardInterfaceInfo *apiPointer,
		void *handel) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 3);
	fileCardStatusStruct file3;
	CopyStreamToCardStatus(&file3, apiPointer->file3);

	if (file3.cardStatus != 'A') {
		return CNS_CARDINTERFACE_CARD_STATUS_ERROR;
	}

	file3.cardStatus = 'P';
	CopyCardStatusToStream(apiPointer->file3, &file3);
	CnsCardInterfaceInternalWriteFile(apiPointer, handel, 3);
	if (ior != 0) {
		return ior;
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Check if the card is of a specific card status
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param cardStatus - check this status
 * @return Status
 */
uint8_t CnsCardInterfaceCardStatus(CnsCardInterfaceInfo *apiPointer,
		void *handel, char cardstatus) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 3);

	if (ior != 0) {
		return ior;
	}
	fileCardStatusStruct file3;
	CopyStreamToCardStatus(&file3, apiPointer->file3);
	if (file3.cardStatus == cardstatus) {
		return CNS_CARDINTERFACE_RETURN_OK;
	}
	return CNS_CARDINTERFACE_CARD_STATUS_ERROR;
}

/**
 * @brief Check if the card is not expired
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param date - the date to verify against
 * @return Status
 */
uint8_t CnsCardInterfaceCardIsCardNotExpired(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 1);

	if (ior != 0) {
		return ior;
	}
	fileCardInformationStruct file1;
	CopyStreamToCardInformation(&file1, apiPointer->file1);

	uint8_t theBcdDate[4];

	CnsCardInterfaceInternalBcdDate(date, theBcdDate);

	if (memcmp(file1.cardExpiredDate, theBcdDate, 4) >= 0) {
		return CNS_CARDINTERFACE_RETURN_OK;
	}
	strcpy(apiPointer->reason, LOCALE_ERROR_CARD_EXPIRED);
	return CNS_CARDINTERFACE_CARD_STATUS_ERROR;
}

/**
 * @brief Check the ticket status
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param ticketstatus - status if the ticket
 * @return Status
 */
uint8_t CnsCardInterfaceTicketStatus(CnsCardInterfaceInfo *apiPointer,
		void *handel, char ticketstatus) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 5);
	if (ior != 0) {
		return ior;
	}
	fileTicketInformationStruct str;
	CopyStreamToTicketInformation(&str, apiPointer->file5);
	if (str.ticketStatus == ticketstatus) {
		return CNS_CARDINTERFACE_RETURN_OK;
	}
	return CNS_CARDINTERFACE_TICKET_STATUS_ERROR;
}

/**
 * @brief Verify if the card purse has this status
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param purseStatus - status of the purse
 * @return Status
 */
uint8_t CnsCardInterfacePurseStatus(CnsCardInterfaceInfo *apiPointer,
		void *handel, char pursestatus) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 8);
	if (ior != 0) {
		return ior;
	}
	filePurseInformationStruct str;
	CopyStreamToPurseInformation(&str, apiPointer->file8);
	if (str.purseStatus == pursestatus) {
		return CNS_CARDINTERFACE_RETURN_OK;
	}
	return CNS_CARDINTERFACE_PURSE_STATUS_ERROR;
}

/**
 * @brief Get the purse balance
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param value - pointer to store the value
 * @return Status
 */
uint8_t CnsCardInterfaceGetPurseBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, int32_t *value) {
	if (value == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0 || apiPointer->isCardInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 9);
	if (ior != 0) {
		return ior;
	}
	*value = apiPointer->file9;
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Debit an amount from the purse
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param amount - the amount to debit
 * @return Status
 */
uint8_t CnsCardInterfaceDebitPurseBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t amount) {
	if (amount == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0 || apiPointer->isCardInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}

	uint8_t ior;
	ior = CnsCardInterfaceInternalDebit(apiPointer, handel, 9, amount);
	if (ior != 0) {
		return CNS_CARDINTERFACE_PURSE_INSUFFICIENT_BALANCE;
	}
	apiPointer->file9 -= amount;
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Get the ticket balance
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param value - pointer to the where to store the value
 * @return Status
 */
uint8_t CnsCardInterfaceGetTicketBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, int32_t *value) {
	if (value == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0 || apiPointer->isCardInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 6);
	if (ior != 0) {
		return ior;
	}
	*value = apiPointer->file6;
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Debit the ticket balance
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param amount  - amount to debit
 * @return Status
 */
uint8_t CnsCardInterfaceDebitTicketBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t amount) {
	if (amount == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0 || apiPointer->isCardInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 6);
	if (ior != 0) {
		return ior;
	}
	ior = CnsCardInterfaceInternalDebit(apiPointer, handel, 6, amount);
	if (ior != 0) {
		return CNS_CARDINTERFACE_INSUFFICIENT_BALANCE;
	}
	apiPointer->file6 -= amount;
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Credit the ticket balance
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param amount - the amount to credit
 * @return Status
 */
uint8_t CnsCardInterfaceCreditTicketBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t amount) {
	if (amount == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0 || apiPointer->isCardInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 6);
	if (ior != 0) {
		return ior;
	}
	ior = CnsCardInterfaceInternalCredit(apiPointer, handel, 6, amount);
	apiPointer->fileIsRead[6 - 1] = 0;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 6);

	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Write to the ticket log
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @parma datetime - current date time
 * @return Status
 */
uint8_t CnsCardInterfaceWriteTicketLog(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *dateTime) {
	fileTicketLogStruct ticketLog;

	ticketLog.fileFormatVersion = 1;
	ticketLog.ticketTransactionsSubType = 0x26;
	CnsCardInterfaceInternalBcdDateTime(dateTime,
			ticketLog.ticketTransactionDateTime);
	ticketLog.ticketBeforeCount = apiPointer->file6 + apiPointer->removedTicket;
	ticketLog.ticketUsedCount = apiPointer->removedTicket;
	ticketLog.ticketValue = apiPointer->file6;
	memcpy(ticketLog.ticketSAM_ID, "???????", 7);
	memset(ticketLog.futureUse, '\0', sizeof(ticketLog).futureUse);

	CopyTicketLogToStream(apiPointer->file7, &ticketLog);
	apiPointer->curApplicationKey = 0; // Force authentication again
	uint8_t ior = CnsCardInterfaceInternalWriteFile(apiPointer, handel, 7);

	return ior;
}

/**
 * @brief Write to the purse log
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param datetime - the current date time
 * @return Status
 */
uint8_t CnsCardInterfaceWritePurseLog(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *dateTime) {
	filePurseLogStruct purseLog;

	purseLog.fileFormatVersion = 1;
	purseLog.purseTransactionsSubType = 0x14;
	CnsCardInterfaceInternalBcdDateTime(dateTime,
			purseLog.purseTransactionDateTime);
	purseLog.purseBeforeCount = apiPointer->file9 + apiPointer->removedPurse;
	purseLog.purseUsedCount = apiPointer->removedPurse;
	purseLog.purseValue = apiPointer->file9;
	memcpy(purseLog.purseSAM_ID, "???????", 7);
	memset(purseLog.futureUse, '\0', sizeof(purseLog).futureUse);
	CopyPurseLogToStream(apiPointer->file10, &purseLog);
	uint8_t ior = CnsCardInterfaceInternalWriteFile(apiPointer, handel, 10);

	return ior;
}

/**
 * @brief Increase the ticket transaction count
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param amount - the amount to increase
 * @return Status
 */
uint8_t CnsCardInterfaceTicketTransactionCount(CnsCardInterfaceInfo *apiPointer,
		void *handel, int32_t amount) {
	if (amount == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0 || apiPointer->isCardInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 8);
	if (ior != 0) {
		return ior;
	}
	ior = CnsCardInterfaceInternalCredit(apiPointer, handel, 8, amount);
	if (ior != 0) {
		return CNS_CARDINTERFACE_INSUFFICIENT_BALANCE;
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Verify if the purse is expired
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param date - the date to check against
 * @return Status
 */
uint8_t CnsCardInterfacePurseExpire(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date) {
	if (date == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 8);
	if (ior != 0) {
		return ior;
	}
	uint8_t theBcdDate[4];
	CnsCardInterfaceInternalBcdDate(date, theBcdDate);
	filePurseInformationStruct str;
	CopyStreamToPurseInformation(&str, apiPointer->file8);
	if (memcmp(str.purseExpiredDate, theBcdDate, 4) <= 0) {
		strcpy(apiPointer->reason, LOCALE_ERROR_PURSE_EXPIRED);
		return CNS_CARDINTERFACE_RETURN_PURSE_IS_EXPIRED;
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Verify if the ticket is expired
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param date - the date to check against
 * @return Status
 */
uint8_t CnsCardInterfaceTicketExpire(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *date) {
	if (date == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 5);
	if (ior != 0) {
		return ior;
	}
	uint8_t theBcdDate[4];
	CnsCardInterfaceInternalBcdDate(date, theBcdDate);
	fileTicketInformationStruct str;
	CopyStreamToTicketInformation(&str, apiPointer->file5);
	uint8_t b[4];
	memcpy(b, str.ticketEffectiveDate, 4);
	CnsCardInterfaceInternalAddDaysToBcdDate(b, str.ticketExpireDays);
	if (memcmp(b, theBcdDate, 4) < 0) {
		strcpy(apiPointer->reason, LOCALE_ERROR_TICKET_EXPIRED);
		return CNS_CARDINTERFACE_RETURN_TICKET_IS_EXPIRED;
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Verify if the ticket is below a value
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param belowIsLow - amount below which the ticket is low
 * @return Status
 */
uint8_t CnsCardInterfaceCheckLowTicketBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t belowIsLow) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 6);
	if (ior != 0) {
		return ior;
	}
	fileTicketValueStruct val;
	CopyStreamToTicketValue(&val, (uint8_t *) &(apiPointer->file6));
	if (val.ticketValue < belowIsLow) {
		return CNS_CARDINTERFACE_TICKET_LOW_BALANCE;
	}

	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Verify if the purse is below a value
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param belowIsLow - amount below which the ticket is low
 * @return Status
 */
uint8_t CnsCardInterfaceCheckLowPurseBalance(CnsCardInterfaceInfo *apiPointer,
		void *handel, uint32_t belowIsLow) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 9);
	if (ior != 0) {
		return ior;
	}
	filePurseValueStruct val;
	CopyStreamToPurseValue(&val, (uint8_t *) &(apiPointer->file9));
	if (val.purseValue < belowIsLow) {
		return CNS_CARDINTERFACE_PURSE_LOW_BALANCE;
	}

	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Perform purse logging
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @param datetime -  the current date/time
 * @return Status
 */
uint8_t CnsCardInterfacePerformPurseLogging(CnsCardInterfaceInfo *apiPointer,
		void *handel, char *datetime, uint16_t busId, uint16_t deviceId,
		uint16_t routeId) {
	if (datetime == 0 || apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0 || apiPointer->isCardInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;

	filePurseLogStruct record;

	CnsCardInterfaceInternalBcdDateTime(datetime,
			record.purseTransactionDateTime);

	CopyPurseLogToStream(apiPointer->file10, &record);

	ior = CnsCardInterfaceInternalWriteFile(apiPointer, handel, 10);

	if (ior != 0) {
		return CNS_CARDINTERFACE_INTERNAL_ERROR;
	}
	return CNS_CARDINTERFACE_RETURN_OK;
}

/**
 * @brief Increate the ticket transaction count
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceTicketTransaction(CnsCardInterfaceInfo *apiPointer,
		void *handel) {
	return CnsCardInterfaceInternalCredit(apiPointer, handel, 11, 1);
}

/**
 * @brief Increate the purse transaction count
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfacePurseTransaction(CnsCardInterfaceInfo *apiPointer,
		void *handel) {
	return CnsCardInterfaceInternalCredit(apiPointer, handel, 12, 1);
}
/**
 * @brief Increate the card transaction count
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceCardTransaction(CnsCardInterfaceInfo *apiPointer,
		void *handel) {
	return CnsCardInterfaceInternalCredit(apiPointer, handel, 13, 1);
}

/**
 * @brief Read the card information
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceReadCardInfo(CnsCardInterfaceInfo *apiPointer,
		void *handel) {
	return CnsCardInterfaceInternalReadFile(apiPointer, handel, 1);
}

/**
 * @brief Read the ticket information
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceReadTicketInfo(CnsCardInterfaceInfo *apiPointer,
		void *handel) {
	uint8_t ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 5);
	if (ior != 0) {
		return ior;
	}
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 6);
	return ior;
}

/**
 * @brief Read the purse information
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceReadPurseInfo(CnsCardInterfaceInfo *apiPointer,
		void *handel) {
	uint8_t ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 8);
	if (ior != 0) {
		return ior;
	}
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 9);
	return ior;
}

/**
 * @brief Verify the card is active
 *
 * @param apiPointer - the pointer to this API
 * @param handel - given by the controller
 * @return Status
 */
uint8_t CnsCardInterfaceVerifyCardActive(CnsCardInterfaceInfo *apiPointer,
		void *handel) {
	if (apiPointer == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_ILLEGAL_ARGUMENTS;
	}
	if (apiPointer->isInitialized == 0) {
		return CNS_CARDINTERFACE_RETURN_ERROR_NOTINITIALIZED;
	}
	uint8_t ior;
	ior = CnsCardInterfaceInternalReadFile(apiPointer, handel, 2);

	if (ior != 0) {
		return ior;
	}
	fileCardConfigurationStruct file2;
	CopyStreamToCardConfiguration(&file2, apiPointer->file2);
	if (file2.isActive == 0x01) {
		return CNS_CARDINTERFACE_RETURN_OK;
	}
	return CNS_CARDINTERFACE_CARD_STATUS_ERROR;
}
/** @} */
