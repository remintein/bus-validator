/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */


#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

#include <stm32f4xx_hal_spi.h>
#include <stm32f4xx_hal_gpio.h>
#include <stm32f4xx_hal.h>

#ifndef SMARTCARDIF_DIRECTIO_H_
#define SMARTCARDIF_DIRECTIO_H_

void nfccSmartcardSetupSpi();
uint8_t Rc663ReadRegister(uint8_t reg);
uint8_t Rc663WriteRegister(uint8_t reg, uint8_t val);
uint8_t Rc663MultiWriteRegister(uint8_t reg, uint8_t* val, uint8_t size);
#endif /* SMARTCARDIF_DIRECTIO_H_ */
