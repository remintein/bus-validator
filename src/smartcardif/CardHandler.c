/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"

#include "CardHandler.h"

#define MAXCARDS 25

CardHandlerInfo cards[MAXCARDS];

#define NO_CARD 0
#define VALID_CARD_THIS_STOP 1
#define VALID_CARD_OTHER_STOP 2
#define IGNORE_CARD 3

#define DELAY_SEEN 1000

/**
 * @file
 * @brief Contains  functions for the card handler
 */

/**
 * @addtogroup smartcardif
 *
 * @{
 */

/**
 * @brief Card handler initializer
 */
void CardHandlerInitializer() {
	int i;
	for (i = 0; i < MAXCARDS; i++) {
		cards[i].status = NO_CARD;
		memset(cards[i].found, 0x00, 7);
	}
}

/**
 * @brief Check whether a card can be continued with
 *
 * @param HandlingStruct - Card handling struct
 * @return Ignore or return
 */
uint8_t CardHandlerContinueWith(DesfireHandlingStruct *card) {
	for (int i = 0; i < MAXCARDS; i++) {
		if (cards[i].status == NO_CARD) {
			continue;
		}
		if (memcmp(cards[i].found, card->cardId, 7) != 0) {
			continue;
		}
		if (cards[i].time + DELAY_SEEN < xTaskGetTickCount()) {
			cards[i].status = NO_CARD;
			continue;
		}
		cards[i].time = xTaskGetTickCount();
		switch (cards[i].status) {
		case IGNORE_CARD:
		case VALID_CARD_THIS_STOP:
			return CARDHANDLER_IGNORE;
		case VALID_CARD_OTHER_STOP:
			return CARDHANDLER_CONTINUE;
		}
	}
	return CARDHANDLER_CONTINUE;
}

/**
 * @brief Set the current card to ignore next time
 *
 * @param HandlingStruct - Card handling struct
 */
void CardHandlerSetToIgnore(DesfireHandlingStruct *cardStruct) {
	int i;
	for (i = 0; i < MAXCARDS; i++) {
		if (cards[i].status == NO_CARD) {
			cards[i].status = IGNORE_CARD;
			memcpy(cards[i].found, cardStruct->cardId, 7);
			cards[i].time = xTaskGetTickCount();
			return;
		}
	}
	// If no slot, just pick one.
	for (i = MAXCARDS - 1; i >= 0; i--) {
		if (cards[i].status == IGNORE_CARD) {
			memcpy(cards[i].found, cardStruct->cardId, 7);
			cards[i].time = xTaskGetTickCount();
			return;
		}
	}
}

/**
 * @brief Set the current card to not ignore next time
 *
 * @param HandlingStruct - Card handling struct
 */
void CardHandlerSetToNotIgnore(DesfireHandlingStruct *cardStruct) {
	int i;
	for (i = 0; i < MAXCARDS; i++) {
		if (cards[i].status == IGNORE_CARD
				&& memcmp(cards[i].found, cardStruct->cardId, 7) == 0) {
			cards[i].status = NO_CARD;
			return;
		}
	}
}

/** @} */
