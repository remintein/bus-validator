/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef CLOCK_H_
#define CLOCK_H_


/**
 * @file
 * @brief Defines the functions for the clock module
 */

/**
 * Initialize routine, called from main.c before the FreeRTOS scheduler is started.
 */
BaseType_t nfccClockInit(UBaseType_t uxPriority);
/**
 * Main loop for the clock routine
 */
void nfccClock(void *pvParameters);
/**
 * Will fetch the time in the output character sequence (should be 6 long)
 * Format: HHMMSS
 */
void apiDnfccClockFetchTime(char *out);
/**
 * Will fetch the time in the output character sequence (should be 8 long)
 * Format: HH:MM:SS
 */
void apiDnfccClockFetchTimeHuman(char *out);
/**
 * Will fetch the date in the output character sequence (should be 8 long)
 * Format: YYYYMMDD
 */
void apiDnfccClockFetchDate(char *out);
/**
 * Will fetch the date in the output character sequence (should be 10 long)
 * Format: DD-MM-YYYY
 */
void apiDnfccClockFetchDateHuman(char *out);
/**
 * Will fetch the date and time in the output character sequence (should be 14 long)
 * Format: YYYYMMDDHHMMSS
 */
void apiDnfccClockFetchDateTime(char *out);
/**
 * Will fetch the date and time in the output character sequence (should be 19 long)
 * Format: DD-MM-YYYY HH:MM:SS
 */
void apiDnfccClockFetchDateTimeHuman(char *out);

/**
 * Will compare the current clock with the in-string. Output as of strncmp (eg -1..0..1 for before, equal, after )
 * Input, is a string like from apiDnfccClockFetchDate()
 */
uint8_t apiDnfccClockCompareWithNow(char *in);

#endif /* CLOCK_H_ */
