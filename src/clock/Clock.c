/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"
#include "clock.h"
#include <string.h>

/**
 * @file
 * @brief Contains the functions for the clock module
 *
 * This file contains the C-routines for the clock module.
 *
 * Clock routine
 *
 * The clock module will use the RTC(Real Time Clock) as embedded in the STM32F407 processor.
 *
 * In the main loop it will
 * - Send the date every 30 seconds to display.c or when changing
 * - Send a keepalive message every 30 seconds to datadistributor
 * - Send the time every change.
 *
 * The following tasks are defined:
 * - Incrementing time / date on second basis
 * - Sending the new Time or Date when changed to the display unit
 * - When a new time or date is transmitted to here, pick it up and use it.
 *
 * This routine will wait for 200 clock ticks (xQueueReceive)
 */

/**
 * @addtogroup Clock
 *
 * @{
 */

/**
 * Size of the stack of the clock-main routine
 */
#define STACK_SIZE 500

/**
 * External refererence to the Real-Time-Clock of the STM32-processor
 */
extern RTC_HandleTypeDef hrtc;

static RTC_TimeTypeDef sTime;
static RTC_DateTypeDef sDate;

/*****************************
 * External functions w.r.t. the clock
 */
/**
 * @brief Fetch the time
 *
 * Fetch the current time in format: 123456 (for 12:45:56)
 * @param out - Variable to put the time in
 */
void apiDnfccClockFetchTime(char *out) {
	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BCD);

	sprintf(out, "%02x%02x%02x", sTime.Hours, sTime.Minutes, sTime.Seconds);
}

/**
 * @brief Fetch the time in human format
 *
 * Fetch the current time in format: 12:34:56 (for 12:45:56)
 * @param out - Variable to put the time in
 */
void apiDnfccClockFetchTimeHuman(char *out) {
	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BCD);

	sprintf(out, "%02x:%02x:%02x", sTime.Hours, sTime.Minutes, sTime.Seconds);
}

/**
 * @brief Fetch the date
 *
 * Fetch the current date in format: 20180815 (for 15 aug 2018)
 * @param out - Variable to put the date in
 */
void apiDnfccClockFetchDate(char *out) {
	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BCD);

	sprintf(out, "20%02x%02x%02x", sDate.Year, sDate.Month, sDate.Date);
}

/**
 * @brief Fetch the date in human format
 *
 * Fetch the current date in format: 15-08-2018 (for 15 aug 2018)
 * @param out - Variable to put the date in
 */
void apiDnfccClockFetchDateHuman(char *out) {
	HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BCD);
	HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BCD);

	sprintf(out, "%02x-%02x-20%02x", sDate.Date, sDate.Month, sDate.Year);
}

/**
 * @brief Fetch the date/time
 *
 * Fetch the current date in format: 20180815122345 (for 15 aug 2018, 12:23:45)
 * @param out - Variable to put the date in
 */
void apiDnfccClockFetchDateTime(char *out) {
	apiDnfccClockFetchDate(out);
	apiDnfccClockFetchTime(out + 8);
}

/**
 * @brief Fetch the date/time in human format
 *
 * Fetch the current date in format: 12:23:34 15-08-2018 (for 15 aug 2018, 12:23:45)
 * @param out - Variable to put the date in
 */
void apiDnfccClockFetchDateTimeHuman(char *out) {
	apiDnfccClockFetchTimeHuman(out);
	out[8] = ' ';
	apiDnfccClockFetchDateHuman(out + 9);
}

/**
 * @brief Compare the date with "now"
 *
 * @param in - the date to compare, in format 20180815 (for 15 aug 2018)
 */
uint8_t apiDnfccClockCompareWithNow(char *in) {
	char clock_date[9];
	apiDnfccClockFetchDate(clock_date);
	uint8_t cmp = strncmp(clock_date, in, 8);
	if (cmp != 0) {
		return cmp;
	}
	char b[7];
	apiDnfccClockFetchTime(b);
	return strncmp(b, in, 8);
}

/**
 * @brief Initialization routine for the clock
 *
 * @param prio - the prio to run on
 */
BaseType_t nfccClockInit(UBaseType_t prio) {
	deviceStatus.clockRxQueue = xQueueCreate(CLOCK_QUEUE_LENGTH,
			CLOCK_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.clockRxQueue, "Clock");

	setWaitForSystemReadyThisTask(TASK_BIT_CLOCK);

	return xTaskCreate(nfccClock, "clock", STACK_SIZE, NULL, prio,
			&deviceStatus.clockTask);
}

/**
 * @brief set the date
 *
 * Set the date according to what is in the variable. This will alter the date in the RTC.
 * Format YYYYMMDD
 */
static void intDnfccSetDate(char *dt) {
	uint32_t date;
	sscanf(dt, "%lx", &date);
	// Unlock the register
	RTC_DateTypeDef sDate;
	sDate.Date = date & 0xff;
	sDate.Month = (date >> 8) & 0xff;
	sDate.Year = (date >> 16) & 0xff;
	sDate.WeekDay = RTC_WEEKDAY_MONDAY;

	HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD);
}

/**
 * @brief set the time
 *
 * Set the time according to what is in the variable. This will alter the date in the RTC.
 * Format HHMMSS
 */
static void intDnfccSetTime(char *tm) {
	uint32_t time;
	sscanf(tm, "%lx", &time);
	// Unlock the register
	RTC_TimeTypeDef tim;
	tim.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	tim.Hours = (time >> 16) & 0xff;
	tim.Minutes = (time >> 8) & 0xff;
	tim.Seconds = (time >> 0) & 0xff;
	tim.TimeFormat = RTC_HOURFORMAT12_AM;
	tim.StoreOperation = RTC_STOREOPERATION_SET;

	HAL_RTC_SetTime(&hrtc, &tim, RTC_FORMAT_BCD);
}

/**
 * @brief send date to display
 *
 * Internal routine to send the date to the display
 */
static void intDnfccSendDate() {
	char buf[10];
	buf[0] = COMCOM_DISPLAY_DATE;
	apiDnfccClockFetchDateHuman(buf + 1);
	queueSendToDisplay(buf, portMAX_DELAY);
}

/**
 * @brief send time to the display
 *
 * Internal routine to send the time to the display
 */
static void intDnfccSendTime() {
	char buf[10];
	buf[0] = COMCOM_DISPLAY_TIME;
	apiDnfccClockFetchTimeHuman(buf + 1);
	queueSendToDisplay(buf, 0);
}
/**
 * @brief Clock main loop
 *
 * The main loop for the clock
 */
void nfccClock(void *pvParameters) {
	char buf[CLOCK_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	TickType_t lastTick = xTaskGetTickCount();
	UBaseType_t uxHighWaterMark;
	TickType_t keepAlive = xTaskGetTickCount() + 10000; // Next keep alive should be sent over 1 minute

	uint32_t lastDR;

	lastDR = hrtc.Instance->DR;
	intDnfccSendDate();
	intDnfccSendTime();

	setTaskIsSystemReady(TASK_BIT_CLOCK);
	waitForSystemReady();

	for (;;) {
		buf[0] = '\0';
		if (xQueueReceive(deviceStatus.clockRxQueue, buf, 200) != pdPASS) {

			TickType_t curTick = xTaskGetTickCount();

			while (lastTick != curTick) {
				lastTick++;
				if (lastTick % 1000 == 0) {
					if (lastDR != hrtc.Instance->DR) {
						lastDR = hrtc.Instance->DR;

						// Date is send directly here, as it changes in a too less frequency Is send when the DR register has changed
						intDnfccSendDate();
					}
					intDnfccSendTime();
				}
			}

			if (keepAlive < xTaskGetTickCount()) {
				keepAlive = xTaskGetTickCount() + 30000;
				stbuf[0] = COMCOM_DATADISTRIBUTOR_KEEPALIVE;
				queueSendToDataDistributor(stbuf, 0);
				intDnfccSendDate(); // TODO: This should be not needed, remove later.
			}

			continue;
		}

		switch (buf[0]) {
		case COMCOM_CLOCK_SETDATE:
			intDnfccSetDate(buf + 1);
			intDnfccSendDate();
			break;
		case COMCOM_CLOCK_SETTIME:
			intDnfccSetTime(buf + 1);
			intDnfccSendTime();
			break;
		case COMCOM_CLOCK_SENDDATE:
			intDnfccSendDate();
			break;
		case COMCOM_CLOCK_SENDTIME:
			intDnfccSendTime();
			break;
		case COMCOM_SELFTEST:
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_CLOCK;
			stbuf[2] = 'T';
			apiDnfccClockFetchTime(stbuf + 3);
			stbuf[9] = 'D';
			apiDnfccClockFetchDate(stbuf + 10);
			stbuf[18] = 'W';
			uxHighWaterMark = uxTaskGetStackHighWaterMark(
					deviceStatus.clockTask);
			sprintf(stbuf + 20, "WM%ld", uxHighWaterMark);
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
		} // Switch[receive buffer]
	} // Endless for loop
}
/** @} */
