/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

/**
 * @file
 * @brief Defines the communication to the backend
 */

#ifndef CONFIGURATION_BACKENDCOMMUNICATION_H_
#define CONFIGURATION_BACKENDCOMMUNICATION_H_


/**
 * The protocol, IP address and the port, as the string used to connect to the backend.
 */
//#define STRING_CONNECT_MIDDLEWARE_SERVER "\"TCP\",\"85.214.102.242\",11080\015\012"
#define STRING_CONNECT_MIDDLEWARE_SERVER "\"TCP\",\"85.214.102.242\",11080\015\012"
//#define STRING_CONNECT_MIDDLEWARE_SERVER "\"TCP\",\"192.168.0.27\",8080\015\012"

/**
 * Commands from the backend:
 * SetId, set the ID for the current device (the backend)
 */
#define BACKEND_PERFORM_SETID "setid"
/**
 * Commands from the backend:
 * Selftest: Perform a selftest. The results will be shared with the backend (might take a few moments)
 */
#define BACKEND_PERFORM_SELFTEST "selftest"

/**
 * Commands from the backend:
 * updateFare - Update the fare file, start retrieve from backend
 */
#define BACKEND_PERFORM_UPDATE_FARE "updateFare"
/**
 * Commands from the backend:
 * updateFare - Update the blacklist file, start retrieve from backend
 */
#define BACKEND_PERFORM_UPDATE_BLACKLIST "updateBlacklist"
/**
 * Commands from the backend:
 * updateFare - Update the printer file, start retrieve from backend
 */
#define BACKEND_PERFORM_UPDATE_PRINTER "updatePrinter"
/**
 * Commands from the backend:
 * updateFare - Update the firmware file, start retrieve from backend
 */
#define BACKEND_PERFORM_UPDATE_FIRMWARE "updateFirmware"
/**
 * Commands from the backend:
 * updateFare - Update the wifi file, start retrieve from backend
 */
#define BACKEND_PERFORM_UPDATE_WIFI "updateWifi"

#define BACKEND_PERFORM_INIT_FILE "init"
#define BACKEND_PERFORM_WRITE_FILE "write"

#define BACKEND_MESSAGE_FIELD_TIME "\"time\":"
#define BACKEND_MESSAGE_FIELD_DATE "\"date\":"
#define BACKEND_MESSAGE_FIELD_PERFORM "\"perform\":"
#define BACKEND_MESSAGE_FIELD_PERFORMDATA "\"performData\":"

#define ENDPOINT_START           "/CnsBackendConnector/rest/cns/start"
#define ENDPOINT_KEEPALIVE       "/CnsBackendConnector/rest/cns/keepalive"
#define ENDPOINT_INITFILE        "/CnsBackendConnector/rest/cns/initfile"
#define ENDPOINT_SELFTEST        "/CnsBackendConnector/rest/cns/selftest"
#define ENDPOINT_REQUESTID       "/CnsBackendConnector/rest/cns/requestid"
#define ENDPOINT_GET_FILE_DATA   "/CnsBackendConnector/rest/cns/getData"

#define ENDPOINT_START_SHIFT     "/CnsBackendConnector/rest/cns/startShift"
#define ENDPOINT_END_SHIFT       "/CnsBackendConnector/rest/cns/endShift"
#define ENDPOINT_START_TRIP      "/CnsBackendConnector/rest/cns/startTrip"
#define ENDPOINT_END_TRIP        "/CnsBackendConnector/rest/cns/endTrip"

#define ENDPOINT_CARDENTERERROR  "/CnsBackendConnector/rest/cns/cardEnterError"
#define ENDPOINT_CARDENTEROK     "/CnsBackendConnector/rest/cns/cardEnterOk"
#define ENDPOINT_CARDTOPUP       "/CnsBackendConnector/rest/cns/cardtopup"


#endif /* CONFIGURATION_BACKENDCOMMUNICATION_H_ */
