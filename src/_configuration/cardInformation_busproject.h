/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SMARTCARDIF_CARDINFORMATION_BUSPROJECT_H_
#define SMARTCARDIF_CARDINFORMATION_BUSPROJECT_H_

/**
 * @file
 * @brief Defines files for the Bus Project
 */


#define CARD_TYPE_DESFIRE 1

#define VALUE_FILE 1
#define DATA_FILE 2
#define BACKUP_DATA_FILE 3
#define CYCLIC_FILE 4

/*********************************************************
 * CNS_PILOT
 * CARD_INFORMATIONS
 * FILE 1
 *********************************************************/

#define APPLICATION_CARD_INFORMATION  0xDF0001
#define AUTHENTICATIONKEY_CARD_INFORMATION 0x12
#define CARDKEY_CARD_INFORMATION 0x02
#define ISO_FILE_CARD_INFORMATION  0x0001
#define FILE_CARD_INFORMATION  0x01
#define SIZE_CARD_INFORMATION  64
#define SIZE_CARD_INFORMATION_READ  (64-29)
#define KIND_CARD_INFORMATION BACKUP_DATA_FILE

#define COMM_MODE_CARD_INFORMATION 0x03
#define ACCESSRIGHTS_CARD_INFORMATION 0x2021

/**
 * @Brief Card information.
 * File holding the card information data.
 */
typedef struct {
    uint8_t fileFormatVersion;	 	/**< The version of this file format */
    uint8_t originalCard_ID[10]; 	/**< Original card ID */
    uint8_t recycleCard_ID; 		/**< Recycle card ID */
    uint8_t cardVersion; 			/**< Card version */
    uint8_t cardCompany[3]; 		/**< Card company */
    int32_t cardFormatAmount;
    int32_t depositAmount;
    uint8_t chipType;
    uint8_t isPerso;
    uint8_t cardIssueDate[4];
    uint8_t cardExpiredDate[4];
    uint8_t loginType;
    char futureUse[29];
} fileCardInformationStruct;		/** Card information */
// Login type:
// &1 = bus-driver
// &2 = Top-up
// &4 = Hand-held
// &8 = maintenance
// &15 = Super User
// One can have multiple roles, just add them


/***
 * CNS_PILOT
 * CARD_CONFIGURATION
 * FILE 2
 */

#define APPLICATION_CARD_CONFIGURATION  0xDF0001
#define AUTHENTICATIONKEY_CARD_CONFIGURATION 0x12
#define CARDKEY_CARD_CONFIGURATION 0x02
#define ISO_FILE_CARD_CONFIGURATION  0x0002
#define FILE_CARD_CONFIGURATION  0x02
#define SIZE_CARD_CONFIGURATION  32
#define SIZE_CARD_CONFIGURATION_READ  (32-26)
#define KIND_CARD_CONFIGURATION BACKUP_DATA_FILE

#define COMM_MODE_CARD_CONFIGURATION 0x03
#define ACCESSRIGHTS_CARD_CONFIGURATION 0x2021
/**
 * File holding the card configuration data
 */
typedef struct {
    uint8_t fileFormatVersion;
    uint8_t isActive;
    uint8_t isPurseEnable;
    uint8_t topUpEnable;
    uint8_t isAutoLoadEnable;
    uint8_t isReturnEnable;
    char futureUse[26];
} fileCardConfigurationStruct;

/***
 * CNS_PILOT
 * CARD_STATUS
 * FILE 3
 */

#define APPLICATION_CARD_STATUS  0xDF0001
#define AUTHENTICATIONKEY_CARD_STATUS 0x12
#define CARDKEY_CARD_STATUS 0x02
#define ISO_FILE_CARD_STATUS  0x0003
#define FILE_CARD_STATUS  0x03
#define SIZE_CARD_STATUS  32
#define SIZE_CARD_STATUS_READ  (32-22)
#define KIND_CARD_STATUS BACKUP_DATA_FILE

#define COMM_MODE_CARD_STATUS 0x03
#define ACCESSRIGHTS_CARD_STATUS 0x2021

typedef struct {
    uint8_t fileFormatVersion;
    uint8_t cardStatus;
    uint8_t lockDate[4];
    uint8_t unLockDate[4];
    char futureUse[22];
} fileCardStatusStruct;

/***
 * CNS_PILOT
 * CARD_HOLDER_INFORMATIONS
 * FILE 4
 */

#define APPLICATION_CARD_HOLDER_INFORMATION  0xDF0001
#define AUTHENTICATIONKEY_CARD_HOLDER_INFORMATION 0x12
#define CARDKEY_CARD_HOLDER_INFORMATION 0x02
#define ISO_FILE_CARD_HOLDER_INFORMATION  0x0004
#define FILE_CARD_HOLDER_INFORMATION  0x04
#define SIZE_CARD_HOLDER_INFORMATION  128
#define SIZE_CARD_HOLDER_INFORMATION_READ  (128-16)
#define KIND_CARD_HOLDER_INFORMATION BACKUP_DATA_FILE

#define COMM_MODE_CARD_HOLDER_INFORMATION 0x03
#define ACCESSRIGHTS_CARD_HOLDER_INFORMATION 0x2021

typedef struct {
    uint8_t fileFormatVersion;
    uint8_t cardHolder_ID[10];
    char fullName[60];
    uint8_t accountType;
    uint8_t gender;
    uint8_t DOB[4];
    char orgCode[5];
    char orgName[30];
    char futureUse[16];
} fileCardHolderInformationStruct;

/*********************************************************
 * CNS_PILOT
 * TICKET_INFORMATIONS
 * FILE 5
 *********************************************************/

#define APPLICATION_TICKET_INFORMATION  0xDF0002
#define AUTHENTICATIONKEY_TICKET_INFORMATION 0x22
#define CARDKEY_TICKET_INFORMATION 0x02
#define ISO_FILE_TICKET_INFORMATION  0x0005
#define FILE_TICKET_INFORMATION  0x01
#define SIZE_TICKET_INFORMATION  32
#define SIZE_TICKET_INFORMATION_READ  (32-3)
#define KIND_TICKET_INFORMATION BACKUP_DATA_FILE

#define COMM_MODE_TICKET_INFORMATION 0x03
#define ACCESSRIGHTS_TICKET_INFORMATION 0x2021

typedef struct {
    uint8_t fileFormatVersion;
    uint8_t ticket_ID[10];
    uint8_t ticketType;
    uint8_t ticketEffectiveDate[4];
    uint16_t ticketExpireDays;
    uint8_t isRefundEnable;
    uint8_t passengerType;
    char signature[8];
    uint8_t ticketStatus;
    char futureUse[3];
} fileTicketInformationStruct;

/***
 * CNS_PILOT
 * TICKET_VALUE
 * FILE 6
 */

#define APPLICATION_TICKET_VALUE 0xDF0002
#define AUTHENTICATIONKEY_TICKET_VALUE 0x22
#define CARDKEY_TICKET_VALUE 0x02
#define ISO_FILE_TICKET_VALUE 0x000A
#define FILE_TICKET_VALUE 0x02
#define SIZE_TICKET_VALUE 4
#define SIZE_TICKET_VALUE_READ 4
#define KIND_TICKET_VALUE VALUE_FILE

#define COMM_MODE_TICKET_VALUE 0x03
#define ACCESSRIGHTS_TICKET_VALUE 0x2021

typedef struct {
    int32_t ticketValue;
} fileTicketValueStruct;

/***
 * CNS_PILOT
 * TICKET_LOG
 * FILE 7
 */

#define APPLICATION_TICKET_LOG 0xDF0002
#define AUTHENTICATIONKEY_TICKET_LOG 0x22
#define CARDKEY_TICKET_LOG 0x02
#define ISO_FILE_TICKET_LOG 0x0006
#define FILE_TICKET_LOG 0x03
#define SIZE_TICKET_LOG 32
#define SIZE_TICKET_LOG_READ (32-4)
#define KIND_TICKET_LOG CYCLIC_FILE

#define COMM_MODE_TICKET_LOG 0x03
#define ACCESSRIGHTS_TICKET_LOG 0x2021

typedef struct {
	uint8_t fileFormatVersion;
	uint8_t ticketTransactionsSubType;
	uint8_t ticketTransactionDateTime[7];
	int32_t ticketBeforeCount;
	int32_t ticketUsedCount;
	int32_t ticketValue;
	uint8_t ticketSAM_ID[7];
    char futureUse[4];
} fileTicketLogStruct;

/*********************************************************
 * CNS_PILOT
 * PURSE_INFORMATIONS
 * FILE 8
 *********************************************************/

#define APPLICATION_PURSE_INFORMATION 0xDF0003
#define AUTHENTICATIONKEY_PURSE_INFORMATION 0x32
#define ISO_FILE_PURSE_INFORMATION 0x0007
#define CARDKEY_PURSE_INFORMATION 0x02
#define FILE_PURSE_INFORMATION 0x01
#define SIZE_PURSE_INFORMATION 32
#define SIZE_PURSE_INFORMATION_READ (32-11)
#define KIND_PURSE_INFORMATION BACKUP_DATA_FILE

#define COMM_MODE_PURSE_INFORMATION 0x03
#define ACCESSRIGHTS_PURSE_INFORMATION 0x2021

typedef struct {
	uint8_t fileFormatVersion;
	uint8_t purse_ID[10];
	uint8_t purseType;
	uint8_t purseEffectiveDate[4];
	uint8_t purseExpiredDate[4];
	uint8_t purseStatus;
    char futureUse[11];
} filePurseInformationStruct;

/***
 * CNS_PILOT
 * PURSE_VALUE
 * FILE 9
 */

#define APPLICATION_PURSE_VALUE 0xDF0003
#define AUTHENTICATIONKEY_PURSE_VALUE 0x32
#define CARDKEY_PURSE_VALUE 0x02
#define ISO_FILE_PURSE_VALUE 0x000B
#define FILE_PURSE_VALUE 0x02
#define SIZE_PURSE_VALUE 4
#define SIZE_PURSE_VALUE_READ 4
#define KIND_PURSE_VALUE VALUE_FILE

#define COMM_MODE_PURSE_VALUE 0x03
#define ACCESSRIGHTS_PURSE_VALUE 0x2021

typedef struct {
    int32_t purseValue;
} filePurseValueStruct;

/***
 * CNS_PILOT
 * FILE 10
 */

#define APPLICATION_PURSE_LOG 0xDF0003
#define AUTHENTICATIONKEY_PURSE_LOG 0x32
#define CARDKEY_PURSE_LOG 0x02
#define ISO_FILE_PURSE_LOG 0x0008
#define FILE_PURSE_LOG 0x03
#define SIZE_PURSE_LOG 32
#define SIZE_PURSE_LOG_READ (32-4)
#define KIND_PURSE_LOG CYCLIC_FILE

#define COMM_MODE_PURSE_LOG 0x03
#define ACCESSRIGHTS_PURSE_LOG 0x2021

typedef struct {
	uint8_t fileFormatVersion;
	uint8_t purseTransactionsSubType;
	uint8_t purseTransactionDateTime[7];
	int32_t purseBeforeCount;
	int32_t purseUsedCount;
	int32_t purseValue;
	uint8_t purseSAM_ID[7];
    char futureUse[4];
} filePurseLogStruct;

/***
 * CNS_PILOT
 * TICKET_TRANSACTION_COUNT
 * FILE 11
 */

#define APPLICATION_TICKET_TRANSACTION_COUNT 0xDF0002
#define AUTHENTICATIONKEY_TICKET_TRANSACTION_COUNT 0x22
#define CARDKEY_TICKET_TRANSACTION_COUNT 0x02
#define ISO_FILE_TICKET_TRANSACTION_COUNT 0x002A
#define FILE_TICKET_TRANSACTION_COUNT 0x04
#define SIZE_TICKET_TRANSACTION_COUNT 4
#define SIZE_TICKET_TRANSACTION_COUNT_READ 4
#define KIND_TICKET_TRANSACTION_COUNT VALUE_FILE

#define COMM_MODE_TICKET_TRANSACTION_COUNT 0x03
#define ACCESSRIGHTS_TICKET_TRANSACTION_COUNT 0x2021

typedef struct {
    int32_t ticketTransactionCount;
} fileTicketTransactionCountStruct;

/***
 * CNS_PILOT
 * PURSE_TRANSACTION_COUNT
 * FILE 12
 */

#define APPLICATION_PURSE_TRANSACTION_COUNT 0xDF0003
#define AUTHENTICATIONKEY_PURSE_TRANSACTION_COUNT 0x32
#define CARDKEY_PURSE_TRANSACTION_COUNT 0x02
#define ISO_FILE_PURSE_TRANSACTION_COUNT 0x003A
#define FILE_PURSE_TRANSACTION_COUNT 0x04
#define SIZE_PURSE_TRANSACTION_COUNT 4
#define SIZE_PURSE_TRANSACTION_COUNT_READ 4
#define KIND_PURSE_TRANSACTION_COUNT VALUE_FILE

#define COMM_MODE_PURSE_TRANSACTION_COUNT 0x03
#define ACCESSRIGHTS_PURSE_TRANSACTION_COUNT 0x2021

typedef struct {
    int32_t purseTransactionCount;
} filePurseTransactionCountStruct;

/***
 * CNS_PILOT
 * CARD_TRANSACTION_COUNT
 * FILE 13
 */

#define APPLICATION_CARD_TRANSACTION_COUNT 0xDF0001
#define AUTHENTICATIONKEY_CARD_TRANSACTION_COUNT 0x12
#define CARDKEY_CARD_TRANSACTION_COUNT 0x02
#define ISO_FILE_CARD_TRANSACTION_COUNT 0x001A
#define FILE_CARD_TRANSACTION_COUNT 0x05
#define SIZE_CARD_TRANSACTION_COUNT 4
#define SIZE_CARD_TRANSACTION_COUNT_READ 4
#define KIND_CARD_TRANSACTION_COUNT VALUE_FILE

#define COMM_MODE_CARD_TRANSACTION_COUNT 0x03
#define ACCESSRIGHTS_CARD_TRANSACTION_COUNT 0x2021

typedef struct {
    int32_t cardTransactionCount;
} fileCardTransactionCountStruct;
/*********************************************************
 *********************************************************/

// Functions:
// 0x0001
void CopyStreamToCardInformation(fileCardInformationStruct *out, uint8_t *data);
void CopyCardInformationToStream(uint8_t *data, fileCardInformationStruct *out);

// 0x0002
void CopyStreamToCardConfiguration(fileCardConfigurationStruct *out, uint8_t *data);
void CopyCardConfigurationToStream(uint8_t *data, fileCardConfigurationStruct *out);

// 0x0003
void CopyStreamToCardStatus(fileCardStatusStruct *out, uint8_t *data);
void CopyCardStatusToStream(uint8_t *data, fileCardStatusStruct *out);

// 0x0004
void CopyStreamToCardHolderInformation(fileCardHolderInformationStruct *out, uint8_t *data);
void CopyCardHolderInformationsStructToStream(uint8_t *data, fileCardHolderInformationStruct *out);

// 0x0005
void CopyStreamToTicketInformation(fileTicketInformationStruct *out, uint8_t *data);
void CopyTicketInformationsToStream(uint8_t *data, fileTicketInformationStruct *out);

// 0x0006
void CopyStreamToTicketValue(fileTicketValueStruct *out, uint8_t *data);
void CopyTicketValueToStream(uint8_t *data, fileTicketValueStruct *out);

// 0x0007
void CopyStreamToTicketLog(fileTicketLogStruct *out, uint8_t *data);
void CopyTicketLogToStream(uint8_t *data, fileTicketLogStruct *out);

// 0x0008
void CopyStreamToPurseInformation(filePurseInformationStruct *out, uint8_t *data);
void CopyPurseInformationsToStream(uint8_t *data, filePurseInformationStruct *out);

// 0x0009
void CopyStreamToPurseValue(filePurseValueStruct *out, uint8_t *data);
void CopyPurseValueToStream(uint8_t *data, filePurseValueStruct *out);

// 0x0010
void CopyStreamToPurseLog(filePurseLogStruct *out, uint8_t *data);
void CopyPurseLogToStream(uint8_t *data, filePurseLogStruct *out);

// 0x0011
void CopyStreamToTicketTransactionCount(fileTicketTransactionCountStruct *out, uint8_t *data);
void CopyTicketTransactionCountToStream(uint8_t *data, fileTicketTransactionCountStruct *out);

// 0x0012
void CopyStreamToPurseTransactionCount(filePurseTransactionCountStruct *out, uint8_t *data);
void CopyPurseTransactionCountToStream(uint8_t *data, filePurseTransactionCountStruct *out);

// 0x0013
void CopyStreamToCardTransactionCount(fileCardTransactionCountStruct *out, uint8_t *data);
void CopyCardTransactionCountToStream(uint8_t *data, fileCardTransactionCountStruct *out);

#endif /* SMARTCARDIF_CARDINFORMATION_BUSPROJECT_H_ */
