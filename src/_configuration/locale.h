/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef CONFIGURATION_LOCALE_H_
#define CONFIGURATION_LOCALE_H_


/**
 * @file
 * @brief Locale definition
 *
 * This file contains the definition strings for the different languages.
 */


// define the language for display and sounds
typedef enum {
	NoSound = '0', SoundVN = '1', SoundEN = '2'
} SoundLanguage; /** Language for the sounds, can be no sound, VN, English */

#if TEXT_LOCALE == 'E'

#define LOCAL_BUTTON_LOGOUT "Logout"
#define LOCAL_BUTTON_TOPUP "Top-up"
#define LOCALE_BALANCE "Balance"
#define LOCALE_BUS_CONSOLE "Bus Console CNS"
#define LOCALE_BUS_LINE_PRINT "Location: %ld"
#define LOCALE_CONFIRM_TOPUP_VALUE "Confirm TopUp Value"
#define LOCALE_ERROR_CARD_EXPIRED "Card expired"
#define LOCALE_ERROR_PURSE_EXPIRED "Purse expired"
#define LOCALE_ERROR_PURSE_LOW "Low purse"
#define LOCALE_ERROR_TICKET_EXPIRED "Ticket expired"
#define LOCALE_ERROR_TICKET_LOW "Not enough balance"
#define LOCALE_EXPIRED_DATE "Expired date"
#define LOCALE_FIRMWARE_VERSION_PRINT "Firmware version: %4.4s"
#define LOCALE_FOR_TOPUP "For Top-Up"
#define LOCALE_FORMAT_ERROR_PRINT "Format error: step %d"
#define LOCALE_FORMAT_OK "Format succeeded"
#define LOCALE_FORMAT_START "Format start"
#define LOCALE_FORMATTER "Format Card"
#define LOCALE_GSM "Gsm"
#define LOCALE_HANDHELD "Handheld"
#define LOCALE_HANDHELD_INFO "Handheld: Info"
#define LOCALE_HANDHELD_READ "Handheld: Read"
#define LOCALE_LOG_IN "Log in"
#define LOCALE_MENU_CANCEL "Cancel"
#define LOCALE_MENU_CONFIRM "Confirm"
#define LOCALE_MENU_CORRECT "Correct"
#define LOCALE_MENU_ENDSHIFT "End shift"
#define LOCALE_MENU_ENDTRIP "End trip"
#define LOCALE_MENU_TICKET1 "Long Trip"
#define LOCALE_MENU_TICKET2 "Short Trip"
#define LOCALE_MENU_ENTER "Enter"
#define LOCALE_MENU_FORMAT_IS_STOPPED "Device STOPPED!"
#define LOCALE_MENU_FORMAT_START "Format start"
#define LOCALE_MENU_FORMAT_STOP "Format stop"
#define LOCALE_MENU_LOGOUT "Logout"
#define LOCALE_MENU_PENALTY "Penalty"
#define LOCALE_MENU_ROUTE "Route"
#define LOCALE_MENU_SELL_TICKET "Sell Ticket"
#define LOCALE_MENU_STARTTRIP "Start Trip"
#define LOCALE_PAY_BY_CARD "Pay by Card"
#define LOCALE_PAY_BY_CASH "Pay by Cash"
#define LOCALE_PENALTY "Penalty"
#define LOCALE_PRINT_BILL "Print bill"
#define LOCALE_QRCODE_FAIL "QRCode Fail"
#define LOCALE_QRCODE_PASS "QrCode Ok"
#define LOCALE_REJECT "Reject"
#define LOCALE_REPORT_BLACKLIST "Blacklist"
#define LOCALE_RETRY "Retry"
#define LOCALE_ROUTE  "Route"
#define LOCALE_SELECT_PAYMENT_METHOD "Select payment method"
#define LOCALE_SELECT_VALUE "Please select value"
#define LOCALE_SERVICE "Service"
#define LOCALE_SERVICE_LINE_PRINT "Device: %ld"
#define LOCALE_SYSTEM_WELCOME_PRINT "Starting up the system: %04lx"
#define LOCALE_TAP "TONY"
#define LOCALE_TAP_FOR_CONTROL "Tap for control"
#define LOCALE_TAP_TO_GO "Tap to CheckIn-Out"
#define LOCALE_TAP_YOUR_CARD "Tap your card"
#define LOCALE_TO_CONFIRM_PAYMENT "To confirm payment"
#define LOCALE_TO_PAY "To pay!"
#define LOCALE_TOPUP "TopUp"
#define LOCALE_TOPUP_BALANCE "TopUp Balance"
#define LOCALE_TOPUP_WITH "Topup with"
#define LOCALE_TRY_AGAIN "Try again"
#define LOCALE_SCHOOL "THPT Tran Dai Nghia"
#define LOCALE_ATTENDANT "Attendant"
#define LOCALE_WELCOME "You are Check-In"
#define LOCALE_NOTWELCOME "You are Check-Out"

#endif

#if TEXT_LOCALE == 'V'

#define LOCALE_BALANCE "Balance"
#define LOCALE_BUS_CONSOLE "Xe Buy\b\x81t CNS"
#define LOCALE_BUS_LINE_PRINT "Buy\b\x81t so\b\x80\b\x81: %ld"
#define LOCALE_CONFIRM_TOPUP_VALUE "Confirm TopUp Value"
#define LOCALE_ERROR_CARD_EXPIRED "The\b\x83 he\b\x80\b\x81t ha\b\x85n"
#define LOCALE_ERROR_PURSE_EXPIRED "Vi\b\x81 he\b\x80\b\x81t ha\b\x85n"
#define LOCALE_ERROR_PURSE_LOW "Vi\b\x81 he\b\x80\b\x81t tie\b\x80\b\x82n"
#define LOCALE_ERROR_TICKET_EXPIRED "Ve\b\x81 he\b\x80\b\x81t ha\b\x85n"
#define LOCALE_ERROR_TICKET_LOW "Kho\b\x80ng d\b\x88u\b\x83 Ve\b\x81"
#define LOCALE_EXPIRED_DATE "Nga\b\x82y he\b\x80\b\x81t ha\b\x85n"
#define LOCALE_FIRMWARE_VERSION_PRINT "Firmware version: %4.4s"
#define LOCALE_FOR_TOPUP "For Top-Up"
#define LOCALE_FORMAT_ERROR_PRINT "Format error: step %d"
#define LOCALE_FORMAT_OK "Format succeeded"
#define LOCALE_FORMAT_START "Format start"
#define LOCALE_FORMATTER "Formatter"
#define LOCALE_GSM "Gsm"
#define LOCALE_HANDHELD "Handheld"
#define LOCALE_LOG_IN "Log in"
#define LOCALE_MENU_CANCEL "Hu\b\x87u"
#define LOCALE_MENU_CONFIRM "Xa\b\x81c nha\b\x80\b\x85n"
#define LOCALE_MENU_ENDSHIFT "Ke\b\x80\b\x81t thu\b\x81c tuye\b\x80\b\x81"
#define LOCALE_MENU_ENDTRIP "Ke\b\x80\b\x81t thu\b\x81c chuye\b\x80\b\x81n"
#define LOCALE_MENU_ENTER "Truy Nha\b\x80\b\x85p"
#define LOCALE_MENU_FORMAT_IS_STOPPED "Device STOPPED!"
#define LOCALE_MENU_FORMAT_START "Start Format"
#define LOCALE_MENU_FORMAT_STOP "Stop Format"
#define LOCALE_MENU_LOGOUT "Thoa\b\x81t Ra"
#define LOCALE_MENU_ROUTE "Tuye\b\x80\b\x81n d\b\x88u\b\x86\b\x82o\b\x87ng"
#define LOCALE_MENU_SELL_TICKET "lmst"
#define LOCALE_MENU_STARTTRIP "Ba\b\x86\b\x81t d\b\x88a\b\x80\b\x81u chuye\b\x80\b\x81n"
#define LOCALE_PAY_BY_CARD "Pay by Card"
#define LOCALE_PAY_BY_CASH "Pay by Cash"
#define LOCALE_PENALTY "Penalty"
#define LOCALE_PRINT_BILL "Print bill"
#define LOCALE_QRCODE_FAIL "Ma\b\x84 va\b\x85ch kho\b\x80ng tha\b\x82nh co\b\x80ng"
#define LOCALE_QRCODE_PASS "Ma\b\x84 va\b\x85ch tha\b\x82nh co\b\x80ng"
#define LOCALE_REJECT "d\b\x88a\b\x86\b\x85c la\b\x85"
#define LOCALE_REPORT_BLACKLIST "So\b\x80\b\x83 d\b\x88en"
#define LOCALE_RETRY "Thu\b\x87\b\x83 la\b\x85i"
#define LOCALE_ROUTE  "Tuye\b\x85\b\x81n d\b\x88u\b\x86\b\x82o\b\x87ng"
#define LOCALE_SELECT_PAYMENT_METHOD "Select payment method"
#define LOCALE_SELECT_VALUE "Please select value"
#define LOCALE_SERVICE "Tuye\b\x80\b\x81n"
#define LOCALE_SERVICE_LINE_PRINT "lslp %ld"
#define LOCALE_SYSTEM_WELCOME_PRINT "Starting up the system: %04lx"
#define LOCALE_TAP "Tap"
#define LOCALE_TAP_FOR_CONTROL "Tap for control"
#define LOCALE_TAP_TO_GO "Cha\bx85m va\b\x82 d\b\x88i"
#define LOCALE_TAP_YOUR_CARD "Tap your card"
#define LOCALE_TO_CONFIRM_PAYMENT "To confirm payment"
#define LOCALE_TO_PAY "To pay!"
#define LOCALE_TOPUP "TopUp"
#define LOCALE_TOPUP_BALANCE "So\b\x80\b\x81 du\b\x87"
#define LOCALE_TRY_AGAIN "thu\b\x87\b\x83 la\b\x85i"
#define LOCALE_ATTENDANT "Diem Danh"
#define LOCALE_WELCOME "Xin Cha\b\x82o"

#endif

#endif /* CONFIGURATION_LOCALE_H_ */
