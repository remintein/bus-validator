/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef CONFIGURATION_WIFIACCESSPOINTS_H_
#define CONFIGURATION_WIFIACCESSPOINTS_H_

/**
 * @file
 * @brief Structures with information on the access points. Will need to move to a file in the device...
 */

typedef struct accessPointDefinition {
    char *name;
    char *password;
} accessPointDefinition_type; /**< Combination name and password structure */


/**
 * List of access points
 */
static accessPointDefinition_type accessPoints[] = {

{ "DutchNFCConsult", "!CNS1705Wifi" },

{ "AUVIET", "Tonynguyen0903325567" },

{ "Dutch NFC", "92Y58MMOZ5PM6" },

{ "Dutch NFC_5G", "92Y58MMOZ5PM6" },

{ "6533NZ20u", "Touring01" },

#if DNFCC_ENABLE_WIFI == 1

{ "BongSen Hotel","11111100" },

{ "BongSen Hotel Extend","11111100" },

{ "CNS-Guest","sgi.wifi0415" },

{ "HUONGSENHOTEL","huongsen2018" },

#endif
};

#endif /* CONFIGURATION_WIFIACCESSPOINTS_H_ */
