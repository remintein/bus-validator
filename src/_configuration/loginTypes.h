/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef CONFIGURATION_LOGINTYPES_H_
#define CONFIGURATION_LOGINTYPES_H_
/**
 * @file
 * @brief Defines the bit in the card which is to be set for what device
 *
 * Each device has a specific bit to set for login.
 * A card with more functions (like the superuser card) is an or between these login types
 */
#define LOGINTYPE_BUSDRIVER 1 /** For bus driver */
#define LOGINTYPE_HANDHELD 2 /** For handheld */
#define LOGINTYPE_TOPUP 4 /** For top-up */
#define LOGINTYPE_MAINTENANCE 8 /** Maintenance card */

#endif /* CONFIGURATION_LOGINTYPES_H_ */
