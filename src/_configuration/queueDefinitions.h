/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef CONFIGURATION_QUEUEDEFINITIONS_H_
#define CONFIGURATION_QUEUEDEFINITIONS_H_

/**
 * @file
 * @brief Definition of all queue lengths in size and nubmer of records.
 *
 * Each set comes with 2 defines:
 * 1: The size of one record (.._BUFFER_SIZE)
 * 2: max number of records in the queue (.._QUEUE_LENGTH)
 */


#define SOUND_BUFFER_SIZE 2
#define SOUND_QUEUE_LENGTH 3

#define CANBUS_BUFFER_SIZE 20
#define CANBUS_QUEUE_LENGTH 20

#define CLOCK_BUFFER_SIZE 9
#define CLOCK_QUEUE_LENGTH 3

#define DATADISTRIBUTOR_BUFFER_SIZE 128
#define DATADISTRIBUTOR_QUEUE_LENGTH 10

#define DISPLAY_BUFFER_SIZE 80
#define DISPLAY_QUEUE_LENGTH 20

#define QRCODE_BUFFER_SIZE 1
#define QRCODE_QUEUE_LENGTH 3

#define FLASH_PROGRAMMER_BUFFER_SIZE (BLOCKLENGTH_FILEDATA * 2) + 52
#define FLASH_PROGRAMMER_QUEUE_LENGTH 2

#define GPS_BUFFER_SIZE 25
#define GPS_QUEUE_LENGTH 3

#define GSM_BUFFER_SIZE 1
#define GSM_QUEUE_LENGTH 3

#define MENU_BUFFER_SIZE 40
#define MENU_QUEUE_LENGTH 10

#define OTG_BUFFER_SIZE 1
#define OTG_QUEUE_LENGTH 3

#define PRINTER_BUFFER_SIZE 40
#define PRINTER_QUEUE_LENGTH 3

#define SAM_BUFFER_SIZE 5
#define SAM_QUEUE_LENGTH 3

#define SDIO_BUFFER_SIZE 52 + (BLOCKLENGTH_FILEDATA * 2)
#define SDIO_QUEUE_LENGTH 2

#define SMARTCARDIF_BUFFER_SIZE 1
#define SMARTCARDIF_QUEUE_LENGTH 3

#define TOUCH_BUFFER_SIZE 5
#define TOUCH_QUEUE_LENGTH 3

#define WIFI_BUFFER_SIZE 1
#define WIFI_QUEUE_LENGTH 3

#define MAX_BUFFER_SIZE SDIO_BUFFER_SIZE /** Maximum size of any buffer */

#endif /* CONFIGURATION_QUEUEDEFINITIONS_H_ */
