/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef CONFIGURATION_DEVICESELECTOR_DEFINES_H_
#define CONFIGURATION_DEVICESELECTOR_DEFINES_H_

/**
 * @file
 * @brief Defines for selecting what to build
 */


#define CNS_BUSCONSOLE  		'B'	/**< Bus console to be build */
#define CNS_VALIDATOR   		'V' /**< Validator to be build */
#define CNS_HANDHELD    		'H' /**< Handheld to be build */
#define CNS_TOPUP       		'T' /**< Top-up to be build */
#define CNS_FORMATTER_BUS   	'F' /**< Formatter for the bus project to be build */

//
/**
 *
 * Sam enabled can be 'S', 'X' or 'N'
 * S = S-Mode
 * X = X-X-mode
 * N = Softkeys
 */
#define DNFCC_SAM_S_MODE 		'S'	/**< With SAM S-mode */
#define DNFCC_SAM_X_MODE 		'X' /**< With Sam X-mode */
#define DNFCC_SAM_SOFTKEYS 		'N' /**< With Sam and Soft-keys */


#define TEXT_LOCALE_ENGLISH		'E' /**< Compile the Locale English */
#define TEXT_LOCALE_VIETNAMESE	'V' /**< Compile the locale Vietnamese */


#endif /* CONFIGURATION_DCONFIGURATION_DEVICESELECTOR_DEFINES_H_EVICESELECTOR_H_ */
