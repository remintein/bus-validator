/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef CONFIGURATION_DEVICESELECTOR_H_
#define CONFIGURATION_DEVICESELECTOR_H_

#include "deviceSelector_defines.h"

/**
 * @file
 * @brief Defines how the code is build, which device etc.
 */

// Firmware version: must be a string of 4 characters
#define FIRMWARE_VERSION "V002"

// For possible values, see deviceSelector_defines.h
#define CURRENT_TARGET CNS_VALIDATOR


#define TEXT_LOCALE						TEXT_LOCALE_ENGLISH

/*********************************************************
 * BLOCKLENGTH_FILEDATA - Size of the data retrieved for files from the backend
 * It must be a division of 512 (eg. 64, 128, 256) due to limitations in the writing of the file (FileUpdater.c)
 *
 * Some defines are derived from this - like the SDIO message buffer length.
 *********************************************************/
#define BLOCKLENGTH_FILEDATA	    256


#define DNFCC_SOUND 				1	// Sound 0 is very short compiled sounds, Sound 1 is only Vietnam, Sound 2 = all
#define DNFCC_NOLOGIN 				1	// No card for login required, autologon
#define VALIDATOR_SIMULATOR 		0	// creates CAN messages to bus console
#define DNFCC_ENABLE_WIFI			0	// Enable CNS access points + hotel access points
#define DNFCC_ADD_GSM_DISABLE		0	// Add a demo button to disable GSM


//
// Defines different for each device. DO NOT CHANGE as they are calculated from the configuration above
//

#if CURRENT_TARGET == CNS_HANDHELD

#define DISPLAY_SIZE 7
#define CURRENT_TARGET_BACKEND "HH"
#undef DNFCC_SOUND
#define DNFCC_SOUND 0

#elif CURRENT_TARGET == CNS_BUSCONSOLE

#define DISPLAY_SIZE 7
#define CURRENT_TARGET_BACKEND "BC"

#elif CURRENT_TARGET == CNS_VALIDATOR

#define DISPLAY_SIZE 7
#define CURRENT_TARGET_BACKEND "VA"

#elif CURRENT_TARGET == CNS_TOPUP

#define DISPLAY_SIZE 7
#define CURRENT_TARGET_BACKEND "TU"
#undef DNFCC_SOUND
#define DNFCC_SOUND 0

#elif CURRENT_TARGET == CNS_FORMATTER_BUS

#define DISPLAY_SIZE 7
#define CURRENT_TARGET_BACKEND "FO"

#endif

#define XSTR(x) STR(x)
#define STR(x) #x

#ifndef CURRENT_TARGET
#error Define a CURRENT_TARGET to build
#endif

#endif /* CONFIGURATION_DEVICESELECTOR_H_ */
