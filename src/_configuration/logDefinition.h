/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef CONFIGURATION_LOGDEFINITION_H_
#define CONFIGURATION_LOGDEFINITION_H_

/**
 * @file
 * @brief Switch logging for a specific module  on or off. 0 = off, 1 = on.
 */

#define LOGGING_ON_CAN 0
#define COLLISION_LOG 0
#define OTG_LOG 0
#define QRCODE_LOG 0
#define RC663_LOG 0
#define RC663_LOWLEVEL_LOG 0
#define RC663_TIMING_LOG 0
#define RTC_LOG 0
#define SAM_LOG 0
#define SDIO_LOG 0
#define SERIAL_GSM_LOG 0
#define SERIAL_PRINTER_LOG 0
#define SERIAL_QRCODE_LOG 0
#define SERIAL_WIFI_LOG 0
#define TOUCH_LOG 0
#define UPLOAD_LOG 0

#endif /* CONFIGURATION_LOGDEFINITION_H_ */
