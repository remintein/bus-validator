/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../platform_config.h"
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "stm32f4xx_hal.h"
#include "../smartcardif/DirectIo.h"

//extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef nfccSmartcardIf_HandleStructure;
extern DAC_HandleTypeDef hdac;

SMARTCARD_HandleTypeDef smartcard_sam;

void UART_GSM_Init(void);
static void SMARTCARD_SAM_Init(void);

void UART_QRCODEPRINTER_Init(void);
void UART_WIFI_Init(void);
void I2C_Touch_Init(void);
void UART_Display_Init(void);
void Error_Handler();
void UART_RS485_Init();
void MX_RTC_Init();
void MX_RNG_Init();
void I2C_Touch_MspInit();

/**
 * @file
 * @brief Contains the main hardware initialization
 */

/**
 * @addtogroup hardware
 *
 * @{
 */

/**
 * @brief Configure the MCU's internal clocks
 */
void SystemClock_Config(void) {

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE()
	;

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI
			| RCC_OSCILLATORTYPE_LSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.LSIState = RCC_LSI_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 50;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
	RCC_OscInitStruct.PLL.PLLQ = 7;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}

	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 336;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 7;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) {
		Error_Handler();
	}

// Added next for RTC
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
	PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK) {
		Error_Handler();
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

}

/**
 * @brief The main hardware initialization routine. Called from main()
 *
 */
void initializeHardware() {
	SystemClock_Config();

	// Enable clock on the modules of the STM32F4 which are needed
	// GPIO enable
	__GPIOA_CLK_ENABLE()
	;
	__GPIOB_CLK_ENABLE()
	;
	__GPIOC_CLK_ENABLE()
	;
	__GPIOD_CLK_ENABLE()
	;
	__GPIOE_CLK_ENABLE()
	;
	__GPIOF_CLK_ENABLE()
	;
	__GPIOG_CLK_ENABLE()
	;
	__GPIOH_CLK_ENABLE()
	;
	// SPI busses
	__SPI1_CLK_ENABLE()
	;
	__SPI2_CLK_ENABLE()
	;
	__CAN1_CLK_ENABLE()
	;
	__HAL_RCC_I2C1_CLK_ENABLE()
	;
	__HAL_RCC_I2C2_CLK_ENABLE()
//	__I2C2_CLK_ENABLE()
	;

	// UART
	__HAL_RCC_USART1_CLK_ENABLE()
	;
	__HAL_RCC_USART2_CLK_ENABLE()
	;
	__HAL_RCC_USART3_CLK_ENABLE()
	;
	__HAL_RCC_UART4_CLK_ENABLE()
	;
	__HAL_RCC_USART6_CLK_ENABLE()
	;

	// SDIO SD-card
//	__HAL_SD_SDIO_ENABLE();
	// DAC
	__HAL_RCC_DAC_CLK_ENABLE()
	;
	// USB
//	__USB_CLK_ENABLE();
	__RNG_CLK_ENABLE()
	;

	MX_RNG_Init();

	// DMA

	GPIO_InitTypeDef GPIO_InitStruct;

	// SAM: PA2
	GPIO_InitStruct.Pin = SAM_PIN_RXTX;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM_GPIO_RXTX, &GPIO_InitStruct);
	HAL_GPIO_WritePin(SAM_GPIO_RXTX, SAM_PIN_RXTX, GPIO_PIN_SET);

	HAL_GPIO_WritePin(SAM_GPIO_RXTX, SAM_PIN_RXTX, GPIO_PIN_RESET);

	// SAM: PA2
	GPIO_InitStruct.Pin = SAM_PIN_RXTX;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM_GPIO_RXTX, &GPIO_InitStruct);

	// RC663: NSS
	GPIO_InitStruct.Pin = CLRC663_PIN_NSS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(CLRC663_GPIO_NSS, &GPIO_InitStruct);

	// RC663: PDOWN
	GPIO_InitStruct.Pin = CLRC663_PIN_PDOWN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(CLRC663_GPIO_PDOWN, &GPIO_InitStruct);

	// RC663 IRQ
	GPIO_InitStruct.Pin = CLRC663_PIN_IRQ;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(CLRC663_GPIO_IRQ, &GPIO_InitStruct);
	// CANBUS: RX1
	GPIO_InitStruct.Pin = CANBUS_PIN_RX1;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
	HAL_GPIO_Init(CANBUS_GPIO_RX1, &GPIO_InitStruct);

	// CANBUS: TX1
	GPIO_InitStruct.Pin = CANBUS_PIN_TX1;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
	HAL_GPIO_Init(CANBUS_GPIO_TX1, &GPIO_InitStruct);
	// DAC
	GPIO_InitStruct.Pin = DAC_PIN_DAC1;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(DAC_GPIO_DAC1, &GPIO_InitStruct);

	// Aart Test:

	UART_Display_Init();

	// RC663 - Set-up the SPI
	nfccSmartcardIf_HandleStructure.Instance = CLRC663_SPI;
	nfccSmartcardIf_HandleStructure.Init.Mode = SPI_MODE_MASTER;
	nfccSmartcardIf_HandleStructure.Init.Direction =
	SPI_DIRECTION_2LINES;
	nfccSmartcardIf_HandleStructure.Init.DataSize = SPI_DATASIZE_8BIT;
	nfccSmartcardIf_HandleStructure.Init.CLKPolarity = SPI_POLARITY_LOW;
	nfccSmartcardIf_HandleStructure.Init.CLKPhase = SPI_PHASE_1EDGE;
	nfccSmartcardIf_HandleStructure.Init.NSS = SPI_NSS_SOFT;
	nfccSmartcardIf_HandleStructure.Init.BaudRatePrescaler =
	SPI_BAUDRATEPRESCALER_256; // 128 -> 256 TODO Check
	nfccSmartcardIf_HandleStructure.Init.FirstBit = SPI_FIRSTBIT_MSB;
	nfccSmartcardIf_HandleStructure.Init.TIMode = SPI_TIMODE_DISABLED;
	nfccSmartcardIf_HandleStructure.Init.CRCCalculation =
	SPI_CRCCALCULATION_DISABLED;
	HAL_SPI_Init(&nfccSmartcardIf_HandleStructure);
	// Setup the UARTS
	UART_GSM_Init();
	SMARTCARD_SAM_Init();
	UART_QRCODEPRINTER_Init();

	// SAM: Reset
	GPIO_InitStruct.Pin = SAM_PIN_RESET;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM_GPIO_RESET, &GPIO_InitStruct);

	// SAM: CS-io
	GPIO_InitStruct.Pin = SAM1_PIN_CS_IO;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM1_GPIO_CS_IO, &GPIO_InitStruct);
//	HAL_GPIO_WritePin(SAM1_GPIO_CS_IO, SAM1_PIN_CS_IO, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SAM1_GPIO_CS_IO, SAM1_PIN_CS_IO, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = SAM2_PIN_CS_IO;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM2_GPIO_CS_IO, &GPIO_InitStruct);
//	HAL_GPIO_WritePin(SAM2_GPIO_CS_IO, SAM2_PIN_CS_IO, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SAM2_GPIO_CS_IO, SAM2_PIN_CS_IO, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = SAM3_PIN_CS_IO;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM3_GPIO_CS_IO, &GPIO_InitStruct);
//	HAL_GPIO_WritePin(SAM2_GPIO_CS_IO, SAM2_PIN_CS_IO, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SAM3_GPIO_CS_IO, SAM3_PIN_CS_IO, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = SAM4_PIN_CS_IO;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM4_GPIO_CS_IO, &GPIO_InitStruct);
//	HAL_GPIO_WritePin(SAM2_GPIO_CS_IO, SAM2_PIN_CS_IO, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SAM4_GPIO_CS_IO, SAM4_PIN_CS_IO, GPIO_PIN_SET);

	// SAM: CS-i2c
	GPIO_InitStruct.Pin = SAM1_PIN_CS_I2C;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM1_GPIO_CS_I2C, &GPIO_InitStruct);
//	HAL_GPIO_WritePin(SAM1_GPIO_CS_I2C, SAM1_PIN_CS_I2C, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SAM1_GPIO_CS_I2C, SAM1_PIN_CS_I2C, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = SAM2_PIN_CS_I2C;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM2_GPIO_CS_I2C, &GPIO_InitStruct);
//	HAL_GPIO_WritePin(SAM2_GPIO_CS_I2C, SAM2_PIN_CS_I2C, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SAM2_GPIO_CS_I2C, SAM2_PIN_CS_I2C, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = SAM3_PIN_CS_I2C;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM3_GPIO_CS_I2C, &GPIO_InitStruct);
//	HAL_GPIO_WritePin(SAM3_GPIO_CS_I2C, SAM3_PIN_CS_I2C, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SAM3_GPIO_CS_I2C, SAM3_PIN_CS_I2C, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = SAM4_PIN_CS_I2C;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(SAM4_GPIO_CS_I2C, &GPIO_InitStruct);
//	HAL_GPIO_WritePin(SAM3_GPIO_CS_I2C, SAM3_PIN_CS_I2C, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(SAM4_GPIO_CS_I2C, SAM4_PIN_CS_I2C, GPIO_PIN_SET);

	GPIO_InitStruct.Pin = RS485_PIN_DE;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(RS485_GPIO_DE, &GPIO_InitStruct);
	HAL_GPIO_WritePin(RS485_GPIO_DE, RS485_PIN_DE, GPIO_PIN_RESET);

	GPIO_InitStruct.Pin = RS485_PIN_CS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(RS485_GPIO_CS, &GPIO_InitStruct);
	HAL_GPIO_WritePin(RS485_GPIO_CS, RS485_PIN_CS, GPIO_PIN_RESET);

	UART_WIFI_Init();
	UART_RS485_Init();
	I2C_Touch_Init();

	MX_RTC_Init();
//	MX_USB_DEVICE_Init(); -- Is done in otg.c...

}

/**
 * @brief the SAM init routine
 */
static void SMARTCARD_SAM_Init(void) {

	smartcard_sam.Instance = USART2;
	smartcard_sam.Init.BaudRate = 18810;
	smartcard_sam.Init.WordLength = SMARTCARD_WORDLENGTH_9B;
	smartcard_sam.Init.StopBits = SMARTCARD_STOPBITS_0_5;
	smartcard_sam.Init.Parity = SMARTCARD_PARITY_EVEN;
	smartcard_sam.Init.Mode = SMARTCARD_MODE_TX_RX;
	smartcard_sam.Init.CLKPolarity = SMARTCARD_POLARITY_HIGH;
	smartcard_sam.Init.CLKPhase = SMARTCARD_PHASE_1EDGE;
	smartcard_sam.Init.CLKLastBit = SMARTCARD_LASTBIT_DISABLE;
	smartcard_sam.Init.Prescaler = 3;  // 6
	smartcard_sam.Init.GuardTime = 8;
	smartcard_sam.Init.NACKState = SMARTCARD_NACK_DISABLE;
	if (HAL_SMARTCARD_Init(&smartcard_sam) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief The MSP init for touch
 */
void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c) {
	if (hi2c->Instance == TOUCH_I2C) {
		I2C_Touch_MspInit();
	}
}

/**
 * @brief the Smartcard Pin init routine
 */
void HAL_SMARTCARD_MspInit(SMARTCARD_HandleTypeDef* hsmartcard) {

	GPIO_InitTypeDef GPIO_InitStruct;
	if (hsmartcard->Instance == SAM_UART) {
		/* USER CODE BEGIN USART2_MspInit 0 */

		/* USER CODE END USART2_MspInit 0 */
		/* Peripheral clock enable */
		__HAL_RCC_USART2_CLK_ENABLE()
		;

		/**USART2 GPIO Configuration
		 PA2     ------> USART2_TX
		 PD7     ------> USART2_CK
		 */
		GPIO_InitStruct.Pin = SAM_PIN_RXTX;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
		HAL_GPIO_Init(SAM_GPIO_RXTX, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = SAM_PIN_CK;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
		HAL_GPIO_Init(SAM_GPIO_CK, &GPIO_InitStruct);

		/* Peripheral interrupt init */
		HAL_NVIC_SetPriority(USART2_IRQn, 0, 1);
		HAL_NVIC_EnableIRQ(USART2_IRQn);
	}
}

/**
 * @brief the DAC init routine
 */
void MX_DAC_Init(void) {

	DAC_ChannelConfTypeDef sConfig;

	/**DAC Initialization
	 */
	hdac.Instance = DAC;
	if (HAL_DAC_Init(&hdac) != HAL_OK) {
		Error_Handler();
	}

	/**DAC channel OUT1 config
	 */
	sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
	if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK) {
		Error_Handler();
	}

}
/** @} */
