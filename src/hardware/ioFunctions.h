/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef HARDWARE_IOFUNCTIONS_H_
#define HARDWARE_IOFUNCTIONS_H_

#include "../platform_config.h"
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "stm32f4xx_hal.h"

// Display functions
void Display_doWaitForReady();
uint8_t DisplayTransmitReceive(uint8_t *in, uint8_t *out);
uint8_t DisplayTransmitReceive3(uint8_t *in, uint8_t *out);
void Display_CmdWrite(uint8_t cmd);
void Display_DataWrite(uint8_t Data);
uint8_t Display_DataRead(void);
uint8_t Display_StatusRead(void);
void Display_perFormHwReset();
void Display_WriteDir(uint8_t Cmd, uint8_t Data);

#endif /* HARDWARE_IOFUNCTIONS_H_ */
