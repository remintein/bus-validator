/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../platform_config.h"

extern SPI_HandleTypeDef displayHspi;

/**
 * @file
 * @brief Display initialization
 */

/**
 * @addtogroup hardware
 *
 * @{
 */

/**
 * @brief Initializes the Display pins
 */
void UART_Display_Gpio(void) {
	GPIO_InitTypeDef GPIO_InitStruct;
	//
	// Display: WAIT
	GPIO_InitStruct.Pin = DISPLAY_PIN_WAIT;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(DISPLAY_GPIO_WAIT, &GPIO_InitStruct);

	// Display: NSS
	GPIO_InitStruct.Pin = DISPLAY_PIN_NSS;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(DISPLAY_GPIO_NSS, &GPIO_InitStruct);

	// Display: RESET
	GPIO_InitStruct.Pin = DISPLAY_PIN_RESET;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(DISPLAY_GPIO_RESET, &GPIO_InitStruct);

	// Display: RGB pins
	GPIO_InitStruct.Pin = DISPLAY_PIN_LED_GREEN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(DISPLAY_GPIO_LED_GREEN, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = DISPLAY_PIN_LED_BLUE;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(DISPLAY_GPIO_LED_BLUE, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = DISPLAY_PIN_LED_RED;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(DISPLAY_GPIO_LED_RED, &GPIO_InitStruct);

	// Display: MISO
	GPIO_InitStruct.Pin = DISPLAY_PIN_MISO; // No Mode AFPP???
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP; // GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(DISPLAY_GPIO_MISO, &GPIO_InitStruct);

	// Display: MOSI
	GPIO_InitStruct.Pin = DISPLAY_PIN_MOSI;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(DISPLAY_GPIO_MOSI, &GPIO_InitStruct);

	//  Display: SCLK
	GPIO_InitStruct.Pin = DISPLAY_PIN_SCK;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
	HAL_GPIO_Init(DISPLAY_GPIO_SCK, &GPIO_InitStruct);

}

/**
 * @brief the init routine for the display
 */
void UART_Display_Init(void) {
	// Display: Set-up the SPI
	displayHspi.Instance = DISPLAY_SPI;
	displayHspi.Init.Mode = SPI_MODE_MASTER;
	displayHspi.Init.Direction = SPI_DIRECTION_2LINES;
	displayHspi.Init.DataSize = SPI_DATASIZE_8BIT;
	displayHspi.Init.CLKPolarity = SPI_POLARITY_LOW;
	displayHspi.Init.CLKPhase = SPI_PHASE_1EDGE;
	displayHspi.Init.NSS = SPI_NSS_SOFT;
	displayHspi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
	displayHspi.Init.FirstBit = SPI_FIRSTBIT_MSB;
	displayHspi.Init.TIMode = SPI_TIMODE_DISABLED;
	displayHspi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;
	HAL_SPI_Init(&displayHspi);
}

/** @} */
