/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../platform_config.h"

UART_HandleTypeDef huart_rs485;
void Error_Handler();

/**
 * @file
 * @brief RS485 initialization
 */

/**
 * @addtogroup hardware
 *
 * @{
 */

/**
 * @brief Initializes the WIFI pins
 */
void UART_RS485_MspInit() {
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = RS485_PIN_TX;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = QRCODEPRINTER_AF;
	HAL_GPIO_Init(RS485_GPIO_TX, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = RS485_PIN_RX;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = QRCODEPRINTER_AF;
	HAL_GPIO_Init(RS485_GPIO_RX, &GPIO_InitStruct);

	HAL_NVIC_SetPriority(RS485_UART_IRQ, 13, 9);
	HAL_NVIC_EnableIRQ(RS485_UART_IRQ);
}

/**
 * @brief the RS485 init routine
 */
void UART_RS485_Init(void) {

	huart_rs485.Instance = RS485_UART;
	huart_rs485.Init.BaudRate = 9600;
	huart_rs485.Init.WordLength = UART_WORDLENGTH_8B;
	huart_rs485.Init.StopBits = UART_STOPBITS_1;
	huart_rs485.Init.Parity = UART_PARITY_NONE;
	huart_rs485.Init.Mode = UART_MODE_TX_RX;
	huart_rs485.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart_rs485.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart_rs485) != HAL_OK) {
		Error_Handler();
	}
}
/** @} */
