/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../platform_config.h"

UART_HandleTypeDef huart_wifi;
void Error_Handler();

/**
 * @file
 * @brief Wifi initialization
 */

/**
 * @addtogroup hardware
 *
 * @{
 */

/**
 * @brief Initializes the WIFI pins
 */
void UART_WIFI_MspInit() {
	GPIO_InitTypeDef GPIO_InitStruct;
	// WIFI: Reset
	GPIO_InitStruct.Pin = WIFI_PIN_RST;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(WIFI_GPIO_RST, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = WIFI_PIN_TX;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = WIFI_AF;
	HAL_GPIO_Init(WIFI_GPIO_TX, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = WIFI_PIN_RX;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = WIFI_AF;
	HAL_GPIO_Init(WIFI_GPIO_RX, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = WIFI_PIN_GPIO0;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(WIFI_GPIO_GPIO0, &GPIO_InitStruct);

	HAL_GPIO_WritePin(WIFI_GPIO_GPIO0, WIFI_PIN_GPIO0, GPIO_PIN_RESET);

	HAL_NVIC_SetPriority(WIFI_UART_IRQ, 14, 10);
	HAL_NVIC_EnableIRQ(WIFI_UART_IRQ);

}
/**
 * @brief the WIFI init routine
 */
void UART_WIFI_Init(void) {

	huart_wifi.Instance = WIFI_UART;
	huart_wifi.Init.BaudRate = 115200;
	huart_wifi.Init.WordLength = UART_WORDLENGTH_8B;
	huart_wifi.Init.StopBits = UART_STOPBITS_1;
	huart_wifi.Init.Parity = UART_PARITY_NONE;
	huart_wifi.Init.Mode = UART_MODE_TX_RX;
	huart_wifi.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart_wifi.Init.OverSampling = UART_OVERSAMPLING_8;

	if (HAL_UART_Init(&huart_wifi) != HAL_OK) {
		Error_Handler();
	}
}
/** @} */
