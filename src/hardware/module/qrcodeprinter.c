/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../platform_config.h"

UART_HandleTypeDef huart_qrcodeprinter;
void Error_Handler();

/**
 * @file
 * @brief QRcode + printer initialization
 */

/**
 * @addtogroup hardware
 *
 * @{
 */

/**
 * @brief Initializes the WIFI pins
 */
void UART_QRCODEPRINTER_MspInit() {
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = PRINTER_PIN_TX;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = QRCODEPRINTER_AF;
	HAL_GPIO_Init(PRINTER_GPIO_TX, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = QRCODE_PIN_RX;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = QRCODEPRINTER_AF;
	HAL_GPIO_Init(QRCODE_GPIO_RX, &GPIO_InitStruct);

	HAL_NVIC_SetPriority(QRCODEPRINTER_UART_IRQ, 14, 9);
	HAL_NVIC_EnableIRQ(QRCODEPRINTER_UART_IRQ);
}

/**
 * @brief the QRcode init routine
 */
void UART_QRCODEPRINTER_Init(void) {

	huart_qrcodeprinter.Instance = QRCODEPRINTER_UART;
	huart_qrcodeprinter.Init.BaudRate = 9600;
	huart_qrcodeprinter.Init.WordLength = UART_WORDLENGTH_8B;
	huart_qrcodeprinter.Init.StopBits = UART_STOPBITS_1;
	huart_qrcodeprinter.Init.Parity = UART_PARITY_NONE;
	huart_qrcodeprinter.Init.Mode = UART_MODE_TX_RX;
	huart_qrcodeprinter.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart_qrcodeprinter.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart_qrcodeprinter) != HAL_OK) {
		Error_Handler();
	}
}
