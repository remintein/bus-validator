/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../../project_includes.h"
#include "../../platform_config.h"

void Error_Handler();

/**
 * @file
 * @brief Touch initialization
 */

/**
 * @addtogroup hardware
 *
 * @{
 */

/**
 * @brief Initializes the Touch pins
 */
I2C_HandleTypeDef i2cTouch;
void I2C_Touch_MspInit() {
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = TOUCH_PIN_SCL;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = TOUCH_SCL_AF;
	HAL_GPIO_Init(TOUCH_GPIO_SCL, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = TOUCH_PIN_SDA;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = TOUCH_SDA_AF;
	HAL_GPIO_Init(TOUCH_GPIO_SDA, &GPIO_InitStruct);

	/* Peripheral clock enable */
	__HAL_RCC_I2C2_CLK_ENABLE();

	// Interrupts
	HAL_NVIC_SetPriority(I2C2_ER_IRQn, 15, 1);
	HAL_NVIC_EnableIRQ(I2C2_ER_IRQn);
	HAL_NVIC_SetPriority(I2C2_EV_IRQn, 15, 2);
	HAL_NVIC_EnableIRQ(I2C2_EV_IRQn);
}

/**
 * @brief the touch init routine
 */
void I2C_Touch_Init(void) {
	i2cTouch.Instance = TOUCH_I2C;

	i2cTouch.Instance = I2C2;
	i2cTouch.Init.ClockSpeed = 200000;
	i2cTouch.Init.DutyCycle = I2C_DUTYCYCLE_2;
	i2cTouch.Init.OwnAddress1 = 0;
	i2cTouch.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	i2cTouch.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	i2cTouch.Init.OwnAddress2 = 0;
	i2cTouch.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	i2cTouch.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

	if (HAL_I2C_Init(&i2cTouch) != HAL_OK) {
		Error_Handler();
	}
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = TOUCH_PIN_WAKE;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = 0;
	HAL_GPIO_Init(TOUCH_GPIO_WAKE, &GPIO_InitStruct);
	HAL_GPIO_WritePin(TOUCH_GPIO_WAKE, TOUCH_PIN_WAKE, GPIO_PIN_SET);

}
/** @} */
