/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "ioFunctions.h"
#include "../platform_config.h"

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

// Handels...

SPI_HandleTypeDef displayHspi;

/**
 * @file
 * @brief Contains the hardware functions
 */

/**
 * @addtogroup hardware
 *
 * @{
 */

/**
 * @brief Wait for pin DISPLAY_WAIT to be 1
 */
void Display_doWaitForReady() {
	int loop = 0;
	int loop2 = 0;
	while (HAL_GPIO_ReadPin(DISPLAY_GPIO_WAIT, DISPLAY_PIN_WAIT) == 0) {
		loop++;
		if (loop > 1000) {
			loop = 0;
			vTaskDelay(1);
			loop2++;
			if (loop2 > 100)
				break;
		}
	}
}

/**
 * @brief Transmit/receive to the display 2 bytes
 *
 * @param in - the input data
 * @param out - the output data
 */
uint8_t DisplayTransmitReceive(uint8_t *in, uint8_t *out) {

	Display_doWaitForReady();

	HAL_GPIO_WritePin(DISPLAY_GPIO_NSS,
	DISPLAY_PIN_NSS, GPIO_PIN_RESET);
	uint8_t t = HAL_SPI_TransmitReceive(&displayHspi, in, out, 2, 1000);
	HAL_GPIO_WritePin(DISPLAY_GPIO_NSS,
	DISPLAY_PIN_NSS, GPIO_PIN_SET);

	return t;
}

/**
 * @brief Transmit/receive to the display 3 bytes
 *
 * @param in - the input data
 * @param out - the output data
 */
uint8_t DisplayTransmitReceive3(uint8_t *in, uint8_t *out) {

	Display_doWaitForReady();

	HAL_GPIO_WritePin(DISPLAY_GPIO_NSS,
	DISPLAY_PIN_NSS, GPIO_PIN_RESET);
	uint8_t t = HAL_SPI_TransmitReceive(&displayHspi, in, out, 2, 1000);
	HAL_GPIO_WritePin(DISPLAY_GPIO_NSS,
	DISPLAY_PIN_NSS, GPIO_PIN_SET);

	return t;
}

/**
 * @brief Write command to the display
 *
 * @param cmd - the command to write
 */
void Display_CmdWrite(uint8_t cmd) {
	uint8_t b[2];
	uint8_t ret[2];
	b[0] = 0x80;
	b[1] = cmd;

	DisplayTransmitReceive(b, ret);
}

/**
 * @brief WRite data or register to the display
 *
 * @param Data - the data to write
 */
void Display_DataWrite(uint8_t Data) {
	uint8_t b[2];
	uint8_t ret[2];
	b[0] = 0x00;
	b[1] = Data;

	DisplayTransmitReceive(b, ret);
}

/**
 * @brief Read data from the display
 *
 * @return the data
 */

uint8_t Display_DataRead(void) {
	uint8_t b[2];
	uint8_t ret[2];
	b[0] = 0x40;
	b[1] = 0x00;

	DisplayTransmitReceive(b, ret);

	return ret[1];
}

/**
 * @brief Read status from the display
 *
 * @return The status
 */

uint8_t Display_StatusRead(void) {
	uint8_t b[2];
	uint8_t ret[2];
	b[0] = 0xc0;
	b[1] = 0x00;

	DisplayTransmitReceive(b, ret);

	return ret[1];
}


/**
 * @brief Transmit/receive to the display 2 bytes
 *
 * @param cmd - the command
 * @param Data - output data
 */

void Display_WriteDir(uint8_t Cmd, uint8_t Data) {
	Display_CmdWrite(Cmd);
	Display_DataWrite(Data);
}


/**
 * @brief Perform hardware reset on the display
 */
void Display_perFormHwReset() {
	vTaskDelay(100);
	HAL_GPIO_WritePin(DISPLAY_GPIO_RESET, DISPLAY_PIN_RESET, RESET);
	vTaskDelay(100);
	HAL_GPIO_WritePin(DISPLAY_GPIO_RESET, DISPLAY_PIN_RESET, SET);
	vTaskDelay(100);
}
/** @} */
