/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "stm32f4xx_hal.h"

#include "../platform_config.h"

void UART_Display_Gpio(void);
void UART_GSM_MspInit();
void UART_QRCODEPRINTER_MspInit();
void UART_WIFI_MspInit();
// Initialization routines:

/**
 * @file
 * @brief Initialization of SPI and UART
 */

/**
 * @addtogroup hardware
 *
 * @{
 */

/**
 * @brief Check if the bus console may handle card
 *
 * @param step - pointer to string pointer to store debug name
 * @param controller - Controller
 * @return 1 = True, 0 = false
 */
void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi) {
	GPIO_InitTypeDef GPIO_InitStruct;

	if (hspi->Instance == CLRC663_SPI) {
		// RC663: SPI1 - MISO
		GPIO_InitStruct.Pin = CLRC663_PIN_MISO; // Set To ModeAFPP ?? (No INPUT??)
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
		HAL_GPIO_Init(CLRC663_GPIO_MISO, &GPIO_InitStruct);

		// RC663: SPI1 - MOSI
		GPIO_InitStruct.Pin = CLRC663_PIN_MOSI;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
		HAL_GPIO_Init(CLRC663_GPIO_MOSI, &GPIO_InitStruct);

		// RC663: SPI1 - SCK
		GPIO_InitStruct.Pin = CLRC663_PIN_SCK;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
		HAL_GPIO_Init(CLRC663_GPIO_SCK, &GPIO_InitStruct);
	} else if (hspi->Instance == DISPLAY_SPI) {
		UART_Display_Gpio();
	}
}

/**
 * @brief the UART init routine
 */
void HAL_UART_MspInit(UART_HandleTypeDef* huart) {
	if (huart->Instance == GSM_UART) {
		UART_GSM_MspInit();
	} else if (huart->Instance == QRCODEPRINTER_UART) {
		UART_QRCODEPRINTER_MspInit();
	} else if (huart->Instance == WIFI_UART) {
		UART_WIFI_MspInit();
	}
}
/** @} */
