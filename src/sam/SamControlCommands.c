/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "SamControlCommands.h"
#include "phhalHw_SamAV2_Cmd.h"
#include "../smartcardif/phhalHw_Rc663_Reg.h"
#include "DnfccSamCrypto.h"
#include "CommunicationCommands.h"
#include "../smartcardif/DirectIo.h"

extern SMARTCARD_HandleTypeDef smartcard_sam;

/**
 * @file
 * @brief Contains  functions for the sam module
 */

/**
 * @addtogroup sam
 *
 * @{
 */

/**
 * @param Perform a SAM reset hardware et all
 */
void SamPerformReset() {
	uint8_t registers[50];
	uint32_t baudBeforePPS = 18810;
	uint32_t baudAfterPPS = 222000;

	// Change the baud rate to the old value...
	smartcard_sam.Instance->BRR = SMARTCARD_BRR(HAL_RCC_GetPCLK1Freq(),
			baudBeforePPS);

	vTaskDelay(100);

	SamResetReceiving();

	HAL_GPIO_WritePin(SAM_GPIO_RESET, SAM_PIN_RESET, GPIO_PIN_RESET);

	vTaskDelay(100);

	HAL_GPIO_WritePin(SAM_GPIO_RESET, SAM_PIN_RESET, GPIO_PIN_SET);

	// Supposed to receive 28 bytes now,
	// being the first thing the SAM transmits after reset
	// eg. ATR
	SamReceiveRaw(registers, 28, 1000);

	vTaskDelay(500);
	// Craft the Protocol Parameter Selection command
	uint8_t bufPpss[4];
	bufPpss[0] = 0xff;
	bufPpss[1] = 0x11;
	bufPpss[2] = 0x18;
	bufPpss[3] = 0xf6;

	__HAL_SMARTCARD_ENABLE_IT(&smartcard_sam, SMARTCARD_IT_TC);

	HAL_SMARTCARD_Transmit_IT(&smartcard_sam, bufPpss, 4);

	SamReceiveRaw(registers, 4, 1000);
	SamReceiveRaw(registers + 4, 4, 1000); // Should be retrieved as well
	vTaskDelay(100);
// Change the baud rate to the new value...
	smartcard_sam.Instance->BRR = SMARTCARD_BRR(HAL_RCC_GetPCLK1Freq(),
			baudAfterPPS);
	vTaskDelay(100);
	SamTransmitCommand((uint8_t *) "\x80\x60\0\0\0", 5);
	uint16_t length = -1;
	SamReceiveEnvelope(registers, &length, 1000);

	Rc663ReadRegister(PHHAL_HW_RC663_REG_VERSION);

	deviceStatus.samNeedReset = 0;
	DnfccSamRcInit(1);
	DnfccSamAuthenticateHost(0, 0, 0);
	DnfccSamRcRFControl(0); // Must be done first, to switch off RF, before we can continue
	vTaskDelay(10);
	DnfccSamRcRFControl(5); // Must be done first, to switch off RF, before we can continue
	vTaskDelay(10);
}

/**
 * @brief - Perform the RcInit command
 *
 * @param withRc - if the Hostctrl register needs to be set up
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamRcInit(uint8_t withRc) {
	int messageLength = 0;
	uint8_t messageToSend[50];
	uint8_t messageReceived[50];
	uint8_t reg;
	uint16_t lengthReceived;
	uint16_t result;

	if (withRc) {
		reg = Rc663ReadRegister(PHHAL_HW_RC663_REG_HOSTCTRL);
		reg |= 0x80;
		Rc663WriteRegister(PHHAL_HW_RC663_REG_HOSTCTRL, reg);
		reg &= 0xbf;
		reg |= 0x08;
		reg |= 0x20;
		reg |= 0x08;
		reg |= 0x80;
		Rc663WriteRegister(PHHAL_HW_RC663_REG_HOSTCTRL, reg);
		reg = Rc663ReadRegister(PHHAL_HW_RC663_REG_HOSTCTRL);
	}
	messageLength = 0;
	messageToSend[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	messageToSend[messageLength++] = PHHAL_HW_SAMAV2_CMD_RC_INIT;
	messageToSend[messageLength++] = 0x00;
	messageToSend[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	SamResetReceiving();
	SamTransmitCommand(messageToSend, messageLength);
	reg = SamReceiveEnvelope(messageReceived, &lengthReceived, 1000);


	if (lengthReceived != 2) {
		return 0;
	}

	result = messageReceived[0] * 256 + messageReceived[1];

	if (lengthReceived != 2 || messageReceived[lengthReceived - 2] != 0x90
			|| messageReceived[lengthReceived - 1] != 0) {
		return result;
	}
	return result;
}

/**
 * @brief - read rc663 registers
 *
 * @param registers - the registers to read
 * @param results - The results data
 * @param numRegisters - number of registers to read
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamRcReadRegisters(uint8_t *registers, uint8_t *results,
		uint16_t numRegisters) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	int i;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_RC_READREGISTER;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = numRegisters;
	for (i = 0; i < numRegisters; i++) {
		message[messageLength++] = registers[i];
	}
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	result = SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}
	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (result != 0x9000) {
		return result;
	}
	for (i = 0; i < lengthReceived - 2; i++) {
		results[i] = message[i];
	}
	return result;
}

/**
 * @brief - Perform a free CID
 *
 * @param channels - which channels to free (0 = all)
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamRcFreeCID(uint8_t channels) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_FREE_CID_INS;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = 1;
	message[messageLength++] = channels;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	result = SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}
	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (result != 0x9000) {
		return result;
	}
	return result;
}

/**
 * @brief - Read a RC663 register
 *
 * @param theRegister - get the register value
 * @param results - The results data
 * @return The status result of the SAM, or 0 if communication failed
 */

uint16_t DnfccSamRcReadSingleRegister(uint8_t theRegister, uint8_t *result) {
	uint8_t reg;

	reg = theRegister;

	return DnfccSamRcReadRegisters(&reg, result, 1);
}

/**
 * @brief - Perform  a write to a RC663 register
 *
 * @param registers - Array of registers
 * @param values -  the corresponding values
 * @param numRegisters - number of register/value pairs
 *
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamRcWriteRegister(uint8_t *registers, uint8_t *values,
		uint16_t numRegisters) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_RC_WRITEREGISTER;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = numRegisters * 2;
	for (int i = 0; i < numRegisters; i++) {
		message[messageLength++] = registers[i];
		message[messageLength++] = values[i];
	}
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}
	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	return result;
}

/**
 * @brief - Check if a card is still in the field
 *
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamPresenceCheck() {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_PRESENCE_CHECK_INS;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	return result;
}

/**
 * @brief - Perform the RF control
 *
 * @param time - the time the RF should be off
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamRcRFControl(uint16_t time) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_RC_RFCONTROL;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = 2;
	message[messageLength++] = time & 0xff;
	message[messageLength++] = (time >> 8) & 0xff;
	;
//	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (result == 0x6401) {
		deviceStatus.samNeedReset = 1;
	}
	return result;
}

/**
 * @brief - Perform the SAM deselect
 *
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamDeselect() {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_DESELECT_INS;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	;
//	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	return result;
}
/**
 * @brief - Perform the Free CId command
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamFreeCids() {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_FREE_CID_INS;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = 1;
	message[messageLength++] = 0xff;
//	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	return result;
}

/**
 * @brief - Load a register set value
 *
 * @param speedId - array of the Speed IDs
 * @param itemCount - array of item counts
 * @param registers - array of register values
 * @param values - the corresponding values
 * @param numRegisters - the number  of registers
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccRcLoadRegisterValueSet(uint8_t *speedId, uint8_t *itemCount,
		uint8_t *registers, uint8_t *values, uint16_t numRegisters) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_RC_LOAD_REG_VALUE_SET;
	message[messageLength++] = numRegisters;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = numRegisters * 4;
	for (int i = 0; i < numRegisters; i++) {
		message[messageLength++] = speedId[i];
		message[messageLength++] = itemCount[i];
		message[messageLength++] = registers[i];
		message[messageLength++] = values[i];
	}
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}
	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	return result;
}


/**
 * @brief - Perform theReqa or Wupa
 *
 * @param ReqaOrWupa - Perform either atqa or wupa, must be set to that value
 * @param atqa - The results data
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamIso14443_3RequestWakeup(uint8_t ReqaOrWupa, uint16_t *atqa) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_REQUEST_WAKEUP_INS;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_REQUEST_WAKEUP_CMD_LC;
	message[messageLength++] = ReqaOrWupa;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 4000);
	SamRelease();

	if (lengthReceived == 0) {
		lengthReceived = 0;
	}
	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (lengthReceived == 4) {
		*atqa = message[0] * 256 + message[1];
	}
	return result;
}

/**
 * @brief - Perform an anti-collision
 *
 * @param selCode -  the selection code (93,95,97)
 * @param data - the resuting data
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamIso14443_3AntiCollisionSelect(uint8_t selCode, uint8_t *data) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;
	uint8_t len = 0;
	switch (selCode) {
	case 0x93:
		len = 1;
		break;
	case 0x95:
		len = 2;
		break;
	case 0x97:
		len = 3;
		break;
	}

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_ANTICOLLSION_SELECT_INS;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = len;
	for (int i = 0; i < len; i++) {
		message[messageLength++] = data[i];
	}

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (result != 0x9000 && result != 0x90c0) {
		return result;
	}
	for (int i = 0; i < lengthReceived - 2; i++) {
		data[i] = message[i];
	}
	return result;
}

/**
 * @brief - Perform the activate Idle on level 3
 *
 * @param numCards - number of cards
 * @param waittime - howlong to wait
 * @param atqaFilter - filter of Atqa
 * @param sakFilter - sak filter
 * @param cardInfo - the resulting info
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamIso14443_3ActivateIdle(uint8_t numCards, uint16_t waittime,
		uint8_t *atqaFilter, uint8_t *sakFilter, uint8_t *cardInfo) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_ACTIVATE_IDLE_INS;
	message[messageLength++] = numCards;
	message[messageLength] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	if (atqaFilter != NULL)
		message[messageLength] |= 1;
	if (sakFilter != NULL)
		message[messageLength] |= 2;
	messageLength++;
	message[messageLength] = 0;
	if (waittime != 0xffff)
		message[messageLength] += 2;
	if (atqaFilter != NULL)
		message[messageLength] += 4;
	if (sakFilter != NULL)
		message[messageLength] += 2;
	messageLength++;

	if (waittime != 0xffff) {
		message[messageLength++] = (waittime >> 8) & 0xff;
		message[messageLength++] = waittime & 0xff;
	}
	if (atqaFilter != NULL) {
		message[messageLength] += atqaFilter[0];
		message[messageLength] += atqaFilter[1];
		message[messageLength] += atqaFilter[2];
		message[messageLength] += atqaFilter[3];
	}
	if (sakFilter != NULL) {
		message[messageLength] += sakFilter[0];
		message[messageLength] += sakFilter[1];
	}
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (result == 0x9000) {
		memcpy(cardInfo, message, lengthReceived - 2);
	}
	return result;
}

/**
 * @brief - Perform a RATS/PPS sequence
 *
 * @param cid - the CID parameter
 * @param dri - the DRI parameter
 * @param dsi - the DSI parameter
 * @param results - the results of the command
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamIso14443_4RatsPps(uint8_t cid, uint8_t dri, uint8_t dsi,
		uint8_t * results) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	int i;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_RATS_PPS_INS;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = 3;
	message[messageLength++] = cid;
	message[messageLength++] = dri;
	message[messageLength++] = dsi;

	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 5000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (result != 0x9000) {
		return result;
	}

	for (i = 0; i < lengthReceived - 2; i++) {
		results[i] = message[i];
	}
	return result;
}

/**
 * @brief - Perform an exchange on level 4
 *
 * @param data - the data to transmit
 * @param len - the length of the data
 * @param results - The results data
 * @param length - pointer to the length of the results
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamIso14443_4Exchange(uint8_t *data, uint8_t len,
		uint8_t *results, uint16_t *length) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	int i;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_EXCHANGE_INS;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE; // lfi = 0
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	message[messageLength++] = len;
	for (i = 0; i < len; i++) {
		message[messageLength++] = data[i];
	}
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 10000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (result != 0x9000) {
		return result;
	}
	*length = lengthReceived - 2;
	for (i = 0; i < lengthReceived - 2; i++) {
		results[i] = message[i];
	}
	return result;
}

/**
 * @brief - Perform an Authenticate Picc command
 *
 * @param keySam - the key of the SAM
 * @param keyPicc - the key of the picc
 * @param pDivInp - the diversification input
 * @param pDivLength - the length of the pdivInp
 * @param results - the result of the call
 * @param length - the length of the results
 * @return The status result of the SAM, or 0 if communication failed
 */

uint16_t DnfccSamDesfireAuthenticatePicc(uint8_t keySam, uint8_t keyPicc,
		uint8_t *pDivInp, uint8_t pDivLength, uint8_t *results,
		uint16_t *length) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	int i;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_DESFIRE_AUTH_PICC_INS;
	if (pDivLength == 0) {
		message[messageLength++] = 0x00;//P1-Autmode;
	} else {
		message[messageLength++] = 0x01;//P1-Autmode; -> Diversification
	}
	message[messageLength++] = 0x00;//P2
	message[messageLength++] = 3 + pDivLength; // Length
	message[messageLength++] = keyPicc;

	message[messageLength++] = keySam;
	message[messageLength++] = 0;

	for (i = 0; i < pDivLength; i++) {
		message[messageLength++] = pDivInp[i];
	}
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();
	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (result != 0x9000) {
		return result;
	}
	*length = lengthReceived - 2;
	for (i = 0; i < lengthReceived - 2; i++) {
		results[i] = message[i];
	}
	return result;
}

/**
 * @brief - Perform the ReadX command
 *
 * @param data - the data to transmit
 * @param len - the length of the data
 * @param results - The results data
 * @param length - pointer to the length of the results
 * @return The status result of the SAM, or 0 if communication failed
 */
uint16_t DnfccSamDesfireReadX(uint8_t *data, uint8_t len, uint8_t *results,
		uint16_t *length) {
	uint16_t messageLength = 0;
	uint16_t lengthReceived = 0;
	uint8_t message[270];
	uint16_t result;

	int i;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	message[messageLength++] = PHHAL_HW_SAMAV2_CMD_DESFIRE_READ_X_INS;
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	message[messageLength++] = 0b00110000; // 0x00110000
	message[messageLength++] = 1 + len;
	message[messageLength++] = 0;
	for (i = 0; i < len; i++) {
		message[messageLength++] = data[i];
	}
	message[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	if (SamClaimTimeout(1000) != pdTRUE) {
		return 0;
	}
	SamTransmitCommand(message, messageLength);
	SamReceiveEnvelope(message, &lengthReceived, 1000);
	SamRelease();

	if (lengthReceived < 2 || lengthReceived > 269) {
		return 0;
	}

	result = message[lengthReceived - 2] * 256 + message[lengthReceived - 1];
	if (result != 0x9000) {
		return result;
	}
	*length = lengthReceived - 2;
	for (i = 0; i < lengthReceived - 2; i++) {
		results[i] = message[i];
	}
	return result;
}
/** @} */
