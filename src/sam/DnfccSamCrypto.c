/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "sam.h"
#include "stm32f4xx_hal_smartcard.h"
#include <mbedtls/cmac.h>
#include <mbedtls/aes.h>
#include "phhalHw_SamAV2_Cmd.h"
#include "DnfccSamCrypto.h"
#include "CommunicationCommands.h"

/**
 * @file
 * @brief Contains  functions for the sam module
 */

/**
 * @addtogroup sam
 *
 * @{
 */

/**
 * @brief- Call the cMAC cypher
 *
 * @param secretKey - the secret key
 * @param cmac - the CMAC
 * @param messages - the messages to work on
 */
void cmacMe(uint8_t *secretKey, uint8_t *cmac, uint8_t *messages) {
	mbedtls_cipher_context_t m_ctx;
	const mbedtls_cipher_info_t *cipher_info;

	cipher_info = mbedtls_cipher_info_from_type(MBEDTLS_CIPHER_AES_128_ECB);
	mbedtls_cipher_init(&m_ctx);
	mbedtls_cipher_setup(&m_ctx, cipher_info);
	mbedtls_cipher_cmac_starts(&m_ctx, secretKey, 128);
	mbedtls_cipher_cmac_update(&m_ctx, cmac, 16);
	mbedtls_cipher_cmac_finish(&m_ctx, messages);
}

/**
 * @brief- Call the AES 128 encryption cypher
 *
 * @param secretKey - the secret key
 * @param input - the input
 * @param output - the output
 * @param length - the message length
 */
void aes128Encrypt(uint8_t *secretKey, uint8_t *input, uint8_t *output, int length) {
	mbedtls_aes_context aes;
	uint8_t iv[16];
	memset(iv, 0x00, 16);
	mbedtls_aes_setkey_enc(&aes, secretKey, 128);
	mbedtls_aes_crypt_cbc(&aes, MBEDTLS_AES_ENCRYPT, length, iv, input, output);
}

/**
 * @brief- Call the AES 128 decryption cypher
 *
 * @param secretKey - the secret key
 * @param input - the input
 * @param output - the output
 */
void aes128Decrypt(uint8_t *secretKey, uint8_t *input, uint8_t *output) {
	mbedtls_aes_context aes;

	mbedtls_aes_setkey_dec(&aes, secretKey, 128);
	mbedtls_aes_crypt_ecb(&aes, MBEDTLS_AES_DECRYPT, input, output);
}

/**
 * @brief Sam -authenticate host
 *
 * @param keyNo - the key number
 * @param keyVers - the key version
 * @param hostMode - the host mode
 */
uint8_t DnfccSamAuthenticateHost(uint8_t keyNo, uint8_t keyVers, uint8_t hostMode) {
	uint16_t messageLength;
	uint8_t cApdu1[10];
	uint16_t lengthReceived;
	uint8_t myMessages[50];
	uint8_t myMessages2[50];
	uint8_t *rnd1 =(uint8_t *) "\x0\x1\x2\x3\x4\x5\x6\x7\x8\x9\xa\xb\xc\xd\xe\xf";
	uint8_t rnd2[12];
	uint8_t *rnda =(uint8_t *) "\x0\x1\x2\x3\x4\x5\x6\x7\x8\x9\xa\xb\xc\xd\xe\xf";
	uint8_t rndb[16];
	uint8_t rndbE[16];
	uint8_t cmacIn[20];
	uint8_t cmacCalc[20];
	uint8_t sv1[16];
	uint8_t kxe[16];
	uint8_t svke[16];
	uint8_t svkm[16];
	uint8_t sessionKey[16];
	uint8_t sessionMacKey[16];
	uint8_t rndAB[32];
	uint8_t rndAbE[32];
	uint8_t *secretKey =(uint8_t *)
//			"\x01\x02\x03\x04\x05\x06\x07\x08\x09\x10\x11\x12\x13\x14\x15\x16";// SAM 20601
//			"\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11\x11";// SAM 20602
			"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";// SAM 20603
	// "\x0\x1\x2\x3\x4\x5\x6\x7\x8\x9\xa\xb\xc\xd\xe\xf";

	messageLength = 0;
	cApdu1[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE; // CLA
	cApdu1[messageLength++] = PHHAL_HW_SAMAV2_CMD_AUTHENTICATE_HOST_INS; // INS
	cApdu1[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE; // P1 = rfu
	cApdu1[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE; // P2 = rfu
	cApdu1[messageLength++] = 0x03; // Lc = length of data
	cApdu1[messageLength++] = keyNo;
	cApdu1[messageLength++] = keyVers;
	cApdu1[messageLength++] = hostMode;
	cApdu1[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE; // Lc = expected length = not specified
// Step 1
	SamResetReceiving();
	SamTransmitCommand(cApdu1, messageLength);
	SamReceiveEnvelope(myMessages, &lengthReceived, 1000);
	if (lengthReceived != 14) {
		return 1;
	}
// Step 2 done
// Got the Rnd2, so pick it up
	memcpy(rnd2, myMessages, 12);
	memcpy(cmacIn, rnd2, 12);
	cmacIn[12] = hostMode;
	cmacIn[13] = 0x00;
	cmacIn[14] = 0x00;
	cmacIn[15] = 0x00;
// Step 4 done
	cmacMe(secretKey, cmacIn, myMessages);

	for (int i = 0; i < 8; i++) {
		cmacCalc[i] = myMessages[1 + i * 2];
	}

	messageLength = 0;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_CMD_AUTHENTICATE_HOST_INS;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	myMessages[messageLength++] = 0x14;
	memcpy(myMessages + messageLength, cmacCalc, 8);
	messageLength += 8;
	memcpy(myMessages + messageLength, rnd1, 12);
	messageLength += 12;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;
// Send step 7
	SamResetReceiving();
	SamTransmitCommand(myMessages, messageLength); //26
	SamReceiveEnvelope(myMessages2, &lengthReceived, 1000);
	if (lengthReceived != 26) {
		return 2;
	}
// Step 8, calculate SV1
	memcpy(sv1, rnd1 + 7, 5);
	memcpy(sv1 + 5, rnd2 + 7, 5);
	sv1[10] = rnd1[0] ^ rnd2[0];
	sv1[11] = rnd1[1] ^ rnd2[1];
	sv1[12] = rnd1[2] ^ rnd2[2];
	sv1[13] = rnd1[3] ^ rnd2[3];
	sv1[14] = rnd1[4] ^ rnd2[4];
	sv1[15] = 0x91;
// Step 9
	aes128Encrypt(secretKey, sv1, kxe, 16);
// Step 10a, retrieve the encrypted Rndb
	memcpy(rndbE, myMessages2 + 8, 16);
	memcpy(cmacIn, rnd1, 12);
	cmacIn[12] = hostMode;
	cmacIn[13] = 0x00;
	cmacIn[14] = 0x00;
	cmacIn[15] = 0x00;
	cmacMe(secretKey, cmacIn, myMessages);

	for (int i = 0; i < 8; i++) {
		cmacCalc[i] = myMessages[1 + i * 2];
	}

	// Step 12
	if (memcmp(myMessages2, cmacCalc, 8) != 0) {
		return 3;
	}
	cmacIn[0] = 0;

// 13, get Rndb
	aes128Decrypt(kxe, rndbE, rndb);

// Step 16: Create RndAB'
	memcpy(rndAB, rnda, 16);
	memcpy(rndAB + 16, rndb + 2, 14);
	memcpy(rndAB + 30, rndb, 2);

	aes128Encrypt(kxe, rndAB, rndAbE, 32);
	cmacIn[0] = 0;
	messageLength = 0;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_CMD_AUTHENTICATE_HOST_INS;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	myMessages[messageLength++] = 0x20;
	memcpy(myMessages + messageLength, rndAbE, 32);
	messageLength += 32;
	myMessages[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	SamResetReceiving();
	SamTransmitCommand(myMessages, messageLength);
	SamReceiveEnvelope(myMessages2, &lengthReceived, 1000);
	if (lengthReceived != 18) {
		return 4;
	}
	aes128Decrypt(kxe, myMessages2, myMessages);
//Step 22:  Check if messages is same as the rnda, rotated by 2 bytes.
	memcpy(svke, rnda + 11, 5);
	memcpy(svke + 5, rndb + 11, 5);
	svke[10] = rnda[4] ^ rndb[4];
	svke[11] = rnda[5] ^ rndb[5];
	svke[12] = rnda[6] ^ rndb[6];
	svke[13] = rnda[7] ^ rndb[7];
	svke[14] = rnda[8] ^ rndb[8];
	svke[15] = 0x81;

	memcpy(svkm, rnda + 7, 5);
	memcpy(svkm + 5, rndb + 7, 5);
	svkm[10] = rnda[0] ^ rndb[0];
	svkm[11] = rnda[1] ^ rndb[1];
	svkm[12] = rnda[2] ^ rndb[2];
	svkm[13] = rnda[3] ^ rndb[3];
	svkm[14] = rnda[4] ^ rndb[4];
	svkm[15] = 0x82;
	aes128Encrypt(secretKey, svke, sessionKey, 16);
	aes128Encrypt(secretKey, svkm, sessionMacKey, 16);
	return 0;
}

uint8_t SamAthenticatePICCPart1(uint8_t wKeyNo, uint8_t wKeyVer, uint8_t *bRndB, uint8_t *bRndAB, uint8_t *length) {
	uint8_t myMessages[50];
	uint8_t myMessages2[50];
	uint16_t lengthReceived;
	uint8_t ior;

	*length = 0;
	if (SamClaimTimeout(10000) != pdTRUE) {
		return 1;
	}

	myMessages[0] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	myMessages[1] = PHHAL_HW_SAMAV2_CMD_AUTHENTICATE_PICC_INS;
	myMessages[2] = 0x00; // Auth mode,
	myMessages[3] = 0x00;
	myMessages[4] = 0x12; // AES
	myMessages[5] = wKeyNo;
	myMessages[6] = wKeyVer;
	memcpy(myMessages + 7, bRndB, 16);
	myMessages[23] = 0;

	SamTransmitCommand(myMessages, 24);
	ior = SamReceiveEnvelope(myMessages2, &lengthReceived, 2000);

	if (ior != 9000 || lengthReceived != 34) {
		SamRelease();
		return 34;
	}

	memcpy(bRndAB, myMessages2, 32);
	*length = 32;
	return 0;
}

/**
 * @brief The second part of the authenticate PICC
 *
 * @param keyno - the key number
 * @param wKeyVer - the key version
 * @param part2 - pointer to part 2
 * @param plen - length
 * @return 0 = OK
 */
uint8_t SamAthenticatePICCPart2(uint8_t wKeyNo, uint8_t wKeyVer, uint8_t *part2, uint16_t plen) {
	uint8_t myMessages[50];
	uint8_t myMessages2[50];
	uint16_t lengthReceived;

	myMessages[0] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	myMessages[1] = PHHAL_HW_SAMAV2_CMD_AUTHENTICATE_PICC_INS;
	myMessages[2] = 0x00;
	myMessages[3] = 0x00;
	myMessages[4] = plen;
	memcpy(myMessages + 5, part2, plen);
	SamTransmitCommand(myMessages, plen + 5); // was 21
	SamReceiveEnvelope(myMessages2, &lengthReceived, 1000);
	if (lengthReceived != 2) {
		SamRelease();
		return 2;
	}
	if (myMessages2[0] != 0x90 || myMessages2[1] != 0x00) { // 67 00
		SamRelease();
		return 90;
	}
	SamRelease();
	return 0;
}
/** @} */
