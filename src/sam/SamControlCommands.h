/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SAM_SAMCONTROLCOMMANDS_H_
#define SAM_SAMCONTROLCOMMANDS_H_

uint16_t DnfccSamRcInit(uint8_t withRc);
uint16_t DnfccSamPresenceCheck();
uint16_t DnfccSamRcReadRegisters(uint8_t *registers, uint8_t *results,
		uint16_t numRegisters);
uint16_t DnfccSamRcReadSingleRegister(uint8_t theRegister, uint8_t *result);
uint16_t DnfccSamRcWriteRegister(uint8_t *registers, uint8_t *values,
		uint16_t numRegisters);
uint16_t DnfccSamRcRFControl(uint16_t time);
uint16_t DnfccRcLoadRegisterValueSet(uint8_t *speedId, uint8_t *itemCount,
		uint8_t *registers, uint8_t *values, uint16_t numRegisters);
uint16_t DnfccSamIso14443_3RequestWakeup(uint8_t ReqaOrWupa, uint16_t *atqa);
uint16_t DnfccSamIso14443_3AntiCollisionSelect(uint8_t selCode, uint8_t *data);
uint16_t DnfccSamIso14443_3ActivateIdle(uint8_t numCards, uint16_t waittime,
		uint8_t *atqaFilter, uint8_t *sakFilter, uint8_t *cardInfo);
uint16_t DnfccSamIso14443_4RatsPps(uint8_t cid, uint8_t pcd, uint8_t dsi,
		uint8_t * results);
uint16_t DnfccSamIso14443_4Exchange(uint8_t *data, uint8_t len,
		uint8_t *results, uint16_t *length);
uint16_t DnfccSamDeselect();
uint16_t DnfccSamDesfireAuthenticatePicc(uint8_t keySam, uint8_t keyPicc,
		uint8_t *pDivInp, uint8_t pDivLength, uint8_t *results,
		uint16_t *length);

void SamPerformReset();
void SamResetReceiving();
uint8_t SamTransmitCommand(uint8_t *s, int length);
void SamInitializeCommunication();
uint8_t SamReceiveEnvelope(uint8_t *s, uint16_t *length, uint16_t timeoutInMs);
uint16_t SamReceiveRaw(uint8_t *s, uint16_t count, uint16_t timeoutInMs);
uint16_t DnfccSamRcFreeCID(uint8_t channels) ;

#endif /* SAM_SAMCONTROLCOMMANDS_H_ */
