/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "sam.h"
#include "stm32f4xx_hal_smartcard.h"
#include "SamControlCommands.h"
#include "CommunicationCommands.h"
#include "phhalHw_SamAV2_Cmd.h"

#define STACK_SIZE 500

/**
 * @file
 * @brief Contains  functions for the sam module
 */

/**
 * @addtogroup sam
 *
 * @{
 */

/*****************************
 * @brief Sam initialization routine
 *
 * The following tasks are defined:
 * - Incrementing time / date on second basis
 * - Sending the new Time or Date when changed to the display unit
 * - When a new time or date is transmitted to here, pick it up and use it.
 *
 * This routine will wait for 200 sam ticks (xQueueReceive)
 *
 * @param prio the FreeRTOS prio
 */
BaseType_t nfccSamInit(UBaseType_t prio) {
	deviceStatus.samRxQueue = xQueueCreate(SAM_QUEUE_LENGTH, SAM_BUFFER_SIZE);

	vQueueAddToRegistry(deviceStatus.samRxQueue, "SAM");

	setWaitForSystemReadyThisTask(TASK_BIT_SAM);

	return xTaskCreate(nfccSam, "sam", STACK_SIZE, NULL, prio,
			&deviceStatus.samTask);
}

/**
 * @brief the SAM main loop
 *
 * @param pvParameters - the FreeRTOS parameters
 */
void nfccSam(void *pvParameters) {
	char buf[SAM_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;
//
// Initialize the pointers in the handling-struct
//
	SamInitializeCommunication();

// Double reset to make sure the number of bytes received do not contain \0

	SamPerformReset();

	SamSetReady();

	setTaskIsSystemReady(TASK_BIT_SAM);
	waitForSystemReady();

	for (;;) {
		if (xQueueReceive(deviceStatus.samRxQueue, buf, 2000) != pdPASS) {
			continue;
		}

		switch (buf[0]) {
		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(deviceStatus.samTask);
			sprintf(stbuf, "!TWM%ld", uxHighWaterMark);
			stbuf[0] = COMCOM_DATADISTRIBUTOR_SELFTESTRESULT;
			stbuf[1] = COMSELFTEST_SAM;
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;
		} // Switch[receive buffer]
	} // Endless for loop
}
/** @} */
