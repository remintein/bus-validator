/*
 * DnfccSamCrypto.h
 *
 *  Created on: 14 jul. 2018
 *      Author: aart
 */

#ifndef SAM_DNFCCSAMCRYPTO_H_
#define SAM_DNFCCSAMCRYPTO_H_

uint8_t DnfccSamAuthenticateHost(uint8_t keyNo, uint8_t keyVers,
		uint8_t hostMode);
void aes128Encrypt(uint8_t *secretKey, uint8_t *input, uint8_t *output,
		int length);
void aes128Decrypt(uint8_t *secretKey, uint8_t *input, uint8_t *output);
uint8_t SamAthenticatePICCPart1(uint8_t wKeyNo, uint8_t wKeyVer, uint8_t *bRndB, uint8_t *bRndAB, uint8_t *length) ;
uint8_t SamAthenticatePICCPart2(uint8_t wKeyNo, uint8_t wKeyVer, uint8_t *part2, uint16_t plen) ;


#endif /* SAM_DNFCCSAMCRYPTO_H_ */
