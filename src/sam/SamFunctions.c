/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "sam.h"
#include "stm32f4xx_hal_smartcard.h"
#include <mbedtls/cmac.h>
#include <mbedtls/aes.h>

#include "phhalHw_SamAV2_Cmd.h"

/**
 * @file
 * @brief Contains  functions for the sam module
 */

/**
 * @addtogroup sam
 *
 * @{
 */

/**
 * @brief Read a register with the SAM, from the RC663
 * @param reg - register to read
 * @return Register value
 */
int DnfccSamRcReadRegister(uint8_t reg) {
	int messageLength = 0;
	uint8_t messageToSend[50];
	uint8_t messageReceived[50];
	uint16_t lengthReceived;
	messageToSend[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_CLA_BYTE;
	messageToSend[messageLength++] = PHHAL_HW_SAMAV2_CMD_RC_READREGISTER;
	messageToSend[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P1_BYTE;
	messageToSend[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_P2_BYTE;
	messageToSend[messageLength++] = 1; // Lc = Length of data
	messageToSend[messageLength++] = reg;
	messageToSend[messageLength++] = PHHAL_HW_SAMAV2_ISO7816_DEFAULT_LE_BYTE;

	SamResetReceiving();
	SamTransmitCommand(messageToSend, messageLength);
	SamReceiveEnvelope(messageReceived, &lengthReceived, 1000);
	if (lengthReceived != 3 || messageReceived[lengthReceived - 2] != 0x90
			|| messageReceived[lengthReceived - 1] != 0) {
		return messageReceived[1];
	}
	return 0;
}

/** @} */
