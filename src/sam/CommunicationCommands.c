/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#include "../project_includes.h"
#include "sam.h"
#include "stm32f4xx_hal_smartcard.h"
#include "CommunicationCommands.h"

extern SMARTCARD_HandleTypeDef smartcard_sam;

uint8_t SamReceiveEnvelope(uint8_t *s, uint16_t *length, uint16_t timeoutInMs);

/**
 * @file
 * @brief Contains  functions for the sam module
 */

/**
 * @addtogroup sam
 *
 * @{
 */
static SamEngineStruct samEngine;
static int toggle = 0;

/**
 * @brief Set the SAM to ready (not in use)
 */
void SamSetReady() {
	xSemaphoreGive(samEngine.samSemaphore);
}
/**
 * @brief Claim the sam, with timeout
 * @param wait - the timeout
 */
BaseType_t SamClaimTimeout(TickType_t wait) {
	return xSemaphoreTake(samEngine.samSemaphore, wait);
}

/**
 * @brief - Release the sam after claim
 */
void SamRelease() {
	xSemaphoreGive(samEngine.samSemaphore);
}
/**
 * @brief - Reset the receiving input
 */
void SamResetReceiving() {
	samEngine.rxProcessPointer = samEngine.rxEndPointer;
}

/**
 * @brief Initialize the communication with the SAM
 */
void SamInitializeCommunication() {
// Set up the interrupt enables and the interrupt routine
	samEngine.rxProcessPointer = samEngine.rxEndPointer = 0;
	samEngine.txProcessPointer = samEngine.txEndPointer = 0;
	samEngine.samSemaphore = xSemaphoreCreateMutex();
//	SamClaimTimeout(100);
// Set up the interrupt enables and the interrupt routine

	__HAL_SMARTCARD_ENABLE_IT(&smartcard_sam, SMARTCARD_IT_ERR);
	__HAL_SMARTCARD_ENABLE_IT(&smartcard_sam, SMARTCARD_IT_RXNE);

	HAL_SMARTCARD_Receive_IT(&smartcard_sam, &samEngine.rxByte, 1);
}

/**
 * @brief transmit a command to the SAM in T1 format
 * @param s - the command
 * @param length - the length of the command
 */
uint8_t SamTransmitCommand(uint8_t *s, int length) {
	char buf[21];
	for (int i = 0; i < length; i++) {
		sprintf(buf + i * 2, "%02x", s[i]);
		if (i > 9) {
			break;
		}
	}
	logSamString(buf);
	SamResetReceiving();
	// Wait if there is a transmit going on
	uint8_t xor = 0;
	samEngine.txBuffer[0] = 0x0; // NAD
	samEngine.txBuffer[1] = 0x0;

	if ((toggle++) % 2) {
		samEngine.txBuffer[1] |= 0x40;
	}

	xor ^= samEngine.txBuffer[0];
	xor ^= samEngine.txBuffer[1];
	samEngine.txBuffer[2] = length;
	xor ^= samEngine.txBuffer[2]; // Length

	for (int i = 0; i < length; i++) {
		samEngine.txBuffer[i + 3] = s[i];
		xor ^= samEngine.txBuffer[i + 3];
	}
	samEngine.txBuffer[length + 3] = xor;

	__HAL_SMARTCARD_ENABLE_IT(&smartcard_sam, SMARTCARD_IT_TC);

	HAL_SMARTCARD_Transmit_IT(&smartcard_sam, samEngine.txBuffer, length + 4);
	while (smartcard_sam.TxXferCount > 0) {
		vTaskDelay(1);
	}
	// Now also receive the command...
	uint16_t retLength;
	int ior = SamReceiveEnvelope(samEngine.txBuffer, &retLength, 100);
	if (ior != 0) {
		ior = ior + 0;
	}
	return 1;
}

/**
 * @brief Receive a raw sam message
 * @param s - the message to receive
 * @param count - the number of bytes to receive
 * @param timeout - the timeout in which the receiving should take place
 */
uint16_t SamReceiveRaw(uint8_t *s, uint16_t count, uint16_t timeoutInMs) {

	TickType_t curTimeoutTime = xTaskGetTickCount() + timeoutInMs;
	while ((samEngine.rxEndPointer - samEngine.rxProcessPointer
			+ ENGINE_RXBUFFER_SIZE) % ENGINE_RXBUFFER_SIZE < count
			&& curTimeoutTime > xTaskGetTickCount()) {
		vTaskDelay(1);
	}
	if ((samEngine.rxEndPointer - samEngine.rxProcessPointer
			+ ENGINE_RXBUFFER_SIZE) % ENGINE_RXBUFFER_SIZE < count) {
		// Timeout
		return count;
	}
	uint16_t rcvd = (samEngine.rxEndPointer - samEngine.rxProcessPointer
			+ ENGINE_RXBUFFER_SIZE) % ENGINE_RXBUFFER_SIZE;
	if (rcvd > count) {
		rcvd = count;
	}
	for (int i = 0; i < rcvd; i++) {
		s[i] = samEngine.rxBuffer[samEngine.rxProcessPointer];
		samEngine.rxProcessPointer = (samEngine.rxProcessPointer + 1)
				% ENGINE_RXBUFFER_SIZE;
	}
	return count - rcvd;
}
/**
 * @brief - Receive a T1 envelope
 * @param s - the result stirng
 * @param length - length to receive
 * @param timeoutInMs - timeout in milliseconds
 */

uint8_t SamReceiveEnvelope(uint8_t *s, uint16_t *length, uint16_t timeoutInMs) {
//	TickType_t curTimeoutTime = xTaskGetTickCount() + timeoutInMs;
	uint8_t header[3];
	uint8_t checksum;
	uint16_t ret;
	*length = 0;
	ret = SamReceiveRaw(header, 3, timeoutInMs);
	if (ret > 0) {
#if SAM_LOG
		logSamString("ReceiveEnv:1");
#endif

		deviceStatus.samNeedReset = 1;
		return ret;
	} // Error, no header-bytes received in the timeout period
	if (header[0] != 0) {
#if SAM_LOG
		logSamString("ReceiveEnv:2");
#endif
		return 2; // Header no good
	}
	*length = header[2];
	ret = SamReceiveRaw(s, header[2], timeoutInMs);
	if (ret > 0) {
#if SAM_LOG
		logSamString("ReceiveEnv:3");
#endif
		return 2;
	} // Error, no data-bytes received in the timeout period
	ret = SamReceiveRaw(&checksum, 1, timeoutInMs);
	if (ret > 0) {
#if SAM_LOG
		logSamString("ReceiveEnv:4");
#endif
		return 3;
	} // Error, no checksum in correct time
	  // TODO: Verify checksum

	while ((header[1] & 0x20) != 0) { // Need additional frame, so request it
		uint8_t rBlock[4];
		rBlock[0] = 0x00;
		rBlock[1] = 0x80;
		if ((header[1] & 0x40) == 0) {
			rBlock[1] |= 0x10;
		}
		rBlock[2] = 0x00;
		rBlock[3] = rBlock[0] ^ rBlock[1] ^ rBlock[2];
		HAL_SMARTCARD_Transmit_IT(&smartcard_sam, rBlock, 4);
		while (smartcard_sam.TxXferCount > 0) {
			vTaskDelay(1);
		}
		ret = SamReceiveRaw(rBlock, 4, timeoutInMs);
		ret = SamReceiveRaw(header, 3, timeoutInMs);
		if (ret > 0) {
#if SAM_LOG
			logSamString("ReceiveEnv:5");
#endif
			return 4;
		} // Error, no header-bytes received in the timeout period
		ret = SamReceiveRaw(s + *length, header[2], timeoutInMs);
		if (ret > 0) {
#if SAM_LOG
			logSamString("ReceiveEnv:6");
#endif
			return 5;
		} // Error, no data-bytes received in the timeout period
		ret = SamReceiveRaw(&checksum, 1, timeoutInMs);
		if (ret > 0) {
#if SAM_LOG
			logSamString("ReceiveEnv:7");
#endif
			return 6;
		} // Error, no checksum in correct time
		*length += header[2];
	}

	// Log Xmt/Rcv
#if SAM_LOG
	char msg[DISPLAY_BUFFER_SIZE + 2];
	sprintf(msg, "L:%02d       ", *length);
	for (int i = 0; i < 10; i++) {
		if (i >= *length) {
			break;
		}
		sprintf(msg + 10 + i * 2, "%02x", s[i]);
	}
	logSamString(msg);
#endif
	return 0;
}

void HAL_SMARTCARD_RxCpltCallback(SMARTCARD_HandleTypeDef *point) {
	samEngine.rxBuffer[samEngine.rxEndPointer] = samEngine.rxByte;
	samEngine.rxEndPointer =
			(samEngine.rxEndPointer + 1) % ENGINE_RXBUFFER_SIZE;
	HAL_SMARTCARD_Receive_IT(&smartcard_sam, &samEngine.rxByte, 1);
}

void HAL_SMARTCARD_TxCpltCallback(SMARTCARD_HandleTypeDef *point) {
	int i = 0;
	i++;

}

void HAL_SMARTCARD_ErrorCallback(SMARTCARD_HandleTypeDef *point) {
	int i = 0;
	i++;

}
//
#if SAM_UART_NR == 1
void UART1_IRQHandler(void) {
#elif SAM_UART_NR == 2
void USART2_IRQHandler(void) {
#elif SAM_UART_NR == 3
	void UART3_IRQHandler(void) {
#elif SAM_UART_NR == 6
		void USART6_IRQHandler(void) {
#endif
	HAL_SMARTCARD_IRQHandler(&smartcard_sam);
}
#if BALANCE_BRACKETS == 3
}}}
#endif
/** @} */
