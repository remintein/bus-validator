/*
 * CommunicationCommands.h
 *
 *  Created on: 14 jul. 2018
 *      Author: aart
 */

#ifndef SAM_COMMUNICATIONCOMMANDS_H_
#define SAM_COMMUNICATIONCOMMANDS_H_


void SamSetReady() ;
BaseType_t SamClaimTimeout(TickType_t wait) ;
void SamRelease() ;
void SamResetReceiving() ;
void SamInitializeCommunication();
uint8_t SamTransmitCommand(uint8_t *s, int length) ;
uint16_t SamReceiveRaw(uint8_t *s, uint16_t count, uint16_t timeoutInMs);
uint8_t SamReceiveEnvelope(uint8_t *s, uint16_t *length, uint16_t timeoutInMs) ;



#endif /* SAM_COMMUNICATIONCOMMANDS_H_ */
