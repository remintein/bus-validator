/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef SAM_H_
#define SAM_H_

#include "../_library/serialEngine/serialEngine.h"
BaseType_t nfccSamInit(UBaseType_t uxPriority);
void nfccSam(void *pvParameters);

#define ENGINE_RXBUFFER_SIZE 512

typedef struct {
	uint8_t curReceiveByte;
	TickType_t lastCommunication;
	SemaphoreHandle_t samSemaphore;

	uint16_t rxEndPointer;
	uint16_t rxProcessPointer;
	uint8_t rxBuffer[ENGINE_RXBUFFER_SIZE];
	uint8_t rxByte;

	uint16_t txEndPointer;
	uint16_t txProcessPointer;
	uint8_t txBuffer[ENGINE_RXBUFFER_SIZE];
} SamEngineStruct;

void SamInitializeCommunication();
void SamResetReceiving();
uint8_t SamTransmitCommand(uint8_t *s, int length);
uint8_t SamReceiveEnvelope(uint8_t *s, uint16_t *length, uint16_t timeoutInMs);
uint16_t SamReceiveRaw(uint8_t *s, uint16_t count, uint16_t timeoutInMs);

#endif /* SAM_H_ */
