/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */
#ifndef DEFINITION_BUTTONTYPES_H_
#define DEFINITION_BUTTONTYPES_H_
/**
 * @file
 * @author Aart
 * @date 18 july 2018
 * @brief File contains  the enum with the button types
 *
 * Definition of the different button types.
 */
typedef enum {
	BUTTON_NORMAL /** Normal button */,
	BUTTON_SMALLTEXT /** Button with small text */,
	BUTTON_LED /** Button with the shape of a LED */,
	BUTTON_LEDCIRCLE /** Button with a LED circle */,
	WINDOW_WITH_TEXT /** Window with text */,
	WINDOW_WITH_TEXT_RED /** Window with text, in RED */,
	WINDOW_WITH_TEXT_GREEN /** Window with text in GREEN */,
	WINDOW_WITH_TEXT_NOCLEAR /** Window with text, but not clearing */,
	WINDOW_WITH_TEXT_NOBACKGROUND /** Window with text, but without background */,
	LOGO_CNS /** Window with the CNS logo */,
	LOGO_CNS_NOCLEAR /** Window with the CNS logo-not cleared */
} buttonType;

#endif /* DEFINITION_BUTTONTYPES_H_ */
