/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef DEFINITION_COMMUNICATIONCOMMANDS_H_
#define DEFINITION_COMMUNICATIONCOMMANDS_H_

#define COMCOM_SELFTEST '?'
#define COMCOM_SELFTEST_DISPLAY '%'

// Canbus no double should be present
#define COMCOM_CANBUS_REJECT_IN 'E'
#define COMCOM_CANBUS_VALID_IN 'I'
#define COMCOM_CANBUS_VALID_IN_LOWVAL 'i'
#define COMCOM_CANBUS_CARD_ENTER_RETRY 'r'
#define COMCOM_CANBUS_ENABLE_VALIDATORS 'V'
#define COMCOM_CANBUS_DISABLE_VALIDATORS 'v'
#define COMCOM_CANBUS_ROUTESERVICE_CHANGE 'R'

// Clock no double should be present
#define COMCOM_CLOCK_SETDATE 'd'
#define COMCOM_CLOCK_SETTIME 't'
#define COMCOM_CLOCK_SENDDATE 'D'
#define COMCOM_CLOCK_SENDTIME 'T'
// Datadistributor no double should be present
#define COMCOM_DATADISTRIBUTOR_SELFTESTRESULT '!'
#define COMCOM_DATADISTRIBUTOR_MESSAGE_SEND 'R'
#define COMCOM_DATADISTRIBUTOR_INIT_FILE 'I'
#define COMCOM_DATADISTRIBUTOR_START_SHIFT 's'
#define COMCOM_DATADISTRIBUTOR_END_SHIFT 'S'
#define COMCOM_DATADISTRIBUTOR_START_TRIP 'l'
#define COMCOM_DATADISTRIBUTOR_END_TRIP 'e'
#define COMCOM_DATADISTRIBUTOR_KEEPALIVE 'K'
#define COMCOM_DATADISTRIBUTOR_OK_CARD 'O'
#define COMCOM_DATADISTRIBUTOR_LOGIN 'L'
#define COMCOM_DATADISTRIBUTOR_ERROR_CARD 'E'
#define COMCOM_DATADISTRIBUTOR_TOPUP_PERFORMED 'T'
#define COMCOM_DATADISTRIBUTOR_GET_DATA_FROM_FILE 'G'
// Display no double should be present
#define COMCOM_DISPLAY_CREATE_BUTTON 'B'
#define COMCOM_DISPLAY_ADD_TO_WINDOW 'w'
#define COMCOM_DISPLAY_CLEAR_AREA_BY_COORD 'c'
#define COMCOM_DISPLAY_DATE 'D'
#define COMCOM_DISPLAY_TIME 'T'
#define COMCOM_DISPLAY_LOGGING 'l'
#define COMCOM_DISPLAY_LOGGING_START 'S'

// MENU no double should be present
#define COMCOM_MENU_CARD_ENTER_REJECT 'E'
#define COMCOM_MENU_QRCODE_REJECT 'q'
#define COMCOM_MENU_CARD_ENTER_RETRY 'R'
#define COMCOM_MENU_CARD_ENTER_SUCCESSFULL 'I'
#define COMCOM_MENU_QRCODE_SUCCESSFULL 'Q'
#define COMCOM_MENU_CARD_ENTER_SUCCESSFULL_WARNING 'J'
#define COMCOM_MENU_DISABLE_VALIDATOR 'D'
#define COMCOM_MENU_DRIVERCARD_ENTER_SUCCESSFULL 'B'
#define COMCOM_MENU_DRIVERCARD_ENTER_SUCCESSFULL_WARNING 'W'
#define COMCOM_MENU_ENABLE_VALIDATOR 'V'
#define COMCOM_MENU_ENABLE_VALIDATOR2 'v'
#define COMCOM_MENU_HANDHELD_CARD_FAIL 'Z'
#define COMCOM_MENU_HANDHELD_CARD_PASS 'z'
#define COMCOM_MENU_HANDHELD_LOGDATA 'h'
#define COMCOM_MENU_PENALTY_SCREEN 'x'
#define COMCOM_MENU_STARTSCREEN 'S'
#define COMCOM_MENU_TOPUP_NEWBALACE 'b'
#define COMCOM_MENU_FORMAT_STARTING 'f'
#define COMCOM_MENU_FORMAT_ADDITIONAL_VALUE 'F'
#define COMCOM_MENU_TOUCH_CURRENT_POSITION 'p' // touched but not released
#define COMCOM_MENU_TOUCH_POSITION 'P'         // release position
#define COMCOM_MENU_VALIDATOR_ENTER_REJECT 'e'
#define COMCOM_MENU_VALIDATOR_ENTER_RETRY 'r'
#define COMCOM_MENU_VALIDATOR_ENTER_SUCCESSFULL 'i'
#define COMCOM_MENU_VALIDATOR_ENTER_SUCCESSFULL_WARNING 'j'

// Printer
#define COMCOM_PRINT_TICKET 'T'
// SDIO no double should be present
#define COMCOM_SDIO_INITIALIZE_FARE 'F'
#define COMCOM_SDIO_INITIALIZE_PRINTER 'P'
#define COMCOM_SDIO_INITIALIZE_BLACKLIST 'B'
#define COMCOM_SDIO_INITIALIZE_WIFI 'W'
#define COMCOM_SDIO_INITIALIZE_GSM 'G'
#define COMCOM_SDIO_INITIALIZE_LOCAL 'L'
#define COMCOM_SDIO_INIT_FILE 'I'
#define COMCOM_SDIO_DATA_FOR_FILE 'D'
// SOUND no double should be present
#define COMCOM_SOUND_VALID_IN 'I'
#define COMCOM_SOUND_VALID_IN_FEMALE 'i'
#define COMCOM_SOUND_VALID_IN_LOW 'L'
#define COMCOM_SOUND_INVALID_IN 'O'
#define COMCOM_SOUND_TRYAGAIN 'T'
#define COMCOM_SOUND_KEYCLICK 'k'
// Flash programmer
#define COMCOM_FLASH_UPDATE_FIRMWARE 'U'
#define COMCOM_FLASH_INIT_FILE 'I'
#define COMCOM_FLASH_DATA_FOR_FIRMWARE 'D'
// TOUCH no double should be present
//
// WIFI no double should be present
//
// All modules, no double should be present.
#define COMSELFTEST_SOUND 'B'
#define COMSELFTEST_CANBUS 'c'
#define COMSELFTEST_CLOCK 'C'
#define COMSELFTEST_DATATDISTRIBUTOR 'd'
#define COMSELFTEST_DISPLAY 'D'
#define COMSELFTEST_QRCODE 'Q'
#define COMSELFTEST_FLASHPROGRAMMER 'F'
#define COMSELFTEST_GPS 'g'
#define COMSELFTEST_GSM 'G'
#define COMSELFTEST_MENU 'M'
#define COMSELFTEST_PRINTER 'P'
#define COMSELFTEST_SAM 'A'
#define COMSELFTEST_SDIO 'S'
#define COMSELFTEST_SMARTCARDIF 's'
#define COMSELFTEST_TOUCH 'T'
#define COMSELFTEST_WIFI 'W'

#endif /* DEFINITION_COMMUNICATIONCOMMANDS_H_ */
