/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"

/**
 * @file
 * @brief Contains queue routines
 *
 */

/**
 * @addtogroup queue
 *
 * @{
 */

/**
 * @brief Send to Sound queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToSound(char *message, TickType_t waitTime) {
	if (deviceStatus.soundRxQueue != NULL) {
		xQueueSend(deviceStatus.soundRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to Canbus queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToCanbus(char *message, TickType_t waitTime) {
	if (deviceStatus.canbusRxQueue != NULL) {
		xQueueSend(deviceStatus.canbusRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to Data Distributor queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToDataDistributor(char *message, TickType_t waitTime) {
	if (deviceStatus.datadistributorRxQueue > 0) {
		xQueueSend(deviceStatus.datadistributorRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to Display queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToDisplay(char *message, TickType_t waitTime) {
	if (deviceStatus.displayRxQueue != NULL) {
		xQueueSend(deviceStatus.displayRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to Clock queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToClock(char *message, TickType_t waitTime) {
	if (deviceStatus.clockRxQueue != NULL) {
		xQueueSend(deviceStatus.clockRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to QRCode queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToQrcode(char *message, TickType_t waitTime) {
	if (deviceStatus.qrcodeRxQueue != NULL) {
		xQueueSend(deviceStatus.qrcodeRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to Flash programmer queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToFlashProgrammer(char *message, TickType_t waitTime) {
	if (deviceStatus.flashProgrammerRxQueue != NULL) {
		xQueueSend(deviceStatus.flashProgrammerRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to SAM queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToSam(char *message, TickType_t waitTime) {
	if (deviceStatus.samRxQueue != NULL) {
		xQueueSend(deviceStatus.samRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to GPS queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToGps(char *message, TickType_t waitTime) {
	if (deviceStatus.gpsRxQueue != NULL) {
		xQueueSend(deviceStatus.gpsRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to GSM queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToGsm(char *message, TickType_t waitTime) {
	if (deviceStatus.gsmRxQueue != NULL) {
		xQueueSend(deviceStatus.gsmRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to menu queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToMenu(char *message, TickType_t waitTime) {
    if (deviceStatus.menuRxQueue != NULL) {
        xQueueSend(deviceStatus.menuRxQueue, message, waitTime);
    }
}

/**
 * @brief Send to SDio queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToSdio(char *message, TickType_t waitTime) {
    if (deviceStatus.sdioRxQueue != NULL) {
        xQueueSend(deviceStatus.sdioRxQueue, message, waitTime);
    }
}

/**
 * @brief Send to printer queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToPrinter(char *message, TickType_t waitTime) {
	if (deviceStatus.printerRxQueue != NULL) {
		xQueueSend(deviceStatus.printerRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to Smartcard queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToSmartcard(char *message, TickType_t waitTime) {
	if (deviceStatus.smartcardRxQueue != NULL) {
		xQueueSend(deviceStatus.smartcardRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to Touch queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToTouch(char *message, TickType_t waitTime) {
	if (deviceStatus.touchRxQueue != NULL) {
		xQueueSend(deviceStatus.touchRxQueue, message, waitTime);
	}
}

/**
 * @brief Send to Wifi queue if available
 * @param message - Message to send
 * @param waitTime - Max wait time if the queue is full
 */
void queueSendToWifi(char *message, TickType_t waitTime) {
	if (deviceStatus.wifiRxQueue != NULL) {
		xQueueSend(deviceStatus.wifiRxQueue, message, waitTime);
	}
}

/** @} */
