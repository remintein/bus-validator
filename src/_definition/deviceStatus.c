/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

/**
 * @file
 * @brief Contains the deviceStatus
 *
 */

/**
 * @addtogroup deviceStatus
 *
 * @{
 */
#include "../project_includes.h"
#include <string.h>
DeviceStatus deviceStatus;
extern char logString[30];

uint8_t *defaultFileVersion = (uint8_t *) "????";

/**
 * @brief Initialize the deviceStatus
 */
void initDeviceStatus() {
	deviceStatus.soundRxQueue = 0;
	deviceStatus.canbusRxQueue = 0;
	deviceStatus.clockRxQueue = 0;
	deviceStatus.datadistributorRxQueue = 0;
	deviceStatus.displayRxQueue = 0;
	deviceStatus.flashProgrammerRxQueue = 0;
	deviceStatus.gpsRxQueue = 0;
	deviceStatus.gsmRxQueue = 0;
	deviceStatus.menuRxQueue = 0;
	deviceStatus.printerRxQueue = 0;
	deviceStatus.qrcodeRxQueue = 0;
	deviceStatus.samRxQueue = 0;
	deviceStatus.sdioRxQueue = 0;
	deviceStatus.smartcardRxQueue = 0;
	deviceStatus.touchRxQueue = 0;
	deviceStatus.wifiRxQueue = 0;

	deviceStatus.soundTask = 0;
	deviceStatus.canbusTask = 0;
	deviceStatus.clockRxQueue = 0;
	deviceStatus.clockTask = 0;
	deviceStatus.datadistributorTask = 0;
	deviceStatus.displayTask = 0;
	deviceStatus.flashProgrammerTask = 0;
	deviceStatus.gpsTask = 0;
	deviceStatus.gsmTask = 0;
	deviceStatus.menuTask = 0;
	deviceStatus.printerTask = 0;
	deviceStatus.qrcodeTask = 0;
	deviceStatus.samTask = 0;
	deviceStatus.sdioTask = 0;
	deviceStatus.smartcardTask = 0;
	deviceStatus.touchTask = 0;
	deviceStatus.wifiTask = 0;

	deviceStatus.isLoggedIn = 0;
	deviceStatus.isFieldOn = 0;

	deviceStatus.loginId[0] = '\0';
	strcpy(deviceStatus.location, "GPS init");
	deviceStatus.systemReady = 0; // Is the system ready; 0 = ready, higher is not (tasks will increment/decrement)?
	deviceStatus.startPurseTransactionCount = 0; // keep busdriver purse transaction count at login time
	deviceStatus.purseTransactionCount = 0; // counter to keep track of busdriver purse transactions
	deviceStatus.deviceRouteId = 0;
	deviceStatus.deviceServiceId = 0;
#if CURRENT_TARGET == CNS_TOPUP
	deviceStatus.topupValue = 0;
	deviceStatus.topupWith = 0;
	deviceStatus.topupBalance = 0;
#endif

	deviceStatus.rotation = 0;

	memcpy(deviceStatus.firmwareVersion, FIRMWARE_VERSION, 4);
	memcpy(deviceStatus.deviceId, "????", 4); // Initial, TODO: Get stored ID if present.
	// TODO: Add for all files
	deviceStatus.blckVersion = defaultFileVersion;
	deviceStatus.fareVersion = defaultFileVersion;
	deviceStatus.printerVersion = defaultFileVersion;
	deviceStatus.wifiVersion = defaultFileVersion;

	deviceStatus.sdioStatus = Initializing;
	deviceStatus.sdioSemaphore = xSemaphoreCreateMutex();

	strcpy(deviceStatus.longitude, "GPS Init");
	strcpy(deviceStatus.lattitude, "GPS Init");

	deviceStatus.curMenu = &menu_loginscreen; // show start screen (see init display.c) until bus driver logs in

// uuid
	uint32_t *p;
	p = (uint32_t *) 0x1FFF7A10; // Position of the UUID in the STM-processor
	sprintf(deviceStatus.uuid, "%4.4lx%4.4lx%4.4lx", *p, *(p + 1), *(p + 2));
	deviceStatus.rc663GotVersion = 0;
	deviceStatus.rc663 = 0;
	deviceStatus.rc663InIrq = 0;
	deviceStatus.penaltyCard = 0;
	deviceStatus.penaltyCash = 0;

	deviceStatus.samNeedReset = 0;
	deviceStatus.cardNeedsWupa = 0;

#if DNFCC_ADD_GSM_DISABLE == 1
	deviceStatus.gsmEnabled = 1;
#endif

	sprintf(logString, "l%%-%d.%ds", DISPLAY_BUFFER_SIZE - 1,
	DISPLAY_BUFFER_SIZE - 1);

}

/**
 * @brief Set waiting bit for task ready
 * @param  bit - Bit to set
 */
void setWaitForSystemReadyThisTask(uint32_t bit) {
	deviceStatus.systemReady |= bit;
}

/**
 * @brief Unset waiting bit for task ready
 * @param  bit - Bit to set
 */
void setTaskIsSystemReady(uint32_t bit) {
	deviceStatus.systemReady &= 0xffffffff - bit;
}

/**
 * @brief Wait for that the system is ready
 */
void waitForSystemReady() {
	while (deviceStatus.systemReady) {
		vTaskDelay(10);
	}
}

/** @} */
