/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */


/**
 * @file CanbusMessages.h
 * @brief Defines the masks for the different CAN bus messages
 */

#ifndef DEFINITION_CANBUSMESSAGES_H_
#define DEFINITION_CANBUSMESSAGES_H_


/**
 * Request a can-address (needed if more validators are present
 */
#define CANBUS_MSG_REQUEST_ADDRESS          0b100000000000
#define CANBUS_MSG_MASK_REQUEST_ADDRESS     0b111100000000
#define CANBUS_MSG_MASK_REQUEST_ADDRESS_ID  0b000011111111

#define CANBUS_MSG_SUBMIT_ADDRESS           0b110000000000

#define CANBUS_MSG_VALIDATOR_ENTER_OK       0b010000010000
#define CANBUS_MSG_VALIDATOR_ENTER_OKLOWVAL 0b010000110000
#define CANBUS_MSG_VALIDATOR_ENTER_REJECT   0b010000000000
#define CANBUS_MSG_VALIDATOR_ENTER_RETRY    0b010001000000
#define CANBUS_MSG_ENABLE_VALIDATORS        0b001000010000
#define CANBUS_MSG_ENABLE_VALIDATORS2       0b001000100000
#define CANBUS_MSG_DISABLE_VALIDATORS       0b001000000000
#define CANBUS_MSG_ROUTESERVICE_CHANGE      0b000100000000
#define CANBUS_MSG_DRIVER_LOGIN             0b000010000000
#define CANBUS_MSG_MASK_MESSAGE             0b111111110000
#define CANBUS_MSG_MASK_SOURCE              0b000000001111

#endif /* DEFINITION_CANBUSMESSAGES_H_ */
