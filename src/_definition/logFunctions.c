/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"

/**
 * @file
 * @brief Contains queue routines
 *
 */

/**
 * @addtogroup queue
 *
 * @{
 */
TickType_t rc663_timing_status = 0;

char logString[30];

/**
 * @brief Log data  RC 663 low level
 * @param s - String to log
 * @param ior - Ior status to log
 * @param status - status to log
 */
void logRc663Lowlevel(char *s, uint8_t ior, uint8_t status) {
#if RC663_LOWLEVEL_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];
	sprintf(buf, "l! %02x %02x %10.10s", ior, status, s);
	queueSendToDisplay (buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data - RC663 timing
 * @param s - String to log
 */
void logRc663TimingString(char *s) {
#if RC663_TIMING_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];
	TickType_t curTick = xTaskGetTickCount();
	sprintf(buf, logString, s);
	sprintf(buf + 20, "=%ld", curTick - rc663_timing_status);
	rc663_timing_status = curTick;
	queueSendToDisplay (buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data QR code
 * @param s - String to log
 */
void logQrcodeString(char *s) {
#if SERIAL_QRCODE_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];

	sprintf(buf, logString, s);
	queueSendToDisplay (buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data printer
 * @param s - String to log
 */
void logPrinterString(char *s) {
#if SERIAL_PRINTER_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];

	sprintf(buf, logString, s);
	queueSendToDisplay (buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data OTG
 * @param s - String to log
 */
void logOtgString(char *s) {
#if SERIAL_OTG_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];

	sprintf(buf, logString, s);
	queueSendToDisplay (buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data Flow Controller
 * @param s - String to log
 */
void apiDnfccLogFlowcontrollerString(char *s) {
#if RC663_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];

	sprintf(buf, logString, s);
	queueSendToDisplay(buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data  Upload
 * @param s - String to log
 */
void logUploadString(char *s) {
#if UPLOAD_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];

	sprintf(buf, logString, s);
	queueSendToDisplay( buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data Collision
 * @param s - String to log
 */
void logCollisionString(uint8_t *s) {
#if COLLISION_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];

	buf[0] = 'l';
	for (int i = 0; i < 7; i++) {
		sprintf(buf + 1 + i*2, "%02x", s[i]);
	}
	queueSendToDisplay( buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data GSM
 * @param s - String to log
 */
void logGsmString(char *s) {
#if SERIAL_GSM_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];

	sprintf(buf, logString, s);
	queueSendToDisplay( buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data - Wifi
 * @param s - String to log
 */
void logWifiString(char *s) {
#if SERIAL_WIFI_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];

	sprintf(buf, logString, s);
	queueSendToDisplay(buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data Canbus
 * @param s - String to log
 */
void logCanString(char *s) {
#if LOGGING_ON_CAN == 1
	char buf[DISPLAY_BUFFER_SIZE+2];
	sprintf(buf, logString, s);
	queueSendToDisplay (buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data - SDIO
 * @param s - String to log
 */
void logSdioString(char *s) {
#if SDIO_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE+2];
	sprintf(buf, logString, s);
	queueSendToDisplay(buf, portMAX_DELAY);
#endif
}

void logTouchString(char *s) {
#if TOUCH_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE+2];
	sprintf(buf, logString, s);
	queueSendToDisplay (buf, portMAX_DELAY);
#endif
}

/**
 * @brief Log data  - SAM
 * @param s - String to log
 */
void logSamString(char *s) {
#if SAM_LOG == 1
	char buf[DISPLAY_BUFFER_SIZE + 2];
	sprintf(buf, logString, s);
	queueSendToDisplay(buf, 1000);
#endif
}
/** @} */
