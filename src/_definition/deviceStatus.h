/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef DEFINITION_DEVICESTATUS_H_
#define DEFINITION_DEVICESTATUS_H_

#include <FreeRTOS.h>
#include <queue.h>
#include <task.h>
#include "timers.h"

#include "../_configuration/deviceSelector.h"
#include "../_configuration/logDefinition.h"
#include "../sdio/Sdio.h"

void logCanString(char *s);
void logGsmString(char *s);
void logQrcodeString(char *s);
void apiDnfccLogFlowcontrollerString(char *s);
void logRc663TimingString(char *s);
void logSamString(char *s);
void logPrinterString(char *s);
void logSdioString(char *s);
void logTouchString(char *s);
void logUploadString(char *s);
void logWifiString(char *s);
void setTaskIsSystemReady(uint32_t bit);
void setWaitForSystemReadyThisTask(uint32_t bit);
void waitForSystemReady();

typedef struct {
	char firmwareVersion[4];
	char deviceId[4];
	char uuid[40];
	uint8_t* blckVersion;
	uint8_t* fareVersion;
	uint8_t* printerVersion;
	uint8_t* wifiVersion;
	char longitude[20];
	char lattitude[20];

	TaskHandle_t soundTask;
	TaskHandle_t canbusTask;
	TaskHandle_t clockTask;
	TaskHandle_t datadistributorTask;
	TaskHandle_t displayTask;
	TaskHandle_t flashProgrammerTask;
	TaskHandle_t gpsTask;
	TaskHandle_t gsmTask;
	TaskHandle_t menuTask;
	TaskHandle_t otgTask;
	TaskHandle_t printerTask;
	TaskHandle_t qrcodeTask;
	TaskHandle_t samTask;
	TaskHandle_t sdioTask;
	TaskHandle_t smartcardTask;
	TaskHandle_t touchTask;
	TaskHandle_t wifiTask;

	QueueHandle_t soundRxQueue;
	QueueHandle_t canbusRxQueue;
	QueueHandle_t clockRxQueue;
	QueueHandle_t datadistributorRxQueue;
	QueueHandle_t displayRxQueue;
	QueueHandle_t flashProgrammerRxQueue;
	QueueHandle_t gpsRxQueue;
	QueueHandle_t gsmRxQueue;
	QueueHandle_t menuRxQueue;
	QueueHandle_t otgRxQueue;
	QueueHandle_t printerRxQueue;
	QueueHandle_t qrcodeRxQueue;
	QueueHandle_t samRxQueue;
	QueueHandle_t sdioRxQueue;
	QueueHandle_t smartcardRxQueue;
	QueueHandle_t touchRxQueue;
	QueueHandle_t wifiRxQueue;

	char rc663;
	char rc663GotVersion;
	char rc663InIrq;

	menuDefinition *curMenu;

	SdioStatus sdioStatus;
	SemaphoreHandle_t sdioSemaphore;

	uint8_t loginId[10]; //ID of logged in card

	uint8_t isLoggedIn; // Flag that device is logged in
	uint8_t isFieldOn;
	char location[40];

	uint32_t systemReady;
	int32_t startPurseTransactionCount;
	int32_t purseTransactionCount;
	uint32_t deviceRouteId;
	uint32_t deviceServiceId;
#if CURRENT_TARGET == CNS_TOPUP
	int32_t topupValue; // Topup value, set to 0 after topup is done or when only need to display value
	int32_t topupWith; // Topup value, set to 0 after topup is done or when only need to display value
	int32_t topupBalance; // Topup value, set to 0 after topup is done or when only need to display value
#endif
	uint8_t rotation;
	uint32_t penaltyCard;
	uint32_t penaltyCash;
	uint8_t samNeedReset;
	uint8_t cardNeedsWupa;

#if DNFCC_ADD_GSM_DISABLE == 1
uint8_t gsmEnabled;
#endif

} DeviceStatus;

// 32 bits; but each needs to have an unique bit set
#define TASK_BIT_SOUND          		0x00001
#define TASK_BIT_CANBUS		   		0x00002
#define TASK_BIT_CLOCK 				0x00004
#define TASK_BIT_DATADISTRIBUTOR		0x00008
#define TASK_BIT_DISPLAY				0x00010
#define TASK_BIT_FLASHPROGRAMMER		0x00020
#define TASK_BIT_GPS					0x00040
#define TASK_BIT_GSM					0x00080
#define TASK_BIT_MENU				0x00100
#define TASK_BIT_OTG					0x00200
#define TASK_BIT_PRINTER				0x00400
#define TASK_BIT_QRCODE				0x00800
#define TASK_BIT_SAM					0x01000
#define TASK_BIT_SDIO				0x02000
#define TASK_BIT_SMARTCARD			0x04000
#define TASK_BIT_TOUCH				0x08000
#define TASK_BIT_WIFI				0x10000

#ifndef LOGSTATUS_C_
extern DeviceStatus deviceStatus;
#endif

#endif /* DEFINITION_DEVICESTATUS_H_ */
