/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef DEFINITION_QUEUESENDROUTINES_H_
#define DEFINITION_QUEUESENDROUTINES_H_

#include "../project_includes.h"

void queueSendToCanbus(char *message, TickType_t waitTime);
void queueSendToClock(char *message, TickType_t waitTime);
void queueSendToDataDistributor(char *message, TickType_t waitTime);
void queueSendToDisplay(char *message, TickType_t waitTime);
void queueSendToFlashProgrammer(char *message, TickType_t waitTime);
void queueSendToGps(char *message, TickType_t waitTime);
void queueSendToGsm(char *message, TickType_t waitTime);
void queueSendToMenu(char *message, TickType_t waitTime);
void queueSendToPrinter(char *message, TickType_t waitTime);
void queueSendToQrcode(char *message, TickType_t waitTime);
void queueSendToSam(char *message, TickType_t waitTime);
void queueSendToSdio(char *message, TickType_t waitTime);
void queueSendToSmartcard(char *message, TickType_t waitTime);
void queueSendToSound(char *message, TickType_t waitTime);
void queueSendToTouch(char *message, TickType_t waitTime);
void queueSendToWifi(char *message, TickType_t waitTime);

#endif /* DEFINITION_QUEUESENDROUTINES_H_ */
