/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef PROJECT_INCLUDES_H_
#define PROJECT_INCLUDES_H_

#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>

#include "_configuration/deviceSelector.h"
#include "_configuration/queueDefinitions.h"
#include "_configuration/filenames.h"
#include "_configuration/loginTypes.h"
#include "_configuration/locale.h"
#include "_definition/communicationCommands.h"
#include "menu/menu.h"
#include "menu/menu_definition.h"

#include "_definition/deviceStatus.h"
#include "_definition/global_functions.h"
#include "_definition/queueSendRoutines.h"
#include "_library/priorityController/priorityController.h"
#include "_library/ledcontrol/ledControl.h"

#include "platform_config.h"

#endif /* PROJECT_INCLUDES_H_ */
