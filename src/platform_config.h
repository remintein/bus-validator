/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#ifndef PLATFORM_CONFIG_H_
#define PLATFORM_CONFIG_H_

#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "_configuration/deviceSelector.h"

// RC663 - interface with the smartcard
#define CLRC663_SPI				SPI1

#define CLRC663_PIN_PDOWN 		GPIO_PIN_0
#define CLRC663_GPIO_PDOWN 		GPIOE

#define CLRC663_PIN_IRQ 			GPIO_PIN_1
#define CLRC663_GPIO_IRQ 		GPIOE

#define CLRC663_PIN_NSS       	GPIO_PIN_4
#define CLRC663_GPIO_NSS       	GPIOC

#define CLRC663_PIN_SCK          GPIO_PIN_5
#define CLRC663_GPIO_SCK         GPIOA

#define CLRC663_PIN_MISO         GPIO_PIN_6
#define CLRC663_GPIO_MISO        GPIOA

#define CLRC663_PIN_MOSI         GPIO_PIN_7
#define CLRC663_GPIO_MOSI        GPIOA

// Display - Interface with the display
#define DISPLAY_SPI				SPI2

#define DISPLAY_PIN_MISO		    GPIO_PIN_2
#define DISPLAY_GPIO_MISO		GPIOC

#define DISPLAY_PIN_MOSI		    GPIO_PIN_3
#define DISPLAY_GPIO_MOSI		GPIOC

#define DISPLAY_PIN_NSS			GPIO_PIN_9
#define DISPLAY_GPIO_NSS		    GPIOB

#define DISPLAY_PIN_SCK			GPIO_PIN_13
#define DISPLAY_GPIO_SCK		    GPIOB

#define DISPLAY_PIN_WAIT         GPIO_PIN_13
#define DISPLAY_GPIO_WAIT        GPIOD

#define DISPLAY_PIN_RESET        GPIO_PIN_12
#define DISPLAY_GPIO_RESET       GPIOD

#define DISPLAY_PIN_LED_GREEN    GPIO_PIN_3
#define DISPLAY_GPIO_LED_GREEN   GPIOD

#define DISPLAY_PIN_LED_BLUE     GPIO_PIN_5
#define DISPLAY_GPIO_LED_BLUE    GPIOD

#define DISPLAY_PIN_LED_RED      GPIO_PIN_4
#define DISPLAY_GPIO_LED_RED     GPIOD

// Touch pins

//#define TOUCH_PIN_WAKE			GPIO_PIN_6
//#define TOUCH_GPIO_WAKE			GPIOD

#define TOUCH_PIN_INT			GPIO_PIN_5
#define TOUCH_GPIO_INT			GPIOB

#define TOUCH_I2C			 	I2C2
#define TOUCH_SCL_AF			    GPIO_AF4_I2C2
#define TOUCH_SDA_AF	      		GPIO_AF4_I2C2

#define TOUCH_PIN_SCL			GPIO_PIN_10
#define TOUCH_GPIO_SCL			GPIOB

#define TOUCH_PIN_SDA			GPIO_PIN_11
#define TOUCH_GPIO_SDA			GPIOB


// For 7" display
#if  DISPLAY_SIZE == 7
#define TOUCH_PIN_WAKE			GPIO_PIN_6
#define TOUCH_GPIO_WAKE			GPIOD

#define TOUCH_PIN_RESET			GPIO_PIN_3
#define TOUCH_GPIO_RESET		    GPIOB
#endif
#if  DISPLAY_SIZE == 5
// For 5" touch
#define TOUCH_PIN_WAKE			GPIO_PIN_3
#define TOUCH_GPIO_WAKE			GPIOB

#define TOUCH_PIN_RESET			GPIO_PIN_6
#define TOUCH_GPIO_RESET		    GPIOD

#endif


// Canbus - Interface with the CAN pins
#define CANBUS_PIN_RX1           GPIO_PIN_0
#define CANBUS_GPIO_RX1          GPIOD

#define CANBUS_PIN_TX1           GPIO_PIN_1
#define CANBUS_GPIO_TX1          GPIOD

// UART1 - Interface to GSM
#define GSM_UART					USART1
#define GSM_UART_NR				1
#define GSM_UART_IRQ				USART1_IRQn

#define GSM_AF					GPIO_AF7_USART1

#define GSM_PIN_TX				GPIO_PIN_6
#define GSM_GPIO_TX				GPIOB

#define GSM_PIN_RX				GPIO_PIN_7
#define GSM_GPIO_RX				GPIOB

#define GSM_PIN_STATUS			GPIO_PIN_8
#define GSM_GPIO_STATUS			GPIOA

#define GSM_PIN_PWRKEY           GPIO_PIN_1
#define GSM_GPIO_PWRKEY          GPIOC

#define GSM_PIN_GPSCTRL          GPIO_PIN_0
#define GSM_GPIO_GPSCTRL         GPIOC

// UART4 - Interface to QRCODEPRINTER
#define QRCODEPRINTER_UART		UART4
#define QRCODEPRINTER_UART_NR	4
#define QRCODEPRINTER_UART_IRQ	UART4_IRQn

#define QRCODEPRINTER_AF			GPIO_AF8_UART4

#define PRINTER_PIN_TX			GPIO_PIN_0
#define PRINTER_GPIO_TX			GPIOA

#define QRCODE_PIN_RX			GPIO_PIN_1
#define QRCODE_GPIO_RX			GPIOA

// UART3
#define RS485_UART		USART3
#define RS485_UART_NR	3
#define RS485_UART_IRQ	USART3_IRQn


#define RS485_PIN_TX				GPIO_PIN_8
#define RS485_GPIO_TX			GPIOD

#define RS485_PIN_RX				GPIO_PIN_9
#define RS485_GPIO_RX			GPIOD

#define RS485_PIN_DE				GPIO_PIN_5
#define RS485_GPIO_DE			GPIOC

#define RS485_PIN_CS				GPIO_PIN_14
#define RS485_GPIO_CS			GPIOD

// UART6 - Interface to WIFI
#define WIFI_UART				USART6
#define WIFI_UART_NR			 	6
#define WIFI_UART_IRQ			USART6_IRQn

#define WIFI_AF					GPIO_AF8_USART6

#define WIFI_PIN_TX				GPIO_PIN_6
#define WIFI_GPIO_TX				GPIOC

#define WIFI_PIN_RX				GPIO_PIN_7
#define WIFI_GPIO_RX				GPIOC

#define WIFI_PIN_RST				GPIO_PIN_6
#define WIFI_GPIO_RST			GPIOE

#define WIFI_PIN_GPIO0			GPIO_PIN_13
#define WIFI_GPIO_GPIO0			GPIOC

// SDIO pins
#define SDCARD_PIN_CMD			GPIO_PIN_2
#define SDCARD_GPIO_CMD			GPIOD

#define SDCARD_PIN_CK			GPIO_PIN_12
#define SDCARD_GPIO_CK			GPIOC

#define SDCARD_PIN_D0			GPIO_PIN_8
#define SDCARD_GPIO_D0			GPIOC

#define SDCARD_PIN_D1			GPIO_PIN_9
#define SDCARD_GPIO_D1			GPIOC

#define SDCARD_PIN_D2			GPIO_PIN_10
#define SDCARD_GPIO_D2			GPIOC

#define SDCARD_PIN_D3			GPIO_PIN_11
#define SDCARD_GPIO_D3			GPIOC

// DAC pins
#define DAC_PIN_DAC1				GPIO_PIN_4
#define DAC_GPIO_DAC1			GPIOA

#define SAM_UART					USART2
#define SAM_UART_NR			    2
#define SAM_UART_IRQ				USART2_IRQn

#define SAM_AF				    GPIO_AF7_USART2

#define SAM_PIN_RXTX				GPIO_PIN_2
#define SAM_GPIO_RXTX		    GPIOA

#define SAM_PIN_CK			    GPIO_PIN_7
#define SAM_GPIO_CK			    GPIOD

#define SAM_PIN_RESET	    		GPIO_PIN_9
#define SAM_GPIO_RESET		    GPIOE

#define SAM1_PIN_CS_IO          GPIO_PIN_2
#define SAM1_GPIO_CS_IO         GPIOE

#define SAM2_PIN_CS_IO          GPIO_PIN_3
#define SAM2_GPIO_CS_IO         GPIOE

#define SAM3_PIN_CS_IO          GPIO_PIN_4
#define SAM3_GPIO_CS_IO         GPIOE

#define SAM4_PIN_CS_IO          GPIO_PIN_5
#define SAM4_GPIO_CS_IO         GPIOE

#define SAM1_PIN_CS_I2C         GPIO_PIN_0
#define SAM1_GPIO_CS_I2C        GPIOB

#define SAM2_PIN_CS_I2C         GPIO_PIN_1
#define SAM2_GPIO_CS_I2C        GPIOB

#define SAM3_PIN_CS_I2C         GPIO_PIN_7
#define SAM3_GPIO_CS_I2C        GPIOE

#define SAM4_PIN_CS_I2C         GPIO_PIN_8
#define SAM4_GPIO_CS_I2C        GPIOE

// USB-OTG

#define OTG_PIN_POWERON         GPIO_PIN_11
#define OTG_GPIO_POWERON        GPIOD

#define OTG_PIN_OVERCURRENT     GPIO_PIN_10
#define OTG_GPIO_OVERCURRENT    GPIOD

#define OTG_PIN_VBUS            GPIO_PIN_9
#define OTG_GPIO_VBUS           GPIOA

#define OTG_PIN_FSID            GPIO_PIN_10
#define OTG_GPIO_FSID           GPIOA

#define OTG_PIN_FSDM            GPIO_PIN_11
#define OTG_GPIO_FSDM           GPIOA

#define OTG_PIN_FSDP            GPIO_PIN_12
#define OTG_GPIO_FSDP           GPIOA

#endif /* PLATFORM_CONFIG_H_ */

