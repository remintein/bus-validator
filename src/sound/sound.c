/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"
#include "sound.h"
#include "SoundSelector.h"

/**
 * @file
 * @brief Contains the functions for the Sound module
 */

/**
 * @addtogroup sound
 *
 * @{
 */


#define STACK_SIZE 200

static void MX_DMA_Init(void);
static void MX_DAC_Init(void);
static void MX_TIM6_Init(void);

void intDnfccSoundSetupHardware();
void Error_Handler();

DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;
TIM_HandleTypeDef htim6;


/**
 * @brief Initialize sound
 *
 * @param uxPriority - the prio to run on
 */
BaseType_t nfccSoundInit(UBaseType_t uxPriority) {
	deviceStatus.soundRxQueue = xQueueCreate(SOUND_QUEUE_LENGTH, SOUND_BUFFER_SIZE);
	/* We want this queue to be viewable in a RTOS kernel aware debugger,
	 so register it. */
	vQueueAddToRegistry(deviceStatus.soundRxQueue, "Sound");

	intDnfccSoundSetupHardware();
	setWaitForSystemReadyThisTask(TASK_BIT_SOUND);
	return xTaskCreate(nfccSound, "Sound", STACK_SIZE, NULL, uxPriority, &deviceStatus.soundTask);

}

/**
 * @brief QRCode main loop
 *
 * The main loop for the Qrcode
 */
void nfccSound(void *pvParameters) {
	char buf[SOUND_BUFFER_SIZE + 1];
	char stbuf[DATADISTRIBUTOR_BUFFER_SIZE + 1];
	UBaseType_t uxHighWaterMark;

	setTaskIsSystemReady(TASK_BIT_SOUND);
	waitForSystemReady();

	apiDnfccSoundValidIn(SoundVN);

	for (;;) {
		buf[0] = '\0';
		if (xQueueReceive(deviceStatus.soundRxQueue, buf,
				portMAX_DELAY) != pdPASS) {
			vTaskDelay(1);
			continue;
		}

		switch (buf[0]) {
		case COMCOM_SOUND_KEYCLICK:
			apiDnfccSoundKeyclick();
			break;

		case COMCOM_SOUND_VALID_IN:
			apiDnfccSoundValidIn(buf[1]);
			break;

		case COMCOM_SOUND_VALID_IN_FEMALE:
			apiDnfccSoundValidInFemale(buf[1]);
			break;

		case COMCOM_SOUND_INVALID_IN:
			apiDnfccSoundInvalidIn(buf[1]);
			break;

		case COMCOM_SOUND_TRYAGAIN:
			apiDnfccSoundTryAgain(buf[1]);
			break;

		case COMCOM_SOUND_VALID_IN_LOW:
			apiDnfccSoundValidInLow(buf[1]);
			break;

		case COMCOM_SELFTEST:
			uxHighWaterMark = uxTaskGetStackHighWaterMark(deviceStatus.soundTask);
			sprintf(stbuf, "%c%cWM%ld", COMCOM_DATADISTRIBUTOR_SELFTESTRESULT,
			COMSELFTEST_CLOCK, uxHighWaterMark);
			queueSendToDataDistributor(stbuf, portMAX_DELAY);
			break;
		}
	}
}

/**
 * @brief DAC init function
 */
static void MX_DAC_Init(void) {

	DAC_ChannelConfTypeDef sConfig;

	/**DAC Initialization
	 */
	hdac.Instance = DAC;
	if (HAL_DAC_Init(&hdac) != HAL_OK) {
		Error_Handler();
	}

	/**DAC channel OUT1 config
	 */
	sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
//    sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
	if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK) {
		Error_Handler();
	}

}

/**
 * @brief TIM6 initialization function
 */
static void MX_TIM6_Init(void) {

	TIM_MasterConfigTypeDef sMasterConfig;

	htim6.Instance = TIM6;
	htim6.Init.Prescaler = 0;
	htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim6.Init.Period = 32768;
	htim6.Init.Period = 3276;
	if (HAL_TIM_Base_Init(&htim6) != HAL_OK) {
		Error_Handler();
	}

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK) {
		Error_Handler();
	}

}

/**
 * @brief Set up the sound hardware
 */
void intDnfccSoundSetupHardware() {
	hdac.Instance = DAC;
	MX_DMA_Init();

	MX_DAC_Init();
	MX_TIM6_Init();

	apiDnfccSoundStartSystem();
}

/**
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void) {
	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE()
	;

	/* DMA interrupt init */
	/* DMA1_Stream5_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);
}

/**
 * @brief Initialize DAC + DMA
 *
 * @param hdac - Hardware DAC
 */
void HAL_DAC_MspInit(DAC_HandleTypeDef* hdac) {

	GPIO_InitTypeDef GPIO_InitStruct;
	if (hdac->Instance == DAC) {
		/* Peripheral clock enable */
		__HAL_RCC_DAC_CLK_ENABLE()
		;

		/**DAC GPIO Configuration
		 PA4     ------> DAC_OUT1
		 */
		GPIO_InitStruct.Pin = DAC_PIN_DAC1;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(DAC_GPIO_DAC1, &GPIO_InitStruct);

		/* Peripheral DMA init*/

		hdma_dac1.Instance = DMA1_Stream5;
		hdma_dac1.Init.Channel = DMA_CHANNEL_7;
		hdma_dac1.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_dac1.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_dac1.Init.MemInc = DMA_MINC_ENABLE;
		hdma_dac1.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
		hdma_dac1.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
		hdma_dac1.Init.Mode = DMA_NORMAL;
		hdma_dac1.Init.Priority = DMA_PRIORITY_LOW;
		hdma_dac1.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
		if (HAL_DMA_Init(&hdma_dac1) != HAL_OK) {
			Error_Handler();
		}

		__HAL_LINKDMA(hdac, DMA_Handle1, hdma_dac1);

		/* Peripheral interrupt init */
		HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
	}

}

/**
 * @brief Initialize the time base
 *
 * @param htim_base - Hardware time base
 */
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base) {

	if (htim_base->Instance == TIM6) {
		/* Peripheral clock enable */
		__HAL_RCC_TIM6_CLK_ENABLE()
		;
		/* Peripheral interrupt init */
		HAL_NVIC_SetPriority(TIM6_DAC_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(TIM6_DAC_IRQn);
	}
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
 * @brief This function handles DMA1 stream5 global interrupt.
 */
void DMA1_Stream5_IRQHandler(void) {
	HAL_DMA_IRQHandler(&hdma_dac1);
}

/**
 * @brief This function handles TIM6 global interrupt, DAC1 and DAC2 underrun error interrupts.
 */
void TIM6_DAC_IRQHandler(void) {
	HAL_DAC_IRQHandler(&hdac);
	HAL_TIM_IRQHandler(&htim6);
}

/** @} */
