/*
 * Copyright (c), Dutch NFC Consult B.V. (DNFCC)
 *
 * (C) DNFCC 2016,2017,2018
 *
 * All rights are reserved. Reproduction in whole or in part is prohibited
 * without the written consent of the copyright owner. DNFCC reserves the
 * right to make changes without notice at any time.
 * DNFCC makes no warranty, expressed, implied or statutory, including but
 * not limited to any implied warranty of merchantability or fitness for any
 * particular purpose, or that the use will not infringe any third party patent,
 * copyright or trademark. DNFCC must not be liable for any loss or damage
 * arising from its use.
 *
 * It is prohibited to make any changes to the software library without
 * written notice to DNFCC.
 * DNFCC grants CNS a personal, non-assignable & non-transferable, perpetual,
 * commercial, royalty free, including the rights to create but not distribute
 * derivative works, non-exclusive license. DNFCC cannot be liable for any
 * warranty arising from derivative work, and cannot be liable for any loss
 * or damage arising from its use.
 *
 */

#include "../project_includes.h"
#include "sound.h"

DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;
TIM_HandleTypeDef htim6;

// Vietnamese
#include "sounds/VN/wav_cam_on.h"        // thank you
#include "sounds/VN/wav_chao_chi.h"      // hello lady
#include "sounds/VN/wav_xin_chao.h"      // hello
#include "sounds/VN/wav_chao_anh.h"      // hello sir
#include "sounds/VN/wav_xin_thu_lai.h"   // try again
#include "sounds/VN/wav_the_bj_loi.h"    // invalid
#include "sounds/VN/wav_gia_trj_thap.h"  // low balance
// English
#include "sounds/EN/wav_thank_you.h"
#include "sounds/EN/wav_hi.h"
#include "sounds/EN/wav_hello.h"
#include "sounds/EN/wav_try_again_please.h"
#include "sounds/EN/wav_invalid_card.h"
#include "sounds/EN/wav_warning_low_balance.h"

#include "sounds/wav_keyclick.h"
/**
 * @file
 * @brief Contains the functions for the individual sounds
 */

/**
 * @addtogroup sound
 *
 * @{
 */

/**
 * @brief Play the data provided to the DAC (start the DAC with DMA)
 *
 * @param sound - Pointer to the sound data
 * @param length - Length of the data
 */
void apiDnfccPlaySound(const uint16_t *sound, const uint16_t length) {
	HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
	HAL_TIM_Base_Start(&htim6);
	HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*) sound, length,
	DAC_ALIGN_12B_R);
}

/**
 * @brief Play sound for start system
 */
void apiDnfccSoundStartSystem() {
	apiDnfccPlaySound(wavDatahi, sizeof(wavDatahi) / sizeof(int16_t));
}

/**
 * @brief Play sound KeyClick
 */
void apiDnfccSoundKeyclick() {
	apiDnfccPlaySound(wavDatakeyclick, sizeof(wavDatakeyclick) / sizeof(int16_t));
//	apiDnfccPlaySound(wavDataxin_chao, sizeof(wavDataxin_chao) / sizeof(int16_t));
}

/**
 * @brief Play sound Valid in
 *
 * @param locale - the locale to use
 */
void apiDnfccSoundValidIn(char locale) {
#if DNFCC_SOUND == 2
	switch (locale) {
		case NoSound:
		break;
		case SoundVN:
#endif
	apiDnfccSoundKeyclick();
//	apiDnfccPlaySound(wavDatachao_chi, sizeof(wavDatachao_chi) / sizeof(int16_t));
#if DNFCC_SOUND == 2
	break;
	case SoundEN:
	apiDnfccPlaySound(wavDatahello, sizeof(wavDatahello) / sizeof(int16_t));
	break;
}
#endif
}

/**
 * @brief Play sound Valid in female
 *
 * @param locale - the locale to use
 */
void apiDnfccSoundValidInFemale(char locale) {
#if DNFCC_SOUND == 2
	switch (locale) {
		case NoSound:
		break;
		case SoundVN:
		apiDnfccPlaySound(wavDatachao_chi, sizeof(wavDatachao_chi) / sizeof(int16_t));
		break;
		case SoundEN:
		apiDnfccPlaySound(wavDatahi, sizeof(wavDatahi) / sizeof(int16_t));
		break;
	}
#else
	apiDnfccSoundKeyclick();
//	apiDnfccPlaySound(wavDatachao_anh, sizeof(wavDatachao_anh) / sizeof(int16_t));
#endif
}

/**
 * @brief Play sound Invalid in (reject card)
 *
 * @param locale - the locale to use
 */
void apiDnfccSoundInvalidIn(char locale) {
#if DNFCC_SOUND == 2
	switch (locale) {
		case NoSound:
		break;
		case SoundVN:
#endif
	apiDnfccSoundKeyclick();

//apiDnfccPlaySound(wavDatathe_bj_loi, sizeof(wavDatathe_bj_loi) / sizeof(int16_t));
#if DNFCC_SOUND == 2
	break;
	case SoundEN:
	apiDnfccPlaySound(wavDatainvalid_card,
			sizeof(wavDatainvalid_card) / sizeof(int16_t));
	break;
}
#endif
}

/**
 * @brief Play sound valid in (low balance)
 *
 * @param locale - the locale to use
 */
void apiDnfccSoundValidInLow(char locale) {
#if DNFCC_SOUND == 2
	switch (locale) {
		case NoSound:
		break;
		case SoundVN:
#endif
	apiDnfccSoundKeyclick();
//	apiDnfccPlaySound(wavDatagia_trj_thap, sizeof(wavDatagia_trj_thap) / sizeof(int16_t));
#if DNFCC_SOUND == 2
	break;
	case SoundEN:
	apiDnfccPlaySound(wavDatawarning_low_balance,
			sizeof(wavDatawarning_low_balance) / sizeof(int16_t));
	break;
}
#endif
}

/**
 * @brief Play sound try again
 *
 * @param locale - the locale to use
 */
void apiDnfccSoundTryAgain(char locale) {
#if DNFCC_SOUND == 2
	switch (locale) {
		case NoSound:
		break;
		case SoundVN:
#endif
	apiDnfccSoundKeyclick();
//	apiDnfccPlaySound(wavDataxin_thu_lai, sizeof(wavDataxin_thu_lai) / sizeof(int16_t));
#if DNFCC_SOUND == 2
	break;
	case SoundEN:
	apiDnfccPlaySound(wavDatatry_again_please, sizeof(wavDatatry_again_please) / sizeof(int16_t));
	break;
}
#endif
}

/** @} */
