var _file_functions_8c =
[
    [ "LOG_RECORD_SIZE", "group__sdio.html#ga51fa2188f047f540edc87f8df1392551", null ],
    [ "SDCARD_VERSION", "_file_functions_8c.html#a321522a9628b7f02df2ffe8508cdd709", null ],
    [ "SIGNATURE", "_file_functions_8c.html#ae72ce63657183560f1b11dbce5a0a5f6", null ],
    [ "apiapiDnfccInitSectorForWriteByRead", "group__sdio.html#ga377ba51334ecd4c21b41db64fcbf229d", null ],
    [ "apiDnfccAllocateFile", "group__sdio.html#gaeb65884ade085394b282aa060c8681c2", null ],
    [ "apiDnfccFlushWrite", "group__sdio.html#ga6c3ccc55e4f1135ad40fed77edc8689f", null ],
    [ "apiDnfccFormatCardIfNeeded", "group__sdio.html#gac66a43ca46880e14e52a3363f83f14f0", null ],
    [ "apiDnfccGetNextMessageFromLogging", "group__sdio.html#ga4bee10efe15caa841e617eb7f47d6c33", null ],
    [ "apiDnfccInitSdioBlockInfo", "group__sdio.html#gacb08b12f263f8d44a5e49e02574844bb", null ],
    [ "apiDnfccInitSector", "group__sdio.html#ga310f39ff2259e0033ff147a4dafbfb10", null ],
    [ "apiDnfccPublishFile", "group__sdio.html#ga300efcba382c727549b92a8d9655723e", null ],
    [ "apiDnfccReadSector", "group__sdio.html#ga1d9ea46494b794bc4a976c76c07440bd", null ],
    [ "apiDnfccReadSectorRaw", "group__sdio.html#ga568d9842b70daf7e809dc5df5762d3fa", null ],
    [ "apiDnfccSetWriteData", "group__sdio.html#gad480579e2309fd5b89922c39577fe6a2", null ],
    [ "apiDnfccUpdateDeviceStructVersions", "group__sdio.html#ga5c7673bedb11f4b6dbc7cffe95493c94", null ],
    [ "apiDnfccWriteLogfile128Bytes", "group__sdio.html#gaac0babebb09c39ef74d204913eacce9f", null ],
    [ "intDnfccFlushWriteRaw", "group__sdio.html#ga513a8801fb2205da8ca993738a90fd36", null ],
    [ "intDnfccInitSectorForWriteByReadRaw", "group__sdio.html#ga6ebdaea45a2b6bc96a5f106f9a7cc37a", null ]
];