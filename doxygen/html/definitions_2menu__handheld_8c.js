var definitions_2menu__handheld_8c =
[
    [ "initAllMenusHandheld", "definitions_2menu__handheld_8c.html#aaf75544cd463c82386c757a7454720a6", null ],
    [ "buttons_cardPayment", "definitions_2menu__handheld_8c.html#a8b511ff2baee897cefcd4ef832699181", null ],
    [ "buttons_cashPayment", "definitions_2menu__handheld_8c.html#a167fbacdf1a05b6de9f3f0a95942471d", null ],
    [ "buttons_handheld", "definitions_2menu__handheld_8c.html#ace4fb37d62cc216bdad0e5c062dc7721", null ],
    [ "buttons_handheldFail", "definitions_2menu__handheld_8c.html#a0c90464df2168e947b0e9b9371faee7d", null ],
    [ "buttons_handheldPass", "definitions_2menu__handheld_8c.html#a558def004f352246a62dfe98217e378b", null ],
    [ "buttons_penaltypay", "definitions_2menu__handheld_8c.html#a725bf2ec4ab96227b32085208ce3bc2e", null ],
    [ "handheldInfo", "definitions_2menu__handheld_8c.html#ad754c32e602c5a082a0db32703ba6953", null ],
    [ "menu_handheld", "definitions_2menu__handheld_8c.html#a517ceffa138220ccc85123f4482cb793", null ],
    [ "menu_handheld_card_payment", "definitions_2menu__handheld_8c.html#a3026039350da27f8f2b020d87fa4ddff", null ],
    [ "menu_handheld_cash_payment", "definitions_2menu__handheld_8c.html#aab85503b28c837b5ababee28bfe36513", null ],
    [ "menu_handheld_selectpenaltypay", "definitions_2menu__handheld_8c.html#a4f6d04d257d0be63111ac2ccd57cad2d", null ],
    [ "menu_handheldFail", "definitions_2menu__handheld_8c.html#ae49b4c4dfba675494c915e42b358ef64", null ],
    [ "menu_handheldPass", "definitions_2menu__handheld_8c.html#ad6a2ec3c8d266f3f77f7869371aee788", null ]
];