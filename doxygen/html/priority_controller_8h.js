var priority_controller_8h =
[
    [ "PriorityState", "priority_controller_8h.html#a542c2cbbb29750d16fbe517ec9af868c", [
      [ "PRIORITY_CARD_HANDLING", "priority_controller_8h.html#a542c2cbbb29750d16fbe517ec9af868ca2528c3e70ec891e8cef5a381be2c676e", null ],
      [ "PRIORITY_IDLE", "priority_controller_8h.html#a542c2cbbb29750d16fbe517ec9af868caecab2be6faa4394734e6f9131c0a5416", null ],
      [ "PRIORITY_GET_DATA_FROM_BACKEND", "priority_controller_8h.html#a542c2cbbb29750d16fbe517ec9af868cac03fb6af8820c0cc4450c5c063d53b23", null ]
    ] ],
    [ "apiDnfccGetPriority", "group___priority.html#gaed8a83151f05156259a1b11e0b0178f3", null ],
    [ "apiDnfccSetPriorityTo", "group___priority.html#ga34a295d87866f6dde73cd387a8ea9d76", null ]
];