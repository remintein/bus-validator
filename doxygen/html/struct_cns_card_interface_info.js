var struct_cns_card_interface_info =
[
    [ "cardUid", "struct_cns_card_interface_info.html#a3968c700539a5c61ef8e953702448b90", null ],
    [ "curApplication", "struct_cns_card_interface_info.html#af0f108558cdeaff8cf75d8e1591e6f2a", null ],
    [ "curApplicationKey", "struct_cns_card_interface_info.html#ada8339f5ea184f586ea7e3ee448beaad", null ],
    [ "file1", "struct_cns_card_interface_info.html#abbd19f0f274582143b5f243774787c7a", null ],
    [ "file10", "struct_cns_card_interface_info.html#a684a5bfdd2426151ea4f1c6fe9b44d72", null ],
    [ "file11", "struct_cns_card_interface_info.html#a311c334f0579d3ea486fd61fa8a7d581", null ],
    [ "file12", "struct_cns_card_interface_info.html#abebda3b93c0c3f1c16a6018db8abb237", null ],
    [ "file13", "struct_cns_card_interface_info.html#a432e00b8ef1091f9793af08f5e038d7f", null ],
    [ "file2", "struct_cns_card_interface_info.html#a2e60881e9c59725a223c94e3df770610", null ],
    [ "file3", "struct_cns_card_interface_info.html#ad4e30469f3847a75e3a6eb6c16f2fd72", null ],
    [ "file4", "struct_cns_card_interface_info.html#a3d4f4e61b29d0efee75d8768b7ff8ceb", null ],
    [ "file5", "struct_cns_card_interface_info.html#a63b3776541e75c81cd01bbfa134b6c4f", null ],
    [ "file6", "struct_cns_card_interface_info.html#aa28fa649705cd5d92b027ecf6bce2633", null ],
    [ "file7", "struct_cns_card_interface_info.html#a27b31c386a8a6a3343224cfd2a385bf9", null ],
    [ "file8", "struct_cns_card_interface_info.html#a87311fc31109ee401f327ec987d774d9", null ],
    [ "file9", "struct_cns_card_interface_info.html#a31f1e38f99aa5d0c980ea10decc1e92f", null ],
    [ "fileIsRead", "struct_cns_card_interface_info.html#ab203bd0c30a2027f29f3d65b5d889aff", null ],
    [ "isCardInitialized", "struct_cns_card_interface_info.html#a956b9f86df6b6d5c4cd130aaa8f62aa2", null ],
    [ "isInitialized", "struct_cns_card_interface_info.html#a3ce7059e2a27f03a777329656f3fcd02", null ],
    [ "previousReason", "struct_cns_card_interface_info.html#ae821704b6564f116a724e2f9e7c03e31", null ],
    [ "reason", "struct_cns_card_interface_info.html#a364211b7f89eb8be8d1b3edbba74ab3f", null ],
    [ "removedPurse", "struct_cns_card_interface_info.html#a16b350fec07b687898b1a214017eca45", null ],
    [ "removedTicket", "struct_cns_card_interface_info.html#a7a595d50d22d5883f30e227791c7ab79", null ]
];