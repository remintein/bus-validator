var structfile_purse_information_struct =
[
    [ "fileFormatVersion", "structfile_purse_information_struct.html#ac2863dffb43412432f5af6520b4aaaf6", null ],
    [ "futureUse", "structfile_purse_information_struct.html#a97406bbdb46254473c074f92e1afa0f8", null ],
    [ "purse_ID", "structfile_purse_information_struct.html#ae8840ada1d7f534e92c6314549102e4c", null ],
    [ "purseEffectiveDate", "structfile_purse_information_struct.html#a7647beb6f77db0fd530699ee908bf5fd", null ],
    [ "purseExpiredDate", "structfile_purse_information_struct.html#a0009b2fc5aebbb05fb04de411268df3b", null ],
    [ "purseStatus", "structfile_purse_information_struct.html#a6a7efce9a60850f543438388e5cab388", null ],
    [ "purseType", "structfile_purse_information_struct.html#a9208c22dccf96686e0369245f6723a45", null ]
];