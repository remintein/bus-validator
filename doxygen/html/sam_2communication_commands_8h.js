var sam_2communication_commands_8h =
[
    [ "SamClaimTimeout", "sam_2communication_commands_8h.html#a8dfe9436aa11b6d77ea068c9b9d04431", null ],
    [ "SamInitializeCommunication", "sam_2communication_commands_8h.html#a32aa712457db883f48a30d3117b2db7b", null ],
    [ "SamReceiveEnvelope", "sam_2communication_commands_8h.html#ae84225be90c118dc5a4c7f56b4a1ffe9", null ],
    [ "SamReceiveRaw", "sam_2communication_commands_8h.html#a33c9da051ec3821f2ad32b4ecdba5767", null ],
    [ "SamRelease", "sam_2communication_commands_8h.html#a798a712a2e3c2e4db71eca2b6e007938", null ],
    [ "SamResetReceiving", "sam_2communication_commands_8h.html#ae1df43dab4d2abe93a5e307a64af443c", null ],
    [ "SamSetReady", "sam_2communication_commands_8h.html#a88672a6d9c1c92349a1d6d4bbc0be0d8", null ],
    [ "SamTransmitCommand", "sam_2communication_commands_8h.html#af91c6da6ab1b1ac471e3f839ba9f90e7", null ]
];