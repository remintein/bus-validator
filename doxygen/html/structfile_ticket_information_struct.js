var structfile_ticket_information_struct =
[
    [ "fileFormatVersion", "structfile_ticket_information_struct.html#a25927665a6c499f728b633817053e41d", null ],
    [ "futureUse", "structfile_ticket_information_struct.html#a5f3ca6c3414959a84502a016a6d97f8f", null ],
    [ "isRefundEnable", "structfile_ticket_information_struct.html#acc049c4c14b0516dfa7f0f033b438fc8", null ],
    [ "passengerType", "structfile_ticket_information_struct.html#a9a05cbd4450e03cafb3c360cb5b8f417", null ],
    [ "signature", "structfile_ticket_information_struct.html#a154faed698ea6be3e77c192c91b5404c", null ],
    [ "ticket_ID", "structfile_ticket_information_struct.html#ad707651f096c048c3889a7537beb41bb", null ],
    [ "ticketEffectiveDate", "structfile_ticket_information_struct.html#afde0eaf0c7e8135b6dc3d322d734a1da", null ],
    [ "ticketExpireDays", "structfile_ticket_information_struct.html#a91a8a11949b0fe7d7e6a8d554b1c0bdf", null ],
    [ "ticketStatus", "structfile_ticket_information_struct.html#a798b7abfe781a8a11d90d9eeaa3def7b", null ],
    [ "ticketType", "structfile_ticket_information_struct.html#aaddb43ed5d43cd01c75bfe7154cf1af9", null ]
];