var utf8_8c =
[
    [ "hex_digit", "utf8_8c.html#ace070dab2e8e5677b00c2a51cb368312", null ],
    [ "octal_digit", "utf8_8c.html#ae05aa5aa89e7a98f8a2c26802f9dee58", null ],
    [ "u8_charnum", "utf8_8c.html#ac7577e058d88a9d526ceb707720ba0d5", null ],
    [ "u8_dec", "utf8_8c.html#a43d2fd1cb6fc23ab114ee6aa08f2ec50", null ],
    [ "u8_escape", "utf8_8c.html#aabb216ea8eeacd07ab5434a70e09c2d5", null ],
    [ "u8_escape_wchar", "utf8_8c.html#ae32c73bb157f1e61ca6ebbed6a245c49", null ],
    [ "u8_inc", "utf8_8c.html#a5106ee6cd5f4391141ba579517130dc6", null ],
    [ "u8_is_locale_utf8", "utf8_8c.html#a990672773c84d879df7f80e2196578b4", null ],
    [ "u8_memchr", "utf8_8c.html#ae33e6d18da877c21a5e765ef42c0f373", null ],
    [ "u8_nextchar", "utf8_8c.html#a93b94aedf2b01816623de2d80394313b", null ],
    [ "u8_offset", "utf8_8c.html#a4eeeec541da17817f20e0227e204be72", null ],
    [ "u8_printf", "utf8_8c.html#aae074cad4c5df3d8f282e6ecdc9bf924", null ],
    [ "u8_read_escape_sequence", "utf8_8c.html#a4b1228190126fa7830134fa8ad95ede9", null ],
    [ "u8_seqlen", "utf8_8c.html#a923ee48171ef771f99c334e7ac14d38f", null ],
    [ "u8_strchr", "utf8_8c.html#a206a030b54bbeb634f5ad07de9398fc5", null ],
    [ "u8_strlen", "utf8_8c.html#a71d3f3bc598393f57391f78a4b1db96f", null ],
    [ "u8_toucs", "utf8_8c.html#af65d7e4c9583f4d69dbb049bc1976597", null ],
    [ "u8_toutf8", "utf8_8c.html#a103999e92a23e856c672191a4a40a926", null ],
    [ "u8_unescape", "utf8_8c.html#aade1b7d7a4b38a6f7f4be355fcac4645", null ],
    [ "u8_vprintf", "utf8_8c.html#a817d025a5399c1f7ba2289ad22611398", null ],
    [ "u8_wc_toutf8", "utf8_8c.html#ab2ffc62dabbb3a02fb788a1b8ea7dacf", null ]
];