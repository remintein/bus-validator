var _clock_8h =
[
    [ "apiDnfccClockCompareWithNow", "group___clock.html#ga0ae8be040ce60a740db747339066b863", null ],
    [ "apiDnfccClockFetchDate", "group___clock.html#gaffb7500dbc66f74fccf87baebfd82dfe", null ],
    [ "apiDnfccClockFetchDateHuman", "group___clock.html#gaef02c78379558f5261a2663617c0c964", null ],
    [ "apiDnfccClockFetchDateTime", "group___clock.html#ga06f6eb9d06b5016df6ebfcad14909984", null ],
    [ "apiDnfccClockFetchDateTimeHuman", "group___clock.html#gac7aad56aa0b69c458646f8ca1cb9050c", null ],
    [ "apiDnfccClockFetchTime", "group___clock.html#ga1e786de7360d275514b851e3029c6989", null ],
    [ "apiDnfccClockFetchTimeHuman", "group___clock.html#ga21c4af6b6dc8281f4e327bd444fee967", null ],
    [ "nfccClock", "group___clock.html#ga4bbf5ff2497bf60d0023f4e815f186ec", null ],
    [ "nfccClockInit", "group___clock.html#ga0a440e2dea8442ecc34a275d587edd7c", null ]
];