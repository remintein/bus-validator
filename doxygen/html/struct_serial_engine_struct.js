var struct_serial_engine_struct =
[
    [ "curReceiveByte", "struct_serial_engine_struct.html#ac70f9c64fac239ab26444166b4be31ff", null ],
    [ "huart", "struct_serial_engine_struct.html#a28277cc4673e682fe9593eeea6d14ca2", null ],
    [ "lastCommunication", "struct_serial_engine_struct.html#ae319941fc2f83bb6bc93acf27c754d48", null ],
    [ "receiveBuffer", "struct_serial_engine_struct.html#afcba7d06b52f20f1c79a4262b485ab8a", null ],
    [ "receiveBufferLength", "struct_serial_engine_struct.html#a5ce1152499b2b9eee7d8b25949a5296e", null ],
    [ "receivePointer", "struct_serial_engine_struct.html#af478b92d8f5658ad42a71a3e5846c23b", null ],
    [ "receiveProcessedTill", "struct_serial_engine_struct.html#ac26ee7c3cf1007a48043b7dc4810991c", null ],
    [ "transmitBuffer", "struct_serial_engine_struct.html#ae48167ab9b5420e8fe04973a4126247b", null ],
    [ "transmitBufferLength", "struct_serial_engine_struct.html#a080324f8ce100937b0ddaa5b072c7664", null ],
    [ "transmitPointer", "struct_serial_engine_struct.html#afcfceb45fe034e148a292721dff4d42f", null ],
    [ "transmitPointerTill", "struct_serial_engine_struct.html#a37204e68c1daa5971f3e16b879a2cbab", null ]
];