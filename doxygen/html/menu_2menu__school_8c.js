var menu_2menu__school_8c =
[
    [ "DELAY_SHOW_BALANCE", "menu_2menu__school_8c.html#a67e2238620a48949548f45c9914773e4", null ],
    [ "initializeLabels", "menu_2menu__school_8c.html#a2307d9a1567c33f12cc1e5169ae97d03", null ],
    [ "verifyMessage", "menu_2menu__school_8c.html#a8fdc9b445abfa36eb183fd0a1ef96e8c", null ],
    [ "verifyTimeout", "menu_2menu__school_8c.html#a9e6017064ff5aed2c76a1353aef466b0", null ],
    [ "busline", "menu_2menu__school_8c.html#ae676a2f52d9234ba0a7ad9793f4c8f5f", null ],
    [ "curBalance", "menu_2menu__school_8c.html#aecd78482a35ce587fa235d6ceee15bd2", null ],
    [ "payInfo", "menu_2menu__school_8c.html#a64798be63afc51503c4032a58e817810", null ],
    [ "payinfoSubtitle", "menu_2menu__school_8c.html#ac4cc28e30a0fbb44cbff4dee0adb7750", null ],
    [ "serviceline", "menu_2menu__school_8c.html#a7b78eccc0ea5085f04e43fb3ac07aa8e", null ],
    [ "ticketvalue", "menu_2menu__school_8c.html#a16170a352a33e618b7be96bcba36c029", null ],
    [ "timeoutForClean", "menu_2menu__school_8c.html#a54d8931ad340a72b8ddc1983748fabfc", null ],
    [ "timeoutForClean2", "menu_2menu__school_8c.html#a42f59a5986a0097336a3ef65a105b0b6", null ],
    [ "topupValueLabel", "menu_2menu__school_8c.html#ac2d68b1d5db429afab51f46a0c521f2f", null ],
    [ "updateinfo", "menu_2menu__school_8c.html#a78cd5456d67812429c0c3529b2f8a4b7", null ],
    [ "validatorButtonLabel", "menu_2menu__school_8c.html#a5a81c5ebcd3a7a8e171cfa3afdaf248e", null ],
    [ "validTillLabel", "menu_2menu__school_8c.html#a0b2754d92aa9ff40ac87b166db254df2", null ],
    [ "valueLabel", "menu_2menu__school_8c.html#aa5ed738a6f9fd02c976449d510b28b1a", null ]
];