var _card_handler_8c =
[
    [ "DELAY_SEEN", "_card_handler_8c.html#ac630d69fc3a55f6b2f5f204af0735769", null ],
    [ "IGNORE_CARD", "_card_handler_8c.html#a33ecec1eaa1a332aed690d6325b69e73", null ],
    [ "MAXCARDS", "_card_handler_8c.html#a700067324da222d69c64a9c412c9c6a6", null ],
    [ "NO_CARD", "_card_handler_8c.html#a1259cdd7523e8895a6932ab74e698c57", null ],
    [ "VALID_CARD_OTHER_STOP", "_card_handler_8c.html#ab83bc91bd2afbed777776d8d3599f666", null ],
    [ "VALID_CARD_THIS_STOP", "_card_handler_8c.html#a442f0185e86e04e07021e76258149a7a", null ],
    [ "CardHandlerContinueWith", "group__smartcardif.html#ga277efb2374dee193f960814db8bc5b2f", null ],
    [ "CardHandlerInitializer", "group__smartcardif.html#ga8c6a821fd9410b56a7e0bca027e91f34", null ],
    [ "CardHandlerSetToIgnore", "group__smartcardif.html#ga5c7ebb25ce428831b5b457734a9a389d", null ],
    [ "CardHandlerSetToNotIgnore", "group__smartcardif.html#ga3e0d7774f69fd7aa863fa95234846f04", null ],
    [ "cards", "_card_handler_8c.html#a1b6750754808a15ae6b471cd4461fd7d", null ]
];