var modules =
[
    [ "SAM Defines", "group__phhal_hw___sam_a_v2___cmd___defines.html", "group__phhal_hw___sam_a_v2___cmd___defines" ],
    [ "SAM Security", "group__phhal_hw___sam_a_v2___cmd___security.html", null ],
    [ "Key Management", "group__phhal_hw___sam_a_v2___cmd___key_mng.html", null ],
    [ "Data Processing", "group__phhal_hw___sam_a_v2___cmd___data_processing.html", null ],
    [ "Public Key Infrastructure", "group__phhal_hw___sam_a_v2___cmd___p_k_i.html", null ],
    [ "MFP in Non-X-Mode", "group__phhal_hw___sam_a_v2___non_x___m_f_p.html", null ],
    [ "DeviceStatus", "group__device_status.html", "group__device_status" ],
    [ "Queue", "group__queue.html", "group__queue" ],
    [ "Blacklist", "group___blacklist.html", "group___blacklist" ],
    [ "FlowController", "group___flow_controller.html", "group___flow_controller" ],
    [ "LED_control", "group___l_e_d__control.html", "group___l_e_d__control" ],
    [ "Priority", "group___priority.html", "group___priority" ],
    [ "Random", "group___random.html", "group___random" ],
    [ "SerialCommunicator", "group___serial_communicator.html", "group___serial_communicator" ],
    [ "Canbus", "group__canbus.html", "group__canbus" ],
    [ "Clock", "group___clock.html", "group___clock" ],
    [ "Datadistributor", "group__datadistributor.html", "group__datadistributor" ],
    [ "Display", "group__display.html", "group__display" ],
    [ "FlashProgrammer", "group___flash_programmer.html", "group___flash_programmer" ],
    [ "Gpsgsm", "group__gpsgsm.html", "group__gpsgsm" ],
    [ "Hardware", "group__hardware.html", "group__hardware" ],
    [ "STM32_USB_OTG_DEVICE_LIBRARY", "group___s_t_m32___u_s_b___o_t_g___d_e_v_i_c_e___l_i_b_r_a_r_y.html", "group___s_t_m32___u_s_b___o_t_g___d_e_v_i_c_e___l_i_b_r_a_r_y" ],
    [ "Main", "group__main.html", "group__main" ],
    [ "Menu", "group__menu.html", "group__menu" ],
    [ "Otg", "group__otg.html", "group__otg" ],
    [ "Printer", "group__printer.html", "group__printer" ],
    [ "Qrcode", "group__qrcode.html", "group__qrcode" ],
    [ "Sam", "group__sam.html", "group__sam" ],
    [ "Ph_Error", "group__ph___error.html", "group__ph___error" ],
    [ "Sdio", "group__sdio.html", "group__sdio" ],
    [ "Smartcardif", "group__smartcardif.html", "group__smartcardif" ],
    [ "Ph_Private", "group__ph___private.html", "group__ph___private" ],
    [ "Sound", "group__sound.html", "group__sound" ],
    [ "CMSIS", "group___c_m_s_i_s.html", "group___c_m_s_i_s" ],
    [ "Touch", "group__touch.html", "group__touch" ],
    [ "Wifi", "group__wifi.html", "group__wifi" ]
];