var searchData=
[
  ['waitforsystemready',['waitForSystemReady',['../device_status_8c.html#a07917a133e3665aebe527ff017f9f8b6',1,'waitForSystemReady():&#160;deviceStatus.c'],['../device_status_8h.html#a07917a133e3665aebe527ff017f9f8b6',1,'waitForSystemReady():&#160;deviceStatus.c']]],
  ['waitirq',['waitIrq',['../_direct_io_8c.html#ad6a2cea0e48e396d9316225c7e3fa3c8',1,'waitIrq(uint8_t mask, uint8_t irq0en, uint8_t irq1en):&#160;DirectIo.c'],['../_direct_io_8h.html#ad6a2cea0e48e396d9316225c7e3fa3c8',1,'waitIrq(uint8_t mask, uint8_t irq0en, uint8_t irq1en):&#160;DirectIo.c']]],
  ['waitirqhw',['waitIrqHw',['../_direct_io_8h.html#adb87b0431071a030e567f4b3c4c0d170',1,'DirectIo.h']]],
  ['waitirqonhardware',['waitIrqOnHardware',['../_direct_io_8c.html#a5f2e204f6b1797731b1b2efc47282db8',1,'waitIrqOnHardware(uint8_t irq0en, uint8_t irq1en):&#160;DirectIo.c'],['../_direct_io_8h.html#a5f2e204f6b1797731b1b2efc47282db8',1,'waitIrqOnHardware(uint8_t irq0en, uint8_t irq1en):&#160;DirectIo.c']]],
  ['apiDnfccWifiEngineAdvance',['apiDnfccWifiEngineAdvance',['../wifi_2engine_8c.html#a7fdec8a19e9a322b7ec836ac920d0dff',1,'apiDnfccWifiEngineAdvance():&#160;engine.c'],['../wifi_2wifi_8c.html#a7fdec8a19e9a322b7ec836ac920d0dff',1,'apiDnfccWifiEngineAdvance():&#160;engine.c']]],
  ['wificallbackfunction',['WifiCallbackFunction',['../wifi_2engine_8c.html#a5c42c2d8380ae57c0e910cd1f261b82c',1,'engine.c']]],
  ['apiDnfccWifiEngineInit',['apiDnfccWifiEngineInit',['../wifi_2engine_8c.html#a747ccbcd9216ef1d552a46cb321936a8',1,'apiDnfccWifiEngineInit():&#160;engine.c'],['../wifi_2wifi_8c.html#a747ccbcd9216ef1d552a46cb321936a8',1,'apiDnfccWifiEngineInit():&#160;engine.c']]],
  ['wifitransmitcommand',['WifiTransmitCommand',['../wifi_2engine_8h.html#a893e1173b3feaf0b24fa7a7e122d985b',1,'engine.h']]]
];
