var searchData=
[
  ['name_5ffile_5fblacklist',['NAME_FILE_BLACKLIST',['../filenames_8h.html#a1a060977429f97143f52dc95ce3967ec',1,'filenames.h']]],
  ['name_5ffile_5ffare',['NAME_FILE_FARE',['../filenames_8h.html#a2aa073e0c3b5477f97c1ab05e59c7be0',1,'filenames.h']]],
  ['name_5ffile_5ffirmware',['NAME_FILE_FIRMWARE',['../filenames_8h.html#a4a96f5222d20c7d952f5898cc0afe516',1,'filenames.h']]],
  ['name_5ffile_5fprinter',['NAME_FILE_PRINTER',['../filenames_8h.html#a2c3716bbe2a25c70f0a0d2f92a4e776e',1,'filenames.h']]],
  ['name_5ffile_5fwifi',['NAME_FILE_WIFI',['../filenames_8h.html#afdbd82d1a246e43062b2da7ec76bea72',1,'filenames.h']]],
  ['no_5fcard',['NO_CARD',['../_card_handler_8c.html#a1259cdd7523e8895a6932ab74e698c57',1,'CardHandler.c']]],
  ['nxpbuild_5f_5fph_5fcryptorng',['NXPBUILD__PH_CRYPTORNG',['../_nxp_lib_8h.html#a4f33000249ac2e8ccd33b644c7ecf50f',1,'NxpLib.h']]],
  ['nxpbuild_5f_5fph_5fcryptorng_5fsw',['NXPBUILD__PH_CRYPTORNG_SW',['../_nxp_lib_8h.html#af7790dfaeb48f6b174d590093e030a70',1,'NxpLib.h']]],
  ['nxpbuild_5f_5fph_5fcryptosym',['NXPBUILD__PH_CRYPTOSYM',['../_nxp_lib_8h.html#a9c9a433859181f5f1fea90582e4c1768',1,'NxpLib.h']]],
  ['nxpbuild_5f_5fph_5fcryptosym_5fsw',['NXPBUILD__PH_CRYPTOSYM_SW',['../_nxp_lib_8h.html#aa9ea38629f3ab7a8a6bfd80c35f38e4c',1,'NxpLib.h']]],
  ['nxpbuild_5f_5fph_5fkeystore',['NXPBUILD__PH_KEYSTORE',['../_nxp_lib_8h.html#aa59a4a10332e51bce850c943e8feab7b',1,'NxpLib.h']]],
  ['nxpbuild_5f_5fph_5fkeystore_5fsw',['NXPBUILD__PH_KEYSTORE_SW',['../_nxp_lib_8h.html#aeb71c302e61d0d8383ee5ea1dc67722f',1,'NxpLib.h']]],
  ['nxpbuild_5fex10_5fmandatory_5flayers',['NXPBUILD_EX10_MANDATORY_LAYERS',['../_nfcrdlib_ex10___m_i_f_a_r_e_d_e_s_fire_8h.html#ad841ebef1d9e954369f8fa511bf87931',1,'NfcrdlibEx10_MIFAREDESFire.h']]]
];
