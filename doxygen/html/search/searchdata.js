var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvw",
  1: "_abcdfgmrstuw",
  2: "bcdefghijlmopqrstuw",
  3: "_abcdefghilmnoqrstuw",
  4: "_abcdefhijlmnoprstuvw",
  5: "afst",
  6: "_abfp",
  7: "beflpsvw",
  8: "abcdfnst",
  9: "bcdfghklmopqrstuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules"
};

