var searchData=
[
  ['valid',['VALID',['../gsm_8h.html#a26216645f1aaffc4218e7abfe0a95060acf0713491d9b887eaccfd80c18abca47',1,'gsm.h']]],
  ['validate_5fcallback',['VALIDATE_CALLBACK',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a45d3b1553e711606cc7641fc6d10148e',1,'SerialCommunicator.h']]],
  ['validatorscreen',['ValidatorScreen',['../_display_8h.html#a3ce11071baa9d5b3e3ac6abf8a26d896af336b840996702a1dacff6d4ed56665f',1,'Display.h']]],
  ['verify_5faccess_5fpoint_5fcallback',['VERIFY_ACCESS_POINT_CALLBACK',['../wifi_2engine_8h.html#a60e82d598ff74149156c2795e5c96302a263f9a54565d5546baafa1956e6fb5cd',1,'engine.h']]],
  ['verify_5fdata',['VERIFY_DATA',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4ad9b261ab2f24847e55b5b7ff3e0835f0',1,'SerialCommunicator.h']]],
  ['verify_5fdata_5fand_5fprocess',['VERIFY_DATA_AND_PROCESS',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4af72fc490032738b2dd3a417fc4189f1e',1,'SerialCommunicator.h']]],
  ['verify_5fdata_5fand_5fwait_5ffor_5freturn',['VERIFY_DATA_AND_WAIT_FOR_RETURN',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4adc679c59bf6397cfeb0723529b11a8d2',1,'SerialCommunicator.h']]],
  ['verify_5fdata_5fwith_5fcallback',['VERIFY_DATA_WITH_CALLBACK',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a6511943787a19a4aa37cea175b4eadd8',1,'SerialCommunicator.h']]],
  ['verify_5fip_5faddress',['VERIFY_IP_ADDRESS',['../wifi_2engine_8h.html#a61b1c65023400ef3dcde76d3a6f7c43ba692bf28010fdabe829b4e546b7a81dec',1,'engine.h']]],
  ['verify_5fip_5faddress_5fresp',['VERIFY_IP_ADDRESS_RESP',['../wifi_2engine_8h.html#a61b1c65023400ef3dcde76d3a6f7c43baa2e3e34dbc860d3438bf9da97ce116b9',1,'engine.h']]],
  ['verify_5fip_5faddress_5frespok',['VERIFY_IP_ADDRESS_RESPOK',['../wifi_2engine_8h.html#a61b1c65023400ef3dcde76d3a6f7c43baf22f7fb543f9eee9555795dab04dba96',1,'engine.h']]],
  ['verify_5ftimeout_5fhas_5fpassed',['VERIFY_TIMEOUT_HAS_PASSED',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4aa920aa082b9b6fbbe90babde249ebd0b',1,'SerialCommunicator.h']]]
];
