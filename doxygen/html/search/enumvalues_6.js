var searchData=
[
  ['validate_5fcallback',['VALIDATE_CALLBACK',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a45d3b1553e711606cc7641fc6d10148e',1,'SerialCommunicator.h']]],
  ['verify_5fdata',['VERIFY_DATA',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4ad9b261ab2f24847e55b5b7ff3e0835f0',1,'SerialCommunicator.h']]],
  ['verify_5fdata_5fand_5fprocess',['VERIFY_DATA_AND_PROCESS',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4af72fc490032738b2dd3a417fc4189f1e',1,'SerialCommunicator.h']]],
  ['verify_5fdata_5fand_5fwait_5ffor_5freturn',['VERIFY_DATA_AND_WAIT_FOR_RETURN',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4adc679c59bf6397cfeb0723529b11a8d2',1,'SerialCommunicator.h']]],
  ['verify_5fdata_5fwith_5fcallback',['VERIFY_DATA_WITH_CALLBACK',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a6511943787a19a4aa37cea175b4eadd8',1,'SerialCommunicator.h']]],
  ['verify_5ftimeout_5fhas_5fpassed',['VERIFY_TIMEOUT_HAS_PASSED',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4aa920aa082b9b6fbbe90babde249ebd0b',1,'SerialCommunicator.h']]]
];
