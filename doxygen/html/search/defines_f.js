var searchData=
[
  ['qrcode_5fbuffer_5fsize',['QRCODE_BUFFER_SIZE',['../queue_definitions_8h.html#aa44d4ed6d9decd5762e21f495f0a8249',1,'queueDefinitions.h']]],
  ['qrcode_5fcommunication',['QRCODE_COMMUNICATION',['../qrcode_8c.html#a141d8727163752b851201644a56f28c6',1,'qrcode.c']]],
  ['qrcode_5fgpio_5frx',['QRCODE_GPIO_RX',['../platform__config_8h.html#a887169229036a70e61102621d9207d94',1,'platform_config.h']]],
  ['qrcode_5flog',['QRCODE_LOG',['../log_definition_8h.html#a82a431834c33814f009eac34aae674ec',1,'logDefinition.h']]],
  ['qrcode_5fmaxlength',['QRCODE_MAXLENGTH',['../qrcode_8c.html#a556c12bf765b3ecad17d53029d975973',1,'qrcode.c']]],
  ['qrcode_5fpin_5frx',['QRCODE_PIN_RX',['../platform__config_8h.html#a1164ff147a695b2a4a0b9a080779b80a',1,'platform_config.h']]],
  ['qrcode_5fqueue_5flength',['QRCODE_QUEUE_LENGTH',['../queue_definitions_8h.html#ad6247a1fecab9bfe25cef337586a69a6',1,'queueDefinitions.h']]],
  ['qrcodeprinter_5faf',['QRCODEPRINTER_AF',['../platform__config_8h.html#ac0b0cc6e761f8a29afde7e88c5d1602f',1,'platform_config.h']]],
  ['qrcodeprinter_5fuart',['QRCODEPRINTER_UART',['../platform__config_8h.html#a3e91ee78961d1747c49bba0fd303e708',1,'platform_config.h']]],
  ['qrcodeprinter_5fuart_5firq',['QRCODEPRINTER_UART_IRQ',['../platform__config_8h.html#ab78f360981f2a145da5fb2cecbf8a1e4',1,'platform_config.h']]],
  ['qrcodeprinter_5fuart_5fnr',['QRCODEPRINTER_UART_NR',['../platform__config_8h.html#afccbc015c1970b2ebcb67ab434f2f719',1,'platform_config.h']]]
];
