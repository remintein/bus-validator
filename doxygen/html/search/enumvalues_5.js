var searchData=
[
  ['send_5fliteral_5ftext',['SEND_LITERAL_TEXT',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4aa6df4fdb31a9bf2a2e30e692a9240fc8',1,'SerialCommunicator.h']]],
  ['send_5fprocessed_5ftext',['SEND_PROCESSED_TEXT',['../group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a5368f6d7ace808dbbbf2286c0920d7ed',1,'SerialCommunicator.h']]],
  ['status_5ffinished',['STATUS_FINISHED',['../group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a7573cce65348568c572c9ccb33291d00',1,'datadistributor.h']]],
  ['status_5fidle',['STATUS_IDLE',['../group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a99ade6ecff7f9e1fb13b62be39b85d82',1,'datadistributor.h']]],
  ['status_5fwaiting',['STATUS_WAITING',['../group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a5a5262e49db47e003a2d28239049955d',1,'datadistributor.h']]],
  ['status_5fwork',['STATUS_WORK',['../group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a12f8edd675e2afc578100877add0a060',1,'datadistributor.h']]]
];
