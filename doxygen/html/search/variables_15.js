var searchData=
[
  ['uartnr',['uartNr',['../struct_serial_communicator_handler__type.html#ae1794bfa7311c3b838a4bfd16c0a07af',1,'SerialCommunicatorHandler_type']]],
  ['unlockdate',['unLockDate',['../structfile_card_status_struct.html#ac0cb641f17324bb1e76a1594b617e6cf',1,'fileCardStatusStruct']]],
  ['updateinfo',['updateinfo',['../menu__general_8c.html#aa5d40f2571f856e26e459c778af7731b',1,'updateinfo():&#160;menu_busconsole.c'],['../menu_2menu__busconsole_8c.html#a78cd5456d67812429c0c3529b2f8a4b7',1,'updateinfo():&#160;menu_busconsole.c'],['../menu_2menu__formatter_8c.html#a78cd5456d67812429c0c3529b2f8a4b7',1,'updateinfo():&#160;menu_formatter.c'],['../menu_2menu__handheld_8c.html#a78cd5456d67812429c0c3529b2f8a4b7',1,'updateinfo():&#160;menu_handheld.c'],['../menu_2menu__school_8c.html#a78cd5456d67812429c0c3529b2f8a4b7',1,'updateinfo():&#160;menu_school.c'],['../menu_2menu__topup_8c.html#a78cd5456d67812429c0c3529b2f8a4b7',1,'updateinfo():&#160;menu_topup.c'],['../menu_2menu__validator_8c.html#a78cd5456d67812429c0c3529b2f8a4b7',1,'updateinfo():&#160;menu_validator.c']]],
  ['usbd_5finterface_5ffops_5ffs',['USBD_Interface_fops_FS',['../group___u_s_b_d___c_d_c.html#ga99394ed19b774f171df96c2848c411ed',1,'usbd_cdc_if.c']]],
  ['userrxbufferfs',['UserRxBufferFS',['../group___u_s_b_d___c_d_c___private___variables.html#ga1e37e6616e6affe5acf9c0befd063f6a',1,'usbd_cdc_if.c']]],
  ['usertxbufferfs',['UserTxBufferFS',['../group___u_s_b_d___c_d_c___private___variables.html#gad48fea85cdb7aed49bf62140a52f23e0',1,'usbd_cdc_if.c']]],
  ['uuid',['uuid',['../struct_device_status.html#aa11546244d4ffb4f27bc7741c783d170',1,'DeviceStatus']]],
  ['uwtick',['uwTick',['../flash_copy_8c.html#a9d411ea525781e633bf7ea7ef2f90728',1,'flashCopy.c']]]
];
