var searchData=
[
  ['bauthmode',['bAuthMode',['../struct_desfire_handling_struct.html#abd70d90a0940c444237985a757411171',1,'DesfireHandlingStruct']]],
  ['bcc',['bcc',['../struct_desfire_handling_struct.html#a22a04ca56d0f5e6692f336dc6a04dcd2',1,'DesfireHandlingStruct']]],
  ['bcid',['bCid',['../struct_desfire_handling_struct.html#a1067da24be99f2947075cbb5f045b280',1,'DesfireHandlingStruct']]],
  ['bcidsupported',['bCidSupported',['../struct_desfire_handling_struct.html#a343a80295d60ef7ee3cd53ef7f522ac1',1,'DesfireHandlingStruct']]],
  ['bcryptomethod',['bCryptoMethod',['../struct_desfire_handling_struct.html#aec053e8f7e5bd2b30ea7c735bb164959',1,'DesfireHandlingStruct']]],
  ['biv',['bIv',['../struct_desfire_handling_struct.html#a50f0da33ca0538cf9b9fcb2f1414c3e1',1,'DesfireHandlingStruct']]],
  ['bkeyno',['bKeyNo',['../struct_desfire_handling_struct.html#a0844d585886e9aa3989c057dc498607a',1,'DesfireHandlingStruct']]],
  ['blastblockbuffer',['bLastBlockBuffer',['../struct_desfire_handling_struct.html#a216a959b32f3d291b0c1eb46fa0aa590',1,'DesfireHandlingStruct']]],
  ['blastblockindex',['bLastBlockIndex',['../struct_desfire_handling_struct.html#a6af4e0f7e2b6f6d1d3b467dd2a0e104e',1,'DesfireHandlingStruct']]],
  ['bnadsupported',['bNadSupported',['../struct_desfire_handling_struct.html#a1c05a2ec3317644e29261bc1b4bad8ad',1,'DesfireHandlingStruct']]],
  ['bsessionkey',['bSessionKey',['../struct_desfire_handling_struct.html#a24cd738089672c34ccda0d6da9eb40f9',1,'DesfireHandlingStruct']]],
  ['buf',['buf',['../group__datadistributor.html#ga9d931f7ca966bf184d66b59586686b17',1,'JsonGenerator.c']]],
  ['buidcomplete',['bUidComplete',['../struct_desfire_handling_struct.html#a187d68528473a1561516483964011873',1,'DesfireHandlingStruct']]],
  ['buidlength',['bUidLength',['../struct_desfire_handling_struct.html#a0a3d01844f0823e2ac31194f2faf224c',1,'DesfireHandlingStruct']]],
  ['bwrappedmode',['bWrappedMode',['../struct_desfire_handling_struct.html#a2f5c058d77fa2a86739e81b066106d36',1,'DesfireHandlingStruct']]]
];
