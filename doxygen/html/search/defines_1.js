var searchData=
[
  ['backend_5fperform_5fselftest',['BACKEND_PERFORM_SELFTEST',['../backend_communication_8h.html#a3c8c1e025f609f6beda84e6f4cd5fea0',1,'backendCommunication.h']]],
  ['backend_5fperform_5fsetid',['BACKEND_PERFORM_SETID',['../backend_communication_8h.html#a32009906c24479e93effb0a5b1316b35',1,'backendCommunication.h']]],
  ['backend_5fperform_5fupdate_5fblacklist',['BACKEND_PERFORM_UPDATE_BLACKLIST',['../backend_communication_8h.html#a8ffb769b315f82e71cb4f13c488e1a19',1,'backendCommunication.h']]],
  ['backend_5fperform_5fupdate_5ffare',['BACKEND_PERFORM_UPDATE_FARE',['../backend_communication_8h.html#a5606dc17b90cc8b244174bab3351636a',1,'backendCommunication.h']]],
  ['backend_5fperform_5fupdate_5ffirmware',['BACKEND_PERFORM_UPDATE_FIRMWARE',['../backend_communication_8h.html#a82b93b1631c3a19b653a8542f48b5be6',1,'backendCommunication.h']]],
  ['backend_5fperform_5fupdate_5fprinter',['BACKEND_PERFORM_UPDATE_PRINTER',['../backend_communication_8h.html#a8253e8203cf427405a2b3ac401cdb280',1,'backendCommunication.h']]],
  ['backend_5fperform_5fupdate_5fwifi',['BACKEND_PERFORM_UPDATE_WIFI',['../backend_communication_8h.html#ac1cd9c98850696bde45e93abb3037638',1,'backendCommunication.h']]]
];
