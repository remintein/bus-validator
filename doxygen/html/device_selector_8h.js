var device_selector_8h =
[
    [ "BLOCKLENGTH_FILEDATA", "device_selector_8h.html#a26eded8a2374c4cef9ed9ea6a7132de1", null ],
    [ "CURRENT_TARGET", "device_selector_8h.html#a6967fe937fb0cc3201d8201bbfaf2c32", null ],
    [ "CURRENT_TARGET_BACKEND", "device_selector_8h.html#afc09e54da14c841e1555baa2fdc93584", null ],
    [ "DISPLAY_SIZE", "device_selector_8h.html#a035c0b93babde37c8919064af8b67928", null ],
    [ "DNFCC_ADD_GSM_DISABLE", "device_selector_8h.html#ac7d5624394ade8bbfc5bc182bc8f53c3", null ],
    [ "DNFCC_ENABLE_WIFI", "device_selector_8h.html#a689d95d5657ac8591a9821dd48f45259", null ],
    [ "DNFCC_NOLOGIN", "device_selector_8h.html#ab77d50701268ef679ba7be05b9ce3aa1", null ],
    [ "DNFCC_SOUND", "device_selector_8h.html#a354baf091cef947cab58f357fb82087e", null ],
    [ "DNFCC_SOUND", "device_selector_8h.html#a354baf091cef947cab58f357fb82087e", null ],
    [ "FIRMWARE_VERSION", "device_selector_8h.html#aa14dc39d52ab121ceb570f1a265385e0", null ],
    [ "STR", "device_selector_8h.html#a18d295a837ac71add5578860b55e5502", null ],
    [ "TEXT_LOCALE", "device_selector_8h.html#aaf1b3a99ee81f3bef20b553ccd77996d", null ],
    [ "VALIDATOR_SIMULATOR", "device_selector_8h.html#a2670d018e0202af2bc34efbe3d123e0b", null ],
    [ "XSTR", "device_selector_8h.html#abe87b341f562fd1cf40b7672e4d759da", null ]
];