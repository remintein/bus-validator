var group__ph___error =
[
    [ "PHHAL_HW_SAMAV2_ERR_CRYPTO", "group__ph___error.html#gac33fa943fdf6d3b2b09f59b704633d2c", null ],
    [ "PHHAL_HW_SAMAV2_ERR_DESFIRE_GEN", "group__ph___error.html#ga4a3768dba82dc4957110b566c8a8106a", null ],
    [ "PHHAL_HW_SAMAV2_ERR_ISO_UID_INCOMPLETE", "group__ph___error.html#ga2626b4651993dbaf383646f90b28d2bf", null ],
    [ "PHHAL_HW_SAMAV2_ERR_MIFARE_GEN", "group__ph___error.html#gade9844578bac91fad8115edec9279ba0", null ],
    [ "PHHAL_HW_SAMAV2_ERR_MIFARE_PLUS_CRYPTO", "group__ph___error.html#ga37fbc2389250c879da5eefa5edb410c2", null ],
    [ "PHHAL_HW_SAMAV2_ERR_MIFARE_PLUS_GEN", "group__ph___error.html#gab37bc656b0f8820deaaa749b11ab431a", null ]
];