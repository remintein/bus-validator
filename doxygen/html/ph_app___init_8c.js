var ph_app___init_8c =
[
    [ "CHECK_SUCCESS", "ph_app___init_8c.html#a6d85cd0c620e5714850812682b93806d", null ],
    [ "debugPrintIt", "ph_app___init_8c.html#a3834164e13ae3259d8d456b53fb9fff6", null ],
    [ "phApp_Comp_Init", "ph_app___init_8c.html#a97b2c015de4847c383d57ada476bf861", null ],
    [ "phApp_Configure_IRQ", "group__smartcardif.html#ga7f246a73ddb23cc70fa161acd10ea204", null ],
    [ "phApp_CPU_Init", "group__smartcardif.html#ga48f98105bc3214ff519972f7be51511d", null ],
    [ "phApp_HALConfigAutoColl", "ph_app___init_8c.html#aef101b98e69a18fca180c5ccc9b096ff", null ],
    [ "phApp_Print_Buff", "ph_app___init_8c.html#ad6273fe94b3a14f50248f4ef2282e176", null ],
    [ "phApp_PrintTagInfo", "ph_app___init_8c.html#a17b9b1a2edbb83e8b3c6514e0c03aa6b", null ],
    [ "phApp_PrintTech", "ph_app___init_8c.html#a5564fa03e94e00348d8343223522661f", null ],
    [ "PrintErrorInfo", "ph_app___init_8c.html#ae9eb9209996f5ad59af3616f5145e6b1", null ]
];