var group__smartcardif =
[
    [ "PH_CRYPTOSYM_SW_NUM_AES_ROUNDS_128", "group__smartcardif.html#ga676723dc0cc7ba27bd40ba7e3e97a19a", null ],
    [ "PH_CRYPTOSYM_SW_NUM_AES_ROUNDS_192", "group__smartcardif.html#ga0afc99e09c3d40845a14703feaf6df94", null ],
    [ "PH_CRYPTOSYM_SW_NUM_AES_ROUNDS_256", "group__smartcardif.html#ga615c6bea6cfafe5e804a5e3baf6dfd80", null ],
    [ "CardHandlerContinueWith", "group__smartcardif.html#ga277efb2374dee193f960814db8bc5b2f", null ],
    [ "CardHandlerInitializer", "group__smartcardif.html#ga8c6a821fd9410b56a7e0bca027e91f34", null ],
    [ "CardHandlerSetToIgnore", "group__smartcardif.html#ga5c7ebb25ce428831b5b457734a9a389d", null ],
    [ "CardHandlerSetToNotIgnore", "group__smartcardif.html#ga3e0d7774f69fd7aa863fa95234846f04", null ],
    [ "CnsCardInterfaceCardIsCardNotExpired", "group__smartcardif.html#gaf6a6d2c44b34c7aa489b03c9d84e4e8c", null ],
    [ "CnsCardInterfaceCardIsEnabled", "group__smartcardif.html#ga84669e2af5a9e6a8d7ce4dd34d66caed", null ],
    [ "CnsCardInterfaceCardIsOfLogintype", "group__smartcardif.html#ga3555479406b5b460bffe02cadb7cb0de", null ],
    [ "CnsCardInterfaceCardStatus", "group__smartcardif.html#gab2353486a884cffbead6627805b4fa6e", null ],
    [ "CnsCardInterfaceCardTransaction", "group__smartcardif.html#ga26e47a3c54d11994c2ebe6dfd19dfa1e", null ],
    [ "CnsCardInterfaceCheckLowPurseBalance", "group__smartcardif.html#ga09ef9b97328cd941df36df7fb729ebd3", null ],
    [ "CnsCardInterfaceCheckLowTicketBalance", "group__smartcardif.html#ga886f7a0f50ca7126cf1fb27017e97380", null ],
    [ "CnsCardInterfaceCreditTicketBalance", "group__smartcardif.html#ga1e06579a1f4e31f2d31d36677bca2436", null ],
    [ "CnsCardInterfaceDebitPurseBalance", "group__smartcardif.html#ga243d4bddf8c6141e94e16ae6be8cd78e", null ],
    [ "CnsCardInterfaceDebitTicketBalance", "group__smartcardif.html#ga0dd15fafb841b9f39b78eacceeedd48e", null ],
    [ "CnsCardInterfaceGetPurseBalance", "group__smartcardif.html#ga6a359378e2b0a210fa75fd3945b04333", null ],
    [ "CnsCardInterfaceGetTicketBalance", "group__smartcardif.html#ga8b03a98cc76336e6daf173add3a60dda", null ],
    [ "CnsCardInterfaceGetUserInformation", "group__smartcardif.html#ga43d2b5fbb966219a6baf447d9416da60", null ],
    [ "CnsCardInterfaceInitialize", "group__smartcardif.html#gaeabb4b8b029eaff8f02b41bc5b42a905", null ],
    [ "CnsCardInterfaceInternal_Sw_CommitTransaction", "group__smartcardif.html#ga74794e81d466de77a013ac63b8366bce", null ],
    [ "CnsCardInterfaceInternal_Sw_CreateApplication", "group__smartcardif.html#ga2ba63613f59517ca0fd25e561273dc9c", null ],
    [ "CnsCardInterfaceInternal_Sw_CreateNormalFile", "group__smartcardif.html#ga27e85e94378e53ec04e3e22faf4c5d28", null ],
    [ "CnsCardInterfaceInternal_Sw_CreateRecordsFile", "group__smartcardif.html#gad72bc2605e2a93ed031c07fbcf8489e1", null ],
    [ "CnsCardInterfaceInternal_Sw_CreateValueFile", "group__smartcardif.html#ga9a23deddbb88ac535fed5e837a2d1b88", null ],
    [ "CnsCardInterfaceInternal_Sw_Credit", "group__smartcardif.html#gaee1aa9e3fad3e6e53d29f55be80010ee", null ],
    [ "CnsCardInterfaceInternal_Sw_Debit", "group__smartcardif.html#ga856511d5e812eb367737fb45a198f144", null ],
    [ "CnsCardInterfaceInternal_Sw_FormatPICC", "group__smartcardif.html#gade8fde61f716687ab51ea3cd1141a68e", null ],
    [ "CnsCardInterfaceInternal_Sw_SelectApplication", "group__smartcardif.html#ga531131049b6b121fd91d5e8d8d0e0257", null ],
    [ "CnsCardInterfaceInternalAddDaysToBcdDate", "group__smartcardif.html#ga060138963b8c073b2ae7a118dbc5087f", null ],
    [ "CnsCardInterfaceInternalAddDaysToDate", "group__smartcardif.html#ga606af52d8b784cc738937aac7d4e1993", null ],
    [ "CnsCardInterfaceInternalBcdDate", "group__smartcardif.html#ga8f3053281e7a92f302e3e61313e84fb2", null ],
    [ "CnsCardInterfaceInternalBcdDateTime", "group__smartcardif.html#ga8a6052a224781f92bda8440ddd9b6361", null ],
    [ "CnsCardInterfaceInternalBcdToDate", "group__smartcardif.html#gaca2eb660f79638e9b75b9b092a455be9", null ],
    [ "CnsCardInterfaceInternalCredit", "group__smartcardif.html#ga3a45c11476f5f47c1f8c39416eb43a87", null ],
    [ "CnsCardInterfaceInternalDebit", "group__smartcardif.html#gad41c9c911ec53114b40bfc81ea69b933", null ],
    [ "CnsCardInterfaceInternalGetValue", "group__smartcardif.html#ga5f7696220926027b9f4581dde8d163d1", null ],
    [ "CnsCardInterfaceInternalReadData", "group__smartcardif.html#gaf8ebe1258fc05d35681551c6c06d1824", null ],
    [ "CnsCardInterfaceInternalReadFile", "group__smartcardif.html#ga5b12a5c64ef29e35fe80e245f1b01b89", null ],
    [ "CnsCardInterfaceInternalReadRecords", "group__smartcardif.html#ga3a4123c20ffa7b106d56cae04a81b81f", null ],
    [ "CnsCardInterfaceInternalSelectApplication", "group__smartcardif.html#ga0bb84a62e63c76941e8a2515c6cf3fa3", null ],
    [ "CnsCardInterfaceInternalWriteData", "group__smartcardif.html#ga1dec0e3013e7f8381501a53fbffdba00", null ],
    [ "CnsCardInterfaceInternalWriteFile", "group__smartcardif.html#ga84a29c6ad9404a7252a849599b14c496", null ],
    [ "CnsCardInterfaceInternalWriteRecord", "group__smartcardif.html#ga5f123f886cdeb0c3ec390e1bceec7960", null ],
    [ "CnsCardInterfacePerformPurseLogging", "group__smartcardif.html#ga3c9c99649682bb1ed7526149d1aced52", null ],
    [ "CnsCardInterfacePurseExpire", "group__smartcardif.html#ga05c87d625c5040b78ac0ec5b0208540d", null ],
    [ "CnsCardInterfacePurseStatus", "group__smartcardif.html#ga056eed4a7b8caee582e2cebbab061fbf", null ],
    [ "CnsCardInterfacePurseTransaction", "group__smartcardif.html#ga10bb46249c74bcea617e01faff88ae3c", null ],
    [ "CnsCardInterfaceReadCardInfo", "group__smartcardif.html#gae93a79bc64260a22917afacd251f06e1", null ],
    [ "CnsCardInterfaceReadPurseInfo", "group__smartcardif.html#ga478d162c72aef9e6b8e75edadcadc127", null ],
    [ "CnsCardInterfaceReadTicketInfo", "group__smartcardif.html#ga6573de8e89ad18ac98937307a042ed49", null ],
    [ "CnsCardInterfaceSam_Sw_IsoAuthenticate", "group__smartcardif.html#gaf574c0a803e529e31780ff6719426717", null ],
    [ "CnsCardInterfaceSetCardFromPtoA", "group__smartcardif.html#ga1734d6bfdad9e2921b53c6612e5d7c75", null ],
    [ "CnsCardInterfaceStartCard", "group__smartcardif.html#ga0edca1d46e6c35e74aa774747306b9e2", null ],
    [ "CnsCardInterfaceTicketExpire", "group__smartcardif.html#gaf547c58d502c240579c61f6b29c93b91", null ],
    [ "CnsCardInterfaceTicketStatus", "group__smartcardif.html#gaa4265442cda523dcb8d2d438243e89cb", null ],
    [ "CnsCardInterfaceTicketTransaction", "group__smartcardif.html#gae01c23b610162657ddbfc969dca8257e", null ],
    [ "CnsCardInterfaceTicketTransactionCount", "group__smartcardif.html#ga46a8950b9ab4e8078fc67e4fc7097bbb", null ],
    [ "CnsCardInterfaceTicketType", "group__smartcardif.html#gaaa02a9137d0b7f20d1dcc989c63c6930", null ],
    [ "CnsCardInterfaceVerifyCardActive", "group__smartcardif.html#ga40131055595343a4866085b2025f03aa", null ],
    [ "CnsCardInterfaceWritePurseLog", "group__smartcardif.html#gab53096db1e5e8652d4dde67086517fef", null ],
    [ "CnsCardInterfaceWriteTicketLog", "group__smartcardif.html#gaa76eedda0c987d13b396089b4bece1b5", null ],
    [ "CopyCardConfigurationToStream", "group__smartcardif.html#gadfe5382439da21d96dd6a9dd465d25d4", null ],
    [ "CopyCardHolderInformationsStructToStream", "group__smartcardif.html#ga61c425180430e6ea25de0037432dd365", null ],
    [ "CopyCardInformationToStream", "group__smartcardif.html#ga93b29b57b7006e1f7ca235ca0b122070", null ],
    [ "CopyCardStatusToStream", "group__smartcardif.html#ga436d7b5542fb373d1eeb6fa4b899906b", null ],
    [ "CopyCardTransactionCountToStream", "group__smartcardif.html#ga8cbf7a9582bdcc0d8898d8c573c5362c", null ],
    [ "CopyPurseInformationsToStream", "group__smartcardif.html#gaba9c3ad93d9b094ff03d0925a209d340", null ],
    [ "CopyPurseLogToStream", "group__smartcardif.html#ga04d519b7a2ebecb5281afabf97e51a4f", null ],
    [ "CopyPurseTransactionCountToStream", "group__smartcardif.html#ga94bf6b83c79e0f0013c42b6b4622978b", null ],
    [ "CopyPurseValueToStream", "group__smartcardif.html#ga985fbf711f2feedc7eeff7c2f640dac4", null ],
    [ "CopyStreamToCardConfiguration", "group__smartcardif.html#ga18c40c799a23d1bb87feedda7670143c", null ],
    [ "CopyStreamToCardHolderInformation", "group__smartcardif.html#ga53df4ff0e26be4eea8c7815c76877f5b", null ],
    [ "CopyStreamToCardInformation", "group__smartcardif.html#ga8de3087530ab067588972bca60f145f3", null ],
    [ "CopyStreamToCardStatus", "group__smartcardif.html#gad31707c23870d8138117905abf842218", null ],
    [ "CopyStreamToCardTransactionCount", "group__smartcardif.html#gae37d914313b99327b1dd042bcced9ad2", null ],
    [ "CopyStreamToPurseInformation", "group__smartcardif.html#ga85f64ccc7bd1777df665617212634d7b", null ],
    [ "CopyStreamToPurseLog", "group__smartcardif.html#ga4732cbd9d7f1d5d3fe0fdf1f543cc029", null ],
    [ "CopyStreamToPurseTransactionCount", "group__smartcardif.html#ga8e6989907ac5854eb14fb2d83a920285", null ],
    [ "CopyStreamToPurseValue", "group__smartcardif.html#ga5efc4d3b693e5c7e4638df3e664834e5", null ],
    [ "CopyStreamToTicketInformation", "group__smartcardif.html#ga0f62199d7fc8a15d75ec22a9dc04f037", null ],
    [ "CopyStreamToTicketLog", "group__smartcardif.html#gad561922b5d1d2374184b9dd3b4fcacc2", null ],
    [ "CopyStreamToTicketTransactionCount", "group__smartcardif.html#gaccbb8da98d1257e5861862cc051a66cc", null ],
    [ "CopyStreamToTicketValue", "group__smartcardif.html#gae89027a210d089957bcd00d4ac5c5a97", null ],
    [ "CopyTicketInformationsToStream", "group__smartcardif.html#gaf8991f32c15a3e5cc6170a9c55ca0133", null ],
    [ "CopyTicketLogToStream", "group__smartcardif.html#gaa1e871e7e7b4f36127c75e270564a860", null ],
    [ "CopyTicketTransactionCountToStream", "group__smartcardif.html#ga7facfb42f487f174b5af6856f55c445d", null ],
    [ "CopyTicketValueToStream", "group__smartcardif.html#ga737997a395e55c77d03c140190b03ebb", null ],
    [ "desfireAuthenticateAES", "group__smartcardif.html#ga3471dc3ba767e63392edf43d41c2d933", null ],
    [ "DnfccSamDesfireAuthenticatePicc", "group__smartcardif.html#ga0cb219d4131309013c2bec8d1ef7cd1d", null ],
    [ "flowController_callBack", "group__smartcardif.html#gad4c7e39ec1b0b2a4b06fff3773eadaac", null ],
    [ "flowControllerClearPenaltyPay", "group__smartcardif.html#ga5150d40f5e0ab21bdaba447641be5efe", null ],
    [ "flowControllerDidPayPenaltyWithCash", "group__smartcardif.html#ga767a60460090f8a2c80c7d03b7ef28d7", null ],
    [ "flowControllerDisplayBusDriverOK", "group__smartcardif.html#gae112508d7c1974c375d7234376b600a9", null ],
    [ "flowControllerDisplayBusDriverOKLowBalance", "group__smartcardif.html#ga1a6294065570f0b19c96ae61a8ddfeaa", null ],
    [ "flowControllerDisplayHandheldStartScreen", "group__smartcardif.html#ga6a4eacad52c67d3273677090ee62c78b", null ],
    [ "flowControllerDisplayOK", "group__smartcardif.html#gab33f49f430bdc6f8764da06d66aabbe2", null ],
    [ "flowControllerDisplayOKLowBalance", "group__smartcardif.html#ga695ffac64b3b97dec513fc56ee6677b3", null ],
    [ "flowControllerDisplayPenaltyScreen", "group__smartcardif.html#ga497ff1c2a3ad80c4b95a33b5df4461ea", null ],
    [ "flowControllerDisplayStartScreen", "group__smartcardif.html#gaa365d659e477f2b02c3b7bea9fe29eec", null ],
    [ "flowControllerDisplayTicketValueInTopup", "group__smartcardif.html#ga667c7f4b246786093a95fa30cbb2304b", null ],
    [ "flowControllerHandheldCardExpired", "group__smartcardif.html#gad28237e7360f06118438c6759899255f", null ],
    [ "flowControllerHandheldCheckLogin", "group__smartcardif.html#ga9e52b3914f49319116a857b046a995f7", null ],
    [ "flowControllerHandheldDisplayFail", "group__smartcardif.html#gad51ef131e54c2179fc7f1354e2ee866c", null ],
    [ "flowControllerHandheldDisplayLogFile", "group__smartcardif.html#ga8179ab143d589ca225a4944d5e6aa523", null ],
    [ "flowControllerHandheldDisplayPass", "group__smartcardif.html#ga79c0f15c2af60dbf49a931a797fd155b", null ],
    [ "flowControllerHandheldInsufficientBalance", "group__smartcardif.html#ga7ca6953ec39ab117f47e8746d661f216", null ],
    [ "flowControllerHandheldLogFile", "group__smartcardif.html#ga89c685ed77add3a8354ddb7287fb81bd", null ],
    [ "flowControllerHandheldPayPenalty", "group__smartcardif.html#ga4849abb2649000bf1c251d9abdda8161", null ],
    [ "flowControllerHandheldReadLogfile", "group__smartcardif.html#ga6a520e284130f8bcd9d4c2bb6dd56e01", null ],
    [ "flowControllerHandheldVerifyLogfile", "group__smartcardif.html#ga30131c61752eec4a491ddee2ba4531e7", null ],
    [ "flowControllerHandheldVerifyTicketBalance", "group__smartcardif.html#ga916de31cd7a57c4f51d1685d2e054f6c", null ],
    [ "flowControllerIncreaseCardTransactionCount", "group__smartcardif.html#ga00063c5fb681ebb14bb3093a3a1a55e7", null ],
    [ "flowControllerIncreaseCardTxnCount", "group__smartcardif.html#gacaa533f48bc9022d7b27153def281318", null ],
    [ "flowControllerIncrementTicketValue", "group__smartcardif.html#ga2bad4e7a84b5004e0a108df3162650bb", null ],
    [ "flowControllerIsMenuBusconsoleMayHandleCard", "group__smartcardif.html#ga688a562d407bed431bf8a0b207cffc2a", null ],
    [ "flowControllerIsMenuHandheldMayHandleCard", "group__smartcardif.html#ga7b84245532f182138d999ce1f2652324", null ],
    [ "flowControllerIsMenuTopupMayHandleCard", "group__smartcardif.html#ga8540466ef086718ff412529941c910e1", null ],
    [ "flowControllerIsMenuValidatorMayHandleCard", "group__smartcardif.html#ga14c2826bfc850e968e4e6bd965788784", null ],
    [ "flowControllerIsPayCardSelected", "group__smartcardif.html#gaeca08001392fffd00c9a9d46a49b72af", null ],
    [ "flowControllerIsPayCashSelected", "group__smartcardif.html#gae74d5e9300918543351b22e54bc3ede1", null ],
    [ "flowControllerIsTicketBalanceEnoughForPenalty", "group__smartcardif.html#ga39461a4b68b4fdd56f0e8a6f2726276b", null ],
    [ "flowControllerIsTopupValueEnabled", "group__smartcardif.html#ga711dd2c6130b0f3cb3ce0f336cb917a6", null ],
    [ "flowControllerLogTopup", "group__smartcardif.html#ga6d06550d5e15464e2b605c28afbf3752", null ],
    [ "flowControllerMustPayPenaltyWithCard", "group__smartcardif.html#ga7b6e20bcb2f6e28995e32f44643763ea", null ],
    [ "flowControllerPrintBill", "group__smartcardif.html#ga59e3ab32b6141b8457dd7695f3c8fd63", null ],
    [ "flowControllerPrintReceipt", "group__smartcardif.html#gad28c360eead42168f2e82bf71d5457e2", null ],
    [ "flowControllerPurseLowBalance", "group__smartcardif.html#ga1210b2cf3f1585930d58f86651d25696", null ],
    [ "flowControllerPursePay", "group__smartcardif.html#ga8f6269a38702aa20cc155744cdfef174", null ],
    [ "flowControllerPurseReject", "group__smartcardif.html#ga5fe7d507d1fc798d2e2ab274f759a530", null ],
    [ "flowControllerReadPurseInfo", "group__smartcardif.html#gafb07d68f2cbdadec7b7931fc817f9b7f", null ],
    [ "flowControllerSetTicketValueNul", "group__smartcardif.html#ga9842ba6a174acd44912ec1e4e3072dca", null ],
    [ "flowControllerTicketLowBalance", "group__smartcardif.html#gaa943830e136ae73106e28ed18dde3cd8", null ],
    [ "flowControllerTicketPay", "group__smartcardif.html#ga946a6ba59ca19cbb6b90141b40e661df", null ],
    [ "flowControllerTicketReject", "group__smartcardif.html#ga641a809b54bd9e85ee01a398c9af6f51", null ],
    [ "flowControllerVerifyPurseExpired_Date", "group__smartcardif.html#gaa9017d95fd4395a05c381b86f1058243", null ],
    [ "flowControllerVerifyPurseStatus", "group__smartcardif.html#gab3971cf04e8af6d826d006d2ca4cf33e", null ],
    [ "flowControllerVerifyTicketStatus", "group__smartcardif.html#gaa44b172f4f0d9f05d433514ed5c558ab", null ],
    [ "flowControllerVerifyTicketType1", "group__smartcardif.html#ga4af00c15e937db179b3cac7414b1d21f", null ],
    [ "flowControllerVerifyTicketType2", "group__smartcardif.html#gafcbb44226b245d01019b10e079637c9d", null ],
    [ "flowControllerVerifyTicketType6", "group__smartcardif.html#ga6c0b1d014005fedadb68f8f92052da0a", null ],
    [ "flowControllerVerifyTicketType9", "group__smartcardif.html#ga0dfe4c089108f77c6e6998a110dd00fb", null ],
    [ "flowControllerWritePurseLog", "group__smartcardif.html#gae9fa4751d16ddefecec603b6b42aa22f", null ],
    [ "intDnfccReadX", "group__smartcardif.html#ga0238b1a65c1f9b4cc95f0969e1858de8", null ],
    [ "intDnfccWritedX", "group__smartcardif.html#gae4574465d319461919aa41eb1d7266f0", null ],
    [ "nfccSmartcardIf", "group__smartcardif.html#ga6a933b5ec5b8e52cf11d097ae7b032db", null ],
    [ "nfccSmartcardIfInit", "group__smartcardif.html#gadd7820674bad38735b3af8f61dab8c2b", null ],
    [ "Rc663MultiWriteRegister", "group__smartcardif.html#ga5711a6b66d8bff47cf2cca9410cf99de", null ],
    [ "Rc663ReadRegister", "group__smartcardif.html#ga88cf166381fb68311b77aa0825b571fd", null ],
    [ "Rc663WriteRegister", "group__smartcardif.html#gaaa45c0f080a50fdb5fcbdba0a8e35d27", null ],
    [ "sendBalancesError", "group__smartcardif.html#gab7a4ed0f6ed6903226a68e0615f95139", null ],
    [ "sendBalancesOk", "group__smartcardif.html#ga7da3d6225da6ae795ee4d090443fca41", null ]
];