var struct_serial_communicator_handler__type =
[
    [ "callbackFunction", "struct_serial_communicator_handler__type.html#a36fa8183aed7edbd1220b9afe5c379c3", null ],
    [ "currentStep", "struct_serial_communicator_handler__type.html#a173c3e51d84299e222ad8385f609ea48", null ],
    [ "engine", "struct_serial_communicator_handler__type.html#a4244f1ee9d6893879493a52e8090922a", null ],
    [ "flow", "struct_serial_communicator_handler__type.html#ab3864c80dca6e912eda9ebdcb2c0d008", null ],
    [ "flowSize", "struct_serial_communicator_handler__type.html#a1c97d04730b6326bc244d28ee8c61e28", null ],
    [ "jsonEndPoint", "struct_serial_communicator_handler__type.html#a3a758d95f649e747c6ce8666e7b13d24", null ],
    [ "jsonReceiveData", "struct_serial_communicator_handler__type.html#a0d9f2890ece373d862faf7403ce67ff4", null ],
    [ "jsonRequestData", "struct_serial_communicator_handler__type.html#a9338ead4eed767ed08bcac31a84cc19a", null ],
    [ "uartNr", "struct_serial_communicator_handler__type.html#ae1794bfa7311c3b838a4bfd16c0a07af", null ]
];