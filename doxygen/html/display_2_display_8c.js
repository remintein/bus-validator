var display_2_display_8c =
[
    [ "BORDER", "group__display.html#ga6d0652ae6ea6a5c4fef68baf139fd085", null ],
    [ "BOTTOMBAR", "group__display.html#gaa2d0f95e110d69e736af85ea6d48415f", null ],
    [ "LOG_START_PIXEL", "group__display.html#gaa80fd7dbd17834d26bd72f2abb52cf55", null ],
    [ "STACK_SIZE", "group__display.html#ga6423a880df59733d2d9b509c7718d3a9", null ],
    [ "TOPBAR", "group__display.html#ga24493b584d8dbe84c1486d9fcccfa884", null ],
    [ "nfccDisplay", "group__display.html#gabb4c29f8f1f2602ebdd0d7e592a25d22", null ],
    [ "nfccDisplayClearScreenByCoordinates", "group__display.html#ga321b69163479973a4f16735256e99d52", null ],
    [ "nfccDisplayInit", "group__display.html#ga946a3efccdbc3e89eec494a46c91203a", null ],
    [ "nfccDisplayStartScreen", "group__display.html#gab24380b0827a2a4143fc9dc2b589de10", null ],
    [ "log_X", "group__display.html#ga44a4b4da898baca0b523f5069a4d3743", null ],
    [ "log_Y", "group__display.html#gafe37f57730b6d6ebb1b3ce29f2b9da05", null ],
    [ "ra8875", "group__display.html#ga9014e0a8907332c950900d951999c98c", null ]
];