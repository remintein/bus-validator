var structfile_card_holder_information_struct =
[
    [ "accountType", "structfile_card_holder_information_struct.html#ad9051ca937ebad8f771b56cb6e43af88", null ],
    [ "cardHolder_ID", "structfile_card_holder_information_struct.html#a6abfaf911b5f10bb0fbd776faba30dfe", null ],
    [ "DOB", "structfile_card_holder_information_struct.html#a3f3eb6f1ee73cc40b71f90aca1cb6533", null ],
    [ "fileFormatVersion", "structfile_card_holder_information_struct.html#a7b76853099940db2e3ccd784ea631016", null ],
    [ "fullName", "structfile_card_holder_information_struct.html#ab2f4619310558247ef06a46305e635af", null ],
    [ "futureUse", "structfile_card_holder_information_struct.html#a8e3a5398b175a2f6ac9058683c96bece", null ],
    [ "gender", "structfile_card_holder_information_struct.html#a068575f9b440a2feaa2a2b46b44accc3", null ],
    [ "orgCode", "structfile_card_holder_information_struct.html#a81f1dc705a96a3c8a209a01192347811", null ],
    [ "orgName", "structfile_card_holder_information_struct.html#a53b1a2c28eec5c20a26f91d548ae942f", null ]
];