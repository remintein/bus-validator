var queue_send_routines_8c =
[
    [ "queueSendToCanbus", "group__queue.html#gada8f603e1a561cc85095e52374ebc62f", null ],
    [ "queueSendToClock", "group__queue.html#ga979a07be3c45ba5aa40846a05cb3ddef", null ],
    [ "queueSendToDataDistributor", "group__queue.html#gae52195ce57d31881fe0b7c18c453e116", null ],
    [ "queueSendToDisplay", "group__queue.html#ga6fbd8d544671e211d35b887e86fc38e5", null ],
    [ "queueSendToFlashProgrammer", "group__queue.html#gac5cd7beae2b7a7c107eafaeb61ffa433", null ],
    [ "queueSendToGps", "group__queue.html#ga537db3bda80f2696b19ea81dfc3cd6e0", null ],
    [ "queueSendToGsm", "group__queue.html#ga350911dc6254d617d19e56d881d64b7e", null ],
    [ "queueSendToMenu", "group__queue.html#ga3d5bf352d65694c4046d70f84b21a1a8", null ],
    [ "queueSendToPrinter", "group__queue.html#ga8969593009e42d551564971b57e69b4d", null ],
    [ "queueSendToQrcode", "group__queue.html#ga92ac6c188ca99a0a681e1b9ae853da05", null ],
    [ "queueSendToSam", "group__queue.html#gaabb17de6ad7ecbe4693b16fffd97c2c8", null ],
    [ "queueSendToSdio", "group__queue.html#ga5418888374057a11c3f5a1e336c835a8", null ],
    [ "queueSendToSmartcard", "group__queue.html#gaa5788d77440ceb66fc6bcc6ab685d938", null ],
    [ "queueSendToSound", "group__queue.html#gafa9fd08f9de5fcc8f327ed68b99dacbc", null ],
    [ "queueSendToTouch", "group__queue.html#ga62d71567fafa4de7217911b236189443", null ],
    [ "queueSendToWifi", "group__queue.html#ga936086daba9389555815faa4a686ad3e", null ]
];