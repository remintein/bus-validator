var _r_a8875_user_settings_8h =
[
    [ "__RA8875ISRINT", "_r_a8875_user_settings_8h.html#a007c6e1f38a4547bb99f283ea2563171", null ],
    [ "_DFT_RA8875_EXTFONTROMCODING", "_r_a8875_user_settings_8h.html#af342be09e4da592b654d493fc445ad40", null ],
    [ "_DFT_RA8875_EXTFONTROMTYPE", "_r_a8875_user_settings_8h.html#a38726387473e932492bf7e9a6ed0c993", null ],
    [ "_DFT_RA8875_TEXTWRAP", "_r_a8875_user_settings_8h.html#a409a3e8f731be7b0e2bf43df5f71cd85", null ],
    [ "_FASTSSPORT", "_r_a8875_user_settings_8h.html#a1fa361060da0696515368cc421f2c154", null ],
    [ "_RA8875_DEFAULTBACKLIGHT", "_r_a8875_user_settings_8h.html#a78ae2d33b9429c0831f73a6509f07ac2", null ],
    [ "_RA8875_DEFAULTSCRROT", "_r_a8875_user_settings_8h.html#add23febe453e6856372f73cfc8f1dece", null ],
    [ "_RA8875_DEFAULTTXTBKGRND", "_r_a8875_user_settings_8h.html#a444776afeb1e33d93468122d78a3db14", null ],
    [ "_RA8875_DEFAULTTXTFRGRND", "_r_a8875_user_settings_8h.html#a2fecbc52905c9dca53e61136e20bb7e7", null ],
    [ "_RA8875_TXTRNDOPTIMIZER", "_r_a8875_user_settings_8h.html#a77686de95a519e1512238aad98c53ec6", null ],
    [ "DEFAULTCURSORBLINKRATE", "_r_a8875_user_settings_8h.html#aea86c1684e9ef273914c0c8a45fe54de", null ],
    [ "DEFAULTINTENCODING", "_r_a8875_user_settings_8h.html#a52e44de1a34d6165ac21522144504d2e", null ],
    [ "FORCE_RA8875_TXTREND_FOLLOW_CURS", "_r_a8875_user_settings_8h.html#a4fc6785d04c70f729d58e8cb85aa703e", null ],
    [ "USE_RA8875_SEPARATE_TEXT_COLOR", "_r_a8875_user_settings_8h.html#af7b5250875dbdf3eb6557a21c57dd102", null ]
];