var _nxp_lib_functions_8c =
[
    [ "fcNxpLibBegin", "_nxp_lib_functions_8c.html#a7f48b5a8673956815ff5ba1f80dcdfec", null ],
    [ "fcNxpLibDetectCard", "_nxp_lib_functions_8c.html#a287cb95a7017ed023e41b24fcc59ab06", null ],
    [ "fcNxpLibGetNextCard", "_nxp_lib_functions_8c.html#a7f61a169be300f3fe4c2d2ef4f916eb5", null ],
    [ "fcNxpLibInitEncryption", "_nxp_lib_functions_8c.html#acd4e76fee269e4fd3adafad5c31a097e", null ],
    [ "fcNxpLibInitialize", "_nxp_lib_functions_8c.html#a7fc30b6df70b5fcf2fbf3c6153e299dc", null ],
    [ "AppContext", "_nxp_lib_functions_8c.html#a86ecc5f4173678a3d6acd812fb2ae3d3", null ],
    [ "palMFDF", "_nxp_lib_functions_8c.html#af64882b8e361f95a88bf8428aa00c1de", null ],
    [ "pCryptoRng", "_nxp_lib_functions_8c.html#ae82ec64185f086fa9bd319e0df15fb0d", null ],
    [ "pCryptoSym", "_nxp_lib_functions_8c.html#a42dc0e4505a6b677f1f8523784a840c8", null ],
    [ "pDiscLoop", "_nxp_lib_functions_8c.html#a41ece646f4aea0b509e8d7e64467ad32", null ],
    [ "pKeyStore", "_nxp_lib_functions_8c.html#a01dc365495023d9eddc0834f7ae7ac7f", null ],
    [ "ppalI14443p3a", "_nxp_lib_functions_8c.html#a108f7e560bb5ae06b77794c513659553", null ],
    [ "ppalI14443p4", "_nxp_lib_functions_8c.html#a331fe2aba071268bbe9e5bb2bb6ba523", null ],
    [ "ppalMifare", "_nxp_lib_functions_8c.html#aac2468791f10af06bb8c1c28e980246b", null ]
];