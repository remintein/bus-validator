var initialize_hardware_8c =
[
    [ "Error_Handler", "group__main.html#gad4e07955fadfeb416e9620ddb42f8fe9", null ],
    [ "HAL_I2C_MspInit", "group__hardware.html#gabe01a202c27b23fc150aa66af3130073", null ],
    [ "HAL_SMARTCARD_MspInit", "group__hardware.html#ga11318995bc017548fc1bb9785a645bf6", null ],
    [ "I2C_Touch_Init", "group__hardware.html#ga79739b241c7ba302d12d8faf21f3cd19", null ],
    [ "I2C_Touch_MspInit", "group__hardware.html#ga9e8a9d1b1485026f0bbb2820684cd64f", null ],
    [ "initializeHardware", "group__hardware.html#gaaaca845b980f160a23d3b990338d4602", null ],
    [ "MX_DAC_Init", "group__hardware.html#gaf109708357f8cefd399825998b3ed536", null ],
    [ "MX_RNG_Init", "group___random.html#ga460c4ce66db82fe318db2b3131604057", null ],
    [ "MX_RTC_Init", "group__hardware.html#gabf4accd1ce479030808e546f3d4642c9", null ],
    [ "SMARTCARD_SAM_Init", "group__hardware.html#ga6dfb81e0502c204f4657e06c204119fe", null ],
    [ "SystemClock_Config", "group__hardware.html#ga70af21c671abfcc773614a9a4f63d920", null ],
    [ "UART_Display_Init", "group__hardware.html#gaa7abfbdfdb40db46f4ece7090d3cb963", null ],
    [ "UART_GSM_Init", "group__hardware.html#gaa37182c12be5c4256a9fd37a14b73322", null ],
    [ "UART_QRCODEPRINTER_Init", "group__hardware.html#gaa0df5d55d055a161b5c994d06a0242f5", null ],
    [ "UART_RS485_Init", "group__hardware.html#gada726ec788c4c37082e2ee46124dc887", null ],
    [ "UART_WIFI_Init", "group__hardware.html#ga2682f93d7749782b4dffcd80e7039826", null ],
    [ "hdac", "group__sound.html#ga40a0d1383beda58531dfda6218720e45", null ],
    [ "nfccSmartcardIf_HandleStructure", "group__smartcardif.html#gad636a5ca3718949b14d4c868cc18e26f", null ],
    [ "smartcard_sam", "initialize_hardware_8c.html#a111faf9bd5e81ec0b70beb589d7b447b", null ]
];