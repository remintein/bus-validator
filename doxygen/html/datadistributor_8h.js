var datadistributor_8h =
[
    [ "MESSAGE_BUFFER", "group__datadistributor.html#ga18c02380d1dd4390554e3019767a64c7", null ],
    [ "MESSAGE_KIND_KEEPALIVE", "group__datadistributor.html#gac627452cd874f075a2dde106708be1fe", null ],
    [ "MESSAGE_KIND_NORMAL", "group__datadistributor.html#gab041cedbfb749cbd4fdad0cc3e5a0b23", null ],
    [ "MESSAGE_MAX_RESPONSE_LENGTH", "group__datadistributor.html#gab320db4421979826b32f52fe05c7c2b6", null ],
    [ "TRANSMIxT_STATUS_FINISHED", "group__datadistributor.html#ga930c2b8c044ca793170aa675e69e024a", null ],
    [ "TRANSMxIT_STATUS_WORK", "group__datadistributor.html#gaa635e83c6d826d0529669fa5839d6d9c", null ],
    [ "TRANSxMIT_STATUS_WAITING", "group__datadistributor.html#ga307dd69887e7a1193e9f15dc4033e27a", null ],
    [ "TRANxSMIT_STATUS_IDLE", "group__datadistributor.html#gaba69d4680fd37b2aba3956b00f45362e", null ],
    [ "TransmitStatus", "group__datadistributor.html#ga252811b6884c084b137306819c88fe31", null ],
    [ "_transmitStatus", "group__datadistributor.html#ga3876d1e167a4e9e0814e5049e161c9f0", [
      [ "STATUS_IDLE", "group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a99ade6ecff7f9e1fb13b62be39b85d82", null ],
      [ "STATUS_WAITING", "group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a5a5262e49db47e003a2d28239049955d", null ],
      [ "STATUS_WORK", "group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a12f8edd675e2afc578100877add0a060", null ],
      [ "STATUS_FINISHED", "group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a7573cce65348568c572c9ccb33291d00", null ]
    ] ],
    [ "nfccDatadistributor", "group__datadistributor.html#ga6dc6648feb5e7322c5a88bc452ab98ef", null ],
    [ "nfccDatadistributorInit", "group__datadistributor.html#ga10e93ce62737f5919b3f288eb2db874b", null ],
    [ "beginMessageQueue", "group__datadistributor.html#ga66b28c831512e43dde672b99874e033b", null ],
    [ "endMessageQueue", "group__datadistributor.html#gac398a8d46c4e58f2c1de05332f086ba7", null ],
    [ "gsmResponse", "group__datadistributor.html#ga9302a24127ad672b417ba0a26563e965", null ],
    [ "lastMessageId", "group__datadistributor.html#gaa30c3804fc0e6c1d6d3fe821f552ce48", null ],
    [ "messages", "group__datadistributor.html#ga79272a9203712f9e78def2b8e79507d6", null ],
    [ "nextMessageId", "group__datadistributor.html#ga3433fd93c2411b8943e34317c900da57", null ],
    [ "TransmitGsmMessage", "group__datadistributor.html#ga9212081cbdb663cf3db2efefa8b09855", null ],
    [ "TransmitWifiMessage", "group__datadistributor.html#ga47b5b69ced5008c7badeffa2d7589e15", null ],
    [ "wifiResponse", "group__datadistributor.html#ga1ef0b8b95422b0b6427ef32bf8fb9715", null ]
];