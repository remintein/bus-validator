var qrcode_8c =
[
    [ "QRCODE_COMMUNICATION", "qrcode_8c.html#a141d8727163752b851201644a56f28c6", null ],
    [ "QRCODE_MAXLENGTH", "group__qrcode.html#ga556c12bf765b3ecad17d53029d975973", null ],
    [ "STACK_SIZE", "group__qrcode.html#ga6423a880df59733d2d9b509c7718d3a9", null ],
    [ "nfccQrcode", "group__qrcode.html#ga2099854228fe5c3c5b4bf6a7c4ecf417", null ],
    [ "nfccQrcodeInit", "group__qrcode.html#ga75250d609cfa97e4eb44ad0d07f543da", null ],
    [ "huart_qrcodeprinter", "group__qrcode.html#ga659c04aa4b66f6258969d8209ae304ec", null ],
    [ "qrcodeReceiveBuffer", "group__qrcode.html#ga6e98ecd970b888474c4f11e5b4838893", null ]
];