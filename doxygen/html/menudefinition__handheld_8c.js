var menudefinition__handheld_8c =
[
    [ "apiDnfccMenuInitializeAllMenusHandheld", "group__menu.html#gae3fb6fa216a3fdbf81883a60a314b73c", null ],
    [ "buttons_cardPayment", "group__menu.html#ga8b511ff2baee897cefcd4ef832699181", null ],
    [ "buttons_cashPayment", "group__menu.html#ga167fbacdf1a05b6de9f3f0a95942471d", null ],
    [ "buttons_handheld", "group__menu.html#gace4fb37d62cc216bdad0e5c062dc7721", null ],
    [ "buttons_handheldFail", "group__menu.html#ga0c90464df2168e947b0e9b9371faee7d", null ],
    [ "buttons_handheldPass", "group__menu.html#ga558def004f352246a62dfe98217e378b", null ],
    [ "buttons_penaltypay", "group__menu.html#ga725bf2ec4ab96227b32085208ce3bc2e", null ],
    [ "handheldInfo", "group__menu.html#ga50d98b49a2927330be6530a6311ba00f", null ],
    [ "menu_handheld", "group__menu.html#ga517ceffa138220ccc85123f4482cb793", null ],
    [ "menu_handheld_card_payment", "group__menu.html#ga3026039350da27f8f2b020d87fa4ddff", null ],
    [ "menu_handheld_cash_payment", "group__menu.html#gaab85503b28c837b5ababee28bfe36513", null ],
    [ "menu_handheld_selectpenaltypay", "group__menu.html#ga4f6d04d257d0be63111ac2ccd57cad2d", null ],
    [ "menu_handheldFail", "group__menu.html#gae49b4c4dfba675494c915e42b358ef64", null ],
    [ "menu_handheldPass", "group__menu.html#gad6a2ec3c8d266f3f77f7869371aee788", null ]
];