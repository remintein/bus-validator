var gsm_8h =
[
    [ "GPSSTATUS", "gsm_8h.html#a26216645f1aaffc4218e7abfe0a95060", [
      [ "VALID", "gsm_8h.html#a26216645f1aaffc4218e7abfe0a95060acf0713491d9b887eaccfd80c18abca47", null ],
      [ "INVALID", "gsm_8h.html#a26216645f1aaffc4218e7abfe0a95060aef2863a469df3ea6871d640e3669a2f2", null ],
      [ "OLD", "gsm_8h.html#a26216645f1aaffc4218e7abfe0a95060a1c0537b026f64ae8d4722fc001a78c41", null ]
    ] ],
    [ "gpsGetStatus", "gsm_8h.html#a1f758c62953c0b4b644b9d7f349c7e73", null ],
    [ "gpsRetrieveLongLat", "gsm_8h.html#afc484e1c4861b6eebf004f0838589086", null ],
    [ "nfccGsmGps", "gsm_8h.html#a0955ed886a83085285387b50d161e2d1", null ],
    [ "nfccGsmGpsInit", "gsm_8h.html#afe15ab7e8011bb70cd5fd6a11d4536c4", null ]
];