var menu_2menu__busconsole_8c =
[
    [ "MAXROUTE", "menu_2menu__busconsole_8c.html#aa22ff97ff35e3ff11bfc38e4a314e34e", null ],
    [ "MAXSERVICE", "menu_2menu__busconsole_8c.html#ae01d8c55a13bcce3e4a8f633b77905cd", null ],
    [ "initializeLabels", "menu_2menu__busconsole_8c.html#a2307d9a1567c33f12cc1e5169ae97d03", null ],
    [ "setPurseFare", "menu_2menu__busconsole_8c.html#a86b9ac7aace3c7df8f068419e9b133b7", null ],
    [ "verifyMessage", "menu_2menu__busconsole_8c.html#a8fdc9b445abfa36eb183fd0a1ef96e8c", null ],
    [ "verifyTimeout", "menu_2menu__busconsole_8c.html#a9e6017064ff5aed2c76a1353aef466b0", null ],
    [ "busline", "menu_2menu__busconsole_8c.html#ae676a2f52d9234ba0a7ad9793f4c8f5f", null ],
    [ "payInfo", "menu_2menu__busconsole_8c.html#a64798be63afc51503c4032a58e817810", null ],
    [ "payinfoSubtitle", "menu_2menu__busconsole_8c.html#ac4cc28e30a0fbb44cbff4dee0adb7750", null ],
    [ "serviceline", "menu_2menu__busconsole_8c.html#a7b78eccc0ea5085f04e43fb3ac07aa8e", null ],
    [ "ticketvalue", "menu_2menu__busconsole_8c.html#a16170a352a33e618b7be96bcba36c029", null ],
    [ "timeoutForClean", "menu_2menu__busconsole_8c.html#a54d8931ad340a72b8ddc1983748fabfc", null ],
    [ "timeoutForClean2", "menu_2menu__busconsole_8c.html#a42f59a5986a0097336a3ef65a105b0b6", null ],
    [ "updateinfo", "menu_2menu__busconsole_8c.html#a78cd5456d67812429c0c3529b2f8a4b7", null ],
    [ "validatorButtonLabel", "menu_2menu__busconsole_8c.html#a5a81c5ebcd3a7a8e171cfa3afdaf248e", null ],
    [ "validatorInfo", "menu_2menu__busconsole_8c.html#a7d27b8bba046c424673955ae285993d9", null ],
    [ "validatorInfoSubtitle", "menu_2menu__busconsole_8c.html#abdeb25cbf84e67575d909aff8eb950f6", null ],
    [ "valueLabel", "menu_2menu__busconsole_8c.html#aa5ed738a6f9fd02c976449d510b28b1a", null ]
];