var menu__topup_8c =
[
    [ "DELAY_SHOW_BALANCE", "menu__topup_8c.html#a67e2238620a48949548f45c9914773e4", null ],
    [ "apiDnfccMenuInitializeLabels", "group__menu.html#ga209aed87e9f55bef62d3ae1e5023a4ec", null ],
    [ "apiDnfccMenuProcessMessage", "group__menu.html#gab3f0bac22183b3f650ee192676d528d9", null ],
    [ "apiDnfccMenuVerifyTimeout", "group__menu.html#gaef9b9c2e2adcf43b1e21365d9199d08a", null ],
    [ "busline", "group__menu.html#gae676a2f52d9234ba0a7ad9793f4c8f5f", null ],
    [ "curBalance", "group__menu.html#gaecd78482a35ce587fa235d6ceee15bd2", null ],
    [ "payInfo", "group__menu.html#ga64798be63afc51503c4032a58e817810", null ],
    [ "payinfoSubtitle", "group__menu.html#gac4cc28e30a0fbb44cbff4dee0adb7750", null ],
    [ "serviceline", "group__menu.html#ga7b78eccc0ea5085f04e43fb3ac07aa8e", null ],
    [ "ticketvalue", "group__menu.html#ga16170a352a33e618b7be96bcba36c029", null ],
    [ "timeoutForClean", "group__menu.html#ga54d8931ad340a72b8ddc1983748fabfc", null ],
    [ "timeoutForClean2", "group__menu.html#ga42f59a5986a0097336a3ef65a105b0b6", null ],
    [ "topupValueLabel", "group__menu.html#gac2d68b1d5db429afab51f46a0c521f2f", null ],
    [ "updateinfo", "group__menu.html#ga78cd5456d67812429c0c3529b2f8a4b7", null ],
    [ "validatorButtonLabel", "group__menu.html#ga5a81c5ebcd3a7a8e171cfa3afdaf248e", null ],
    [ "validTillLabel", "group__menu.html#ga0b2754d92aa9ff40ac87b166db254df2", null ],
    [ "valueLabel", "group__menu.html#gaa5ed738a6f9fd02c976449d510b28b1a", null ]
];