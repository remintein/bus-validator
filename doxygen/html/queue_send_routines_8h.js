var queue_send_routines_8h =
[
    [ "queueSendToCanbus", "queue_send_routines_8h.html#ada8f603e1a561cc85095e52374ebc62f", null ],
    [ "queueSendToClock", "queue_send_routines_8h.html#a979a07be3c45ba5aa40846a05cb3ddef", null ],
    [ "queueSendToDataDistributor", "queue_send_routines_8h.html#ae52195ce57d31881fe0b7c18c453e116", null ],
    [ "queueSendToDisplay", "queue_send_routines_8h.html#a6fbd8d544671e211d35b887e86fc38e5", null ],
    [ "queueSendToFlashProgrammer", "queue_send_routines_8h.html#ac5cd7beae2b7a7c107eafaeb61ffa433", null ],
    [ "queueSendToGps", "queue_send_routines_8h.html#a537db3bda80f2696b19ea81dfc3cd6e0", null ],
    [ "queueSendToGsm", "queue_send_routines_8h.html#a350911dc6254d617d19e56d881d64b7e", null ],
    [ "queueSendToMenu", "queue_send_routines_8h.html#a3d5bf352d65694c4046d70f84b21a1a8", null ],
    [ "queueSendToPrinter", "queue_send_routines_8h.html#a8969593009e42d551564971b57e69b4d", null ],
    [ "queueSendToQrcode", "queue_send_routines_8h.html#a92ac6c188ca99a0a681e1b9ae853da05", null ],
    [ "queueSendToSam", "queue_send_routines_8h.html#aabb17de6ad7ecbe4693b16fffd97c2c8", null ],
    [ "queueSendToSdio", "queue_send_routines_8h.html#a5418888374057a11c3f5a1e336c835a8", null ],
    [ "queueSendToSmartcard", "queue_send_routines_8h.html#aa5788d77440ceb66fc6bcc6ab685d938", null ],
    [ "queueSendToSound", "queue_send_routines_8h.html#afa9fd08f9de5fcc8f327ed68b99dacbc", null ],
    [ "queueSendToTouch", "queue_send_routines_8h.html#a62d71567fafa4de7217911b236189443", null ],
    [ "queueSendToWifi", "queue_send_routines_8h.html#a936086daba9389555815faa4a686ad3e", null ]
];