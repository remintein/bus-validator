var _sdio_8h =
[
    [ "SdioBlockInfo", "struct_sdio_block_info.html", "struct_sdio_block_info" ],
    [ "firstBlockData", "structfirst_block_data.html", "structfirst_block_data" ],
    [ "DWORD", "_sdio_8h.html#a6fbc9bf098929d0ceec7c56a0f8bc764", null ],
    [ "DnfcFileNo", "_sdio_8h.html#a5e0a73089e3fd5e16c4ac9825aeaf538", [
      [ "FIRST_BLOCKFILE", "_sdio_8h.html#a5e0a73089e3fd5e16c4ac9825aeaf538ad3e5d3ed7ceaf46fed656304114c1fc2", null ],
      [ "WIFI_FILE", "_sdio_8h.html#a5e0a73089e3fd5e16c4ac9825aeaf538a909ad874fab0af3e011f4557eefadc4d", null ],
      [ "PRINTER_FILE", "_sdio_8h.html#a5e0a73089e3fd5e16c4ac9825aeaf538abe5f883d6bbdc83f51a534039338db2d", null ],
      [ "FARE_FILE", "_sdio_8h.html#a5e0a73089e3fd5e16c4ac9825aeaf538ae1b8b7c9bc7a7f76ef99a54b3c9d673d", null ],
      [ "BLACKLIST_FILE", "_sdio_8h.html#a5e0a73089e3fd5e16c4ac9825aeaf538acf101825eb3cb8808ba963622bba5206", null ],
      [ "LOGFILE", "_sdio_8h.html#a5e0a73089e3fd5e16c4ac9825aeaf538afb1d2f51f994e35d9f2769a5af51570e", null ]
    ] ],
    [ "DRESULT", "_sdio_8h.html#aacdfef1dad6565f65c26d12fe0ea4b2b", [
      [ "RES_OK", "_sdio_8h.html#aacdfef1dad6565f65c26d12fe0ea4b2ba2ea4b6ef3fffc17dd1d38ab5c2837737", null ],
      [ "RES_ERROR", "_sdio_8h.html#aacdfef1dad6565f65c26d12fe0ea4b2ba78011f5557679ec178fb40bd21e89840", null ],
      [ "RES_WRPRT", "_sdio_8h.html#aacdfef1dad6565f65c26d12fe0ea4b2ba442a6d4393dc404827067bc4e981b322", null ],
      [ "RES_NOTRDY", "_sdio_8h.html#aacdfef1dad6565f65c26d12fe0ea4b2baad64c27c69eb1ff39ae67c5f77bb2b1d", null ],
      [ "RES_PARERR", "_sdio_8h.html#aacdfef1dad6565f65c26d12fe0ea4b2baf4dcc07fd46310b5495fa8025c89a9f3", null ]
    ] ],
    [ "sdioBlockStatus", "_sdio_8h.html#a159641f4fa0c869b2bcc2afc3cc9f7a6", [
      [ "EMPTY", "_sdio_8h.html#a159641f4fa0c869b2bcc2afc3cc9f7a6a2f0d18fc0d0fa4a6cd92dc328501874d", null ],
      [ "READING", "_sdio_8h.html#a159641f4fa0c869b2bcc2afc3cc9f7a6a6c1e47c2556673ca09497ada3062b14c", null ],
      [ "READY_READ", "_sdio_8h.html#a159641f4fa0c869b2bcc2afc3cc9f7a6a9cc6627e672f7ddcffa64218114e98e2", null ],
      [ "DIRTY_WRITE", "_sdio_8h.html#a159641f4fa0c869b2bcc2afc3cc9f7a6ab5462a6a65e15fa96e63defd798f9d6d", null ],
      [ "READY_WRITE", "_sdio_8h.html#a159641f4fa0c869b2bcc2afc3cc9f7a6a321cfcad0e3bc46027981a7c2b2f3e1c", null ]
    ] ],
    [ "SdioStatus", "_sdio_8h.html#a02fd1de6d883f06ff0cdfb716c61886c", [
      [ "Initializing", "_sdio_8h.html#a02fd1de6d883f06ff0cdfb716c61886cad1fe1e4629dd7396ce7a00cfa8177b31", null ],
      [ "Busy", "_sdio_8h.html#a02fd1de6d883f06ff0cdfb716c61886ca2c75f7f845345ca21b3f4b5d9ee960f0", null ],
      [ "Ready", "_sdio_8h.html#a02fd1de6d883f06ff0cdfb716c61886ca0e73e048b83849a411148a8dddc6dfcb", null ]
    ] ],
    [ "DnfccGetDataForFile", "_sdio_8h.html#a79f6d3c3df03a2f37e1721d776d4a320", null ],
    [ "DnfccInitializeBlacklistTransfer", "_sdio_8h.html#ab87300f6139197dd8926d863cc81a681", null ],
    [ "DnfccInitializeFareTransfer", "_sdio_8h.html#a5c49ac7da5a68873d0786d98bd1331ff", null ],
    [ "DnfccInitializeFile", "_sdio_8h.html#ab1a3bf6a776f5f87da230491e5a81c5a", null ],
    [ "DnfccInitializePrinterTransfer", "_sdio_8h.html#aaa29a0d6eee6d168c6e15402463b88f9", null ],
    [ "DnfccInitializeWifiTransfer", "_sdio_8h.html#a80516a14deb91896ea6bd517844ee2c4", null ],
    [ "DnfccRequestNextBlock", "_sdio_8h.html#a22b08a6f6323e5310c9a7315bc294ed2", null ],
    [ "DnfccStoreFileAsActive", "_sdio_8h.html#a5d6ce3e6b0014bcf79fd7ac3054d0765", null ],
    [ "nfccSdio", "_sdio_8h.html#a5e465c001fb7be3b1afc7f049b821861", null ],
    [ "nfccSdioInit", "_sdio_8h.html#abaddbba105972b8670217d638e7ff463", null ]
];