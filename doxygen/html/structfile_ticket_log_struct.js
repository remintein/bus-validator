var structfile_ticket_log_struct =
[
    [ "fileFormatVersion", "structfile_ticket_log_struct.html#a79e3e1c53034642a0813ffecc1a3559e", null ],
    [ "futureUse", "structfile_ticket_log_struct.html#ab3da992ae2ec5fb7db411958bc543d27", null ],
    [ "ticketBeforeCount", "structfile_ticket_log_struct.html#aa520fe52819974c2b61da94dad077b48", null ],
    [ "ticketSAM_ID", "structfile_ticket_log_struct.html#a28d23cdbdbea137ae5985814b279fcd3", null ],
    [ "ticketTransactionDateTime", "structfile_ticket_log_struct.html#a15cca1c596d5063f18beca373546c3d5", null ],
    [ "ticketTransactionsSubType", "structfile_ticket_log_struct.html#ad25789f766250354a6c17287787ecfd4", null ],
    [ "ticketUsedCount", "structfile_ticket_log_struct.html#ade7fa5ed7769e8afaf1e2b4fce7ca150", null ],
    [ "ticketValue", "structfile_ticket_log_struct.html#ad180bac0c0aaf85ab671d024e4505bff", null ]
];