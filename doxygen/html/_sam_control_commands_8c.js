var _sam_control_commands_8c =
[
    [ "DnfccRcLoadRegisterValueSet", "group__sam.html#ga16e45b7fc86bb53c643050b201765e91", null ],
    [ "DnfccSamDeselect", "group__sam.html#gaba4dfe2db2970370926f46f503911c38", null ],
    [ "DnfccSamDesfireAuthenticatePicc", "group__sam.html#ga0cb219d4131309013c2bec8d1ef7cd1d", null ],
    [ "DnfccSamDesfireReadX", "group__sam.html#gafadb9bc97f10df4a76c3a0d72e93689c", null ],
    [ "DnfccSamFreeCids", "group__sam.html#ga9956a58463012ffa47a93418ea5f9156", null ],
    [ "DnfccSamIso14443_3ActivateIdle", "group__sam.html#ga9d92db5fb1421e8e24add9a99826e3e5", null ],
    [ "DnfccSamIso14443_3AntiCollisionSelect", "group__sam.html#gabdeb3c2d30457513330650edbeff3d08", null ],
    [ "DnfccSamIso14443_3RequestWakeup", "group__sam.html#ga5b7123e89ed39679baec9bda101c296a", null ],
    [ "DnfccSamIso14443_4Exchange", "group__sam.html#gafadc97798042b9039730e49c67c9eab4", null ],
    [ "DnfccSamIso14443_4RatsPps", "group__sam.html#ga621f5d7fd3fb03133b3c40ce0bd412b4", null ],
    [ "DnfccSamPresenceCheck", "group__sam.html#ga97ac03c1fd440d57410131775023bfb6", null ],
    [ "DnfccSamRcFreeCID", "group__sam.html#ga855b4154e82b5e7b45c3c2c8030d3fc9", null ],
    [ "DnfccSamRcInit", "group__sam.html#ga29616b3db85c7f84b5e158e41fdd0e97", null ],
    [ "DnfccSamRcReadRegisters", "group__sam.html#ga00a15786775a2d1e3cd9cf010836dea7", null ],
    [ "DnfccSamRcReadSingleRegister", "group__sam.html#ga6b4196ed85e8a49380c709cea0613083", null ],
    [ "DnfccSamRcRFControl", "group__sam.html#ga6b153c09083668ed6033e738719b8693", null ],
    [ "DnfccSamRcWriteRegister", "group__sam.html#gaeb1058db8447044f70b651991b1d6b86", null ],
    [ "SamPerformReset", "group__sam.html#gae652186071c7618afc341739edf742eb", null ],
    [ "smartcard_sam", "_sam_control_commands_8c.html#a111faf9bd5e81ec0b70beb589d7b447b", null ]
];