var menu_2menu__handheld_8c =
[
    [ "initializeLabels", "menu_2menu__handheld_8c.html#a2307d9a1567c33f12cc1e5169ae97d03", null ],
    [ "verifyMessage", "menu_2menu__handheld_8c.html#a8fdc9b445abfa36eb183fd0a1ef96e8c", null ],
    [ "verifyTimeout", "menu_2menu__handheld_8c.html#a9e6017064ff5aed2c76a1353aef466b0", null ],
    [ "handheldInfo", "menu_2menu__handheld_8c.html#a50d98b49a2927330be6530a6311ba00f", null ],
    [ "payInfo", "menu_2menu__handheld_8c.html#a64798be63afc51503c4032a58e817810", null ],
    [ "payinfoSubtitle", "menu_2menu__handheld_8c.html#ac4cc28e30a0fbb44cbff4dee0adb7750", null ],
    [ "ticketvalue", "menu_2menu__handheld_8c.html#a16170a352a33e618b7be96bcba36c029", null ],
    [ "timeoutForClean", "menu_2menu__handheld_8c.html#a54d8931ad340a72b8ddc1983748fabfc", null ],
    [ "timeoutForClean2", "menu_2menu__handheld_8c.html#a42f59a5986a0097336a3ef65a105b0b6", null ],
    [ "updateinfo", "menu_2menu__handheld_8c.html#a78cd5456d67812429c0c3529b2f8a4b7", null ],
    [ "validatorButtonLabel", "menu_2menu__handheld_8c.html#a5a81c5ebcd3a7a8e171cfa3afdaf248e", null ],
    [ "valueLabel", "menu_2menu__handheld_8c.html#aa5ed738a6f9fd02c976449d510b28b1a", null ]
];