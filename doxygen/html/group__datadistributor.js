var group__datadistributor =
[
    [ "TransmitMessage", "struct_transmit_message.html", [
      [ "data", "struct_transmit_message.html#a497af80b4a32724838dd6464badc47ec", null ],
      [ "endpoint", "struct_transmit_message.html#a053d4043294233f213c2bcb5e2019115", null ],
      [ "messageId", "struct_transmit_message.html#a587c533f6a994b5984018fb03f78677b", null ],
      [ "response", "struct_transmit_message.html#aad6fe05d98bfea221ae94aff6502ea09", null ],
      [ "transmitStatus", "struct_transmit_message.html#aa9c9ad6abbb69d5d74f8ccb94e69dcad", null ]
    ] ],
    [ "MessageToSend", "struct_message_to_send.html", [
      [ "data", "struct_message_to_send.html#a13101a3f9322fbd238833a8c08dca59a", null ],
      [ "endpoint", "struct_message_to_send.html#a3c9dc36eea69c0955653e0a9bc596bab", null ],
      [ "messageId", "struct_message_to_send.html#ad4d5b586a54a769f24fc573677a3af0b", null ],
      [ "messageKind", "struct_message_to_send.html#ab9e504bb5c105bd40f060f2098ded764", null ],
      [ "transmitStatus", "struct_message_to_send.html#aed65cbc8ec22dcd7354310bc5acb2b73", null ]
    ] ],
    [ "MESSAGE_MAX_RESPONSE_LENGTH", "group__datadistributor.html#gab320db4421979826b32f52fe05c7c2b6", null ],
    [ "TRANSMIxT_STATUS_FINISHED", "group__datadistributor.html#ga930c2b8c044ca793170aa675e69e024a", null ],
    [ "TRANSMxIT_STATUS_WORK", "group__datadistributor.html#gaa635e83c6d826d0529669fa5839d6d9c", null ],
    [ "TRANSxMIT_STATUS_WAITING", "group__datadistributor.html#ga307dd69887e7a1193e9f15dc4033e27a", null ],
    [ "TransmitStatus", "group__datadistributor.html#ga252811b6884c084b137306819c88fe31", null ],
    [ "_transmitStatus", "group__datadistributor.html#ga3876d1e167a4e9e0814e5049e161c9f0", [
      [ "STATUS_IDLE", "group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a99ade6ecff7f9e1fb13b62be39b85d82", null ],
      [ "STATUS_WAITING", "group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a5a5262e49db47e003a2d28239049955d", null ],
      [ "STATUS_WORK", "group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a12f8edd675e2afc578100877add0a060", null ],
      [ "STATUS_FINISHED", "group__datadistributor.html#gga3876d1e167a4e9e0814e5049e161c9f0a7573cce65348568c572c9ccb33291d00", null ]
    ] ],
    [ "apiDnfccGenerateJsonMessageInQueue", "group__datadistributor.html#gadfdc8c437f74057845a8efc0206b0aeb", null ],
    [ "apiDnfccProcessMessageFromBackend", "group__datadistributor.html#gaf7478b5d69b06f8b00a32a2158a257a6", null ],
    [ "apiDnfccStartJsonHeader", "group__datadistributor.html#gad1e188f394a5f36052bfa1f85b898685", null ],
    [ "getField", "group__datadistributor.html#ga25c31dad0a224c5f5f1407b6f4a57ebd", null ],
    [ "intDnfccSanitize4", "group__datadistributor.html#ga2d10e23e0bb06ac6e396729c632ac060", null ],
    [ "intDnfccStartJsonHeaderDateTime", "group__datadistributor.html#ga05b7e322db4d49eeeed1677abb68680c", null ],
    [ "nfccDatadistributor", "group__datadistributor.html#ga6dc6648feb5e7322c5a88bc452ab98ef", null ],
    [ "nfccDatadistributorInit", "group__datadistributor.html#ga10e93ce62737f5919b3f288eb2db874b", null ],
    [ "buf", "group__datadistributor.html#ga9d931f7ca966bf184d66b59586686b17", null ],
    [ "defaultFileVersion", "group__datadistributor.html#ga1a218d52925e2578752324e10c694fc5", null ],
    [ "messages", "group__datadistributor.html#ga79272a9203712f9e78def2b8e79507d6", null ],
    [ "sdioFirstBlockDataPointer", "group__datadistributor.html#ga8f990c1d8e9c2c8baad74858789894a1", null ]
];