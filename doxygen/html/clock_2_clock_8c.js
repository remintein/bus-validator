var clock_2_clock_8c =
[
    [ "STACK_SIZE", "group___clock.html#ga6423a880df59733d2d9b509c7718d3a9", null ],
    [ "apiDnfccClockCompareWithNow", "group___clock.html#ga0ae8be040ce60a740db747339066b863", null ],
    [ "apiDnfccClockFetchDate", "group___clock.html#gaffb7500dbc66f74fccf87baebfd82dfe", null ],
    [ "apiDnfccClockFetchDateHuman", "group___clock.html#gaef02c78379558f5261a2663617c0c964", null ],
    [ "apiDnfccClockFetchDateTime", "group___clock.html#ga06f6eb9d06b5016df6ebfcad14909984", null ],
    [ "apiDnfccClockFetchDateTimeHuman", "group___clock.html#gac7aad56aa0b69c458646f8ca1cb9050c", null ],
    [ "apiDnfccClockFetchTime", "group___clock.html#ga1e786de7360d275514b851e3029c6989", null ],
    [ "apiDnfccClockFetchTimeHuman", "group___clock.html#ga21c4af6b6dc8281f4e327bd444fee967", null ],
    [ "intDnfccSendDate", "group___clock.html#ga0a027880eafbc3c2af9af717730ffada", null ],
    [ "intDnfccSendTime", "group___clock.html#gacefbf9191869b17d1862d1033a9f4349", null ],
    [ "intDnfccSetDate", "group___clock.html#gaa425ecd941fec3f4a6d5666593e90ad6", null ],
    [ "intDnfccSetTime", "group___clock.html#ga7f42215c6545882048bafcf8f52f074a", null ],
    [ "nfccClock", "group___clock.html#ga4bbf5ff2497bf60d0023f4e815f186ec", null ],
    [ "nfccClockInit", "group___clock.html#ga0a440e2dea8442ecc34a275d587edd7c", null ],
    [ "hrtc", "group___clock.html#gaa0c7fca836406ade332e1e3f1039d8ab", null ],
    [ "sDate", "group___clock.html#gaa805acf5185e2a95f250a2da91bf84e0", null ],
    [ "sTime", "group___clock.html#gafed4f29eda0f2d0900a0b347e29a8c91", null ]
];