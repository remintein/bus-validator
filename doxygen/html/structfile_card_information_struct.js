var structfile_card_information_struct =
[
    [ "cardCompany", "structfile_card_information_struct.html#aae2c7261700f91160288e9da5f55cc57", null ],
    [ "cardExpiredDate", "structfile_card_information_struct.html#adb0c838f0307b3a4de259042e6552c09", null ],
    [ "cardFormatAmount", "structfile_card_information_struct.html#a2618e13f8f4c4ced2879757681bde1a8", null ],
    [ "cardIssueDate", "structfile_card_information_struct.html#a4705b57190a7baae49dcb412257c05f0", null ],
    [ "cardVersion", "structfile_card_information_struct.html#a41779c4f26227a0d261e245e31a819ec", null ],
    [ "chipType", "structfile_card_information_struct.html#a18430169eb35cff1d9e4d15de18abf67", null ],
    [ "depositAmount", "structfile_card_information_struct.html#ab340d507b8e79daab2de981b9d4aa4b1", null ],
    [ "fileFormatVersion", "structfile_card_information_struct.html#a7d547a72e667264a2dc5e5f01b9d4b81", null ],
    [ "futureUse", "structfile_card_information_struct.html#a0814cc0b03cef0907abd39ba497f5ae0", null ],
    [ "isPerso", "structfile_card_information_struct.html#a87b60da5256b2e7c924d6be073031315", null ],
    [ "loginType", "structfile_card_information_struct.html#aba0020080ac2866edf937ebb84aa66b9", null ],
    [ "originalCard_ID", "structfile_card_information_struct.html#a897e66bed9f5d8a789e2c6cbe749c15b", null ],
    [ "recycleCard_ID", "structfile_card_information_struct.html#a2236bbb1180f79a2c1d549186aa28512", null ]
];