var printer_8c =
[
    [ "PRINTER_COMMUNICATION", "printer_8c.html#aaa7fa8e1d2f6b55fa155ac6403656bb2", null ],
    [ "STACK_SIZE", "group__printer.html#ga6423a880df59733d2d9b509c7718d3a9", null ],
    [ "nfccPrinter", "group__printer.html#ga8ae7ad73fbbdeb4936afc42e23169ddc", null ],
    [ "nfccPrinterInit", "group__printer.html#ga2228353bdbcf117095cd12f131b83793", null ],
    [ "huart_qrcodeprinter", "group__printer.html#ga659c04aa4b66f6258969d8209ae304ec", null ],
    [ "printerTransmitBuffer", "group__printer.html#gaa0303421b5e201fa076f2fffa4ddf076", null ],
    [ "sdioFirstBlockDataPointer", "group__printer.html#ga8f990c1d8e9c2c8baad74858789894a1", null ],
    [ "sdioPrinterDataPointer", "group__printer.html#gabbd92716cfb80be226b0df1a071acfc4", null ]
];