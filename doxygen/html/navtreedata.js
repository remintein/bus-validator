/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CnsSource", "index.html", [
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"__library_2flowcontroller_2flow_controller_8c.html",
"card_information__busproject_8h.html#ad5a66bd5a90c9d4e6dcd3bd6615e9fcd",
"group___flash_programmer.html#gaca5215978c8a8c9ae0c3fd39a01e9726",
"group__device_status.html",
"group__display.html#gafb90e909414ec8c05ef2a45f66843890",
"group__menu.html#ga42f59a5986a0097336a3ef65a105b0b6",
"group__ph___private.html#ga6af89a034f6c245e05bef2ae7a192261",
"group__phhal_hw___sam_a_v2___cmd___defines.html#ga3edd7eeae74287543488f02cb4dc6f2d",
"group__phhal_hw___sam_a_v2___cmd___defines.html#gab91456a48f85748acb0e0d7a82f82ced",
"group__sam.html#ga7e84ac177a2150d609e93d0af7013d6b",
"group__smartcardif.html#ga5f123f886cdeb0c3ec390e1bceec7960",
"group__sound.html#ga400925d582c3e8c7b709ebb6eff7c504",
"struct_desfire_handling_struct.html#a187d68528473a1561516483964011873",
"structaccess_point_definition.html#ac9cd2b1395e53572b4e4df45ef7d1dea"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';