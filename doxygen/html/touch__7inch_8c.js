var touch__7inch_8c =
[
    [ "SLAVE_ADDRESS", "group__touch.html#gae2f0ff6faf548539a21b93a034e278e8", null ],
    [ "STACK_SIZE", "group__touch.html#ga6423a880df59733d2d9b509c7718d3a9", null ],
    [ "I2C2_ER_IRQHandler", "group__touch.html#gaecd40b8012604ac4236bda3f65857c37", null ],
    [ "I2C2_EV_IRQHandler", "group__touch.html#gabdb05db0781544b33e806a12940d062c", null ],
    [ "nfccTouch", "group__touch.html#ga7ce6888cf7f5347e06148b7eb2038084", null ],
    [ "nfccTouchInit", "group__touch.html#gab223a53fbc9fafa947a1d3c9ae23e1ec", null ],
    [ "i2cTouch", "group__touch.html#ga1b485146ee911009db161addc900bdff", null ]
];