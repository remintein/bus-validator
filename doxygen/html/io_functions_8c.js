var io_functions_8c =
[
    [ "Display_CmdWrite", "group__hardware.html#gaacbedc2b20fbbb9550ddc653763b3982", null ],
    [ "Display_DataRead", "group__hardware.html#gac5a56da77eb616314da35eb194c90446", null ],
    [ "Display_DataWrite", "group__hardware.html#ga9e92bf4804f570900f2073a318071479", null ],
    [ "Display_doWaitForReady", "group__hardware.html#ga8fc0d31664f5caae2a79beeeac646649", null ],
    [ "Display_perFormHwReset", "group__hardware.html#ga97a039260abd47974ce0717df18597ed", null ],
    [ "Display_StatusRead", "group__hardware.html#gac4964e9b714cf47cf26b7ab8ddd4feba", null ],
    [ "Display_WriteDir", "group__hardware.html#ga8b1cc0b287f8249d646d1a1f11cb25d4", null ],
    [ "DisplayTransmitReceive", "group__hardware.html#ga3fd09206e1b17811f6fa6f2fe60eb1e8", null ],
    [ "DisplayTransmitReceive3", "group__hardware.html#ga70cd1dbf1f59eeac3cc38bd3b9b9b957", null ],
    [ "displayHspi", "group__display.html#ga8e49a06ddf90e54412ab16fdd681bd63", null ]
];