var struct_sdio_block_info =
[
    [ "data", "struct_sdio_block_info.html#a87d5d7ef519e4a33983fffc78ed541d2", null ],
    [ "file", "struct_sdio_block_info.html#acf2a9aba59db2cd81988831274f500a0", null ],
    [ "readErrors", "struct_sdio_block_info.html#a51e120026bd2eacddef67d3ea8aff438", null ],
    [ "readStatus", "struct_sdio_block_info.html#a72aacb8f464d8a3ba62a3171e664e546", null ],
    [ "sectorOffsetInFile", "struct_sdio_block_info.html#a2d59b6ea3d0acec4902804b90db05298", null ],
    [ "writeErrors", "struct_sdio_block_info.html#ad531e2f3a7649aaa6f4c2604aa606234", null ],
    [ "writeStatus", "struct_sdio_block_info.html#a9e32a38e2cf5b7c5add535bfcf6ff896", null ]
];