var group___serial_communicator =
[
    [ "SerialControllerFlow", "struct_serial_controller_flow.html", [
      [ "data", "struct_serial_controller_flow.html#aefe270abe7724cfccfa9d1178badf9c7", null ],
      [ "intData", "struct_serial_controller_flow.html#afef9300451befc402676c6c2ccce8a64", null ],
      [ "nextStep", "struct_serial_controller_flow.html#ac87531f72ee3c1f760e9fca97ad0d873", null ],
      [ "stepToRun", "struct_serial_controller_flow.html#a922b11290b630b02d1e90ba925ec53aa", null ],
      [ "thisStep", "struct_serial_controller_flow.html#a00054c7943f60f343afa2e3651d866b7", null ]
    ] ],
    [ "SerialCommunicatorHandler_type", "struct_serial_communicator_handler__type.html", [
      [ "callbackFunction", "struct_serial_communicator_handler__type.html#a36fa8183aed7edbd1220b9afe5c379c3", null ],
      [ "currentStep", "struct_serial_communicator_handler__type.html#a173c3e51d84299e222ad8385f609ea48", null ],
      [ "engine", "struct_serial_communicator_handler__type.html#a4244f1ee9d6893879493a52e8090922a", null ],
      [ "flow", "struct_serial_communicator_handler__type.html#ab3864c80dca6e912eda9ebdcb2c0d008", null ],
      [ "flowSize", "struct_serial_communicator_handler__type.html#a1c97d04730b6326bc244d28ee8c61e28", null ],
      [ "jsonEndPoint", "struct_serial_communicator_handler__type.html#a3a758d95f649e747c6ce8666e7b13d24", null ],
      [ "jsonReceiveData", "struct_serial_communicator_handler__type.html#a0d9f2890ece373d862faf7403ce67ff4", null ],
      [ "jsonRequestData", "struct_serial_communicator_handler__type.html#a9338ead4eed767ed08bcac31a84cc19a", null ],
      [ "uartNr", "struct_serial_communicator_handler__type.html#ae1794bfa7311c3b838a4bfd16c0a07af", null ]
    ] ],
    [ "SerialControllerCallback", "group___serial_communicator.html#gaa4eed0d8d3a889b2edcb3fe8e025d167", null ],
    [ "SerialControllerFlow_type", "group___serial_communicator.html#ga9d3cf13f4d48d5487f8e47d4c7aa98e4", null ],
    [ "action", "group___serial_communicator.html#ga2f4ab7bf743142dae2e459aa18f9f1d4", [
      [ "VERIFY_DATA", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4ad9b261ab2f24847e55b5b7ff3e0835f0", null ],
      [ "VERIFY_DATA_WITH_CALLBACK", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a6511943787a19a4aa37cea175b4eadd8", null ],
      [ "VERIFY_DATA_AND_PROCESS", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4af72fc490032738b2dd3a417fc4189f1e", null ],
      [ "VERIFY_DATA_AND_WAIT_FOR_RETURN", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4adc679c59bf6397cfeb0723529b11a8d2", null ],
      [ "EXECUTE_CALLBACK_WITH_PARAMETER", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a9f6a0d3ad3e9378cf73c4ad3294f29af", null ],
      [ "SEND_LITERAL_TEXT", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4aa6df4fdb31a9bf2a2e30e692a9240fc8", null ],
      [ "SEND_PROCESSED_TEXT", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a5368f6d7ace808dbbbf2286c0920d7ed", null ],
      [ "VALIDATE_CALLBACK", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a45d3b1553e711606cc7641fc6d10148e", null ],
      [ "VERIFY_TIMEOUT_HAS_PASSED", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4aa920aa082b9b6fbbe90babde249ebd0b", null ]
    ] ],
    [ "apiDnfccInitializeSerialEngineStruct", "group___serial_communicator.html#ga425a7a81ace2a6b86f8249f98ad72f58", null ],
    [ "apiDnfccSerialCommunicationAdvance", "group___serial_communicator.html#ga487df97f91fd7237a9fa860bf6d202cb", null ],
    [ "apiDnfccSerialCommunicationInitialize", "group___serial_communicator.html#ga495fce2a86356f199681cce52e78cf7e", null ],
    [ "apiDnfccSerialCommunicationVerifyStringPresentInReceiveBuffer", "group___serial_communicator.html#ga6d3be14ce291aee48a45ea5992923cbd", null ],
    [ "apiDnfccSerialEngineStartListening", "group___serial_communicator.html#ga2ff25a124761d35fd63bab7379068fb5", null ],
    [ "apiDnfccSerialEngineTransmit", "group___serial_communicator.html#gab795ad0fcc0bd277821ec60f7a71afee", null ],
    [ "apiDnfccSerialEngineTransmitBinary", "group___serial_communicator.html#ga3faed0242c4953c27c34673ae87461fb", null ],
    [ "apiDnfccSerialEngineVerifyStringPresent", "group___serial_communicator.html#ga90f2d85ecf303b7104dbac4510664b83", null ],
    [ "intDnfccSerialEngineHandleInterrupt", "group___serial_communicator.html#gaa1ad97088526b5106c7df546eaa3b236", null ]
];