var otg_2otg_8c =
[
    [ "DELAY", "otg_2otg_8c.html#a62249e384b997229a3e2ae74ade334e2", null ],
    [ "OTG_COMMUNICATION", "otg_2otg_8c.html#aa127ffdaacecd109fe0a9f0690029c6e", null ],
    [ "STACK_SIZE", "otg_2otg_8c.html#a6423a880df59733d2d9b509c7718d3a9", null ],
    [ "CDC_Transmit_FS", "group__otg.html#ga5137d6201dbdef2bf351c5b4941c24f4", null ],
    [ "MX_USB_DEVICE_Init", "group__otg.html#gaaa861dc060757d1d428ebceff9dc085b", null ],
    [ "nfccOtg", "group__otg.html#ga27502f6d1651f02a6f7c79a580aa2e1a", null ],
    [ "nfccOtgInit", "group__otg.html#ga1c693b3782e7e88666186dddc77709bb", null ],
    [ "otgReceiveBuffer", "group__otg.html#ga87b135b6f446254f9f80e0097f7834a1", null ]
];