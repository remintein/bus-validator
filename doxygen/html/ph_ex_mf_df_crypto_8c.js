var ph_ex_mf_df_crypto_8c =
[
    [ "PH_EXMFCRYPTO_MIFAREDESFIRE_KEYSETTINGS1", "ph_ex_mf_df_crypto_8c.html#a7a64487369eef5ecc7a1bfe9f4a30967", null ],
    [ "PH_EXMFCRYPTO_MIFAREDESFIRE_KEYSETTINGS2", "ph_ex_mf_df_crypto_8c.html#af472838078ecdd4887d3ef885861e1af", null ],
    [ "PH_EXMFCRYPTO_MIFAREDESFIRE_NUMBER_OF_KEYENTRIES", "ph_ex_mf_df_crypto_8c.html#ac23489ddf0f6062d0488b96d01ad2466", null ],
    [ "PH_EXMFCRYPTO_MIFAREDESFIRE_NUMBER_OF_KEYVERSIONPAIRS", "ph_ex_mf_df_crypto_8c.html#a02d20d3982b0d723a711515407da73a3", null ],
    [ "PH_EXMFCRYPTO_MIFAREDESFIRE_NUMBER_OF_KUCENTRIES", "ph_ex_mf_df_crypto_8c.html#a218fedf26266e04b1bf2fabd7e216a76", null ],
    [ "phExMfCrypto_Init", "ph_ex_mf_df_crypto_8c.html#aaf93c29c6ca432fc42148b58d0e0ac37", null ],
    [ "phExMfCrypto_KeySet", "ph_ex_mf_df_crypto_8c.html#ac573d2d3e7c947c6d5eaeb8a66c4a53c", null ],
    [ "phExMfDfCrypto_Auth_keyCreation", "ph_ex_mf_df_crypto_8c.html#a095e6975cba9ce6dd47d1da51ed1e003", null ],
    [ "gkphExMfCrypto_pNewKey", "ph_ex_mf_df_crypto_8c.html#a530fc3153d2170ad8b4af9bfb8651c5e", null ],
    [ "palMFDF", "ph_ex_mf_df_crypto_8c.html#af64882b8e361f95a88bf8428aa00c1de", null ],
    [ "pCryptoRng", "ph_ex_mf_df_crypto_8c.html#ae82ec64185f086fa9bd319e0df15fb0d", null ],
    [ "pCryptoSym", "ph_ex_mf_df_crypto_8c.html#a42dc0e4505a6b677f1f8523784a840c8", null ],
    [ "pKeyEntries", "ph_ex_mf_df_crypto_8c.html#a378c30b280e49bf739ae79bc1ce4fa8a", null ],
    [ "pKeyStore", "ph_ex_mf_df_crypto_8c.html#a01dc365495023d9eddc0834f7ae7ac7f", null ],
    [ "pKeyVersionPairs", "ph_ex_mf_df_crypto_8c.html#a770fd15da17d1cc378eb156bf2088350", null ],
    [ "pKUCEntries", "ph_ex_mf_df_crypto_8c.html#a4517c5171c4e8fd4e9f8c4f87c6ec284", null ],
    [ "ppalMifare", "ph_ex_mf_df_crypto_8c.html#aac2468791f10af06bb8c1c28e980246b", null ],
    [ "sCryptoSymRnd", "ph_ex_mf_df_crypto_8c.html#a1ce75b83f338a61398a3854bd25ce96a", null ]
];