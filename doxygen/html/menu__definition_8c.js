var menu__definition_8c =
[
    [ "apiDnfccMenuInitializeAllMenus", "group__menu.html#ga2a0d584cc2175f54118292b5ab4a2eb3", null ],
    [ "apiDnfccMenuInitializeAllMenusBusConsole", "group__menu.html#gaf79a4a55439d324ae50cba8914866597", null ],
    [ "apiDnfccMenuInitializeAllMenusHandheld", "group__menu.html#gae3fb6fa216a3fdbf81883a60a314b73c", null ],
    [ "apiDnfccMenuInitializeAllMenusTopup", "group__menu.html#ga7bfea679b6457d7b5949b685042ae0ee", null ],
    [ "apiDnfccMenuInitializeAllMenusValidator", "group__menu.html#ga0ee379df4738e1659447a90492963fc5", null ],
    [ "apiDnfccMenuInitializeOneMenu", "group__menu.html#ga9d1b9231878ca930e5ed1b4420069e73", null ]
];