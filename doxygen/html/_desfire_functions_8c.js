var _desfire_functions_8c =
[
    [ "DIV_INPUT", "_desfire_functions_8c.html#adb8b3c5c877cee2488a42559e15e32d9", null ],
    [ "DIV_LENGTH", "_desfire_functions_8c.html#abb27e285d11e753e708453f015465c3a", null ],
    [ "DIV_MODE", "_desfire_functions_8c.html#ad0e9b8b25ea2b37c879fa497cf979c7c", null ],
    [ "PH_CRYPTOSYM_SW_NUM_AES_ROUNDS_128", "group__smartcardif.html#ga676723dc0cc7ba27bd40ba7e3e97a19a", null ],
    [ "PH_CRYPTOSYM_SW_NUM_AES_ROUNDS_192", "group__smartcardif.html#ga0afc99e09c3d40845a14703feaf6df94", null ],
    [ "PH_CRYPTOSYM_SW_NUM_AES_ROUNDS_256", "group__smartcardif.html#ga615c6bea6cfafe5e804a5e3baf6dfd80", null ],
    [ "desfireAuthenticateAES", "group__smartcardif.html#ga3471dc3ba767e63392edf43d41c2d933", null ],
    [ "DnfccSamDesfireAuthenticatePicc", "group__smartcardif.html#ga0cb219d4131309013c2bec8d1ef7cd1d", null ],
    [ "apiInterface", "group__smartcardif.html#gaf9de8835f55a9729d94f095417567a3d", null ]
];