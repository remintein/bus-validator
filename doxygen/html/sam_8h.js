var sam_8h =
[
    [ "SamEngineStruct", "struct_sam_engine_struct.html", "struct_sam_engine_struct" ],
    [ "ENGINE_RXBUFFER_SIZE", "sam_8h.html#a81525200f3e9e59ccfdf29e2b1faee5e", null ],
    [ "nfccSam", "sam_8h.html#a50b108dbdfbe4130a46fd237a92dd695", null ],
    [ "nfccSamInit", "sam_8h.html#a7de961a43807a01dbb4db75651e5c5ca", null ],
    [ "SamInitializeCommunication", "sam_8h.html#a32aa712457db883f48a30d3117b2db7b", null ],
    [ "SamReceiveEnvelope", "sam_8h.html#ae84225be90c118dc5a4c7f56b4a1ffe9", null ],
    [ "SamReceiveRaw", "sam_8h.html#a33c9da051ec3821f2ad32b4ecdba5767", null ],
    [ "SamResetReceiving", "sam_8h.html#ae1df43dab4d2abe93a5e307a64af443c", null ],
    [ "SamTransmitCommand", "sam_8h.html#af91c6da6ab1b1ac471e3f839ba9f90e7", null ]
];