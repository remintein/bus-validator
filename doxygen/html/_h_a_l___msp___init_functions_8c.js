var _h_a_l___msp___init_functions_8c =
[
    [ "HAL_SPI_MspInit", "group__hardware.html#ga17f583be14b22caffa6c4e56dcd035ef", null ],
    [ "HAL_UART_MspInit", "group__hardware.html#ga0e553b32211877322f949b14801bbfa7", null ],
    [ "UART_Display_Gpio", "group__hardware.html#gab6010c186c69321429d1fb9f7e1d39f9", null ],
    [ "UART_GSM_MspInit", "group__hardware.html#gae1c7925768060e1d9f414c0b86050844", null ],
    [ "UART_QRCODEPRINTER_MspInit", "group__hardware.html#ga600bb2c3d1a643386fdeb67798251646", null ],
    [ "UART_WIFI_MspInit", "group__hardware.html#gae91d6dbe75422df2047d855309e7d496", null ]
];