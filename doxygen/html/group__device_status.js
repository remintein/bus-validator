var group__device_status =
[
    [ "initDeviceStatus", "group__device_status.html#ga34f88750856a3c470d56a7a8ae841abb", null ],
    [ "setTaskIsSystemReady", "group__device_status.html#ga229df4e9c8f89e4352aeaad42b1b62e6", null ],
    [ "setWaitForSystemReadyThisTask", "group__device_status.html#ga89dcd050185fc142fc4609bf3e3378dd", null ],
    [ "waitForSystemReady", "group__device_status.html#ga07917a133e3665aebe527ff017f9f8b6", null ],
    [ "defaultFileVersion", "group__device_status.html#ga1a218d52925e2578752324e10c694fc5", null ]
];