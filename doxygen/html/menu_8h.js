var menu_8h =
[
    [ "button", "structbutton.html", "structbutton" ],
    [ "menuDefinition", "structmenu_definition.html", "structmenu_definition" ],
    [ "buttonState", "menu_8h.html#ad9ff7db4c5690b4aa56ccef90e30b8bf", [
      [ "STANDBY", "menu_8h.html#ad9ff7db4c5690b4aa56ccef90e30b8bfae4634ae4352b512b38c5da9dc1610ca6", null ],
      [ "ACTIVE", "menu_8h.html#ad9ff7db4c5690b4aa56ccef90e30b8bfa33cf1d8ef1d06ee698a7fabf40eb3a7f", null ],
      [ "DISABLED", "menu_8h.html#ad9ff7db4c5690b4aa56ccef90e30b8bfa40f39385238042f6ec0cbac821a19fc4", null ],
      [ "HOVER", "menu_8h.html#ad9ff7db4c5690b4aa56ccef90e30b8bfa8df21af5ed7da4b83ff86f107b52ef75", null ],
      [ "BUTTON_RED", "menu_8h.html#ad9ff7db4c5690b4aa56ccef90e30b8bfa0e8d5df6256ff74a4711927d0a961a93", null ],
      [ "BUTTON_GREEN", "menu_8h.html#ad9ff7db4c5690b4aa56ccef90e30b8bfad441eb9b7f6f2fb9d48cf51fab08cc16", null ]
    ] ],
    [ "commandType", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662", [
      [ "COMMAND_0", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662acab00b2aa075ddb03d1e4b5e878b5820", null ],
      [ "COMMAND_1", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a4c9cbd6ee400df299dbb8250e73f9d37", null ],
      [ "COMMAND_2", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662addfc51e92ea0f883a33102b256b0bd94", null ],
      [ "COMMAND_3", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a3589be54b831d054d7503c9d83ad0855", null ],
      [ "COMMAND_4", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a5ef2335fa675cd4f17eff80b374ee8d9", null ],
      [ "COMMAND_5", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662ab561ead7bedd1efc1e77a20f37dab3d0", null ],
      [ "COMMAND_6", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662abe6fd85d07582e2f00e171774ba0beb4", null ],
      [ "COMMAND_7", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662ae53b5e860b1c32490e3574f7848f72b7", null ],
      [ "COMMAND_8", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662aa88ef590367d8e64cd25efbc24167916", null ],
      [ "COMMAND_9", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a5fff3acbb7ca935b85914404d34589f5", null ],
      [ "COMMAND_BACK", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a21e33d97897d1919f9c4e5dbe46019f3", null ],
      [ "COMMAND_MIN", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a4d83ad7c337d4792a67d516e2c9f6aa9", null ],
      [ "COMMAND_D", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a8cc8a4e6aae574e402f5789ddea99fcf", null ],
      [ "COMMAND_CANCEL", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662aa025beab47719399a05bfe7598570035", null ],
      [ "COMMAND_MENU_MAIN", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662ad92f60265cf3bf73f3615d4cb8c9bc3c", null ],
      [ "COMMAND_MENU_ROUTESELECT", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a3f72a71efd856ea3962a8c1eb6a1d292", null ],
      [ "COMMAND_MENU_ROUTE_ENTER", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662af9bd82c561a94543972ac3de4b3e0ee2", null ],
      [ "COMMAND_MENU_SERVICE_ENTER", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662af32f90f18d0d1bf3d367d7653c2fa4d4", null ],
      [ "COMMAND_MENU_STARTTRIP", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a08053d987f2200cb6139cdc8bec96118", null ],
      [ "COMMAND_MENU_ENDTRIP", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a2b4e15d978a93db9cfc906d5e7ded03f", null ],
      [ "COMMAND_VOID", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662aac92d84d21662436042342925f51de2f", null ],
      [ "COMMAND_TICKET_1", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662acad0e9f11fe0513b4825c975a90a9186", null ],
      [ "COMMAND_TICKET_2", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a8b1c4292e38696a63d30861d947cd03b", null ],
      [ "COMMAND_MENU_TOPUP_SELECT", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a58f239bb7ed1a07f12ea962efb7d8e4c", null ],
      [ "COMMAND_MENU_FORMATTER_START", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a739974ed3de42cb6de685d8d39cd1dde", null ],
      [ "COMMAND_MENU_FORMATTER_STOP", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a4177ee5ad81d20cf7ce6237cceb1a3c2", null ],
      [ "COMMAND_MENU_LOGOUT", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a254549b971c493d2af153da3955941a8", null ],
      [ "COMMAND_MENU_TOPUP1", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a94eab160d5d91a72add47208c09a5ac1", null ],
      [ "COMMAND_MENU_TOPUP2", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a5b72e2d47a5d1100b536346fd6afee53", null ],
      [ "COMMAND_MENU_TOPUP3", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a647dc6564d84fa80c9eec316255a131d", null ],
      [ "COMMAND_MENU_TOPUP4", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a5b6902d56e2283fef03de9e3ca6a1bf8", null ],
      [ "COMMAND_MENU_TOPUP_CANCEL", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a6758a5ef061b55ce14a6956e5f9abd62", null ],
      [ "COMMAND_MENU_TOPUP_HELP", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662ac3370eec94aeeb5ccaa75abf50a440ae", null ],
      [ "COMMAND_MENU_TOPUP_CONFIRM", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a18094879b90df74df340b82d6454d1bc", null ],
      [ "COMMAND_MENU_HANDHELD_CANCEL", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662addbfb26f3391f723459ffa60acb5a2e9", null ],
      [ "COMMAND_MENU_HANDHELD_PRINT_BILL", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662aab8bb3fb1ed68aef99805806cdad3e9b", null ],
      [ "COMMAND_MENU_HANDHELD_CORRECT", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a8d3855db95346cc2ae7182b6bbadb9ba", null ],
      [ "COMMAND_MENU_HANDHELD_PENALTY", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a33480709e9261afcc5a21097708fd0b3", null ],
      [ "COMMAND_MENU_HANDHELD_CARD", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662aae0a1f9f8ba7f409a542f543d04f72e9", null ],
      [ "COMMAND_MENU_HANDHELD_CASH", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662a069c05ad5bbbeecb49c18e29191c39a6", null ],
      [ "COMMAND_MENU_HANDHELD_HELP", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662af0f084938e4d09741137847bbcdb4190", null ],
      [ "COMMAND_ENDOFLIST", "menu_8h.html#a088611a7119ef2d3d69edf76f2f50662ad492685349503916cb5e98b59773328e", null ]
    ] ],
    [ "nfccMenu", "menu_8h.html#a7d3bd06acd0c95577146ec6c28eff06b", null ],
    [ "nfccMenuInit", "menu_8h.html#a5256dadb27bd4c1808abe66926e5b4ac", null ]
];