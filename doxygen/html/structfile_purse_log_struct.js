var structfile_purse_log_struct =
[
    [ "fileFormatVersion", "structfile_purse_log_struct.html#a9d545bf667dfce6de617c4df13d1ac0d", null ],
    [ "futureUse", "structfile_purse_log_struct.html#ae283f099eb73b8c71f2cc8b462429458", null ],
    [ "purseBeforeCount", "structfile_purse_log_struct.html#a95964c664113f3abc94cbbbed4b9c7fd", null ],
    [ "purseSAM_ID", "structfile_purse_log_struct.html#acd8463cc152f92eed3737a1db5024d90", null ],
    [ "purseTransactionDateTime", "structfile_purse_log_struct.html#a1eb4dc7a1145ebde6462f1c4f3708c43", null ],
    [ "purseTransactionsSubType", "structfile_purse_log_struct.html#a636535f00f201738b5131f4695468729", null ],
    [ "purseUsedCount", "structfile_purse_log_struct.html#a112c9ed812825420836930b9a6f978d6", null ],
    [ "purseValue", "structfile_purse_log_struct.html#a3a5688318242f890f2fab063a7251159", null ]
];