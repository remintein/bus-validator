var hardware_2_io_functions_8c =
[
    [ "Display_CmdWrite", "hardware_2_io_functions_8c.html#aacbedc2b20fbbb9550ddc653763b3982", null ],
    [ "Display_DataRead", "hardware_2_io_functions_8c.html#ac5a56da77eb616314da35eb194c90446", null ],
    [ "Display_DataWrite", "hardware_2_io_functions_8c.html#a9e92bf4804f570900f2073a318071479", null ],
    [ "Display_doWaitForReady", "hardware_2_io_functions_8c.html#a8fc0d31664f5caae2a79beeeac646649", null ],
    [ "Display_perFormHwReset", "hardware_2_io_functions_8c.html#a97a039260abd47974ce0717df18597ed", null ],
    [ "Display_StatusRead", "hardware_2_io_functions_8c.html#ac4964e9b714cf47cf26b7ab8ddd4feba", null ],
    [ "DisplayTransmitReceive", "hardware_2_io_functions_8c.html#a3fd09206e1b17811f6fa6f2fe60eb1e8", null ],
    [ "DisplayTransmitReceive3", "hardware_2_io_functions_8c.html#a70cd1dbf1f59eeac3cc38bd3b9b9b957", null ],
    [ "displayHspi", "hardware_2_io_functions_8c.html#a8e49a06ddf90e54412ab16fdd681bd63", null ]
];