var global__functions_8h =
[
    [ "apiDnfccClockCompareWithNow", "group___clock.html#ga0ae8be040ce60a740db747339066b863", null ],
    [ "apiDnfccClockFetchDate", "group___clock.html#gaffb7500dbc66f74fccf87baebfd82dfe", null ],
    [ "apiDnfccClockFetchDateTime", "group___clock.html#ga06f6eb9d06b5016df6ebfcad14909984", null ],
    [ "apiDnfccClockFetchTime", "group___clock.html#ga1e786de7360d275514b851e3029c6989", null ],
    [ "apiDnfccFillRandomBytes", "group___random.html#gadef70ad75365c6c21f82b763e2f04012", null ],
    [ "apiDnfccGetRandomUint16", "group___random.html#ga3c06b6ffef85ca46a3c96a9720b5b918", null ],
    [ "apiDnfccGetRandomUint32", "group___random.html#ga54322aa52d964b412400095fad381773", null ],
    [ "apiDnfccGetRandomUint8", "group___random.html#ga0632e8c37519367207a814dcc8887183", null ]
];