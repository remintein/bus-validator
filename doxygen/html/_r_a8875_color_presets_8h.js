var _r_a8875_color_presets_8h =
[
    [ "RA8875_BLACK", "_r_a8875_color_presets_8h.html#a937e10e550c933a8bfb74ae4491bcd99", null ],
    [ "RA8875_BLUE", "_r_a8875_color_presets_8h.html#aa5efb0212e002298145fd6197c0e1aa2", null ],
    [ "RA8875_CYAN", "_r_a8875_color_presets_8h.html#a33931ef8d7df977acd2f93b84d1fa5d4", null ],
    [ "RA8875_DARK_BLUE", "_r_a8875_color_presets_8h.html#ab79663774268602a7105d46abcd8d2fa", null ],
    [ "RA8875_DARK_GREEN", "_r_a8875_color_presets_8h.html#a252fc2d9f743cc596dcc4a05e7f614bc", null ],
    [ "RA8875_DARK_GREY", "_r_a8875_color_presets_8h.html#add10d34f040c6d879ed5d79e102c6be3", null ],
    [ "RA8875_DARK_ORANGE", "_r_a8875_color_presets_8h.html#adc9a05e660ba3bf6015db9d473fb4c5d", null ],
    [ "RA8875_GRANITE", "_r_a8875_color_presets_8h.html#af682cee1ff222b45f44e08ffb56fe134", null ],
    [ "RA8875_GRAYSCALE", "_r_a8875_color_presets_8h.html#a104f89f2249aa6f5cf64779c9ca5abdd", null ],
    [ "RA8875_GREEN", "_r_a8875_color_presets_8h.html#a02c8ab8f790c965b4461cfe00b7cfc38", null ],
    [ "RA8875_GREY", "_r_a8875_color_presets_8h.html#adff68c0aac368c4199b15a40d3e1d26f", null ],
    [ "RA8875_LIGHT_GREY", "_r_a8875_color_presets_8h.html#aba4de892432a770e306c555638bebf7a", null ],
    [ "RA8875_LIGHT_ORANGE", "_r_a8875_color_presets_8h.html#aa6c0d015a6a53f6164eff3175301d148", null ],
    [ "RA8875_MAGENTA", "_r_a8875_color_presets_8h.html#a2a9988a6c537f48f008a12a1868cd36b", null ],
    [ "RA8875_PINK", "_r_a8875_color_presets_8h.html#ae6e2c2f328702620476b732a78efc5dd", null ],
    [ "RA8875_PURPLE", "_r_a8875_color_presets_8h.html#acfa5b8deb13f595cb20877d595f71d7d", null ],
    [ "RA8875_RED", "_r_a8875_color_presets_8h.html#aa0a12ed58e64d7895e1b667ad139ff69", null ],
    [ "RA8875_WHITE", "_r_a8875_color_presets_8h.html#a9af696102c07fd5028b28e67439c46f3", null ],
    [ "RA8875_YELLOW", "_r_a8875_color_presets_8h.html#a28675b330b35528004804fd2c2562df4", null ]
];