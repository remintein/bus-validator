var struct_sam_engine_struct =
[
    [ "curReceiveByte", "struct_sam_engine_struct.html#a8e2e07fded7ac9246f2c42f17194b222", null ],
    [ "lastCommunication", "struct_sam_engine_struct.html#a8326c5dc3debe2eeefdbe635e66364ed", null ],
    [ "rxBuffer", "struct_sam_engine_struct.html#a1480c48967ac27271aca5d7c033e14b1", null ],
    [ "rxByte", "struct_sam_engine_struct.html#ae739b6e31a0b9c5d9a3e7519cbbcacff", null ],
    [ "rxEndPointer", "struct_sam_engine_struct.html#a2857af4bdba54a5df8c64753b3228528", null ],
    [ "rxProcessPointer", "struct_sam_engine_struct.html#a55e102b60c0487ff80eb1cbb5c7dff50", null ],
    [ "samSemaphore", "struct_sam_engine_struct.html#ac6fa95839e08683f2c521c95138bf566", null ],
    [ "txBuffer", "struct_sam_engine_struct.html#aff2fdda6b9ce3bfd0eb55f28ae3c0344", null ],
    [ "txEndPointer", "struct_sam_engine_struct.html#a82083e87048cd480689ef79a82283911", null ],
    [ "txProcessPointer", "struct_sam_engine_struct.html#a8d61f29671802fda9765cfd08a53d426", null ]
];