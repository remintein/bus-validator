var _file_updater_8c =
[
    [ "FileUpdateInfo", "group__sdio.html#gac7e1e372f4e7d8c423275f8bd6466c0e", null ],
    [ "intDnfccGetDataForFile", "group__sdio.html#ga0c0a9872b75a4437dc17fd3d0d328fe5", null ],
    [ "intDnfccInitializeFile", "group__sdio.html#ga22e5c02ed35ae5985cd017df60f0efbd", null ],
    [ "intDnfccInitializeFileTransfer", "group__sdio.html#gacec96257b975b651304740d866766251", null ],
    [ "intDnfccRequestNextBlock", "group__sdio.html#ga30d7a3402759c1aae81036a22bd40157", null ],
    [ "intDnfccStoreFileAsActive", "group__sdio.html#ga40695ed1f7088091c0c617a32c7bcc03", null ],
    [ "fileUpdateInfo", "group__sdio.html#ga29bd9bdcd66cdb25d25ec55abe8bfec3", null ],
    [ "sdioBlacklistDataPointer", "group__sdio.html#ga012e1884e4580bad36da72921f1efbee", null ],
    [ "sdioFareDataPointer", "group__sdio.html#ga8e0223aaaee562b9f8e04e83d80f9d82", null ],
    [ "sdioPrinterDataPointer", "group__sdio.html#gabbd92716cfb80be226b0df1a071acfc4", null ],
    [ "sdioWifiDataPointer", "group__sdio.html#ga2a614cd3ab26ca77759e4ab6258fdc7e", null ]
];