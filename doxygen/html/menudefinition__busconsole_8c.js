var menudefinition__busconsole_8c =
[
    [ "apiDnfccMenuInitializeAllMenusBusConsole", "group__menu.html#gaf79a4a55439d324ae50cba8914866597", null ],
    [ "busline", "group__menu.html#gae676a2f52d9234ba0a7ad9793f4c8f5f", null ],
    [ "buttons_busconsole_main", "group__menu.html#gab51eaca3e65dbeaedd109604ddac15e1", null ],
    [ "buttons_driveoperations", "group__menu.html#ga63c6a3a33b303cef3ffe5289a30ab416", null ],
    [ "buttons_routeselect", "group__menu.html#ga42e5caa6d26d89401a11865c6a08783d", null ],
    [ "buttons_serviceselect", "group__menu.html#gabdfcae5588616d9ada0bf9dd597cf197", null ],
    [ "menu_busconsole_main", "group__menu.html#ga4d46cc8ebe8b2338aeb05cff43e4ed68", null ],
    [ "menu_driveoperations", "group__menu.html#ga1dc11458c47812016a40adbe4ddf6146", null ],
    [ "menu_routeselect", "group__menu.html#ga2798a15ccabed7bae63fadf20e302b7e", null ],
    [ "menu_serviceselect", "group__menu.html#ga5846cf511169c9e78cbe771f9d68ed10", null ],
    [ "payInfo", "group__menu.html#ga64798be63afc51503c4032a58e817810", null ],
    [ "payinfoSubtitle", "group__menu.html#gac4cc28e30a0fbb44cbff4dee0adb7750", null ],
    [ "serviceline", "group__menu.html#ga7b78eccc0ea5085f04e43fb3ac07aa8e", null ],
    [ "validatorInfo", "group__menu.html#ga7d27b8bba046c424673955ae285993d9", null ],
    [ "validatorInfoSubtitle", "group__menu.html#gabdeb25cbf84e67575d909aff8eb950f6", null ],
    [ "valueLabel", "group__menu.html#gaa5ed738a6f9fd02c976449d510b28b1a", null ]
];