var blacklist_verify_8c =
[
    [ "apiDnfccVerifyCardIsNotInBlacklist", "group___blacklist.html#ga5574133f72398b7fcbe4a3d8a4c886f3", null ],
    [ "intDnfccProcessDataBlock", "blacklist_verify_8c.html#ad3d50bc293958fcc88a2184fd8165542", null ],
    [ "intDnfccProcessIndexBlock", "blacklist_verify_8c.html#ab8cd126b76e91d40cb348f84e8e9858f", null ],
    [ "sdioBlacklistDataReadPointer", "group__sdio.html#gaf96010b211760f2eeac45c45ddbedc43", null ],
    [ "sdioFirstBlockDataPointer", "group__sdio.html#ga8f990c1d8e9c2c8baad74858789894a1", null ]
];