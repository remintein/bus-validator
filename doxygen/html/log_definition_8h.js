var log_definition_8h =
[
    [ "COLLISION_LOG", "log_definition_8h.html#aad670fe3b07bc522562b364cd0f1b726", null ],
    [ "LOGGING_ON_CAN", "log_definition_8h.html#ac11b98b9549634c3ff26736afa642ea5", null ],
    [ "OTG_LOG", "log_definition_8h.html#a0ceb7d08a9e682c7be04bdd55aee86b2", null ],
    [ "QRCODE_LOG", "log_definition_8h.html#a82a431834c33814f009eac34aae674ec", null ],
    [ "RC663_LOG", "log_definition_8h.html#ac12591c2160567901cbb99a725ad83a9", null ],
    [ "RC663_LOWLEVEL_LOG", "log_definition_8h.html#a72eb630e3d359bc1f1dbb77b02fb75d7", null ],
    [ "RC663_TIMING_LOG", "log_definition_8h.html#a70759e2446e0ec13ed1e1b814a4820f1", null ],
    [ "RTC_LOG", "log_definition_8h.html#a98df5f5dc2f1e4ef98abf87d5d14cc31", null ],
    [ "SAM_LOG", "log_definition_8h.html#a6de646777d4679e652ffdfa6ba737cef", null ],
    [ "SDIO_LOG", "log_definition_8h.html#acdc0e8cf393b734c921151a826d35966", null ],
    [ "SERIAL_GSM_LOG", "log_definition_8h.html#a858334b604217e4f965aa0f169a15992", null ],
    [ "SERIAL_PRINTER_LOG", "log_definition_8h.html#a2dc213e1b3a4099e036c84ffc9fe39cf", null ],
    [ "SERIAL_QRCODE_LOG", "log_definition_8h.html#a6bd8a6352becb3a06c45eb4f5e6f4cf2", null ],
    [ "SERIAL_WIFI_LOG", "log_definition_8h.html#ab415f13c7518398937aa1a90e30547a6", null ],
    [ "TOUCH_LOG", "log_definition_8h.html#a2d456da032ed1f41ff970bcdc37b21e4", null ],
    [ "UPLOAD_LOG", "log_definition_8h.html#a29718e3d6873fceed5585faf0c2e21b3", null ]
];