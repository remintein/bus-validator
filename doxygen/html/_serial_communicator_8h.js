var _serial_communicator_8h =
[
    [ "SerialControllerCallback", "group___serial_communicator.html#gaa4eed0d8d3a889b2edcb3fe8e025d167", null ],
    [ "SerialControllerFlow_type", "group___serial_communicator.html#ga9d3cf13f4d48d5487f8e47d4c7aa98e4", null ],
    [ "action", "group___serial_communicator.html#ga2f4ab7bf743142dae2e459aa18f9f1d4", [
      [ "VERIFY_DATA", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4ad9b261ab2f24847e55b5b7ff3e0835f0", null ],
      [ "VERIFY_DATA_WITH_CALLBACK", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a6511943787a19a4aa37cea175b4eadd8", null ],
      [ "VERIFY_DATA_AND_PROCESS", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4af72fc490032738b2dd3a417fc4189f1e", null ],
      [ "VERIFY_DATA_AND_WAIT_FOR_RETURN", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4adc679c59bf6397cfeb0723529b11a8d2", null ],
      [ "EXECUTE_CALLBACK_WITH_PARAMETER", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a9f6a0d3ad3e9378cf73c4ad3294f29af", null ],
      [ "SEND_LITERAL_TEXT", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4aa6df4fdb31a9bf2a2e30e692a9240fc8", null ],
      [ "SEND_PROCESSED_TEXT", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a5368f6d7ace808dbbbf2286c0920d7ed", null ],
      [ "VALIDATE_CALLBACK", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4a45d3b1553e711606cc7641fc6d10148e", null ],
      [ "VERIFY_TIMEOUT_HAS_PASSED", "group___serial_communicator.html#gga2f4ab7bf743142dae2e459aa18f9f1d4aa920aa082b9b6fbbe90babde249ebd0b", null ]
    ] ],
    [ "apiDnfccSerialCommunicationAdvance", "group___serial_communicator.html#ga487df97f91fd7237a9fa860bf6d202cb", null ],
    [ "apiDnfccSerialCommunicationInitialize", "group___serial_communicator.html#ga495fce2a86356f199681cce52e78cf7e", null ],
    [ "apiDnfccSerialCommunicationVerifyStringPresentInReceiveBuffer", "group___serial_communicator.html#ga6d3be14ce291aee48a45ea5992923cbd", null ]
];