var _sam_control_commands_8h =
[
    [ "DnfccRcLoadRegisterValueSet", "_sam_control_commands_8h.html#a16e45b7fc86bb53c643050b201765e91", null ],
    [ "DnfccSamDeselect", "_sam_control_commands_8h.html#aba4dfe2db2970370926f46f503911c38", null ],
    [ "DnfccSamDesfireAuthenticatePicc", "_sam_control_commands_8h.html#a0cb219d4131309013c2bec8d1ef7cd1d", null ],
    [ "DnfccSamIso14443_3ActivateIdle", "_sam_control_commands_8h.html#a9d92db5fb1421e8e24add9a99826e3e5", null ],
    [ "DnfccSamIso14443_3AntiCollisionSelect", "_sam_control_commands_8h.html#abdeb3c2d30457513330650edbeff3d08", null ],
    [ "DnfccSamIso14443_3RequestWakeup", "_sam_control_commands_8h.html#a5b7123e89ed39679baec9bda101c296a", null ],
    [ "DnfccSamIso14443_4Exchange", "_sam_control_commands_8h.html#afadc97798042b9039730e49c67c9eab4", null ],
    [ "DnfccSamIso14443_4RatsPps", "_sam_control_commands_8h.html#a5ac020d6ab2dd4494593531467fba4a6", null ],
    [ "DnfccSamPresenceCheck", "_sam_control_commands_8h.html#a97ac03c1fd440d57410131775023bfb6", null ],
    [ "DnfccSamRcFreeCID", "_sam_control_commands_8h.html#a855b4154e82b5e7b45c3c2c8030d3fc9", null ],
    [ "DnfccSamRcInit", "_sam_control_commands_8h.html#a29616b3db85c7f84b5e158e41fdd0e97", null ],
    [ "DnfccSamRcReadRegisters", "_sam_control_commands_8h.html#a00a15786775a2d1e3cd9cf010836dea7", null ],
    [ "DnfccSamRcReadSingleRegister", "_sam_control_commands_8h.html#a6b4196ed85e8a49380c709cea0613083", null ],
    [ "DnfccSamRcRFControl", "_sam_control_commands_8h.html#a6b153c09083668ed6033e738719b8693", null ],
    [ "DnfccSamRcWriteRegister", "_sam_control_commands_8h.html#aeb1058db8447044f70b651991b1d6b86", null ],
    [ "SamInitializeCommunication", "_sam_control_commands_8h.html#a32aa712457db883f48a30d3117b2db7b", null ],
    [ "SamPerformReset", "_sam_control_commands_8h.html#ae652186071c7618afc341739edf742eb", null ],
    [ "SamReceiveEnvelope", "_sam_control_commands_8h.html#ae84225be90c118dc5a4c7f56b4a1ffe9", null ],
    [ "SamReceiveRaw", "_sam_control_commands_8h.html#a33c9da051ec3821f2ad32b4ecdba5767", null ],
    [ "SamResetReceiving", "_sam_control_commands_8h.html#ae1df43dab4d2abe93a5e307a64af443c", null ],
    [ "SamTransmitCommand", "_sam_control_commands_8h.html#af91c6da6ab1b1ac471e3f839ba9f90e7", null ]
];