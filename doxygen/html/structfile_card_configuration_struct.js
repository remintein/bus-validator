var structfile_card_configuration_struct =
[
    [ "fileFormatVersion", "structfile_card_configuration_struct.html#a4b0a297a42cbc396e0f3eed34a2c1aa3", null ],
    [ "futureUse", "structfile_card_configuration_struct.html#a1a6c1fd3a291cf0c5548a6da7484c42b", null ],
    [ "isActive", "structfile_card_configuration_struct.html#addb29b9783dc716a03222e5fe1448476", null ],
    [ "isAutoLoadEnable", "structfile_card_configuration_struct.html#a7b4f30f31f2c8f00cc7affbe128fb6a5", null ],
    [ "isPurseEnable", "structfile_card_configuration_struct.html#a4c189702a7891a0079849c6a8e529c4f", null ],
    [ "isReturnEnable", "structfile_card_configuration_struct.html#aa8b55a250b703ceea4d516ed2a63a64d", null ],
    [ "topUpEnable", "structfile_card_configuration_struct.html#a704eb9b6c4ead9188e7a11a7eda9227d", null ]
];