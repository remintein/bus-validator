var menu_8c =
[
    [ "MAXROUTE", "group__menu.html#gaa22ff97ff35e3ff11bfc38e4a314e34e", null ],
    [ "MAXSERVICE", "group__menu.html#gae01d8c55a13bcce3e4a8f633b77905cd", null ],
    [ "STACK_SIZE", "group__menu.html#ga6423a880df59733d2d9b509c7718d3a9", null ],
    [ "apiDnfccMenuInitializeLabels", "group__menu.html#ga209aed87e9f55bef62d3ae1e5023a4ec", null ],
    [ "apiDnfccMenuProcessMessage", "group__menu.html#gab3f0bac22183b3f650ee192676d528d9", null ],
    [ "apiDnfccMenuVerifyTimeout", "group__menu.html#gaef9b9c2e2adcf43b1e21365d9199d08a", null ],
    [ "nfccMenu", "group__menu.html#ga7d3bd06acd0c95577146ec6c28eff06b", null ],
    [ "nfccMenuInit", "group__menu.html#ga5b9af8af61f57a808726d41cbf6c657b", null ]
];