var lowlevel_8c =
[
    [ "DisplayTransmitReceive", "group__display.html#ga3fd09206e1b17811f6fa6f2fe60eb1e8", null ],
    [ "DisplayTransmitReceive3", "group__display.html#ga70cd1dbf1f59eeac3cc38bd3b9b9b957", null ],
    [ "RA8875_PWMout", "group__display.html#gad64eaaac77b997b827da3e80ceb70a3a", null ],
    [ "RA8875_PWMsetup", "group__display.html#gaaa6a50210fd44f2960ce94976e862152", null ],
    [ "RA8875_readData", "group__display.html#ga1737afb24c65831b75acd2b09c5ae752", null ],
    [ "RA8875_readRegister", "group__display.html#gacaf614cc862260f7862c4eeb9dea6d86", null ],
    [ "RA8875_readStatus", "group__display.html#gacdb98a9f6adefdd6a65273ce60dcf6e5", null ],
    [ "RA8875_waitBusy", "group__display.html#ga40c75a075d6d6ca5a02c624cde6aab4c", null ],
    [ "RA8875_waitPoll", "group__display.html#ga67d8b0c051bdecd3e529f2d08bdc8b20", null ],
    [ "RA8875_writeCommand", "group__display.html#ga7d8e071c65ab7c2daa326deb299c4782", null ],
    [ "RA8875_writeData", "group__display.html#ga33f4f161f69023b42e86fb933518c4c6", null ],
    [ "RA8875_writeData16", "group__display.html#gaf57b4d809b0ef51f59773f8f235b5075", null ],
    [ "RA8875_writeRegister", "group__display.html#gad257a57ebf5a314dd07c4d055b014e57", null ],
    [ "displayHspi", "group__display.html#ga8e49a06ddf90e54412ab16fdd681bd63", null ]
];