var device_selector__defines_8h =
[
    [ "CNS_BUSCONSOLE", "device_selector__defines_8h.html#a14a76d07067c3b9faa7b051708cacd92", null ],
    [ "CNS_FORMATTER_BUS", "device_selector__defines_8h.html#abcbb1a568701cdd2bb0271c739aa7eb4", null ],
    [ "CNS_HANDHELD", "device_selector__defines_8h.html#ab0584896b5da4cafc63b2c044af50de8", null ],
    [ "CNS_TOPUP", "device_selector__defines_8h.html#a138ca1e6edf6c17b5cd2687925469edd", null ],
    [ "CNS_VALIDATOR", "device_selector__defines_8h.html#a30e920a78dff86bd5a2a5bcbf7538c25", null ],
    [ "DNFCC_SAM_S_MODE", "device_selector__defines_8h.html#a00e00e2fc3f835b8956d6d06f3dcee33", null ],
    [ "DNFCC_SAM_SOFTKEYS", "device_selector__defines_8h.html#a6ca834d46ced07ca371259c155bbeab7", null ],
    [ "DNFCC_SAM_X_MODE", "device_selector__defines_8h.html#a4fe502eaccb9eba89218262f70b8b68c", null ],
    [ "TEXT_LOCALE_ENGLISH", "device_selector__defines_8h.html#a57f5c2f0511784cb7a73ec47fdb34dec", null ],
    [ "TEXT_LOCALE_VIETNAMESE", "device_selector__defines_8h.html#a99e96a427ac04458c4dc61961179b032", null ]
];